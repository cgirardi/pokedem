#!/bin/bash

DIR=`dirname $0`
export BASE_FOLDER=$DIR/..
export CONFIG_FILE=$BASE_FOLDER/config.txt
export LOG_FOLDER=$BASE_FOLDER/log

flock -n $LOG_FOLDER/twitter-new.lock php $BASE_FOLDER/scripts/twitter-new.php $CONFIG_FILE >& $LOG_FOLDER/twitter-new.log
flock -n $LOG_FOLDER/twitter-users.lock php $BASE_FOLDER/scripts/twitter-users.php $CONFIG_FILE >& $LOG_FOLDER/twitter-users.log 
flock -n $LOG_FOLDER/twitter-follower.lock php $BASE_FOLDER/scripts/twitter-follower.php $CONFIG_FILE >& $LOG_FOLDER/twitter-follower.log

