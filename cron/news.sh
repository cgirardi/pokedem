#!/bin/bash

DIR=`dirname $0`
export BASE_FOLDER=$DIR/..
export CONFIG_FILE=$BASE_FOLDER/config.txt
export LOG_FOLDER=$BASE_FOLDER/log
export OLD_LOG_FOLDER=$LOG_FOLDER/old
export SCRIPT_FOLDER=$BASE_FOLDER/cron
export MODEL_FOLDER=$BASE_FOLDER/models
export API_FOLDER=$BASE_FOLDER/api

export PERMANENT_ARTICLES_INDEX=$MODEL_FOLDER/permanent-articles-index/
export LINKS_INDEX=$MODEL_FOLDER/links-index/
export TEMPORARY_ARTICLES_INDEX=$MODEL_FOLDER/temporary-articles-index/

echo "permanent articles index: $PERMANENT_ARTICLES_INDEX"
echo "link index: $LINKS_INDEX"
echo "temporary articles index: $TEMPORARY_ARTICLES_INDEX"


cd $SCRIPT_FOLDER
export TOKEN=`date +%y%m%d%H%M`

# Temporary fix to remove possible appended process
#echo "kill -9 $(ps aux | grep SaveIntoIndex | grep -v grep | grep $CONFIG_FILE | awk '{print $2}')"
#kill -9 $(ps aux | grep SaveIntoIndex | grep -v grep | grep $CONFIG_FILE | awk '{print $2}')

# $LUCENE non e' definita!
file=$LUCENE/write.lock
if [ -f "$file" ]; then
	rm $LUCENE/write.lock
fi


echo "java -cp $API_FOLDER/dist/pokedem.jar -Dlog-config=$BASE_FOLDER/api/log-config.txt -Dfile.encoding=UTF-8 -mx4G eu.fbk.fm.pokedem.download.SaveIntoIndex -C --link $LINKS_INDEX --temporary $TEMPORARY_ARTICLES_INDEX -P $CONFIG_FILE"
flock -n $LOG_FOLDER/news.lock java -cp $API_FOLDER/dist/pokedem.jar -Dlog-config=$BASE_FOLDER/api/log-config.txt -Dfile.encoding=UTF-8 eu.fbk.fm.pokedem.download.SaveIntoIndex -C --link $LINKS_INDEX --temporary $TEMPORARY_ARTICLES_INDEX -P $CONFIG_FILE >& $LOG_FOLDER/news_savetoindex.log

echo "java -cp $API_FOLDER/dist/pokedem.jar -Dlog-config=$BASE_FOLDER/api/log-config.txt -Dfile.encoding=UTF-8 -mx2G eu.fbk.fm.pokedem.download.FindPoliticians --permanent $PERMANENT_ARTICLES_INDEX --link $LINKS_INDEX --temporary $TEMPORARY_ARTICLES_INDEX -P $CONFIG_FILE"
flock -n $LOG_FOLDER/news.lock java -cp $API_FOLDER/dist/pokedem.jar -Dlog-config=$BASE_FOLDER/api/log-config.txt -Dfile.encoding=UTF-8 eu.fbk.fm.pokedem.download.FindPoliticians --permanent $PERMANENT_ARTICLES_INDEX --link $LINKS_INDEX --temporary $TEMPORARY_ARTICLES_INDEX -P $CONFIG_FILE >& $LOG_FOLDER/news_find.log

# Log rotation
if [ ! -e $OLD_LOG_FOLDER ]; then
   mkdir $OLD_LOG_FOLDER
fi

find $LOG_FOLDER/ -maxdepth 1 -type f -mmin +360 -exec mv {} $OLD_LOG_FOLDER/ \;
find $OLD_LOG_FOLDER/ -maxdepth 1 -type f -iname "*.log" -exec gzip -f {} \;

