#!/bin/bash

DIR=`dirname $0`
export BASE_FOLDER=$DIR/..
CONFIG_FILE=$BASE_FOLDER/config.txt
LOG_FOLDER=$BASE_FOLDER/log
POKEDEM_API_HOME=$BASE_FOLDER/api

ymd=`date +%Y-%m-%d_%H-%M`
#ymd=`date +%Y-%m-%d`

#echo $ymd

log=${LOG_FOLDER}/facebook.log

echo "log file: $log"

echo "java -Dlog-config=$BASE_FOLDER/api/log-config.txt -Dfile.encoding=UTF-8 -mx8G -cp ${POKEDEM_API_HOME}/dist/pokedem.jar eu.fbk.fm.pokedem.social.FBPosts -P $CONFIG_FILE >& $log &"
flock -n ${LOG_FOLDER}/facebook.lock java -Dlog-config=$BASE_FOLDER/api/log-config.txt -Dfile.encoding=UTF-8 -cp ${POKEDEM_API_HOME}/dist/pokedem.jar eu.fbk.fm.pokedem.social.FBPosts -P $CONFIG_FILE >& $log

