-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pokedem_new
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_tag`
--

DROP TABLE IF EXISTS `article_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_tag` (
  `link` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `rel` double NOT NULL,
  `wikipage` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`link`,`tag`,`wikipage`),
  KEY `link` (`link`),
  KEY `tag` (`tag`),
  KEY `wikipage` (`wikipage`),
  KEY `class` (`class`(333))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `article_tag_new`
--

DROP TABLE IF EXISTS `article_tag_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_tag_new` (
  `link` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `rel` double NOT NULL,
  `wikipage` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `link` (`link`,`tag`,`wikipage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `link` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `politician` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `newspaper` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `snippet` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `when` datetime NOT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link`,`politician`),
  KEY `newspaper` (`newspaper`(333)),
  KEY `when` (`when`),
  KEY `link` (`link`),
  KEY `politician` (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `articles_done`
--

DROP TABLE IF EXISTS `articles_done`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_done` (
  `link` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci,
  `tweeted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link`),
  KEY `tweeted` (`tweeted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attivisti_fb`
--

DROP TABLE IF EXISTS `attivisti_fb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attivisti_fb` (
  `user_id` bigint(20) NOT NULL,
  `week` int(11) NOT NULL,
  `party_12` int(11) NOT NULL DEFAULT '0',
  `party_5` int(11) NOT NULL DEFAULT '0',
  `party_2` int(11) NOT NULL DEFAULT '0',
  `party_16` int(11) NOT NULL DEFAULT '0',
  `party_30` int(11) NOT NULL DEFAULT '0',
  `party_13` int(11) NOT NULL DEFAULT '0',
  `party_4` int(11) NOT NULL DEFAULT '0',
  `party_15` int(11) NOT NULL DEFAULT '0',
  `party_33` int(11) NOT NULL DEFAULT '0',
  `party_38` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`week`),
  KEY `week` (`week`),
  KEY `party_12` (`party_12`),
  KEY `party_5` (`party_5`),
  KEY `party_2` (`party_2`),
  KEY `party_16` (`party_16`),
  KEY `party_30` (`party_30`),
  KEY `party_13` (`party_13`),
  KEY `party_4` (`party_4`),
  KEY `party_15` (`party_15`),
  KEY `party_33` (`party_33`),
  KEY `total` (`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attivisti_rt`
--

DROP TABLE IF EXISTS `attivisti_rt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attivisti_rt` (
  `user_id` bigint(20) NOT NULL,
  `week` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `tweets` int(11) NOT NULL,
  `party_12` int(11) NOT NULL,
  `party_5` int(11) NOT NULL,
  `party_2` int(11) NOT NULL,
  `party_16` int(11) NOT NULL,
  `party_30` int(11) NOT NULL,
  `party_13` int(11) NOT NULL,
  `party_4` int(11) NOT NULL,
  `party_15` int(11) NOT NULL,
  `party_33` int(11) NOT NULL,
  `party_38` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`week`),
  KEY `week` (`week`),
  KEY `party_12` (`party_12`),
  KEY `party_5` (`party_5`),
  KEY `party_2` (`party_2`),
  KEY `party_16` (`party_16`),
  KEY `party_30` (`party_30`),
  KEY `party_13` (`party_13`),
  KEY `party_4` (`party_4`),
  KEY `party_15` (`party_15`),
  KEY `party_33` (`party_33`),
  KEY `total` (`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_interval` int(11) DEFAULT NULL,
  `timestamp` bigint(20) NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `update_interval` (`update_interval`),
  KEY `type` (`type`),
  KEY `timestamp` (`timestamp`),
  KEY `type_2` (`type`,`update_interval`),
  KEY `type_3` (`type`,`update_interval`,`timestamp`),
  KEY `type_4` (`type`,`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `id` int(11) NOT NULL,
  `label` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_read`
--

DROP TABLE IF EXISTS `email_read`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_read` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `read` datetime DEFAULT NULL,
  `log` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `recipient` (`recipient`),
  KEY `read` (`read`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_comments`
--

DROP TABLE IF EXISTS `fb_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_comments` (
  `id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_post` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `politician` int(11) DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_post` (`id_post`),
  KEY `created_time` (`created_time`),
  KEY `user_id` (`user_id`),
  KEY `politician` (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_last_comment`
--

DROP TABLE IF EXISTS `fb_last_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_last_comment` (
  `post_id` varchar(200) NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_last_like`
--

DROP TABLE IF EXISTS `fb_last_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_last_like` (
  `post_id` varchar(200) NOT NULL,
  `user_ids` varchar(200) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_likes`
--

DROP TABLE IF EXISTS `fb_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_likes` (
  `politician` int(11) NOT NULL,
  `fb_user` bigint(11) NOT NULL,
  PRIMARY KEY (`politician`,`fb_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_plikes`
--

DROP TABLE IF EXISTS `fb_plikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_plikes` (
  `post_id` varchar(200) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_updates_done`
--

DROP TABLE IF EXISTS `fb_updates_done`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_updates_done` (
  `politician` int(11) NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fb_users`
--

DROP TABLE IF EXISTS `fb_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fb_users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` enum('m','f') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `georegions`
--

DROP TABLE IF EXISTS `georegions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `georegions` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `houses`
--

DROP TABLE IF EXISTS `houses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `houses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('info','error','warning') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info',
  `action` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` int(11) DEFAULT NULL,
  `politician` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `referer` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`),
  KEY `email` (`email`),
  KEY `politician` (`politician`),
  KEY `user` (`user`),
  KEY `action` (`action`),
  KEY `type` (`type`),
  KEY `referer` (`referer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Log di tutti i click del sito';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_cache`
--

DROP TABLE IF EXISTS `log_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `query` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `ip` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lostpass`
--

DROP TABLE IF EXISTS `lostpass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lostpass` (
  `user` int(11) NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_tweets`
--

DROP TABLE IF EXISTS `my_tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_tweets` (
  `id` bigint(20) NOT NULL,
  `when` datetime NOT NULL,
  `shares` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `json` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mine` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `when` (`when`),
  KEY `shares` (`shares`),
  KEY `likes` (`likes`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neutral_tags`
--

DROP TABLE IF EXISTS `neutral_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neutral_tags` (
  `tag` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `from` (`from`,`to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ngrams`
--

DROP TABLE IF EXISTS `ngrams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ngrams` (
  `ngram` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `df` int(11) NOT NULL,
  `lf` int(11) NOT NULL,
  PRIMARY KEY (`ngram`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `older_twitter_politicians`
--

DROP TABLE IF EXISTS `older_twitter_politicians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `older_twitter_politicians` (
  `tweet` bigint(20) unsigned NOT NULL,
  `politician` int(11) NOT NULL,
  `when_tweet` datetime DEFAULT NULL,
  PRIMARY KEY (`tweet`,`politician`),
  KEY `politician` (`politician`),
  KEY `tweet` (`tweet`),
  KEY `when_tweet` (`when_tweet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `older_twitter_updates`
--

DROP TABLE IF EXISTS `older_twitter_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `older_twitter_updates` (
  `id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime DEFAULT NULL,
  `shares` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `comments` int(11) NOT NULL DEFAULT '0',
  `link` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link2` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type2` (`type2`),
  KEY `created_time` (`created_time`),
  KEY `name` (`name`),
  KEY `object_id` (`object_id`),
  KEY `shares` (`shares`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parties`
--

DROP TABLE IF EXISTS `parties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `goodname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shortname` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `abbr` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coalition` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pertinence_notifications`
--

DROP TABLE IF EXISTS `pertinence_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pertinence_notifications` (
  `user` int(11) NOT NULL,
  `politician` int(11) NOT NULL,
  `link` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `user_2` (`user`,`politician`,`link`),
  KEY `user` (`user`),
  KEY `politician` (`politician`),
  KEY `link` (`link`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `politician_info`
--

DROP TABLE IF EXISTS `politician_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `politician_info` (
  `politician` int(11) DEFAULT NULL,
  `bio` text NOT NULL,
  `surname` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `url` tinytext NOT NULL,
  `image` tinytext NOT NULL,
  `image_big` varchar(500) NOT NULL DEFAULT '',
  `image_small` varchar(500) NOT NULL DEFAULT '',
  `data` tinytext NOT NULL,
  `email` varchar(200) NOT NULL,
  `id` varchar(20) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `politician` (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `politician_model`
--

DROP TABLE IF EXISTS `politician_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `politician_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `origin_region` varchar(255) DEFAULT NULL,
  `administrative_region` varchar(255) DEFAULT NULL,
  `coalition` varchar(255) DEFAULT NULL,
  `coalition_leader` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `sex` varchar(1) DEFAULT 'M',
  `party` varchar(255) DEFAULT NULL,
  `party_leader` varchar(255) DEFAULT NULL,
  `wikipedia_page` varchar(255) DEFAULT NULL,
  `name_variant_list` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `black_list` varchar(255) DEFAULT NULL,
  `politician` int(11) NOT NULL,
  `office` varchar(255) DEFAULT NULL,
  `ambiguity` int(11) DEFAULT '0',
  `higher_office` varchar(255) DEFAULT NULL,
  `stopwords` varchar(255) DEFAULT NULL,
  `trust_name_surname_only` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `politicians`
--

DROP TABLE IF EXISTS `politicians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `politicians` (
  `id` int(11) NOT NULL,
  `idwiki` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) DEFAULT NULL,
  `region` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `house` int(11) NOT NULL,
  `coalition` int(11) NOT NULL,
  `party` int(11) DEFAULT NULL,
  `sex` enum('M','F') NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `in_charts` tinyint(1) NOT NULL DEFAULT '1',
  `max_citati` int(11) NOT NULL,
  `max_social` int(11) NOT NULL,
  `domain` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  KEY `idwiki` (`idwiki`),
  KEY `party` (`party`),
  KEY `coalition` (`coalition`),
  KEY `active` (`active`),
  KEY `coalition_2` (`coalition`),
  KEY `domain` (`domain`),
  KEY `created_by` (`created_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pro_actions`
--

DROP TABLE IF EXISTS `pro_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `action` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `general_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tweet_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_account` (`id_account`),
  KEY `action` (`action`),
  KEY `target` (`target`),
  KEY `when` (`when`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pro_pol`
--

DROP TABLE IF EXISTS `pro_pol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_pol` (
  `user` int(11) NOT NULL,
  `politician` int(11) NOT NULL,
  PRIMARY KEY (`user`,`politician`),
  KEY `user` (`user`),
  KEY `politician` (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pro_scheduled`
--

DROP TABLE IF EXISTS `pro_scheduled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_scheduled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `label` varchar(20) NOT NULL,
  `options` text NOT NULL,
  `interval` int(11) NOT NULL DEFAULT '0',
  `last` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_account` (`id_account`),
  KEY `label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pro_scheduled_tweets`
--

DROP TABLE IF EXISTS `pro_scheduled_tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_scheduled_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `user` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_on_expiration` tinyint(1) NOT NULL DEFAULT '1',
  `expire_mins` int(11) DEFAULT NULL,
  `sent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sent` (`sent`),
  KEY `schedule` (`schedule`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pro_twitteraccounts`
--

DROP TABLE IF EXISTS `pro_twitteraccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_twitteraccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `tw_username` varchar(200) NOT NULL,
  `tw_userid` bigint(20) DEFAULT NULL,
  `tw_picture` varchar(200) NOT NULL DEFAULT '',
  `oauth_access_token` varchar(200) NOT NULL,
  `oauth_access_token_secret` varchar(200) NOT NULL,
  `consumer_key` varchar(200) NOT NULL,
  `consumer_secret` varchar(200) NOT NULL,
  `use_twitter` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `tw_username` (`tw_username`),
  KEY `active` (`active`),
  KEY `deleted` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regionbyname`
--

DROP TABLE IF EXISTS `regionbyname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regionbyname` (
  `name` varchar(200) NOT NULL,
  `region` int(11) NOT NULL,
  `population` int(11) NOT NULL,
  KEY `name` (`name`),
  KEY `region` (`region`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_2` (`name`,`description`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `name` varchar(20) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sexbyname`
--

DROP TABLE IF EXISTS `sexbyname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sexbyname` (
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  KEY `sex` (`sex`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `site_types`
--

DROP TABLE IF EXISTS `site_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `rss` longtext NOT NULL,
  `pattern` longtext NOT NULL,
  `images` mediumtext NOT NULL,
  `tags` mediumtext NOT NULL,
  `sections` longtext NOT NULL,
  `user_agent` mediumtext NOT NULL,
  `basepath` varchar(200) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `url` text NOT NULL,
  `test_output` longtext NOT NULL,
  `test_time` datetime DEFAULT NULL,
  `test_checked_output` longtext NOT NULL,
  `test_fail_time` datetime DEFAULT NULL,
  `encoding` varchar(30) NOT NULL,
  `entities` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(250))
) ENGINE=MyISAM AUTO_INCREMENT=442 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_info`
--

DROP TABLE IF EXISTS `social_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_info` (
  `politician` int(11) NOT NULL,
  `party` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_personal` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_public` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_active` tinyint(1) NOT NULL DEFAULT '1',
  `twitter_id` bigint(20) DEFAULT NULL,
  `fb_active` tinyint(1) NOT NULL DEFAULT '1',
  `rss` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `followed` tinyint(1) NOT NULL DEFAULT '0',
  `ignore_social` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`politician`),
  KEY `ignore` (`ignore_social`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_info_cache`
--

DROP TABLE IF EXISTS `social_info_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_info_cache` (
  `politician` int(11) NOT NULL,
  `twitter_id` bigint(20) NOT NULL,
  `follower` int(11) NOT NULL,
  `when` date NOT NULL,
  PRIMARY KEY (`politician`,`when`),
  KEY `twitter_id` (`twitter_id`),
  KEY `follower` (`follower`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_updates`
--

DROP TABLE IF EXISTS `social_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_updates` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `politician` int(11) NOT NULL,
  `link` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `socialnetwork` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `when` datetime NOT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `politician` (`politician`,`socialnetwork`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_updates_blacklisted`
--

DROP TABLE IF EXISTS `social_updates_blacklisted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_updates_blacklisted` (
  `social` enum('twitter','facebook') NOT NULL,
  `pattern` text NOT NULL,
  KEY `social` (`social`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_updates_new`
--

DROP TABLE IF EXISTS `social_updates_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_updates_new` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `politician` int(11) NOT NULL,
  `type` enum('facebook','twitter') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime DEFAULT NULL,
  `shares` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `comments` int(11) NOT NULL DEFAULT '0',
  `link` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link2` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`,`type`),
  KEY `politician` (`politician`),
  KEY `id` (`id`),
  KEY `type` (`type`),
  KEY `created_time` (`created_time`),
  KEY `type2` (`type2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_updates_new_entities`
--

DROP TABLE IF EXISTS `social_updates_new_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_updates_new_entities` (
  `tweet` bigint(20) NOT NULL,
  `tag` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('hashtag','user') NOT NULL,
  KEY `tweet` (`tweet`,`tag`,`type`),
  KEY `tag` (`tag`,`type`),
  KEY `tweet_2` (`tweet`),
  KEY `type` (`type`),
  KEY `tag_2` (`tag`),
  KEY `tweet_3` (`tweet`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tag_blacklisted`
--

DROP TABLE IF EXISTS `tag_blacklisted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_blacklisted` (
  `type` enum('class','wikipage') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'wikipage',
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `multiplier` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`type`,`label`),
  KEY `label` (`label`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temparlamentari`
--

DROP TABLE IF EXISTS `temparlamentari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temparlamentari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(500) NOT NULL,
  `cognome` varchar(500) NOT NULL,
  `nomecognome` varchar(500) NOT NULL,
  `regione` varchar(500) NOT NULL,
  `partito` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_europarl`
--

DROP TABLE IF EXISTS `tmp_europarl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_europarl` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `party` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `email` varchar(200) NOT NULL,
  `web` varchar(200) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_new_europarl`
--

DROP TABLE IF EXISTS `tmp_new_europarl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_new_europarl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `circ` varchar(2) NOT NULL,
  `party` varchar(5) NOT NULL,
  `id2` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `wikipedia` varchar(200) NOT NULL,
  `web` text NOT NULL,
  `photo` varchar(200) NOT NULL,
  `altname` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_new_europarl2`
--

DROP TABLE IF EXISTS `tmp_new_europarl2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_new_europarl2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `circ` varchar(2) NOT NULL,
  `party` varchar(5) NOT NULL,
  `id2` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `wikipedia` varchar(200) NOT NULL,
  `web` text NOT NULL,
  `photo` varchar(200) NOT NULL,
  `altname` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_partyeu`
--

DROP TABLE IF EXISTS `tmp_partyeu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_partyeu` (
  `party` varchar(5) NOT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `twitter` varchar(200) NOT NULL,
  PRIMARY KEY (`party`),
  KEY `party` (`party`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_blacklisted`
--

DROP TABLE IF EXISTS `twitter_blacklisted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_blacklisted` (
  `id` bigint(20) unsigned NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pd_followers`
--

DROP TABLE IF EXISTS `twitter_pd_followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pd_followers` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `username` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `followers` int(11) NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `follower` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pd_following`
--

DROP TABLE IF EXISTS `twitter_pd_following`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pd_following` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `following` tinyint(1) NOT NULL DEFAULT '1',
  `followers` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pd_log`
--

DROP TABLE IF EXISTS `twitter_pd_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pd_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_tweet` bigint(20) DEFAULT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `action` (`action`,`username`),
  KEY `when` (`when`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pd_log_notification`
--

DROP TABLE IF EXISTS `twitter_pd_log_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pd_log_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `tweet_id` bigint(20) DEFAULT NULL,
  `log` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `action` (`action`),
  KEY `user_id` (`user_id`),
  KEY `tweet_id` (`tweet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pol_followers`
--

DROP TABLE IF EXISTS `twitter_pol_followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pol_followers` (
  `politician` int(11) NOT NULL,
  `follower_id` bigint(20) NOT NULL,
  PRIMARY KEY (`politician`,`follower_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pol_followers_blacklist`
--

DROP TABLE IF EXISTS `twitter_pol_followers_blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pol_followers_blacklist` (
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_pol_followers_done`
--

DROP TABLE IF EXISTS `twitter_pol_followers_done`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_pol_followers_done` (
  `politician` int(11) NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_politicians`
--

DROP TABLE IF EXISTS `twitter_politicians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_politicians` (
  `tweet` bigint(20) unsigned NOT NULL,
  `politician` int(11) NOT NULL,
  `when_tweet` datetime DEFAULT NULL,
  PRIMARY KEY (`tweet`,`politician`),
  KEY `politician` (`politician`),
  KEY `tweet` (`tweet`),
  KEY `when_tweet` (`when_tweet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_priority`
--

DROP TABLE IF EXISTS `twitter_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_priority` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_queue`
--

DROP TABLE IF EXISTS `twitter_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` varchar(200) NOT NULL,
  `sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_scheduled`
--

DROP TABLE IF EXISTS `twitter_scheduled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_scheduled` (
  `id` varchar(20) NOT NULL,
  `interval` int(11) NOT NULL DEFAULT '0',
  `last` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_scheduled_tweets`
--

DROP TABLE IF EXISTS `twitter_scheduled_tweets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_scheduled_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `send_on_expiration` tinyint(1) NOT NULL DEFAULT '1',
  `expire_mins` int(11) DEFAULT NULL,
  `sent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sent` (`sent`),
  KEY `campaign` (`campaign`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_tag_blacklisted`
--

DROP TABLE IF EXISTS `twitter_tag_blacklisted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_tag_blacklisted` (
  `tag` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_updates`
--

DROP TABLE IF EXISTS `twitter_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_updates` (
  `id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_time` datetime NOT NULL,
  `updated_time` datetime DEFAULT NULL,
  `shares` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `comments` int(11) NOT NULL DEFAULT '0',
  `link` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link2` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `object_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type2` (`type2`),
  KEY `created_time` (`created_time`),
  KEY `name` (`name`),
  KEY `object_id` (`object_id`),
  KEY `shares` (`shares`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_updates_done`
--

DROP TABLE IF EXISTS `twitter_updates_done`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_updates_done` (
  `politician` int(11) NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitter_users`
--

DROP TABLE IF EXISTS `twitter_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitter_users` (
  `id` bigint(20) unsigned NOT NULL,
  `username` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` int(11) DEFAULT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `followers` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `tweets` int(11) NOT NULL,
  `last_tweet` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `username` (`username`(250)),
  KEY `location` (`location`),
  KEY `sex` (`sex`),
  KEY `region` (`region`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_clicks`
--

DROP TABLE IF EXISTS `user_clicks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_clicks` (
  `link` varchar(32) NOT NULL,
  `user` int(11) NOT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`link`,`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Molti-a-molti che collega gli utenti agli articoli cliccati';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_politician`
--

DROP TABLE IF EXISTS `user_politician`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_politician` (
  `user` int(11) NOT NULL,
  `house` int(11) NOT NULL,
  `politician` int(11) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `notification_real` tinyint(1) NOT NULL DEFAULT '0',
  `notification_day` tinyint(1) NOT NULL DEFAULT '0',
  `notification_week` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user`,`house`,`politician`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Molti-a-molti che connette utenti e politici seguiti';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_settings`
--

DROP TABLE IF EXISTS `user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_settings` (
  `user` int(11) NOT NULL,
  `max_view` int(11) NOT NULL,
  `max_mail` int(11) NOT NULL,
  `receive_mail` tinyint(1) NOT NULL,
  `enable_social` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Impostazioni dell''utente (max articoli, mail, ecc.)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `regtype` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `regid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `activation` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debug` tinyint(1) NOT NULL DEFAULT '0',
  `pro` tinyint(1) NOT NULL DEFAULT '0',
  `proall` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fbid` (`regid`),
  KEY `proall` (`proall`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weekly_charts`
--

DROP TABLE IF EXISTS `weekly_charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weekly_charts` (
  `week` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `realposition` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `username` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `followers` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `tweets` int(11) NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `num` int(11) NOT NULL,
  `retweet` int(11) NOT NULL DEFAULT '0',
  `retweet_pol` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`week`,`position`),
  KEY `id` (`id`),
  KEY `username` (`username`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weekly_charts_pol`
--

DROP TABLE IF EXISTS `weekly_charts_pol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weekly_charts_pol` (
  `week` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `realposition` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `username` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `followers` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `tweets` int(11) NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `num` int(11) NOT NULL,
  `retweet` int(11) NOT NULL DEFAULT '0',
  `retweet_pol` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`week`,`position`),
  KEY `id` (`id`),
  KEY `username` (`username`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weekly_user_info`
--

DROP TABLE IF EXISTS `weekly_user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weekly_user_info` (
  `week` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `index` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`week`,`user`,`index`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weeks`
--

DROP TABLE IF EXISTS `weeks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weeks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startdate` datetime NOT NULL,
  `average` double NOT NULL DEFAULT '0',
  `avg_rt` double NOT NULL DEFAULT '0',
  `avg_rt_pol` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-08  4:46:44
