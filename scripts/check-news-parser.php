<?php
setlocale(LC_ALL, "en_US.UTF-8");
if (!isset($argv[1])) {
    exit("The first parameter must be the configuration file\n");
}

$Config = parse_ini_file($argv[1]);
foreach ($Config as $index => $value) {
    $$index = $value;
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

require_once($Folder_inc."/include-cli.php");

$query = "SELECT * FROM sites WHERE url!='' and test_output!=''";
$DB->query($query, 2);
$counter=0;
while ($r = $DB->fetch(2)) {
    if (isset($r['url']) && ($r['pattern'] || $r['images'])) {
        $url = $r['url'];
        print "Testing \"{$r['name']}\" on $url";
        $counter++;
        if (substr($url, 0, 4) != "http" || test_output == '') {
            $DB->query("UPDATE sites SET test_checked_output='',test_fail_time=CURRENT_TIMESTAMP() WHERE id='{$r['id']}'");
            continue;
        }
        $articleName = tempnam(sys_get_temp_dir(), "article");
        $imagesName = tempnam(sys_get_temp_dir(), "images");
        $htmlName = tempnam(sys_get_temp_dir(), "html");
        $tagsName = tempnam(sys_get_temp_dir(), "tags");
        $sectName = tempnam(sys_get_temp_dir(), "sect");

        $article = fopen($articleName, "w");
        fwrite($article, $r['pattern']);
        fclose($article);

        $images = fopen($imagesName, "w");
        fwrite($images, $r['images']);
        fclose($images);

        //la pagine viene scaricata da PageParser
        //file_put_contents($htmlName, file_get_contents($url));
        $tags = fopen($tagsName, "w");
        fwrite($tags, $r['tags']);
        fclose($tags);

        $sections = fopen($sectName, "w");
        fwrite($sections, $r['sections']);
        fclose($sections);

        $ua = $r['user_agent'];
        $encoding = $r['encoding'];
	$encoding_option="";
        if ($encoding != "") {
	    $encoding_option=" -encoding $encoding";
        }
        $command = "$Folder_scripts/../api/pageparser.sh -o json".$encoding_option." -f $htmlName -p $articleName -i $imagesName -t $tagsName -s $sectName -u '$url' 2> /dev/null";
        $res = shell_exec($command);

        unlink($htmlName);
        unlink($imagesName);
        unlink($articleName);
        unlink($tagsName);
        unlink($sectName);
	
print "\n>>> from MYSQL ".strlen($r['test_output'])." :: {{" .$r['test_output']."}}\n-------------\n<<< fromPARSER ".strlen(addslashes($res))." :: {{".$res."}}\n";
echo "\nCOMMAND: $command\n\n";
	//$hashsql = array();
	//$hashsql["test_checked_output"]=addslashes($res);
	$DB->query("UPDATE sites SET test_checked_output='".addslashes($res)."' WHERE id='{$r['id']}'");
	//$DB->queryupdate("sites", $hashsql, array("id"=>$r['id']));

//        if (addslashes($res) != $r['test_output']) {
//            $count_failed++;
//            print " ...FAILED!\n";
//            array_push($update_queries, "UPDATE sites SET test_fail_time=CURRENT_TIMESTAMP() WHERE id='{$r['id']}'");
//        } else {
//            print " ...OK!\n";
//            array_push($update_queries, "UPDATE sites SET test_time=CURRENT_TIMESTAMP(), test_fail_time=NULL WHERE id='{$r['id']}'");
//        }
    } else {
    	print "Test on {$r['name']} has been skipped (no pattern)!";
    }
}

$query="select id,name,test_output=test_checked_output as testeq from sites WHERE url!='' AND test_output!='' AND test_checked_output!='' order by id";
$DB->query($query,2);
$count_failed = 0;
while ($r = $DB->fetch(2)) {
    print $r['id'] . ". " .$r['name'];
    if ($r['testeq'] == "1") {
	print " ...PASSED\n";
        $DB->query("UPDATE sites SET test_time=CURRENT_TIMESTAMP(), test_fail_time=NULL WHERE id='{$r['id']}'");
    } else {
	$count_failed++;
    	print " ...TEST FAILED!\n";
	$DB->query("UPDATE sites SET test_fail_time=CURRENT_TIMESTAMP() WHERE id='{$r['id']}'");
    }
}

if ($count_failed > 0 && $Sentry_DSN) {
    //send a report
    require_once($Folder_inc."/raven-php/lib/Raven/Autoloader.php");
    Raven_Autoloader::register();
    $client = new Raven_Client($Sentry_DSN);
    $client->captureMessage('Parser of the news failed! Check the page $Host/admin to fix them.');
}
print "Test on $counter sources is finished (failed $count_failed tests).\n";

