<?php

if (!isset($argv[1])) {
    exit("The first parameter must be the configuration file\n");
}

$Config = parse_ini_file($argv[1]);
foreach ($Config as $index => $value) {
    $$index = $value;
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

require_once($Folder_inc."/twitteroauth/twitteroauth/twitteroauth.php");
require_once($Folder_inc."/include-cli.php");

$connectionBot = new TwitterOAuth($Tw_PDB_ck, $Tw_PDB_cks, $Tw_PDB_at, $Tw_PDB_ats);
$MaxFollowers = 500000;


//deprecated
//if (getSetting("run_followers", 0)) {
//    wlog("This script is already running");
//    exit();
//}
//setSetting("run_followers", 1);


// Updating Twitter's users
wlog("Updating Twitter's users");
$Todo = array();

$query = "SELECT *
	FROM social_info as s
	LEFT JOIN twitter_users u ON u.id = s.twitter_id
	WHERE twitter_active = '1' AND twitter_id IS NOT NULL
	ORDER BY u.followers DESC";
$DB->query($query);
while ($r = $DB->fetch()) {
	if ($r['username']) {
		continue;
	}

	$Todo[] = $r['twitter_id'];
}

$n = count($Todo);
wlog("Saving $n user(s)");

if ($n) {
	$stuff = getUserInformation($Todo);
	if ($stuff) {
		foreach ($stuff as $user) {
			saveUser($user);
		}
	}
}


// Getting followers
wlog("Getting followers");
do {
	$next_cursor = getSetting("followers_cursor");
	$next_politician = getSetting("followers_politician");
	$next_twitter_id = getSetting("followers_twitter_id");

	$userID = false;
	$polID = false;
	$cursor = -1;

	if (!$next_cursor) {
		$query = "SELECT i.politician, i.twitter, i.twitter_id, u.followers
		    FROM social_info i
		    LEFT JOIN twitter_pol_followers_done s ON s.politician = i.politician
		    LEFT JOIN twitter_users u ON u.id = i.twitter_id
		    WHERE twitter_active = '1' AND twitter_id IS NOT NULL
		    AND u.followers < '$MaxFollowers'
		    ORDER BY s.`when` IS NULL DESC, `when`, RAND()";
		$r = $DB->queryfetch($query);

		$userID = $r['twitter_id'];
		$polID = $r['politician'];
	}
	else {
		$cursor = $next_cursor;
		$polID = $next_politician;
		$userID = $next_twitter_id;
	}

	wlog("Politician: $polID");

	$api = "https://api.twitter.com/1.1/followers/ids.json";
	$api .= "?user_id=$userID&count=5000";
	$api .= "&cursor=$cursor";
	wlog("Executing: $api");
	$stuff = $connectionBot->get($api);

	if (@count($stuff->errors)) {
	    foreach ($stuff->errors as $error) {
	        wlog("[{$error->code}]: {$error->message}", "ERROR");
	        break 2;
	    }
	}

	foreach ($stuff->ids as $id) {
		$data = array();
		$data['politician'] = $polID;
		$data['follower_id'] = $id;
		$DB->queryinsertodku("twitter_pol_followers", $data);
	}

	if ($stuff->next_cursor) {
		setSetting("followers_cursor", $stuff->next_cursor);
		setSetting("followers_politician", $polID);
		setSetting("followers_twitter_id", $userID);
	}
	else {
		$data = array();
		$data['politician'] = $polID;
		$data['when'] = date("Y-m-d H:i:s");
		$DB->queryinsertodku("twitter_pol_followers_done", $data);
		setSetting("followers_cursor", 0);
		setSetting("followers_politician", 0);
		setSetting("followers_twitter_id", 0);
	}
} while (false);


// Getting followers info
wlog("Getting followers info");
for ($i = 1; $i <= 12; $i++) {
	wlog("Loop: $i");
	$query = "SELECT DISTINCT follower_id
		FROM `twitter_pol_followers` f
		LEFT JOIN twitter_users u ON u.id = f.follower_id
		LEFT JOIN twitter_pol_followers_blacklist b ON b.id = f.follower_id
		WHERE u.id IS NULL AND b.id IS NULL
		LIMIT 100";
	$n = $DB->querynum($query);
	if ($n == 0) {
		break;
	}

	$Todo = array();
	while ($r = $DB->fetch()) {
		$Todo[] = $r['follower_id'];
	}

	$stuff = getUserInformation($Todo);
	if (!$stuff) {
		break;
	}
	foreach ($stuff as $user) {
		saveUser($user);
	}
}


wlog("End");
//deprecated
//setSetting("run_followers", 0);

// print_r($stuff);

// $api = "https://api.twitter.com/1.1/users/lookup.json?screen_name=twitterapi,twitter";
// $stuff = $connectionBot->get($api);

// print_r($stuff);
// echo count($Todo)."\n";

// --

function saveUser($user) {
	global $DB;
	$DBid = md5("saveUser");

	$UserID = $user->id;
	echo "Saving user $UserID\n";
	// User
	$dati = array();
	$dati['id'] = $UserID;
	$dati['username'] = $user->screen_name;
	$dati['location'] = $user->location;
	$dati['name'] = $user->name;
	$dati['description'] = $user->description;
	$dati['picture'] = $user->profile_image_url;
	$dati['followers'] = $user->followers_count;
	$dati['following'] = $user->friends_count;
	$dati['tweets'] = $user->statuses_count;
	$DB->queryinsertodku("twitter_users", $dati, array(), $DBid);

}

function getUserInformation($list = array(), $screeName = false) {
	global $connectionBot, $DB;
	$DBid = md5("getUserInformation");

	if (!count($list) || count($list) > 100) {
		return false;
	}
	wlog("Number of users: ".count($list));

	$req = implode(",", $list);
	$api = "https://api.twitter.com/1.1/users/lookup.json";
	if ($screeName) {
		$api .= "?screen_name=$req";
	}
	else {
		$api .= "?user_id=$req";
	}

	$stuff = $connectionBot->get($api);

	if (@count($stuff->errors)) {
	    foreach ($stuff->errors as $error) {
	        wlog("[{$error->code}]: {$error->message}", "ERROR");

	        if ($error->code == 34) {
	        	foreach ($list as $id) {
	        		$d = array("id" => $id);
	        		$DB->queryinsertodku("twitter_pol_followers_blacklist", $d, array(), $DBid);
	        	}
	        } 	        	

	        return false;
	    }
	}
	else {
		return $stuff;
	}

}
