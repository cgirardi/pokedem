<?php

if (!isset($argv[1])) {
    exit("The first parameter must be the configuration file\n");
}

$Config = parse_ini_file($argv[1]);
foreach ($Config as $index => $value) {
    $$index = $value;
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

require_once($Folder_inc."/include-cli.php");
$DB->debug=true;
$DB->forceEncoding("utf8mb4");

if (!isset($argv[2])) {
	sleep(20);
}

if (getSetting("update_charts", 0)) {
    wlog("This script is already running");
    exit();
}

//deprecated
//setSetting("update_charts", 1);

$query = "delete from cache where `timestamp` < unix_timestamp(now()) - 86400;";
$DB->query($query);

$whatToSave = array(
	"piucitati" => array(2, 6, 24, 168, 720, 2160),
	"piusocial" => array(2, 6, 24, 168, 720, 2160),
	"piutwittati" => array(2, 6, 24, 168, 720, 2160)
	//"piucommentati" => array(2, 6, 24, 168),
	//"piuattivi" => array(2, 6, 24, 168),
	// "piucitatiEU" => array(2, 6, 24, 168),
	// "piusocialEU" => array(2, 6, 24, 168),
	// "piutwittatiEU" => array(2, 6, 24, 168)
);

$maxValues = array();
$query = "SELECT type, update_interval, MAX(timestamp) ts
	FROM `cache`
	GROUP BY type, update_interval";
$DB->query($query);
while ($r = $DB->fetch()) {
	if (!isset($maxValues[$r['type']])) {
		$maxValues[$r['type']] = array();
	}
	$maxValues[$r['type']][$r['update_interval']] = $r['ts'];
}

foreach ($whatToSave as $index => $value) {

	switch ($index) {
		case "piucitati":
		$c = new PiuCitati();
		break;

		case "piucommentati":
		$c = new PiuCommentati();
		break;

		case "piusocial":
		$c = new PiuSocial();
		break;

		case "piutwittati":
		$c = new PiuTwittati();
		break;

		case "piuattivi":
		$c = new PiuAttivi();
		break;

		case "piucitatiEU":
		$c = new PiuCitatiEU();
		break;

		case "piusocialEU":
		$c = new PiuSocialEU();
		break;

		case "piutwittatiEU":
		$c = new PiuTwittatiEU();
		break;

	}

	if (isset($c)) {
		$valid_types = $c->getValidTypes();
		foreach ($valid_types as $interval) {
			if (!isset($maxValues[$index][$interval]) || (time() - $maxValues[$index][$interval]) >= ($interval * 60 * 60 / 12)) {
				wlog("Chart $index-$interval to do!");
				try {
					$c->run($interval, 0, true);
				} catch (Exception $e) {
					wlog("done (with errors)!");
					setSetting("update_charts", 0);
					exit();
				}
			}
		}
	}
}

wlog("done!");
//deprecated
//setSetting("update_charts", 0);
