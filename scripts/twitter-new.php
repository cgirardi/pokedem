<?php
// CG: [add] ini_set("default_charset", "utf8");
if (!isset($argv[1])) {
    exit("The first parameter must be the configuration file\n");
}

$Config = parse_ini_file($argv[1]);
foreach ($Config as $index => $value) {
    $$index = $value;
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

require_once($Folder_inc."/twitteroauth/twitteroauth/twitteroauth.php");
require_once($Folder_inc."/include-cli.php");
error_reporting(E_ALL ^ E_NOTICE);
$DB->debug = true;

// pokedembot
$consumer_key = $Tw_PDB_ck;
$consumer_secret = $Tw_PDB_cks;
$oauth_access_token = $Tw_PDB_at;
$oauth_access_token_secret = $Tw_PDB_ats;

$connection = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_access_token, $oauth_access_token_secret);

// $url = "https://api.twitter.com/1.1/statuses/home_timeline.json?exclude_replies=true&count=200&exclude_replies=false";
$url = "https://api.twitter.com/1.1/statuses/home_timeline.json?count=200&exclude_replies=false";
$stuff = $connection->get($url);

$query = "SELECT COUNT(*) num FROM social_updates_new";
$r = $DB->queryfetch($query);
wlog("Rows: {$r['num']}");

$Blacklisted = array();
wlog("Loading blacklisted patterns");
$query = "SELECT * FROM social_updates_blacklisted WHERE social = 'twitter'";
$DB->query($query);
while ($r = $DB->fetch()) {
    $Blacklisted[] = $r['pattern'];
}

// print_r($Blacklisted);

if (count($stuff->errors)) {
    foreach ($stuff->errors as $error) {
        wlog("[{$error->code}]: {$error->message}", "ERROR");
    }
}
//CG: add the case when there is just an error
else if (isset($stuff->error)) {
    exit($stuff->error."\n");
}
else {
    $tot = 0;
    foreach ($stuff as $post) {
        if (!$post->id) {
            continue;
        }

        $query = "SELECT * FROM social_info WHERE twitter = '".addslashes($post->user->screen_name)."'";
        if (!$DB->querynum($query)) {
            wlog("User not found ({$post->user->screen_name})", "WARNING");
            continue;
        }
        $R = $DB->fetch();

        $data = array();
        $data['politician'] = $R['politician'];
        $data['id'] = $post->id;
        $data['type'] = "twitter";

        $text = $post->text;
        $twent = $post->entities;
        
        print $data['politician'] . " [[" .$text. "]]\n";
        
        foreach ($Blacklisted as $bl) {
            if (strpos($text, $bl) !== false) {
                // wlog("Tweet blacklisted: ".$post->user->screen_name." -- ".$text);
                continue 2;
            }
        }

        if ($post->retweeted_status) {
            $data['type2'] = "retweet";
            $data['name'] = $post->retweeted_status->user->screen_name;
            $data['link2'] = "https://twitter.com/{$post->retweeted_status->user->screen_name}/status/{$post->retweeted_status->id}";
            $data['object_id'] = $post->retweeted_status->id;

            $text = $post->retweeted_status->text;
            $twent = $post->retweeted_status->entities;
        }

        $entities = array();
        
        $hashtags = array();
        $user_mentions = array();

        foreach ($twent->hashtags as $entity) {
            $hashtags[] = $entity->text;
            $entities[$entity->indices[0]] = array(
                "url" => "https://twitter.com/search?q=%23".urlencode($entity->text)."&amp;src=hash",
                "start" => $entity->indices[0],
                "end" => $entity->indices[1],
            );
        }
        foreach ($twent->urls as $entity) {
            $entities[$entity->indices[0]] = array(
                "url" => $entity->url,
                "start" => $entity->indices[0],
                "end" => $entity->indices[1],
            );
        }
        foreach ($twent->user_mentions as $entity) {
            $user_mentions[] = $entity->screen_name;
            $entities[$entity->indices[0]] = array(
                "url" => "https://twitter.com/".$entity->screen_name,
                "start" => $entity->indices[0],
                "end" => $entity->indices[1],
            );
        }

        $newtext = "";
        $close_after = -1;
        for ($id = 0; $id < mb_strlen($text, "UTF-8"); $id++) {
            $char = mb_substr($text, $id, 1, "UTF-8");
            if (isset($entities[$id])) {
                $newtext .= "<a target='_blank' href='{$entities[$id]['url']}'>";
                $close_after = $entities[$id]['end'] - 1;
            }
            $newtext .= $char;
            if ($close_after >= 0 && $close_after == $id) {
                $newtext .= "</a>";
                $close_after = -1;
            }
        }
        if ($close_after >= 0) {
            $newtext .= "</a>";
        }


		//$data['message'] = $newtext;
        //CG: remove le last t.co link because it is the link of the tweet itself
        $data['message'] = preg_replace("/\s+http:\/\/t\.co\/.+$/","",trim($newtext));
        $data['created_time'] = date("Y-m-d H:i:s", strtotime($post->created_at));
        $data['shares'] = $post->retweet_count;
        $data['likes'] = $post->favorite_count;
        $data['link'] = "https://twitter.com/{$post->user->screen_name}/status/{$post->id}";


        if (isset($twent->media[0]->media_url)) {
            $data['picture'] = $twent->media[0]->media_url;
        }

        $tot++;
        $DB->queryinsertodku("social_updates_new", $data);
        
        $ID = $post->id;
        
        $query = "DELETE FROM social_updates_new_entities WHERE tweet = '$ID'";
        $DB->query($query);
        
        // Hashtags
        $hashtags = array_unique($hashtags);
        foreach ($hashtags as $tag) {
            $data = array();
            $data['tweet'] = $ID;
            $data['type'] = "hashtag";
            $data['tag'] = $tag;
            $DB->queryinsert("social_updates_new_entities", $data);
        }
        
        // User mentions
        $user_mentions = array_unique($user_mentions);
        foreach ($user_mentions as $tag) {
            $data = array();
            $data['tweet'] = $ID;
            $data['type'] = "user";
            $data['tag'] = $tag;
            $DB->queryinsert("social_updates_new_entities", $data);
        }
        
    }
    
    wlog("Finished, $tot tweets added/updated");
}
