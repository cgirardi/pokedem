<?php

if (!isset($argv[1])) {
    exit("The first parameter must be the configuration file\n");
}

$Config = parse_ini_file($argv[1]);
foreach ($Config as $index => $value) {
    $$index = $value;
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

require_once($Folder_inc."/twitteroauth/twitteroauth/twitteroauth.php");
require_once($Folder_inc."/include-cli.php");
error_reporting(E_ALL ^ E_NOTICE);

$Debug = false;
if ($Debug) {
    wlog("Debug mode ON");
    $DB->debug = true;
}

// poke_dem
$connection = new TwitterOAuth($Tw_PD_ck, $Tw_PD_cks, $Tw_PD_at, $Tw_PD_ats);

// pokedembot
$connectionBot = new TwitterOAuth($Tw_PDB_ck, $Tw_PDB_cks, $Tw_PDB_at, $Tw_PDB_ats);

$Num = 200;
$Iterate = true;
$Update = true;
$Unfollow = false;
$UpdateFollowing = true;
$counterFollow = 0;
$maxUpdateFollow = 100;

$DistanceBetweenTweets = 12; // in minutes
$ProbOfTweetUser = 0.4;
$ProbOfTweetPol = 0.96;
$DistanceBetweenTweetsSameUsersUser = 4; // in days
$DistanceBetweenTweetsSameUsersPol = 1; // in days

$DistanceBetweenUpdates = 20; // in minutes

$Min_followers = 500;
$Min_period = "2 MONTH";

$previousDays = 15; // Must be < 28
$Min_avg = 5;
$Ratio_tweet = 1.5;
$Max_len_tweet = 115;
$Min_tag_frequency = 2;
$PositionPoliticians = 15;

// ---

if ($Debug) {
    $Update = false;
    $Unfollow = false;
    $UpdateFollowing = false;
}

//deprecated
//if (getSetting("run_automatic", 0)) {
//    wlog("This script is already running");
//    exit();
//}
//setSetting("run_automatic", 1);


// Update following

if ($UpdateFollowing) {
    $twitters = array();
    $query = "SELECT politician, twitter FROM social_info WHERE twitter IS NOT NULL AND twitter != '' AND twitter_active=1 AND followed = '0'";
    $DB->query($query);
    while ($r = $DB->fetch()) {
        $twitters[$r['politician']] = strtolower($r['twitter']);
    }

    $api = "https://api.twitter.com/1.1/friendships/create.json";

    foreach ($twitters as $id => $tw) {
        //reach the max update followers
        if ($counterFollow >= $maxUpdateFollow) {
        	echo "WARNING! Following limit reached (set to $maxUpdateFollow).\n";
            break;
        }

        wlog("Following user $tw");
        $stuff = $connectionBot->post($api, array("follow" => true, "screen_name" => urlencode($tw)));
        
        if (count($stuff->errors)) {
            foreach ($stuff->errors as $error) {
                wlog("[{$error->code}]: {$error->message} [{$tw}]", "ERROR");
                // break 2;
            }
            $query = "UPDATE social_info SET twitter_active = '0' WHERE politician = '$id'";
            $DB->query($query);
        } 
    	//CG: add the case when there is just an error
    	else if (isset($stuff->error)) {
    	    $query = "UPDATE social_info SET twitter_active = '0' WHERE politician = '$id'";
            $DB->query($query);
    		exit($stuff->error."\n");
    	}
    	else {
            $query = "UPDATE social_info SET twitter_id = '{$stuff->id}', followed = '1' WHERE politician = '$id'";
            $DB->query($query);
            $counterFollow++;
        }
    }
}


// Collecting Twitter accounts

$query = "SELECT twitter FROM social_info WHERE twitter IS NOT NULL AND twitter != ''";
$Twitters = array();
$DB->query($query);
while ($r = $DB->fetch()) {
    $Twitters[] = strtolower($r['twitter']);
}


// Update

$query = "SELECT * FROM twitter_pd_followers
    WHERE DATE_ADD(`when`, INTERVAL $DistanceBetweenUpdates MINUTE) > NOW()";
if ($DB->querynum($query)) {
    wlog("Update not needed");
    $Update = false;
}

if ($Update) {
    
    // Followers
    wlog("Getting followers");

    $query = "UPDATE twitter_pd_followers SET follower = '0'";
    $DB->query($query);
    $follower_count = 0;

    $cursor = -1;
    $api = "https://api.twitter.com/1.1/followers/ids.json?screen_name=".$Tw_PD_name."&count=5000";
    $error = false;
    do {
        $url = $api . "&cursor=" . $cursor;
        wlog("Opening url $url");
        $stuff = $connection->get($url);

        if (property_exists($stuff, "errors") && count($stuff->errors)) {
            foreach ($stuff->errors as $err) {
                wlog("[{$err->code}]: {$err->message}", "ERROR");
            }
            $error = true;
            break;
        }
        
        if (is_array($stuff->ids)) {
            foreach ($stuff->ids as $userID) {
        
                $data = array();
                // $data['username'] = $user->screen_name;
                // $data['followers'] = $user->followers_count;
                $data['follower'] = 1;
                $data['id'] = $userID;
                $DB->queryinsertodku("twitter_pd_followers", $data);
                $follower_count++;
            }
        }
        $cursor = $stuff->next_cursor;
        if (!$Iterate) {
            $cursor = 0;
        }
    } while ($cursor != 0);

    wlog("Number of followers: {$follower_count}");

    if (!$error) {
        $query = "SELECT * FROM twitter_pd_followers WHERE follower = '0'";
        $DB->query($query);
        while ($r = $DB->fetch()) {
            tlog("lost", $r['username']);
        }

        $query = "DELETE FROM twitter_pd_followers WHERE follower = '0'";
        $DB->query($query);
    }
    else {
        $Unfollow = false;
    }

    $query = "UPDATE `twitter_pd_followers` f
        LEFT JOIN twitter_users u ON u.id = f.id
        SET f.username = u.username, f.followers = u.followers";
    $DB->query($query);

    // Following
    wlog("Getting following");

    $query = "DELETE FROM twitter_pd_following";
    $DB->query($query);

    $cursor = -1;
    $api = "https://api.twitter.com/1.1/friends/ids.json?count=5000";
    $error = false;
    do {
        $url = $api . "&cursor=" . $cursor;
        wlog("opening url $url");
        $stuff = $connection->get($url);

        if (property_exists($stuff, "errors") && count($stuff->errors)) {
            foreach ($stuff->errors as $err) {
                wlog("[{$err->code}]: {$err->message}", "ERROR");
            }
            $error = true;
            break;
        }

        if (is_array($stuff->ids)) {
            foreach ($stuff->ids as $userID) {
        
                $data = array();
                $data['following'] = 1;
                $data['id'] = $userID;
                $DB->queryinsertodku("twitter_pd_following", $data);
            }
        }
        $cursor = $stuff->next_cursor;
        if (!$Iterate) {
            $cursor = 0;
        }
    } while ($cursor != 0);

    $query = "UPDATE `twitter_pd_following` f
        LEFT JOIN twitter_users u ON u.id = f.id
        SET f.username = u.username, f.last_post = u.last_tweet";
    $DB->query($query);
    
    if ($error) {
        $Unfollow = false;
    }
}


// Unfollow users

wlog("Unfollowing users");
$query = "SELECT id, username
    FROM twitter_pd_following
    WHERE
        id != ALL (SELECT id FROM twitter_pd_followers)
        AND id != ALL (SELECT twitter_id FROM social_info WHERE twitter_active = '1')
        AND (followers < '$Min_followers' OR followers IS NULL)
        AND DATE_ADD(last_post, INTERVAL $Min_period) < NOW()";
$n = $DB->querynum($query);
wlog("$n users should be removed");
    while ($r = $DB->fetch()) {
        $userID = $r['id'];
        $username = $r['username'];

        wlog("User: $username ({$userID})");
        if ($Unfollow) {
            $api = "https://api.twitter.com/1.1/friendships/destroy.json";
            wlog("Opening url $api (unfollow $username)");
            $stuff = $connection->post($api, array("user_id" => $userID));

            if (count($stuff->errors)) {
                foreach ($stuff->errors as $err) {
                    wlog("[{$err->code}]: {$err->message}", "ERROR");
                }
                continue;
            }

            tlog("unfollow", $username);
        }
    }
if (!$Unfollow) {
    wlog("Unfollow mode OFF, no users removed");
}


// Tweet a scheduled

$api = "https://api.twitter.com/1.1/statuses/update.json";

$query = "SELECT *, TIMESTAMPDIFF(MINUTE, `last`, now()) difference
    FROM twitter_scheduled
    WHERE (last is NULL OR TIMESTAMPDIFF(MINUTE, `last`, now()) > `interval`)
    AND active = '1'
    ORDER BY difference DESC";
if ($DB->querynum($query, 26)) {
    while ($Rsched = $DB->fetch(26)) {
        // $Rsched = $DB->fetch(26);
        wlog("Tweeting a scheduled [{$Rsched['id']}]");
        
        $hasBeenTweeted = false;
        
        switch ($Rsched['id']) {


            case "party_chart":
            $Tweets = array(
                "Voti %s alle #Europee2014? Scopri i suoi candidati %s",
                "Voti %s? Scopri i suoi candidati alle #Europee2014 %s",
                "Scopri i candidati %s alle #Europee2014 %s"
            );

            $query = "SELECT *,
                    (SELECT `when` FROM twitter_pd_log l
                    WHERE l.action = 'tweet_party_chart' AND l.username = p.party
                    ORDER BY `when` DESC LIMIT 1) t
                FROM tmp_partyeu p
                WHERE p.disabled = '0'
                ORDER BY t IS NULL DESC, t, RAND()
                LIMIT 1";
            $DB->query($query);
            while ($r = $DB->fetch()) {
                $Tweet = $Tweets[array_rand($Tweets)];
                $Tweet = sprintf($Tweet, $r['twitter'], "http://www.pokedem.it/piutwittatiEU?party={$r['id']}&utm_source=Twitter&utm_campaign=party_chart");

                if (!$Debug) {
                    //CG: $stuff = $connection->post($api, array("status" => $Tweet));

                    if (count($stuff->errors)) {
                        foreach ($stuff->errors as $err) {
                            wlog("[{$err->code}]: {$err->message}", "ERROR");
                        }
                    }
                    else {
                        $hasBeenTweeted = true;
                        tlog("tweet_party_chart", $r['party'], $Tweet, $stuff->id);

                        $query = "UPDATE twitter_scheduled SET `last` = NOW() WHERE id = '{$Rsched['id']}'";
                        $DB->query($query, 12);
                    }
                }
                else {
                    $hasBeenTweeted = true;
                }

                wlog("$Tweet");
            }
            break;
            
            
            case "blogpost":
            require($Folder_blog."/wp-blog-header.php");
            function filter_where($where = '') {
                $where .= " AND post_date > DATE_SUB(NOW(), INTERVAL 7 DAY)";
                return $where;
            }
            add_filter('posts_where', 'filter_where');

            query_posts('category_name=nuove-feature&showposts=5&orderby=rand');
            
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                $c = get_post_meta($post->ID, "tweet", true);
                $l = get_permalink($post->ID);
                if ($c && $l) {
                    $Tweet = "$c $l";
                    
                    if (!$Debug) {
                        //CG: $stuff = $connection->post($api, array("status" => $Tweet));

                        if (count($stuff->errors)) {
                            foreach ($stuff->errors as $err) {
                                wlog("[{$err->code}]: {$err->message}", "ERROR");
                            }
                        }
                        else {
                            $hasBeenTweeted = true;
                            tlog("tweet_{$Rsched['id']}", "", $Tweet, $stuff->id);

                            // $query = "UPDATE twitter_scheduled SET `last` = NOW() WHERE id = '{$Rsched['id']}'";
                            // $DB->query($query, 12);
                        }
                    }
                    else {
                        $hasBeenTweeted = true;
                    }

                    wlog("$Tweet");
                    break;
                }
            endwhile; endif;
            
            // If I don't put it here, it always try to send a blogpost and it would never send any scheduled tweet
            $query = "UPDATE twitter_scheduled SET `last` = NOW() WHERE id = '{$Rsched['id']}'";
            $DB->query($query, 12);
            
            break;



            case "trends":
            $Positions = array();
            $NumPositions = 5;

            $query = "SELECT LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) tag, COUNT(*) num
                        FROM social_updates_new_entities e
                        LEFT JOIN twitter_politicians p ON p.tweet = e.tweet
                        WHERE p.politician IS NOT NULL
                        AND p.when_tweet > DATE_SUB(NOW(), INTERVAL 2 HOUR)
                        AND p.when_tweet < NOW()
                        AND e.type = 'hashtag'
                        AND LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) != ALL (SELECT * FROM twitter_tag_blacklisted)
                        GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
                        ORDER BY num DESC
                        LIMIT $NumPositions
            ";
            $DB->query($query);
            $i = 0;
            while ($r = $DB->fetch()) {
                $i++;
                // wlog("$i. {$r['tag']}");
                if (!isset($Positions[$r['tag']])) {
                    $Positions[$r['tag']] = array();
                    $Positions[$r['tag']][0] = 100;
                    $Positions[$r['tag']][1] = 100;
                    $Positions[$r['tag']][2] = 100;
                }
                $Positions[$r['tag']][0] = $i;
            }

            $query = "SELECT LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) tag, COUNT(*) num
                        FROM social_updates_new_entities e
                        LEFT JOIN twitter_politicians p ON p.tweet = e.tweet
                        WHERE p.politician IS NOT NULL
                        AND p.when_tweet > DATE_SUB(DATE_SUB(NOW(), INTERVAL 2 HOUR), INTERVAL 15 MINUTE)
                        AND p.when_tweet < DATE_SUB(NOW(), INTERVAL 15 MINUTE)
                        AND e.type = 'hashtag'
                        AND LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) != ALL (SELECT * FROM twitter_tag_blacklisted)
                        GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
                        ORDER BY num DESC
                        LIMIT $NumPositions
            ";
            $DB->query($query);
            $i = 0;
            while ($r = $DB->fetch()) {
                $i++;
                // wlog("$i. {$r['tag']}");
                if (!isset($Positions[$r['tag']])) {
                    $Positions[$r['tag']] = array();
                    $Positions[$r['tag']][0] = 100;
                    $Positions[$r['tag']][1] = 100;
                    $Positions[$r['tag']][2] = 100;
                }
                $Positions[$r['tag']][1] = $i;
            }

            $query = "SELECT LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) tag, COUNT(*) num
                        FROM social_updates_new_entities e
                        LEFT JOIN twitter_politicians p ON p.tweet = e.tweet
                        WHERE p.politician IS NOT NULL
                        AND p.when_tweet > DATE_SUB(DATE_SUB(NOW(), INTERVAL 2 HOUR), INTERVAL 30 MINUTE)
                        AND p.when_tweet < DATE_SUB(NOW(), INTERVAL 30 MINUTE)
                        AND e.type = 'hashtag'
                        AND LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) != ALL (SELECT * FROM twitter_tag_blacklisted)
                        GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
                        ORDER BY num DESC
                        LIMIT $NumPositions
            ";
            $DB->query($query);
            $i = 0;
            while ($r = $DB->fetch()) {
                $i++;
                // wlog("$i. {$r['tag']}");
                if (!isset($Positions[$r['tag']])) {
                    $Positions[$r['tag']] = array();
                    $Positions[$r['tag']][0] = 100;
                    $Positions[$r['tag']][1] = 100;
                    $Positions[$r['tag']][2] = 100;
                }
                $Positions[$r['tag']][2] = $i;
            }

            $Sentences = array();
            $Hashes = array();

            $avoidHashes = array();
            $query = "SELECT username
                FROM twitter_pd_log
                WHERE action = 'tweet_scheduled'
                AND ADDDATE(`when`, INTERVAL 10 MINUTE) > NOW()
                AND username LIKE '#%'";
            $DB->query($query);
            while ($r = $DB->fetch()) {
                $avoidHashes[$r['username']] = true;
            }
            // print_r($avoidHashes);

            foreach ($Positions as $key => $values) {
                $key = utf8_encode($key);

                $diff = array();
                $diff[0] = ($values[0] > $values[1]) ? "-" : (($values[0] < $values[1]) ? "+" : "=");
                $diff[1] = ($values[1] > $values[2]) ? "-" : (($values[1] < $values[2]) ? "+" : "=");

                $numTimes = $values[0] == 100 ? 1 : floor(2 * log($NumPositions + 2 - $values[0]));
                $txt = "";

                if ($values[0] == 100 || $values[1] == 100 || $values[2] == 100) {
                    switch (true) {
                        case ($values[0] == 100 && $values[1] != 100):
                        // $txt = "#$key esce dalla top-ten dei #trendingtopic in politica";
                        break;

                        case ($values[0] != 100 && $values[1] == 100):
                        $txt = "#$key entra nella top-ten dei #trendingtopic in politica, alla posizione n. {$values[0]}";
                        break;
                    }
                }
                else {
                    switch ("{$diff[0]}{$diff[1]}") {
                        case "=+":
                        $numTimes = floor($numTimes / 2);
                        $txt = "#$key rimane saldamente alla posizione n. {$values[0]} dei #trendingtopic in politica";
                        break;

                        case "=-":
                        $numTimes = floor($numTimes / 2);
                        $pos = $values[1] - $values[2];
                        $posTxt = "di una posizione";
                        if ($pos > 1) {
                            $posTxt = "di $pos posizioni";
                        }
                        $txt = "Dopo il precedente calo $posTxt, #$key mantiene la posizione n. {$values[0]} dei #trendingtopic in politica";
                        break;

                        case "==":
                        $numTimes = floor($numTimes / 3);
                        $txt = "#$key continua a mantenere la posizione n. {$values[0]} dei #trendingtopic in politica";
                        break;

                        case "++":
                        $numTimes = floor($numTimes * 2);
                        $txt = "In netta salita, #$key raggiunge la posizione n. {$values[0]} fra i #trendingtopic in politica";
                        break;

                        case "+=":
                        $txt = "#$key sale alla posizione n. {$values[0]} fra i #trendingtopic in politica";
                        break;

                        case "+-":
                        $txt = "#$key risale alla posizione n. {$values[0]} fra i #trendingtopic in politica";
                        break;

                        case "-+":
                        // $txt = "#$key riscende alla posizione n. {$values[0]} fra i #trendingtopic in politica";
                        break;

                        case "--":
                        $numTimes = floor($numTimes * 2);
                        // $txt = "#$key in netta discesa fra i #trendingtopic in politica, ora alla posizione n. {$values[0]}";
                        break;

                        case "-=":
                        // $txt = "#$key scende alla posizione n. {$values[0]} fra i #trendingtopic in politica";
                        break;

                    }
                }

                if ($txt) {
                    for ($i = 0; $i < $numTimes; $i++) {
                        if (isset($avoidHashes["#$key"])) {
                            continue;
                        }
                        $Sentences[] = $txt;
                        $Hashes[] = $key;
                    }
                    // wlog("-- $key -- {$values[0]} {$values[1]} {$values[2]} -- {$diff[0]} {$diff[1]} -- $numTimes");
                    // wlog($txt);
                }

            }

            if (count($Sentences)) {
                $id = array_rand($Sentences);
                $Tweet = $Sentences[$id];
                $hash = $Hashes[$id];

                if (strlen($txt) <= 117) {
                    $Tweet .= " ".Link::Tendenze();
                    $Tweet .= "?utm_source=Twitter&utm_campaign=trends";
                }

                if (!$Debug) {
                    //CG: $stuff = $connection->post($api, array("status" => $Tweet));
                
                    if (count($stuff->errors)) {
                        foreach ($stuff->errors as $err) {
                            wlog("[{$err->code}]: {$err->message}", "ERROR");
                        }
                    }
                    else {
                        $hasBeenTweeted = true;
                        tlog("tweet_{$Rsched['id']}", "", $Tweet, $stuff->id);

                        $query = "UPDATE twitter_scheduled SET `last` = NOW() WHERE id = '{$Rsched['id']}'";
                        $DB->query($query, 12);
                    }
                }
                else {
                    $hasBeenTweeted = true;
                }

                wlog($Tweet);
                // wlog($hash);
            }

            break;
            
            
            
            case "trendingtopics":
            $DateTime = time() - (time() % 300);
            $Type = 2;
            $query = "SELECT e.tag tag, COUNT(*) num
                FROM social_updates_new_entities e
                LEFT JOIN twitter_updates u ON e.tweet = u.id
                LEFT JOIN twitter_politicians p ON p.tweet = u.id
                WHERE p.politician IS NOT NULL
                AND p.when_tweet > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
                AND p.when_tweet < FROM_UNIXTIME($DateTime)
                AND e.type = 'hashtag'
                AND LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) != ALL (SELECT * FROM twitter_tag_blacklisted)
                GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
                ORDER BY num DESC
                LIMIT 10";
            $Tweet = "I #trendingtopic del momento in politica sono";
            
            $DB->query($query, 10);
            while ($rT = $DB->fetch(10)) {
                if ($rT['num'] < $Min_tag_frequency) {
                    continue;
                }
                $TText_test = $Tweet . " #{$rT['tag']}";
                if (strlen($TText_test) < $Max_len_tweet) {
                    $Tweet = $TText_test;
                }
            }
            
            $Tweet .= " ".Link::Tendenze();
            
            if (!$Debug) {
                //CG: $stuff = $connection->post($api, array("status" => $Tweet));
            
                if (count($stuff->errors)) {
                    foreach ($stuff->errors as $err) {
                        wlog("[{$err->code}]: {$err->message}", "ERROR");
                    }
                }
                else {
                    $hasBeenTweeted = true;
                    tlog("tweet_{$Rsched['id']}", "", $Tweet, $stuff->id);

                    $query = "UPDATE twitter_scheduled SET `last` = NOW() WHERE id = '{$Rsched['id']}'";
                    $DB->query($query, 12);
                }
            }
            else {
                $hasBeenTweeted = true;
            }
            
            wlog("$Tweet");
            
            break;
            
        }
        
        // if ($hasBeenTweeted && !$Debug) {
        //     setSetting("run_automatic", 0);
        //     exit();
        // }
    }
}
else {
    wlog("No scheduled");
}

//Check unreacheable Twitter's account
$query = "SELECT politician, twitter FROM social_info WHERE twitter_active=0 AND (twitter IS NOT NULL AND twitter != '')";
$n = $DB->querynum($query);
if ($n > 0) {
    echo "\nCheck the following unreacheable Twitter accounts:\n";
    while ($r = $DB->fetch()) {
         echo $r['twitter'] . " (ID: ".$r['politician']. ")\n";
    }
}
