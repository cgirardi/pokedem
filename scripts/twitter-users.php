<?php
//setlocale(LC_ALL, "en_US.UTF-8");
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");

function collectTweets($TwitterAccounts = array(), $Tweets = 100) {
    global $connection, $DB;
    $DBid = md5("collectTweets");
//   $DB->forceEncoding();
 
    $url = "https://api.twitter.com/1.1/search/tweets.json?q=".implode("%20OR%20", $TwitterAccounts)."&count=$Tweets";
    wlog("URL: $url");
    wlog("Politicians: ".count($TwitterAccounts));
    wlog("Length: ".strlen($url));

    $stuff = $connection->get($url);
    if (isset($stuff->errors)) {
        foreach ($stuff->errors as $error) {
            wlog("[{$error->code}] {$error->message}", "ERROR");
        }
        return;
    } 
    //CG: add the case when there is just an error
    else if (isset($stuff->error)) {
    	exit($stuff->error."\n");
    }
    
    foreach ($stuff->statuses as $post) {
        if (!$post->id) {
            continue;
        }
    
        // print_r($post);
    
        $UserID = $post->user->id;
        $TweetID = $post->id;
    
        // User
        $dati = array();
        $dati['id'] = $UserID;
        $dati['username'] = $post->user->screen_name;
        $dati['location'] = $post->user->location;
        $dati['name'] = $post->user->name;
        $dati['description'] = $post->user->description;
        $dati['picture'] = $post->user->profile_image_url;
        $dati['followers'] = $post->user->followers_count;
        $dati['following'] = $post->user->friends_count;
        $dati['tweets'] = $post->user->statuses_count;
        $DB->queryinsertodku("twitter_users", $dati, array(), $DBid);
    
        // Citation
        foreach ($post->entities->user_mentions as $entity) {
            $query = "SELECT politician FROM social_info WHERE twitter = '".addslashes($entity->screen_name)."'";
            if ($DB->querynum($query, $DBid)) {
                $r = $DB->fetch($DBid);
            
                $dati = array();
                $dati['tweet'] = $TweetID;
                $dati['politician'] = $r['politician'];
                $dati['when_tweet'] = date("Y-m-d H:i:s", strtotime($post->created_at));
                $DB->queryinsertodku("twitter_politicians", $dati);
            }
        }
    
        // Tweet
        $data = array();
        $data['id'] = $TweetID;
        $data['user_id'] = $UserID;
    
        $text = $post->text;
        $twent = $post->entities;
    
        if ($post->retweeted_status) {
            $data['type2'] = "retweet";
            $data['name'] = $post->retweeted_status->user->screen_name;
            $data['link2'] = "https://twitter.com/{$post->retweeted_status->user->screen_name}/status/{$post->retweeted_status->id}";
            $data['object_id'] = $post->retweeted_status->id;
            
            $dataPol = array();
            $dataPol['shares'] = $post->retweeted_status->retweet_count;
            $dataPol['likes'] = $post->retweeted_status->favorite_count;
            $DB->queryupdate("social_updates_new", $dataPol, array("id" => $post->retweeted_status->id), $DBid);
    
            $text = $post->retweeted_status->text;
            $twent = $post->retweeted_status->entities;
        }
    
        $entities = array();
    
        $hashtags = array();
        $user_mentions = array();

        foreach ($twent->hashtags as $entity) {
            $hashtags[] = $entity->text;
            $entities[$entity->indices[0]] = array(
                "url" => "https://twitter.com/search?q=%23".urlencode($entity->text)."&amp;src=hash",
                "start" => $entity->indices[0],
                "end" => $entity->indices[1],
            );
        }
        foreach ($twent->urls as $entity) {
            $entities[$entity->indices[0]] = array(
                "url" => $entity->url,
                "start" => $entity->indices[0],
                "end" => $entity->indices[1],
            );
        }
        foreach ($twent->user_mentions as $entity) {
            $user_mentions[] = $entity->screen_name;
            $entities[$entity->indices[0]] = array(
                "url" => "https://twitter.com/".$entity->screen_name,
                "start" => $entity->indices[0],
                "end" => $entity->indices[1],
            );
        }
    
        $newtext = "";
        $close_after = -1;
        for ($id = 0; $id < mb_strlen($text, "UTF-8"); $id++) {
            $char = mb_substr($text, $id, 1, "UTF-8");
            if (isset($entities[$id])) {
                $newtext .= "<a target='_blank' href='{$entities[$id]['url']}'>";
                $close_after = $entities[$id]['end'] - 1;
            }
            $newtext .= $char;
            if ($close_after >= 0 && $close_after == $id) {
                $newtext .= "</a>";
                $close_after = -1;
            }
        }
        if ($close_after >= 0) {
            $newtext .= "</a>";
        }
    
	 	//$data['message'] = $newtext;
        //CG: remove le last t.co link because it is the link of the tweet itself
        $data['message'] = preg_replace("/\s+http:\/\/t\.co\/[^\s]+\s*$/","",trim($newtext));
        $data['created_time'] = date("Y-m-d H:i:s", strtotime($post->created_at));
        $data['shares'] = $post->retweet_count;
        $data['likes'] = $post->favorite_count;
        $data['link'] = "https://twitter.com/{$post->user->screen_name}/status/{$post->id}";
    
        if (isset($twent->media[0]->media_url)) {
            $data['picture'] = $twent->media[0]->media_url;
        }
    
        $DB->queryinsertodku("twitter_updates", $data, array(), $DBid);
        
        $ID = $TweetID;
        
        $query = "DELETE FROM social_updates_new_entities WHERE tweet = '$ID'";
        $DB->query($query, $DBid);
        
        // Hashtags
        $hashtags = array_unique($hashtags);
        foreach ($hashtags as $tag) {
            $data = array();
            $data['tweet'] = $ID;
            $data['type'] = "hashtag";
            $data['tag'] = $tag;
            $DB->queryinsert("social_updates_new_entities", $data, $DBid);
        }
        
        // User mentions
        $user_mentions = array_unique($user_mentions);
        foreach ($user_mentions as $tag) {
            $data = array();
            $data['tweet'] = $ID;
            $data['type'] = "user";
            $data['tag'] = $tag;
            $DB->queryinsert("social_updates_new_entities", $data, $DBid);
        }
        
    }
    
}

function updatePoliticians($PolsToUpdate) {
    global $DB;
    $DBid = md5("updatePoliticians");
    
    foreach ($PolsToUpdate as $pol) {
        $data = array();
        $data['politician'] = $pol;
        $data['when'] = date("Y-m-d H:i:s");
        $DB->queryinsertodku("twitter_updates_done", $data, array(), $DBid);
    }
}

if (!isset($argv[1])) {
    exit("The first parameter must be the configuration file\n");
}

$Config = parse_ini_file($argv[1]);
foreach ($Config as $index => $value) {
    $$index = $value;
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

require_once($Folder_inc."/twitteroauth/twitteroauth/twitteroauth.php");
require_once($Folder_inc."/include-cli.php");
error_reporting(E_ALL ^ E_NOTICE);

// pokedembot
$consumer_key = $Tw_PDB_ck;
$consumer_secret = $Tw_PDB_cks;
$oauth_access_token = $Tw_PDB_at;
$oauth_access_token_secret = $Tw_PDB_ats;

$connection = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_access_token, $oauth_access_token_secret);

$Tweets = 100;

$NumberOfUsers = 25;
$PosInCharts = $NumberOfUsers / 3;

$twitterChart = array();
$PolsToUpdateCharts = array();

// Getting charts
wlog("Collecting charts");

// #piùcitati
$query = "SELECT p.id politician, soc.twitter
    FROM articles a
    LEFT JOIN politicians p ON p.id = a.politician
    LEFT JOIN politician_info i ON i.politician = a.politician
    LEFT JOIN social_info soc ON soc.politician = a.politician
    LEFT JOIN parties pt ON p.party = pt.id
    LEFT JOIN houses h ON h.id = p.house
    WHERE a.`when` > DATE_SUB(NOW(), INTERVAL 2 HOUR)
        AND a.`when` < NOW()
    GROUP BY a.politician
    ORDER BY COUNT(DISTINCT a.title) DESC";
$resQ = $QP->get($query, 300);
$added = 0;
while ($r = $resQ->next()) {
    if (!$r['twitter']) {
        continue;
    }
    if (!in_array(urlencode("@".$r['twitter']), $twitterChart)) {
        $added++;
        $twitterChart[] = urlencode("@".$r['twitter']);
        $PolsToUpdateCharts[] = $r['politician'];
        if ($added >= $PosInCharts) {
            break;
        }
    }
}

// #piùsocial
$query = "SELECT s.politician, soc.twitter
    FROM `social_updates_new` s
    LEFT JOIN politicians p ON p.id = s.politician
    LEFT JOIN social_info soc ON soc.politician = s.politician
    WHERE
        `created_time` > DATE_SUB(NOW(), INTERVAL 2 HOUR)
        AND `created_time` < NOW()
    GROUP BY politician
    ORDER BY COUNT(*) DESC";
$resQ = $QP->get($query, 300);
$added = 0;
while ($r = $resQ->next()) {
    if (!$r['twitter']) {
        continue;
    }
    if (!in_array(urlencode("@".$r['twitter']), $twitterChart)) {
        $added++;
        $twitterChart[] = urlencode("@".$r['twitter']);
        $PolsToUpdateCharts[] = $r['politician'];
        if ($added >= $PosInCharts) {
            break;
        }
    }
}

// #piùtwittati
$query = "SELECT t.politician, soc.twitter
    FROM twitter_politicians t
    LEFT JOIN twitter_updates u ON u.id = t.tweet
    LEFT JOIN politicians p ON p.id = t.politician
    LEFT JOIN social_info soc ON soc.politician = t.politician
    WHERE t.when_tweet > DATE_SUB(NOW(), INTERVAL 2 HOUR)
        AND t.when_tweet < NOW()
    GROUP BY t.politician
    ORDER BY COUNT(*) DESC";
$resQ = $QP->get($query, 300);
$added = 0;
while ($r = $resQ->next()) {
    if (!$r['twitter']) {
        continue;
    }
    if (!in_array(urlencode("@".$r['twitter']), $twitterChart)) {
        $added++;
        $twitterChart[] = urlencode("@".$r['twitter']);
        $PolsToUpdateCharts[] = $r['politician'];
        if ($added >= $PosInCharts) {
            break;
        }
    }
}

// Each "run" performs two requests to search/tweets API
$Runs = 4;

for ($I = 1; $I <= $Runs; $I++) {

    wlog("Run $I");
    
    wlog("Tweets of charts");
    collectTweets($twitterChart, $Tweets);
    updatePoliticians($PolsToUpdateCharts);

    $twitter = array();
    $PolsToUpdate = array();

    $query = "SELECT i.politician, i.twitter
        FROM social_info i
        LEFT JOIN twitter_updates_done s ON s.politician = i.politician
	WHERE ignore_social = '0'
        ORDER BY s.`when` IS NULL DESC, `when`";
    $DB->query($query);
    $added = 0;
    while ($r = $DB->fetch()) {
        if (!$r['twitter']) {
            continue;
        }
        if (!in_array(urlencode("@".$r['twitter']), $twitter)) {
            $added++;
            $twitter[] = urlencode("@".$r['twitter']);
            $PolsToUpdate[] = $r['politician'];
            if ($added >= $NumberOfUsers) {
                break;
            }
        }
    }

    wlog("Tweets out of charts");
    collectTweets($twitter, $Tweets);
    updatePoliticians($PolsToUpdate);
}

wlog("The end!");
