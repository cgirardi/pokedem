<?php

###########################################
# Necessaria la classe di FCK
# Necessaria la funzione span()
###########################################

class Modulo {

	###########################################
	# Variabili della classe
	###########################################

	var $stringa = "";
	var $classe = "";
	var $tab_classe = "";
	var $picc_classe = "";
	var $method = "post";
	var $action = "";
	var $spaziocelle = "3";
	var $hiddenclass = "nascosto";
	var $def;
	var $duecolonne = 1;
	var $enctype = "";
	var $tabindex = 0;
	
	###########################################
	# Costruttore della classe
	###########################################

	function Modulo($classe, $tab_classe, $action, $def = array()) {
		$this->classe = $classe;
		$this->tab_classe = $tab_classe;
		$this->action = $action;
		$this->def = $def;
		if (!isset($GLOBALS['indice_per_tab'])) {
			$GLOBALS['indice_per_tab'] = 10;
		}
		$this->tabindex = $GLOBALS['indice_per_tab'] + 1;
	}

	###########################################
	# Funzioni varie
	###########################################

	function pulisci($testo) {
		return str_replace("'", "&#039;", $testo);
	}
	
	function agg_free($testo) {
    $this->tabindex++;
    $testo = str_replace("--tab--", $tabindex, $testo);
		$this->stringa .= $testo."\n";
	}
		
	###########################################
	# Funzioni per i soli campi
	###########################################

	function n_text($nome, $dimensione, $titolo, $testo = "", $piccolino = "") {
		$this->tabindex++;
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$this->stringa .=  "<input type='text' name='".$nome."' value='".$this->pulisci($testo)
			. "' size='".$dimensione."' tabindex='".$this->tabindex."'"
			. ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "");
	}
	
	function n_check($nome, $titolo, $testo = "", $piccolino = "") {
		$this->tabindex++;
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$this->stringa .= "<input type='checkbox' tabindex='".$this->tabindex
			. "' name='".$nome."' id='".$nome."'".($testo ? " checked='checked'" : "")
			. ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. "<label for='".$nome."'>".$titolo."</label>\n"
			. ($piccolino ? "<br />".span($piccolino, $this->picc_classe) : "");
	}
	
	function n_textlongfico($nome, $dim1, $dim2, $titolo, $testo = "", $piccolino = "", $toolbar = "Basic") {
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$fck = new FCKeditor($nome);
		$fck->Value = $testo;
		$fck->Width = $dim1;
		$fck->Height = $dim2;
		$fck->Config['ForcePasteAsPlainText'] = true;
		$fck->ToolbarSet = $toolbar;
		$this->stringa .= $fck->CreateHtml()
		. ($piccolino ? span($piccolino, $this->picc_classe) : "");
	}
	
	function n_password($nome, $dimensione, $titolo, $piccolino = "") {
		$this->tabindex++;
		$this->stringa .= "<input type='password' tabindex='".$this->tabindex
			. "' name='".$nome."' value='".$this->pulisci($testo)."' size='"
			. $dimensione."'" . ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "");
	}
	
	function n_file($nome, $dimensione, $titolo, $testo = "", $piccolino = "") {
		$this->tabindex++;
		$this->stringa .= "<input type='file' tabindex='".$this->tabindex
			. "' name='".$nome."' value='".$this->pulisci($testo)."' size='"
			. $dimensione."'" . ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "");
	}
	
	function n_submit($testo = "Invia", $piccolino = "") {
		$this->tabindex++;
		$this->stringa .= "<input type='submit' tabindex='".$this->tabindex."' value='"
			. $this->pulisci($testo)."'" . ($this->classe ? " class='".$this->classe."'" : "")
			. " />\n" // onclick='this.disabled = true; return true;'
			. ($piccolino ? span($piccolino, $this->picc_classe) : "");
	}
	
	function n_select($nome, $titolo, $array, $testo = "", $piccolino = "", $type = "") {
		$this->tabindex++;
		$nomesenzaparentesi = str_replace("[]", "", $nome);
		if (!$testo && $this->def[$nomesenzaparentesi])
			$testo = $this->def[$nomesenzaparentesi];
		$this->stringa .= "\t<select name='".$nome."' tabindex='".$this->tabindex."'"
			. ($this->classe ? " class='".$this->classe."'" : "")
			. ($type ? " ".$type : "") . ">\n";
		foreach ($array as $indice=>$valore) {
			$this->stringa .= "\t\t<option value='".$indice."'";
			if (is_array($testo)) {
				$this->stringa .= (in_array($indice, $testo) ? " selected" : "");
			}
			else {
				$this->stringa .= ($testo == $indice ? " selected" : "");
			}
			$this->stringa .= ">".($valore !== "" ? $valore : "[Non specificato]")."</option>\n";
		}
		$this->stringa .= "\t</select>\n"
				. ($piccolino ? span($piccolino, $this->picc_classe) : "");
	}	

	###########################################
	# Funzioni per i campi con le tabelle
	###########################################
	
	function agg_text($nome, $dimensione, $titolo, $testo = "", $piccolino = "", $calendario = 0) {
    $this->tabindex++;
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>" . (!$this->duecolonne ? $titolo." " : "")
			. "<input type='text' name='".$nome."' id='".$nome."' value='".$this->pulisci($testo)
      . "' size='".$dimensione."' tabindex='".$this->tabindex."'"
      . ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($calendario ? "<input type=\"button\" value=\" ... \" id='".$nome."123'/>
<script type=\"text/javascript\">
  Calendar.setup(
    {
      inputField  : \"".$nome."\",         // ID of the input field
      ifFormat    : \"%Y-%m-%d\",    // the date format
      button      : \"".$nome."123\",       // ID of the button
step: 1
    }
  );
</script>" : "")
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}

	function agg_img($nomeimmagine, $titolo = "", $piccolino = "") {
    $this->tabindex++;
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>" . (!$this->duecolonne ? $titolo." " : "")
			. "<input type='image' tabindex='".$this->tabindex."' src='".$nomeimmagine."'" . ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}

	function agg_check($nome, $titolo, $testo = "", $piccolino = "") {
    $this->tabindex++;
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'></td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>"
			. "<input type='checkbox' tabindex='".$this->tabindex."' name='".$nome."' id='".$nome."'".($testo ? " checked='checked'" : "")
			. ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. "<label for='".$nome."'>".$titolo."</label>\n"
			. ($piccolino ? "<br />".span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}
	
	function agg_empty() {
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>&nbsp;</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>&nbsp;\t</td>\n</tr>\n";
	}

	function agg_text_nomod($nome, $dimensione, $titolo, $testo = "", $piccolino = "") {
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>" . (!$this->duecolonne ? $titolo." " : "")
			. "<strong>".$this->pulisci($testo)."</strong>\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}

	function agg_textlong($nome, $dim1, $dim2, $titolo, $testo = "", $piccolino = "") {
    $this->tabindex++;
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>"
			. "<textarea name='".$nome."' tabindex='".$this->tabindex."' rows='".$dim2."' cols='".$dim1."'" . ($this->classe ? " class='".$this->classe."'" : "") . ">"
			. htmlspecialchars($testo)."</textarea>\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}
	
	function agg_textlongfico($nome, $dim1, $dim2, $titolo, $testo = "", $piccolino = "", $toolbar = "Basic") {
		if (!$testo && $this->def[$nome])
			$testo = $this->def[$nome];
		$fck = new FCKeditor($nome);
		$fck->Value = $testo;
		$fck->Width = $dim1;
		$fck->Height = $dim2;
		$fck->ToolbarSet = $toolbar;
		$fck->BasePath = '../fck/';
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right' valign='top'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>"
			. $fck->CreateHtml()
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}

	function agg_password($nome, $dimensione, $titolo, $piccolino = "") {
    $this->tabindex++;
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>"
			. "<input type='password' tabindex='".$this->tabindex."' name='".$nome."' value='".$this->pulisci($testo)."' size='".$dimensione."'" . ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}
	
	function agg_file($nome, $dimensione, $titolo, $testo = "", $piccolino = "") {
    $this->tabindex++;
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			" align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>"
			. "<input type='file' tabindex='".$this->tabindex."' name='".$nome."' value='".$this->pulisci($testo)."' size='".$dimensione."'" . ($this->classe ? " class='".$this->classe."'" : "") . " />\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}
	
	function agg_submit($testo = "Invia", $piccolino = "", $altro = "") {
    $this->tabindex++;
		$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			"></td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			." align='left'>"
			. "<input type='submit' tabindex='".$this->tabindex."' value='".$this->pulisci($testo)."'" . ($this->classe ? " class='".$this->classe."'" : "")
			. " />\n" // onclick='this.disabled = true; return true;'
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. $altro
			. "\t</td>\n</tr>\n";
	}
	
	function agg_select($nome, $titolo, $array, $testo = "", $piccolino = "", $type = "") {
    $this->tabindex++;
	  $nomesenzaparentesi = str_replace("[]", "", $nome);
		if (!$testo && $this->def[$nomesenzaparentesi])
			$testo = $this->def[$nomesenzaparentesi];
  	$this->stringa .= "<tr>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='right'>".$titolo."</td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			. " align='left'>\n"
			. "\t<select name='".$nome."' id='".$nome."' tabindex='".$this->tabindex."'" . ($this->classe ? " class='".$this->classe."'" : "")
      . ($type ? " ".$type : "") . ">\n";
		foreach ($array as $indice=>$valore) {
			$this->stringa .= "\t\t<option value='".$indice."'";
			if (is_array($testo)) {
				$this->stringa .= (in_array($indice, $testo) ? " selected" : "");
			}
			else {
			  $this->stringa .= ($testo == $indice ? " selected" : "");
			}
      $this->stringa .= ">".($valore !== "" ? $valore : "[Non specificato]")."</option>\n";
		}
		$this->stringa .= "\t</select>\n"
			. ($piccolino ? span($piccolino, $this->picc_classe) : "")
			. "\t</td>\n</tr>\n";
	}

	function agg_hidden($nome, $valore = "") {
		$this->stringa .= "<tr class='".$this->hiddenclass."'>\n"
			. ($this->duecolonne ? "\t<td" . ($this->classe ? " class='".$this->classe."'" : "") .
			"></td>\n" : "")
			. "\t<td" . ($this->classe ? " class='".$this->classe."'" : "")
			." align='left'>"
			. "<input id='hidden_$nome' type='hidden' value='".$valore."' name='".$nome."' />\n"
			. "\t</td>\n</tr>\n";
	}
	
	###########################################
	# Funzioni di output
	###########################################
	
	function scrivi_html() {
		echo $this->retrieve_html();
	}
	
	function retrieve_html() {
		$tmpstringa = "<form method='".$this->method."' action='".$this->action."'"
			. ($this->enctype ? " enctype='".$this->enctype."'" : "").">\n"
			. "<table" . ($this->tab_classe ? " class='".$this->tab_classe."'" : "")
			. " cellspacing='".$this->spaziocelle."'>\n"
			. $this->stringa
			. "</table>\n</form>\n";
      $GLOBALS['indice_per_tab'] = $this->tabindex;
		return $tmpstringa;
	}

}

?>