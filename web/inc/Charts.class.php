<?php

// TODO: blacklist
// if (isset($r['followers']) && $r['followers'] == 0) {
//     $query = "INSERT IGNORE INTO twitter_blacklisted (id) VALUES ({$r['id']})";
//     $DB->query($query, 123);
//     tlog("blacklist", $r['username']);
// }


abstract class Chart {
	abstract protected function getQueryBefore();
	abstract protected function getQuery();

	protected $type = "";
	protected $default_image = "";
	protected $linkPage = "";
	protected $where = array();
	protected $validTypes = array();

	function __construct($type = "") {
		if (strlen($type) > 0) {
			$this->type = $type;
		}
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function setDefaultImage($image) {
        $this->default_image = $image;
    }

	public function addWhere($str) {
		$this->where[] = $str;
	}

	public function getValidTypes() {
		return $this->validTypes;
	}

	static public function getTextForType($Type, $prefix = false) {
		$txt = "";

		if ($Type <= 24) {
			$txt .= ($prefix ? "nelle " : "");
		    $txt .= "ultime $Type ore";
		}
		else {
		    $days = $Type / 24;
	    	$txt .= ($prefix ? "negli " : "");
	        $txt .= "ultimi $days giorni";
		}

		return $txt;
	}

	static public function updatePositions($array) {
		$pos = 0;
		$oldnum = 0;
		$oldpos = 0;

		$OldPositions = array();
		$pos = 0;
		foreach ($array as $label => &$value) {
			$pos++;
			
			if ($oldnum == $value['num']) {
			    $value['pos'] = $oldpos;
			}
			else {
			    $value['pos'] = $oldpos = $pos;
			}
			$oldnum = $value['num'];
		}

		return $array;
	}

	static private function calcTrend($oldChart, $newChart) {
		foreach ($newChart as $key => &$value) {
			$pos = $value['pos'];
			if (isset($oldChart[$key])) {
			    $old = $oldChart[$key]['pos'];
			    if ($old > $pos) {
			        // Up
			        $value['trend'] = "up";
			    }
			    elseif ($old < $pos) {
			        // Down
			    	$value['trend'] = "down";
			    }
			    else {
			        // None
			        $value['trend'] = "no";
			    }
			}
			else {
			    // New entry
			    $value['trend'] = "new";
			}
		}
		return $newChart;
	}

	function get($interval = 2, $timestamp = 0) {
		global $DB;
		$DBid = md5("Chart::get");

		$chart = array();
		if (!$this->type) {
			return $chart;
		}

		$interval = addslashes($interval);

		// Get last chart
		if ($timestamp == 0) {
			$query = "SELECT * FROM cache WHERE type = '{$this->type}' AND update_interval = '$interval' ORDER BY `timestamp` DESC";
			if ($DB->querynum($query, $DBid)) {
				$r = $DB->fetch($DBid);
				$tmp = json_decode($r['value'], true);
				foreach($tmp as &$v) {
					$v['cached'] = "yes-last (ts: {$r['timestamp']})";
				}
				return $tmp;
			}
		}
		else {
			$query = "SELECT * FROM cache
				WHERE type = '{$this->type}'
					AND `timestamp` <= '$timestamp'
					AND update_interval = '$interval'
				ORDER BY `timestamp` DESC";
				if ($DB->querynum($query, $DBid)) {
					$r = $DB->fetch($DBid);
					$tmp = json_decode($r['value'], true);
					foreach($tmp as &$v) {
						$v['cached'] = "yes-old (ts: {$r['timestamp']})";
					}
					return $tmp;
				}
		}

		$tmp = $this->run($interval, $timestamp);
		foreach($tmp as &$v) {
			$v['cached'] = "no";
		}
		return $tmp;

	}

	function run($interval = 2, $timestamp = 0, $save = false) {
		global $DB, $thisDB;

		$DBid = md5("Chart::run");
		$Where = $this->where;

		if (!$timestamp) {
			$timestamp = time();
		}

		$timestamp = $timestamp - ($timestamp % 300);
		$timestampBefore = $timestamp - 60 * 60 * $interval;

		$Where[] = "p.in_charts = '1'";
		$WhereDef = implode(" AND ", $Where);

		// Old chart
		$query = $this->getQueryBefore();
		$query = sprintf($query, $timestampBefore, $interval, $WhereDef);
		$thisDB->querynum($query, $DBid);
		$oldChart = array();
		while ($r = $thisDB->fetch_a($DBid)) {
			$oldChart[$r['label']] = $r;
			// if ($r['max_citati'] > $pos || $r['max_citati'] == 0) {
			//     $DB->queryupdate("politicians", array("max_citati" => $pos), array("id" => $r['politician']), 1);
			// }
		}

		$oldChart = $this->updatePositions($oldChart);

		// $query = "select @@hostname";
		// $thisDB->query($query, $DBid);
		// print_r($thisDB->fetch($DBid));

		// New chart
		$query = $this->getQuery();
		$query = sprintf($query, $timestamp, $interval, $WhereDef);
		// echo $query;

		$thisDB->querynum($query, $DBid);
		$newChart = array();
		while ($r = $thisDB->fetch_a($DBid)) {
			$newChart[$r['label']] = $r;
		}

		$newChart = $this->updatePositions($newChart);

		$newChart = $this->calcTrend($oldChart, $newChart);

		if ($save && $this->type) {
			$data = array();
			$data['type'] = $this->type;
			$data['update_interval'] = $interval;
			$data['timestamp'] = $timestamp;
			$data['value'] = json_encode($newChart);
			$DB->queryinsert("cache", $data, $DBid);
		}

		// print_r($newChart);
		return $newChart;
	}

	function printPoliticianRow($r, $pos, $Hashtag, $Period, $TwUrl) {
		global $TButtonUrl;

		$newpos = $r['pos'];

		$Twitter = "@".$r['twitter'];
		if (!$Twitter || strlen($Twitter) == 1) {
		    $Twitter = $r['name']." ".$r['surname'];
		}
		
		if (strlen($Twitter) > 25) {
		    $Twitter = $r['surname'];
		}

		if ($newpos == 1) {
		    $TText = "$Twitter è al primo posto dei $Hashtag $Period";
		}
		elseif ($newpos == 2) {
		    $TText = "$Twitter è medaglia d'argento dei $Hashtag $Period";
		}
		elseif ($newpos == 3) {
		    $TText = "$Twitter è sul gradino più basso del podio dei $Hashtag $Period";
		}
		elseif ($newpos <= 10) {
		    $TText = "$Twitter è nella top-ten dei $Hashtag $Period";
		}
		else {
		    $TText = "$Twitter è alla $newpos posizione dei $Hashtag $Period";
		}
		// $TButton_def = sprintf($TButton, $TText);
		$TButton_def = sprintf($TButtonUrl, urlencode($TText), urlencode($TwUrl));

		$img = "arrows/{$r['trend']}.png";
		if ($r['trend'] == "new") {
		    $img = "arrows/up.png";
		}

		$firsttime = "";

		$link = Link::Politician($r['label'], $this->linkPage);

		ob_start();

		if (isset($r['picture'])) { // Attivista
			$description = $r['description'];
			$picture = $r['picture'];
			$info = "Followers: <strong>{$r['followers']}</strong> | Following: <strong>{$r['following']}</strong> | Tweets: <strong>{$r['tweets']}</strong>";

			echo "<tr id='pos{$pos}'>";
			echo "<td class='statistics-pos'>$newpos<br />$TButton_def</td>";
			echo "<td class='statistics-img'><img style='background-image: url({$picture});' class='img-attivisti-small' /></td>";
			echo "<td class='statistics-politician'>$firsttime<a target='_blank' href=\"https://www.twitter.com/".urlencode($r['username'])."/\">{$r['name']}</a>
			    <div class='statistics-info'>
			    <small>$info</small>
			    <br />
			    <small>{$description}</small>
			    </div>
			    </td>";
			echo "<td class='statistics-num'>{$r['num']} <img src='/img/$img' class='img-arrow' /></td>";
			echo "</tr>";
		}
		else { // Politico

			$user = "{$r['name']} {$r['surname']}";
			if ($r['twitter']) {
				$user = "@{$r['twitter']}";
			}
			$image = $r['image'];
			if ($image == "" && $this->default_image != "") {
        		$image = $this->default_image;
        	}	

			$TwUrl = Link::Politician($r['label']);
			$TText = "$user vorrei seguirti, quando attivi il tuo profilo? $TwUrl";

			echo "<tr id='pos{$pos}'>";
			echo "<td class='statistics-pos'>$newpos<br />$TButton_def</td>";
			if (!isset($r['active']) || $r['active']) {
				echo "<td class='statistics-img'><img style='background-image: url($image);' class='img-politician-small' /></td>";
				echo "<td class='statistics-politician'>$firsttime<a href=\"$link\">{$r['name']} {$r['surname']}</a><br />
				    <!--<small>".Politician::getGoodDescription($r['goodname'], $r['description'])."</small>--></td>";
			}
			else {
				echo "<td class='grey-profile statistics-img'><img style='background-image: url($image);' class='img-politician-small' /></td>";
				echo "<td class='grey-profile statistics-politician'>
					<p>$firsttime<a href=\"$link\">{$r['name']} {$r['surname']}</a></p>
				    <p class='politician_reg_info'>Vuoi seguire {$r['name']} {$r['surname']}?
				    <a href='https://twitter.com/intent/tweet?via=poke_dem&amp;related=poke_dem&amp;text=".urlencode($TText)."'>
				    	<i class='fa fa-twitter'></i> Twittaglielo!</a><br />
				    Sei {$r['name']} {$r['surname']} e vuoi farti conoscere meglio? <a href='/blog/?p=89'>Clicca qui!</a>
				    </p>
				    </td>";
				// echo "<td class='grey-profile statistics-politician'>{$r['name']} {$r['surname']}<br />
				//     <small>".Politician::getGoodDescription($r['goodname'], $r['description'])."</small></td>";
			}
			echo "<td class='statistics-num'>{$r['num']} <img src='/img/$img' class='img-arrow' /></td>";
			echo "</tr>";
		}

		return ob_get_clean();
	}

}

class PiuCitati extends Chart {

	private $queryBefore = 'SELECT a.politician label, COUNT(DISTINCT a.title) num
	    FROM articles a
	    LEFT JOIN politicians p ON p.id = a.politician
	    WHERE a.`when` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND a.`when` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	    GROUP BY a.politician
	    ORDER BY num DESC';

    private $query = 'SELECT a.politician label, COUNT(DISTINCT a.title) num, p.name, p.surname, i.image, pt.name party,
            soc.twitter, p.max_citati, pt.goodname, r.description, p.active, pt.id partyid
        FROM articles a
        LEFT JOIN politicians p ON p.id = a.politician
        LEFT JOIN politician_info i ON i.politician = a.politician
        LEFT JOIN social_info soc ON soc.politician = a.politician
        LEFT JOIN roles r ON r.id = p.role
        LEFT JOIN parties pt ON p.party = pt.id
        WHERE a.`when` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
            AND a.`when` < FROM_UNIXTIME(%1$d)
            AND %3$s
        GROUP BY a.politician
        ORDER BY num DESC';

	protected function getQueryBefore() {
		return $this->queryBefore;
	}
	protected function getQuery() {
		return $this->query;
	}

	function __construct($type = "piucitati") {
		parent::__construct($type);
		$this->linkPage = "news";
		$this->validTypes = array(2, 6, 24, 168, 720, 2160);
	}

}

class PiuSocial extends Chart {

	private $queryBefore = 'SELECT s.politician label, COUNT(*) num
	    FROM `social_updates_new` s
	    LEFT JOIN politicians p ON p.id = s.politician
	    WHERE s.`created_time` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND s.`created_time` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	    GROUP BY s.politician
	    ORDER BY num DESC';
    private $query = 'SELECT s.politician label, COUNT(*) num, p.name, p.surname, i.image, pt.name party,
	        soc.twitter, p.max_social, pt.goodname, r.description, p.active, pt.id partyid
	    FROM `social_updates_new` s
	    LEFT JOIN politicians p ON p.id = s.politician
	    LEFT JOIN politician_info i ON i.politician = s.politician
	    LEFT JOIN social_info soc ON soc.politician = s.politician
	    LEFT JOIN roles r ON r.id = p.role
	    LEFT JOIN parties pt ON p.party = pt.id
	    WHERE s.`created_time` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND s.`created_time` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	    GROUP BY s.politician
	    ORDER BY num DESC';

	protected function getQueryBefore() {
		return $this->queryBefore;
	}
	protected function getQuery() {
		return $this->query;
	}

	function __construct($type = "piusocial") {
		parent::__construct($type);
		$this->linkPage = "social";
		$this->validTypes = array(2, 6, 24, 168, 720, 2160);
	}

}

class PiuTwittati extends Chart {

	private $queryBefore = 'SELECT t.politician label, COUNT(*) num
	    FROM twitter_politicians t
	    LEFT JOIN politicians p ON p.id = t.politician
	    WHERE t.`when_tweet` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND t.`when_tweet` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	    GROUP BY t.politician
	    ORDER BY num DESC';
    private $query = 'SELECT t.politician label, COUNT(*) num, p.name, p.surname, i.image, pt.name party,
	        soc.twitter, p.max_social, pt.goodname, r.description, p.active, pt.id partyid
	    FROM twitter_politicians t
	    LEFT JOIN politicians p ON p.id = t.politician
	    LEFT JOIN politician_info i ON i.politician = t.politician
	    LEFT JOIN social_info soc ON soc.politician = t.politician
	    LEFT JOIN roles r ON r.id = p.role
	    LEFT JOIN parties pt ON p.party = pt.id
	    WHERE t.`when_tweet` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND t.`when_tweet` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	    GROUP BY t.politician
	    ORDER BY num DESC';

	protected function getQueryBefore() {
		return $this->queryBefore;
	}
	protected function getQuery() {
		return $this->query;
	}

	function __construct($type = "piutwittati") {
		parent::__construct($type);
		$this->linkPage = "tweets";
		$this->validTypes = array(2, 6, 24, 168, 720, 2160);
	}
}

class PiuAttivi extends Chart {

	private $queryBefore = 'SELECT us.id label, COUNT(*) num
	    FROM twitter_politicians t
	    LEFT JOIN politicians p ON p.id = t.politician
	    LEFT JOIN twitter_updates u ON u.id = t.tweet
		LEFT JOIN twitter_users us ON us.id = u.user_id
	    WHERE t.`when_tweet` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND t.`when_tweet` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	        AND us.id != ALL(SELECT id FROM twitter_blacklisted WHERE active = \'1\')
	        AND LOWER(us.username) != ALL(SELECT LOWER(twitter) FROM social_info)
		GROUP BY u.user_id
		ORDER BY num DESC
		LIMIT 5000';
	private $query = 'SELECT us.id label, us.username, us.name, us.description, us.followers, us.following, us.tweets, us.picture, COUNT(*) num
	    FROM twitter_politicians t
	    LEFT JOIN politicians p ON p.id = t.politician
	    LEFT JOIN twitter_updates u ON u.id = t.tweet
		LEFT JOIN twitter_users us ON us.id = u.user_id
	    WHERE t.`when_tweet` > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
	        AND t.`when_tweet` < FROM_UNIXTIME(%1$d)
	        AND %3$s
	        AND us.id != ALL(SELECT id FROM twitter_blacklisted WHERE active = \'1\')
	        AND LOWER(us.username) != ALL(SELECT LOWER(twitter) FROM social_info)
		GROUP BY u.user_id
		ORDER BY num DESC
		LIMIT 1000';

	protected function getQueryBefore() {
		return $this->queryBefore;
	}
	protected function getQuery() {
		return $this->query;
	}

	function __construct($type = "piuattivi") {
		parent::__construct($type);
		$this->linkPage = "";
		$this->validTypes = array(2, 6, 24, 168);
	}
}

class PiuCitatiEU extends PiuCitati {
	function __construct($type = "piucitatiEU") {
		parent::__construct($type);
		$this->addWhere("(p.role = '82' OR p.id = ANY(SELECT DISTINCT id2 FROM `tmp_new_europarl` WHERE id2 != 0))");
		$this->validTypes = array(2, 6, 24, 168);
	}
}

class PiuSocialEU extends PiuSocial {
	function __construct($type = "piusocialEU") {
		parent::__construct($type);
		$this->addWhere("(p.role = '82' OR p.id = ANY(SELECT DISTINCT id2 FROM `tmp_new_europarl` WHERE id2 != 0))");
		$this->validTypes = array(2, 6, 24, 168);
	}
}

class PiuTwittatiEU extends PiuTwittati {
	function __construct($type = "piutwittatiEU") {
		parent::__construct($type);
		$this->addWhere("(p.role = '82' OR p.id = ANY(SELECT DISTINCT id2 FROM `tmp_new_europarl` WHERE id2 != 0))");
		$this->validTypes = array(2, 6, 24, 168);
	}
}

class PiuCommentati extends Chart {

	private $queryBefore = 'SELECT c.politician label, COUNT(*) num
		FROM `fb_comments` c
		LEFT JOIN politicians p ON p.id = c.politician
		WHERE c.created_time > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
		    AND c.created_time < FROM_UNIXTIME(%1$d)
		    AND %3$s
		GROUP BY c.politician
		ORDER BY num DESC';
	private $query = 'SELECT c.politician label, COUNT(*) num, p.name, p.surname, i.image, pt.name party,
	        soc.twitter, p.max_social, pt.goodname, r.description, p.active, pt.id partyid
		FROM `fb_comments` c
		LEFT JOIN politicians p ON p.id = c.politician
		LEFT JOIN politician_info i ON i.politician = c.politician
		LEFT JOIN social_info soc ON soc.politician = c.politician
		LEFT JOIN roles r ON r.id = p.role
		LEFT JOIN parties pt ON p.party = pt.id
		WHERE c.created_time > DATE_SUB(FROM_UNIXTIME(%1$d), INTERVAL %2$d HOUR)
		    AND c.created_time < FROM_UNIXTIME(%1$d)
		    AND %3$s
		GROUP BY c.politician
		ORDER BY num DESC';

	protected function getQueryBefore() {
		return $this->queryBefore;
	}
	protected function getQuery() {
		return $this->query;
	}

	function __construct($type = "piucommentati") {
		parent::__construct($type);
		$this->linkPage = "social";
		$this->validTypes = array(2, 6, 24, 168);
	}
}

function getChart($type) {

	$Chart = null;

	switch ($type) {
		case 'piucitatiEU':
			$Chart = new PiuCitatiEU();
			break;
		
		case 'piusocialEU':
			$Chart = new PiuSocialEU();
			break;
			
		case 'piutwittatiEU':
			$Chart = new PiuTwittatiEU();
			break;
			
		case 'piucitati':
			$Chart = new PiuCitati();
			break;
		
		case 'piucommentati':
		    $Chart = new PiuCommentati();
			break;
		
		case 'piusocial':
			$Chart = new PiuSocial();
			break;
			
		case 'piutwittati':
			$Chart = new PiuTwittati();
			break;
			
		case 'piuattivi':
			$Chart = new PiuAttivi();
			break;
	}

	return $Chart;
}


// SELECT c.politician, COUNT(*) num
// FROM `fb_comments` c
// LEFT JOIN politicians p ON p.id = c.politician
// WHERE ADDDATE(c.created_time, INTERVAL 2 HOUR) > NOW()
// AND p.active = '1'
// GROUP BY c.politician
// ORDER BY num DESC
