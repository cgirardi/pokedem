<?php
define ("MAXNEWS", 20);
class Politician {

    private $DB;
    private $QP;
    private $BM;
    
    private $ID;
    private $R = array();
    private $addInfo = array();
    private $socialInfo = array();
    protected $default_image = "";

    public function setDefaultImage($image) {
        $this->default_image = $image;
    }

    public static function getGoodDescription($party, $desc) {
        $arr = array();
        if ($party) {
            $arr[] = $party;
        }
        if ($desc) {
            $arr[] = $desc;
        }
        return implode(" - ", $arr);
    }

    public function isActive() {
        return $this->R['active'];
    }
    
    public function isInCharts() {
        return $this->R['in_charts'];
    }
    
    public static function getTabs($SS, $ID, $detail = true) {
        ob_start();
        
        if ($detail) {
            $func = "Politician";
        }
        else {
            $func = "PoliticianInDashboard";
        }
        
        echo "<ul class='nav nav-tabs'>\n";

        // echo "<li".(!$SS ? " class='active'" : "")."><a href='".Link::$func($ID)."'>Riepilogo</a></li>";
        echo "<li".($SS == "news" ? " class='active'" : "")."><a href='".Link::$func($ID, "news")."'>Notizie</a></li>";
        echo "<li".($SS == "social" ? " class='active'" : "")."><a href='".Link::$func($ID, "social")."'>Post social</a></li>";
        echo "<li".($SS == "considerati" || $SS == "tweets" ? " class='active'" : "")."><a href='".Link::$func($ID, "tweets")."'>Tweet ricevuti</a></li>";
        echo "<li".($SS == "comments" ? " class='active'" : "")."><a href='".Link::$func($ID, "comments")."'>Commenti Facebook</a></li>";
        
        if ($UsersLoginAccess == 1) {
        	if (isProEnabled($ID)) {
            echo "<li".($SS == "pro" ? " class='active'" : "")."><a onclick=\"_gaq.push(['_trackEvent', 'Pro', 'AdvancedStats', '$ID']);\" href='".Link::$func($ID, "pro")."'>Analisi Twitter</a></li>";
            echo "<li".($SS == "profb" ? " class='active'" : "")."><a onclick=\"_gaq.push(['_trackEvent', 'Pro', 'AdvancedStatsFB', '$ID']);\" href='".Link::$func($ID, "profb")."'>Analisi Facebook</a></li>";
        }
        else {
            $msg = "Scopri gli influencer a favore o contro, sottoscrivi l'account Pro!";
            $msg = htmlentities($msg, ENT_QUOTES);
            echo "<li style='height: 20px;' class='disabled'><a title='$msg' onclick=\"_gaq.push(['_trackEvent', 'FakePro', 'AdvancedStats', '$ID']);\" href='".Link::Features()."'>Analisi Twitter <span class='label' style='font-size: .8em;'>PRO</span></a></li>";
            echo "<li style='height: 20px;' class='disabled'><a title='$msg' onclick=\"_gaq.push(['_trackEvent', 'FakePro', 'AdvancedStatsFB', '$ID']);\" href='".Link::Features()."'>Analisi Facebook <span class='label' style='font-size: .8em;'>PRO</span></a></li>";
        }
	}
        echo "</ul>\n";
        
        return ob_get_clean();
    }
    
    public function followLink() {
        
        $id = $this->ID;
        
        global $FollowedPoliticians, $MaxNumOfPoliticians;
        
        // Login non eseguito
        if ($UsersLoginAccess == 1) {
        if (!$_SESSION['Login']) {
            return "<a class='btn btn-success follow disabled' href='#' onclick='return false;'>Accedi o registrati per seguire</a>";
        }

        // Già seguito
        if (isset($FollowedPoliticians[$id])) {
            $c_real = $FollowedPoliticians[$id]['real'] ? " icon-ok" : "";
            $c_day = $FollowedPoliticians[$id]['day'] ? " icon-ok" : "";
            $c_week = $FollowedPoliticians[$id]['week'] ? " icon-ok" : "";
            $a_real = $FollowedPoliticians[$id]['real'] ? " class='checked'" : "";
            $a_day = $FollowedPoliticians[$id]['day'] ? " class='checked'" : "";
            $a_week = $FollowedPoliticians[$id]['week'] ? " class='checked'" : "";
            
            $disabled = isProEnabled() ? false : true;
            $liReal = "";
            if ($disabled) {
                $liReal = "<li class='disabled'><a href='#' onclick='return false;'>Real-time <span class='pull-right label' style='font-size: .8em;'>PRO</span></a></li>";
            }
            else {
                $liReal = "<li><a{$a_real} href='#' onclick='return changeFollowedState(this, 1, $id, event);'>Real-time <span class='pull-right{$c_real}'></span></a></li>";
            }
            
            return sprintf("
            <div class='btn-group buttons-segui'>
                <a class='btn btn-info dropdown-toggle' href='#' data-toggle='dropdown' onclick='return false;'>
                    <nobr><i class='icon-ok icon-white'></i> Seguito <span class='caret'></span></nobr>
                </a>
                <ul class='dropdown-menu'>
                    <li class='nav-header'>Ricevi notifiche</li>
                    $liReal
                    <li><a{$a_day} href='#' onclick='return changeFollowedState(this, 2, $id, event);'>Giornaliera <span class='pull-right{$c_day}'></span></a></li>
                    <li><a{$a_week} href='#' onclick='return changeFollowedState(this, 3, $id, event);'>Settimanale <span class='pull-right{$c_week}'></span></a></li>
                    <li class='divider'></li>
                    <li><a href='#' onclick='return changeState(this, $id);'>Non seguire più</a></li>
                </ul>
            </div>
            ", $id);
        }
        
        // Non seguito, ma posti non disponibili
        if ($NumOfPoliticians >= $MaxNumOfPoliticians && !$_SESSION['LoginRow']['debug']) {
            return "<a class='btn btn-success follow disabled' href='#' onclick='return false;'>Segui già $MaxNumOfPoliticians rappresentanti</a>";
        }
        
        // Non seguito, con posti disponibili
        return sprintf("<div class='btn-group buttons-segui'><a class='btn btn-success' href='#' onclick='return changeState(this, %d); return false;'>Segui</a></div>", $id);
        }
        
    }

    public function __construct($ID, $DB = null, $QP = null, $BM = null) {
        
        if (!$DB) {
            global $DB;
        }
        $this->DB = $DB;
        
        if (!$QP) {
            global $QP;
        }
        $this->QP = $QP;
        
        if (!$BM) {
            global $BM;
        }
        $this->BM = $BM;
        
        $ID = addslashes($ID);
        $this->ID = $ID;
        
        $query = "SELECT p.*, r.description roledesc, p2.goodname, i.image
            FROM politicians p
            LEFT JOIN politician_info i ON i.politician = p.id
            LEFT JOIN parties p2 ON p2.id = p.party
            LEFT JOIN roles r ON r.id = p.role
            WHERE p.id = '{$this->ID}'";
        $res = $this->QP->get($query, 300);
        if ($res->num()) {
            $this->R = $res->next();
        }
        else {
            throw new Exception("Politician not found ({$this->ID})");
        }
    }
    
    public function getID() {
        return $this->ID;
    }
    
    public function getSexSuffix() {
        if ($this->R['sex'] == 'M' || $this->R['sex'] == 'm') {
            return "o";
        }
        return "a";
    }
    
    public function getMetaBio() {
        $pol = $this->getName();
        
        $Meta = array();
        $Meta['title'] = $pol." - Biografia e contatti";
        $Meta['description'] = "Scheda riassuntiva di $pol con biografia, contatti email e breve wiki.";
        $Meta['keywords'] = $pol.", biografia, contatti, email, scheda, informazioni";
        
        return $Meta;
    }
    
    public function getMetaTweet() {
        $pol = $this->getName();
        
        $Meta = array();
        $Meta['title'] = $pol." - Tweet";
        $Meta['description'] = "Tweet ricevuti da $pol, aggiornati in tempo reale";
        $Meta['keywords'] = $pol.", twitter, social network, social, post, interventi";
        
        return $Meta;
    }
    
    public function getMetaSocial() {
        $pol = $this->getName();
        
        $Meta = array();
        $Meta['title'] = $pol." - Ultimi post Facebook e Twitter";
        $Meta['description'] = "Interventi sui Social Network di $pol: post Facebook e Twitter, tutti aggiornati in tempo reale";
        $Meta['keywords'] = $pol.", facebook, twitter, social network, social, post, interventi";
        
        return $Meta;
    }
    
    public function getMetaComments() {
        $pol = $this->getName();
        
        $Meta = array();
        $Meta['title'] = "$pol - Ultimi commenti ricevuti su Facebook";
        $Meta['description'] = "Ultimi commenti Facebook ricevuti da $pol, aggiornati in tempo reale";
        $Meta['keywords'] = $pol.", facebook, social network, social, post, interventi, commenti";
        
        return $Meta;
    }
    
    public function getMetaNews() {
        $pol = $this->getName();
        
        $Meta = array();
        $Meta['title'] = $pol." - Ultime notizie e news";
        $Meta['description'] = "Rassegna stampa su $pol: news e notizie in tempo reale da centinaia di fonti";
        $Meta['keywords'] = $pol.", ultime notizie, notizie, news, rassegna stampa, articoli, ultimissime";
        
        return $Meta;
    }
    
    public function getMetaAll() {
        $pol = $this->getName();
        
        $Meta = array();
        $Meta['title'] = $pol . " - Ultime notizie e post social";
        $Meta['description'] = $pol . " - Rassegna stampa su $pol: news in tempo reale da centinaia di fonti e interventi sui social network";
        $Meta['keywords'] = $pol.", ultime notizie, notizie, news, rassegna stampa, articoli, ultimissime, facebook, twitter, social network, social, post, interventi";
        
        return $Meta;
    }

    public function getPositions($charts = array("piucitati", "piusocial", "piutwittati", "piucommentati", "piucitatiEU", "piusocialEU", "piutwittatiEU")) {
        $pos = array();

        foreach ($charts as $chart) {
            $C = getChart($chart);
            if ($C != null) {
                $rows = $C->get();
                foreach ($rows as $row) {
                    if ($row['label'] == $this->ID) {
                        $pos[$chart] = $row['pos'];
                    }
                }
            }
        }

        return $pos;
    }
    
    public function getPageHeader($black = false) {
        
        $info = array();
        $chartPos = array();

        $addInfo = $this->addPolInfo();
        $socialInfo = $this->addSocialInfo();
        
        $new = "";
        $ID = $this->ID;
        if ($this->R['role'] > 2) {
            $new = "_gaq.push(['_trackEvent', 'Politician_noParl', '$ID', document.location.href]);";
        }
        else {
            $new = "_gaq.push(['_trackEvent', 'Politician_Parl', '$ID', document.location.href]);";
        }
        echo "<script type='text/javascript' charset='utf-8'>
        $(document).ready(function() {
            $new
        });
        </script>";
        
        if ($addInfo['email']) {
            $info[] = "<a title='Indirizzo e-mail' class='sb gradient email' href='mailto:{$addInfo['email']}'>E-mail</a>";
        }
        if ($addInfo['url']) {
            $info[] = "<a title='Sito istituzionale' class='sb gradient institution' target='_blank' href='{$addInfo['url']}'>Sito web istituzionale</a>";
        }
        if ($socialInfo['fb_public']) {
            $info[] = "<a title='Pagina Facebook' class='sb gradient facebook' target='_blank' href='https://www.facebook.com/{$socialInfo['fb_public']}'>Pagina Facebook</a>";
        }
        if ($socialInfo['twitter']) {
            $info[] = "<a title='Profilo Twitter' class='sb gradient twitter' target='_blank' href='https://twitter.com/{$socialInfo['twitter']}'>Profilo Twitter</a>";
        }
        if ($socialInfo['blog']) {
            $info[] = "<a title='Sito web personale' class='sb gradient home' target='_blank' href='{$socialInfo['blog']}'>Sito web personale</a>";
        }
        if ($this->R['idwiki']) {
            $info[] = "<a title='Pagina Wikipedia' class='sb gradient wikipedia' target='_blank' href='{$this->R['idwiki']}'>Pagina Wikipedia</a>";
        }
        
        $Intestazione = $this->getName()." ".($this->R['active'] ? $this->followLink($this->ID) : "");

        $pos = $this->getPositions();
        foreach ($pos as $key => $value) {
            $len = strlen($value);
            // $Intestazione .= " <a title='Classifica #$key' class='sb gradient sb-chart sb-chart-size-$len'>$value</a>";
            $chartPos[] = "<a href='/$key' title='Classifica #$key' class='sb gradient sb-chart sb-chart-size-$len'>$value</a>";
        }

        $Buttons_info = "";
        if (count($info)) {
            $Buttons_info = "Info: ".implode(" ", $info);
        }
        $Buttons_charts = "";
        if (count($chartPos)) {
            $Buttons_charts = "Classifiche: ".implode(" ", $chartPos);
        }

        $Buttons = "<p class='pol-buttons'>$Buttons_charts $Buttons_info</p>";
        $Image = $this->R['image'];
        if ($Image == "" && $this->default_image != "") {
            $Image = $this->default_image;
        }

        $desc = Politician::getGoodDescription($this->R['goodname'], $this->R['roledesc']);

        $days = $this->getDays();
        if ($black) {
            $Intestazione = "<div class='pol-title'><span style='margin-left: 6px; background-image: url(\"$Image\");' class='img-politician-small'></span>
                <h2>$Intestazione</h2>
                <!--<p class='description'>$desc - non ancora su PokeDem</p>-->
                $Buttons
                </div>";
        }
        else {
            $Intestazione = "<div class='pol-title'><span style='margin-left: 6px; background-image: url(\"$Image\");' class='img-politician-small'></span>
                <h2>$Intestazione</h2>
                <!--<p class='description'>$desc - su PokeDem da $days giorni</p>-->
                $Buttons
                </div>";
        }
        return $Intestazione;
    }
    
    public function GetPageBio() {
        
        $addInfo = $this->addPolInfo();
        $socialInfo = $this->addSocialInfo();
        
        ob_start();
        
        if (count($addInfo)) {
            if ($addInfo['image'] == "") {
                echo "<img src='".$this->default_image."' class='politician-card-img img-polaroid' />";
            } else {
               echo "<img src='". $addInfo['image'] ."' class='politician-card-img img-polaroid' />";
            }
            ?>

	        <div class='politician-card'>
                <p>
                    <?php echo $addInfo['bio']; ?><br />
                    <?php echo $addInfo['data']; ?>
                </p>
                <p>
                    <?php
                    
                    $info = array();
                    
                    if ($addInfo['email']) {
                        $info[] = "Email: <a href='mailto:{$addInfo['email']}'>{$addInfo['email']}</a>";
                    }
                    if ($addInfo['url']) {
                        $info[] = "<a target='_blank' href='{$addInfo['url']}'>Sito web istituzionale</a>";
                    }
                    if ($socialInfo['fb_public']) {
                        $info[] = "<a target='_blank' href='https://www.facebook.com/{$socialInfo['fb_public']}'>Pagina Facebook</a>";
                    }
                    if ($socialInfo['twitter']) {
                        $info[] = "<a target='_blank' href='https://twitter.com/{$socialInfo['twitter']}'>Profilo Twitter</a>";
                    }
                    if ($socialInfo['blog']) {
                        $info[] = "<a target='_blank' href='{$socialInfo['blog']}'>Sito web personale</a>";
                    }
                    if ($this->R['idwiki']) {
                        $info[] = "<a target='_blank' href='{$this->R['idwiki']}'>Pagina Wikipedia</a>";
                    }
                    
                    echo implode(" | ", $info);
                    ?>
                </p>
            </div>
            <?php
        }
        
        return ob_get_clean();
    }
    
    public function getPageTweet($NumNews = MAXNEWS, $Page = 1) {
        $pol = $this->getName();
        $ID = $this->ID;
        
        ob_start();
        
        // Sidebar
        echo "<div id='sidebar' class='sticky'>";
        echo $this->BM->legenda();
        echo $this->BM->politicianWidget("hashToBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartTweetsBox", array("id" => $ID, "pol" => $pol));
        echo "</div>";

        // Main part
        // $json = array(
        //     "pol" => $pol,
        //     "sub" => "tweetFlow",
        //     "title" => "Tweet ricevuti",
        //     "id" => $ID
        // );
        // $json = json_encode($json);
        echo "<div id='mainbar'>";
        echo "<p class='pol-subtitle'>Tutti i tweet ricevuti da $pol, aggiornati in tempo reale.</p>";
        $link = Link::CustomizeWidget();
        $link .= "#type=tweetFlow&id=$ID";
        //CG: echo "<p class='pol-subtitle-small'>Vuoi inserire i tweet ricevuti da $pol sul tuo sito?
        //    Clicca su <a href='$link' class='embed-tag-inline'>embed</a></p>";
        echo $this->BM->tweetFlowPostponed(array("id" => $ID, "limit" => $NumNews, "page" => $Page-1));
        //if ($NumNews == MAXNEWS) {
            //CG: echo "<p>Questa versione è limitata a " . MAXNEWS ." tweet. Ne vuoi leggere di più? Registrati gratuitamente cliccando <a href='#' onclick='return triggerLogin(event);'>qui</a>.</p>";
        //}
        echo $this->BM->getPaging($ID, "tweets", $Page, $NumNews);

        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function getPageComments($NumNews = MAXNEWS, $Page = 1) {
        
        $pol = $this->getName();
        $ID = $this->ID;
        
        ob_start();
        
        // Sidebar
        echo "<div id='sidebar' class='sticky'>";
        // echo $this->BM->legenda();
        // echo $this->BM->politicianWidget("hashToBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartTweetsBox", array("id" => $ID, "pol" => $pol));
        echo "</div>";

        echo "<div id='mainbar'>";
        echo "<p class='pol-subtitle'>Tutti i commenti ricevuti da $pol sui suoi post Facebook, aggiornati in tempo reale.</p>";
        // $link = Link::CustomizeWidget();
        // $link .= "#type=tweetFlow&id=$ID";
        // echo "<p class='pol-subtitle-small'>Vuoi inserire i tweet ricevuti da $pol sul tuo sito?
        //     Clicca su <a href='$link' class='embed-tag-inline'>embed</a></p>";
        echo $this->BM->commentFlowPostponed(array("id" => $ID, "limit" => $NumNews, "page" => $Page-1));
        //if ($NumNews == MAXNEWS) {
        //	echo "<p>Questa versione è limitata a " . MAXNEWS ." commenti. Ne vuoi leggere di più? Registrati gratuitamente cliccando <a href='#' onclick='return triggerLogin(event);'>qui</a>.</p>";
        //}
        echo $this->BM->getPaging($ID, "comments", $Page, $NumNews);

        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function getPageSocial($NumNews = MAXNEWS, $Page = 1) {
        
        $pol = $this->getName();
        $ID = $this->ID;
        
        ob_start();
        
        // Sidebar
        echo "<div id='sidebar' class='sticky'>";
        echo $this->BM->legenda();
        //echo $this->BM->politicianWidget("hashFromBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartSocialBox", array("id" => $ID, "pol" => $pol));
        echo "</div>";

        // Main part
        // $json = array(
        //     "pol" => $pol,
        //     "sub" => "socialFlow",
        //     "title" => "Interventi Facebook e Twitter",
        //     "id" => $ID
        // );
        // $json = json_encode($json);
        echo "<div id='mainbar'>";
        echo "<p class='pol-subtitle'>Interventi di $pol su Facebook e Twitter aggiornati in tempo reale.</p>";
        $link = Link::CustomizeWidget();
        $link .= "#type=tweetFlow&id=$ID";
        //CG: echo "<p class='pol-subtitle-small'>Vuoi inserire gli interventi social di $pol sul tuo sito?
        //    Clicca su <a href='$link' class='embed-tag-inline'>embed</a></p>";
        
        if (isProEnabled($ID)) {
            ?>
            
            <div class='well'>
                <form class="form-inline form-pro" role="form">
                    <input type='hidden' name='query-id' value='<?php echo $ID; ?>' />
                <p><strong>Filtra risultati</strong></p>
            
                <div class="form-group row-fluid">
                    
                    <div class='span2'>
                        Periodo:
                    </div>
                    
                    <div class='span5'>
                        <label for="input-when-from" class="col-sm-3 control-label">Da:</label>
                        <div class="input-append date form_datetime" data-link-field='input-field-from'>
                            <input type="text" value="" readonly id='input-when-from'>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                        <input type="hidden" name="query-when-from" id="input-field-from" value="">
                    </div>

                    <div class='span5'>
                        <label for="input-when-to" class="col-sm-3 control-label">a:</label>
                        <div class="input-append date form_datetime" data-link-field='input-field-to'>
                            <input type="text" value="" readonly id='input-when-to'>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                        <input type="hidden" name="query-when-to" id="input-field-to" value="">
                    </div>
                </div>
            
                <div class="form-group row-fluid">
                    
                    <div class='span2'>
                        Tipologia:
                    </div>
                    <div class='span10'>
                        <label class="checkbox checkbox-pro">
                            <input name='query-facebook' type="checkbox"> Facebook
                        </label>
                        <label class="checkbox checkbox-pro">
                            <input name='query-twitter' type="checkbox"> Twitter
                        </label>
                        
                        <button class='btn-selectall btn btn-success btn-small' type='button'><i class="icon-ok-circle icon-white"></i></button>
                        <button class='btn-selectnone btn btn-danger btn-small' type='button'><i class="icon-ban-circle icon-white"></i></button>
                    </div>
                </div>
            
                <div class="form-group row-fluid">
                    <div class='span2'>
                        Raggruppa:
                    </div>
                    <div class='span4'>
                        <select name='query-group' onchange='return hideOnValue(this, "sort-filter");'>
                            <option value=''>[Non raggruppare]</option>
                            <option value='hashtag'>Per hashtag</option>
                            <option value='type'>Per social network</option>
                            <option value='day'>Per giorno</option>
                            <option value='hour'>Per fascia oraria</option>
                        </select>
                    </div>
                    <div id='sort-filter'>
                        <div class='span2' style='text-align: right;'>
                            Ordina:
                        </div>
                        <div class='span3'>
                            <select style='width: 100%;' name='query-orderby'>
                                <option value='created_time' selected='selected'>Data</option>
                                <option value='shares'>Numero di share/retweet</option>
                            </select>
                        </div>
                        <div class='span1'>
                            <select style='width: 100%;' name='query-ascdesc'>
                                <option value='asc'>&#8593;</option>
                                <option value='desc' selected='selected'>&#8595;</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group row-fluid">
                    <div class="span4 offset8 text-right">
                        <button class="btn btn-primary btn-submit-form" type="button" data-type='show' data-page='social'>Applica filtro</button>
                        <button class="btn btn-warning btn-submit-form" type="button" data-type='export' data-page='social'><i class="icon-white icon-list-alt"></i> Esporta</button>
                    </div>
                </div>
                
                </form>
            </div>
            
            <?php
        }
        
        echo $this->BM->socialFlowPostponed(array("id" => $ID, "limit" => $NumNews, "page" => $Page-1));
        //if ($NumNews == MAXNEWS) {
            //echo "<p>Questa versione è limitata a ". MAXNEWS . " post. Ne vuoi leggere di più? Registrati gratuitamente cliccando <a href='#' onclick='return triggerLogin(event);'>qui</a>.</p>";
        //}
        echo $this->BM->getPaging($ID, "social", $Page, $NumNews);

        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function getPageNews($NumNews = MAXNEWS, $NumFonti = 0, $Page = 1) {
        
        $pol = $this->getName();
        $ID = $this->ID;
        
        ob_start();
        
        // Sidebar
        echo "<div id='sidebar' class='sticky'>";
        echo $this->BM->legenda();
        // echo $this->BM->politicianWidget("temiBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartNewsBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartNewspapersPie", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartConnectionsPie", array("id" => $ID, "pol" => $pol));
        echo "</div>";

        // Main part
        // $json = array(
        //     "pol" => $pol,
        //     "sub" => "newsFlow",
        //     "title" => "Ultime news",
        //     "id" => $ID
        // );
        // $json = json_encode($json);
        $linkList = Link::Fonti();
        echo "<div id='mainbar'>";
        echo "<p class='pol-subtitle'>Rassegna stampa in tempo reale di $pol estratta da <a href='$linkList'>$NumFonti fonti online</a>.</p>";
        $link = Link::CustomizeWidget();
        $link .= "#type=newsFlow&id=$ID";
        //CG: echo "<p class='pol-subtitle-small'>Vuoi inserire le ultime news di $pol sul tuo sito?
        //    Clicca su <a href='$link' class='embed-tag-inline'>embed</a></p>";
        
        if (isProEnabled($ID)) {
            ?>
            
            <div class='well'>
                <form class="form-inline form-pro" role="form">
                    <input type='hidden' name='query-id' value='<?php echo $ID; ?>' />
                <p><strong>Filtra risultati</strong></p>
            
                <div class="form-group row-fluid">
                    
                    <div class='span2'>
                        Periodo:
                    </div>
                    
                    <div class='span5'>
                        <label for="input-when-from" class="col-sm-3 control-label">Da:</label>
                        <div class="input-append date form_datetime" data-link-field='input-field-from'>
                            <input type="text" value="" readonly id='input-when-from'>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                        <input type="hidden" name="query-when-from" id="input-field-from" value="">
                    </div>

                    <div class='span5'>
                        <label for="input-when-to" class="col-sm-3 control-label">a:</label>
                        <div class="input-append date form_datetime" data-link-field='input-field-to'>
                            <input type="text" value="" readonly id='input-when-to'>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                        <input type="hidden" name="query-when-to" id="input-field-to" value="">
                    </div>
                </div>
            
                <div class="form-group row-fluid">
                    
                    <div class='span2'>
                        Tipologia:
                    </div>
                    <div class='span10'>
                        <label class="checkbox checkbox-pro">
                            <input name='query-national-press' type="checkbox"> Stampa naz./agenzia
                        </label>
                        <label class="checkbox checkbox-pro">
                            <input name='query-local-press' type="checkbox"> Stampa locale
                        </label>
                        <label class="checkbox checkbox-pro">
                            <input name='query-blog-press' type="checkbox"> Blog
                        </label>
                        <label class="checkbox checkbox-pro">
                            <input name='query-party-press' type="checkbox"> Organo di partito
                        </label>
                        
                        <button class='btn-selectall btn btn-success btn-small' type='button'><i class="icon-ok-circle icon-white"></i></button>
                        <button class='btn-selectnone btn btn-danger btn-small' type='button'><i class="icon-ban-circle icon-white"></i></button>
                    </div>
                </div>
            
                <div class="form-group row-fluid">
                    <div class='span2'>
                        Raggruppa:
                    </div>
                    <div class='span4'>
                        <select name='query-group' onchange='return hideOnValue(this, "sort-filter");'>
                            <option value=''>[Non raggruppare]</option>
                            <option value='newspaper'>Per testata</option>
                            <option value='type'>Per tipologia</option>
                            <option value='day'>Per giorno</option>
                            <option value='hour'>Per fascia oraria</option>
                        </select>
                    </div>
                    <div id='sort-filter'>
                        <div class='span2' style='text-align: right;'>
                            Ordina:
                        </div>
                        <div class='span3'>
                            <select style='width: 100%;' name='query-orderby'>
                                <option value='when' selected='selected'>Data</option>
                                <option value='title'>Titolo</option>
                                <option value='newspaper'>Testata</option>
                            </select>
                        </div>
                        <div class='span1'>
                            <select style='width: 100%;' name='query-ascdesc'>
                                <option value='asc'>&#8593;</option>
                                <option value='desc' selected='selected'>&#8595;</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group row-fluid">
                    <div class="span4 offset8 text-right">
                        <button class="btn btn-primary btn-submit-form" type="button" data-type='show' data-page='news'>Applica filtro</button>
                        <button class="btn btn-warning btn-submit-form" type="button" data-type='export' data-page='news'><i class="icon-white icon-list-alt"></i> Esporta</button>
                    </div>
                </div>
                
                </form>
            </div>
            
            <?php
        }

        echo $this->BM->newsFlowPostponed(array("id" => $ID, "limit" => $NumNews, "page" => $Page-1));
        //if ($NumNews == MAXNEWS) {
            //echo "<p>Questa versione è limitata a " . MAXNEWS . " news. Ne vuoi leggere di più? Registrati gratuitamente cliccando <a href='#' onclick='return triggerLogin(event);'>qui</a>.</p>";
        //}
        echo $this->BM->getPaging($ID, "news", $Page, $NumNews);
        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function getPageAll($NumNews = MAXNEWS, $NumFonti = 0) {
        
        $pol = $this->getName();
        $ID = $this->ID;
        
        ob_start();
        
        // Sidebar
        echo "<div id='sidebar'>";
        echo $this->BM->legenda();
        //echo $this->BM->politicianWidget("hashFromBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartSocialBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("hashToBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartTweetsBox", array("id" => $ID, "pol" => $pol));
        //echo $this->BM->politicianWidget("temiBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartNewsBox", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartNewspapersPie", array("id" => $ID, "pol" => $pol));
        echo $this->BM->politicianWidget("chartConnectionsPie", array("id" => $ID, "pol" => $pol));
        echo "</div>";

        // Main part
        // $json = array(
        //     "pol" => $pol,
        //     "sub" => "allFlow",
        //     "title" => "Ultime news e interventi sui social network",
        //     "id" => $ID
        // );
        // $json = json_encode($json);
        $linkList = Link::Fonti();
        echo "<div id='mainbar'>";
        echo "<p class='pol-subtitle'>Rassegna stampa estratta da <a href='$linkList'>$NumFonti fonti online</a> e interventi su Twitter e Facebook di $pol.</p>";
        $link = Link::CustomizeWidget();
        $link .= "#type=allFlow&id=$ID";
        //CG: echo "<p class='pol-subtitle-small'>Vuoi inserire la rassegna di $pol sul tuo sito?
        //    Clicca su <a href='$link' class='embed-tag-inline'>embed</a></p>";
        echo $this->BM->allFlowPostponed(array("id" => $ID, "limit" => $NumNews));
        //if ($NumNews == MAXNEWS) {
           // echo "<p>Questa versione è limitata a " . MAXNEWS . " notizie. Ne vuoi leggere di più? Registrati gratuitamente cliccando <a href='#' onclick='return triggerLogin(event);'>qui</a>.</p>";
        //}
        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function getPagePro() {
        $DBid = md5("getPagePro");
        
        $pol = $this->getName();
        $ID = $this->ID;
        
        if (!isProEnabled($ID)) {
            return "Errore!";
        }
        
        ob_start();
        
        // echo "<div id='mainbar'>";
        
        ?>
        
        <div class='well'>
            <form class="form-inline form-pro" role="form">
                <input type='hidden' name='query-id' value='<?php echo $ID; ?>' />
            <p><strong>Filtra risultati</strong></p>
        
            <div class="form-group row-fluid">
                
                <div class='span2'>
                    Periodo:
                </div>
                
                <div class='span4'>
                    <label for="input-when-from" class="col-sm-3 control-label">Da:</label>
                    <div class="input-append date form_datetime" data-link-field='input-field-from'>
                        <input type="text" value="" readonly id='input-when-from'>
                        <span class="add-on"><i class="icon-remove"></i></span>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                    <input type="hidden" name="query-when-from" id="input-field-from" value="">
                </div>

                <div class='span4'>
                    <label for="input-when-to" class="col-sm-3 control-label">a:</label>
                    <div class="input-append date form_datetime" data-link-field='input-field-to'>
                        <input type="text" value="" readonly id='input-when-to'>
                        <span class="add-on"><i class="icon-remove"></i></span>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                    <input type="hidden" name="query-when-to" id="input-field-to" value="">
                </div>

                <div class="span2 text-right">
                    <button class="btn btn-primary btn-submit-form" type="button" data-type='show' data-page='polProStats'>Applica filtro</button>
                </div>

            </div>
        
            </form>
        </div>
        
        <?php
        echo "<div id='contentstats'>";
        echo "</div>";
        
        // echo "</div>";
        
        $this->BM->addPostponedLoader("contentstats", array("sub" => "polProStats", "pol" => $ID));
        
        return ob_get_clean();
        
        // echo $this->BM->politicianWidget("hashFromBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartSocialBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("hashToBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartTweetsBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("temiBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartNewsBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartNewspapersPie", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartConnectionsPie", array("id" => $ID, "pol" => $pol));

    }
    
    public function getPageProFB() {
        $DBid = md5("getPageProFB");
        
        $pol = $this->getName();
        $ID = $this->ID;
        
        if (!isProEnabled($ID)) {
            return "Errore!";
        }
        
        ob_start();
        
        // echo "<div id='mainbar'>";
        
        ?>
        
        <div class='well'>
            <form class="form-inline form-pro" role="form">
                <input type='hidden' name='query-id' value='<?php echo $ID; ?>' />
            <p><strong>Filtra risultati</strong></p>
        
            <div class="form-group row-fluid">
                
                <div class='span2'>
                    Periodo:
                </div>
                
                <div class='span4'>
                    <label for="input-when-from" class="col-sm-3 control-label">Da:</label>
                    <div class="input-append date form_datetime" data-link-field='input-field-from'>
                        <input type="text" value="" readonly id='input-when-from'>
                        <span class="add-on"><i class="icon-remove"></i></span>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                    <input type="hidden" name="query-when-from" id="input-field-from" value="">
                </div>

                <div class='span4'>
                    <label for="input-when-to" class="col-sm-3 control-label">a:</label>
                    <div class="input-append date form_datetime" data-link-field='input-field-to'>
                        <input type="text" value="" readonly id='input-when-to'>
                        <span class="add-on"><i class="icon-remove"></i></span>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                    <input type="hidden" name="query-when-to" id="input-field-to" value="">
                </div>

                <div class="span2 text-right">
                    <button class="btn btn-primary btn-submit-form" type="button" data-type='show' data-page='polProStatsFB'>Applica filtro</button>
                </div>

            </div>
        
            </form>
        </div>
        
        <?php
        echo "<div id='contentstats'>";
        echo "</div>";
        
        // echo "</div>";
        
        $this->BM->addPostponedLoader("contentstats", array("sub" => "polProStatsFB", "pol" => $ID));
        
        return ob_get_clean();
        
        // echo $this->BM->politicianWidget("hashFromBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartSocialBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("hashToBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartTweetsBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("temiBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartNewsBox", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartNewspapersPie", array("id" => $ID, "pol" => $pol));
        // echo $this->BM->politicianWidget("chartConnectionsPie", array("id" => $ID, "pol" => $pol));

    }
    
    public function getName() {
        $ret = array();
        if ($this->R['name']) {
            $ret[] = $this->R['name'];
        }
        if ($this->R['surname']) {
            $ret[] = $this->R['surname'];
        }
        return utf8_encode(implode(" ", $ret));
    }
    
    public function addInfo() {
        $this->addPolInfo();
        $this->addSocialInfo();
    }
    
    public function getDays() {
        $query = "SELECT DATEDIFF(NOW(), GREATEST(MIN(`when`), '2013-03-01')) + 1 days
        FROM articles
        WHERE politician = '{$this->ID}'";
	$res = $this->QP->get($query, 300);
        if ($res->num()) {
            $r = $res->next();
            if ($r['days']) {
                return $r['days'];
            }
        }
        return 2;
    }
    
    public function addPolInfo() {
        
        if (!count($this->addInfo)) {
            $query = "SELECT * FROM politician_info
                WHERE politician = '{$this->ID}'";
            $res = $this->QP->get($query, 300);
            if ($res->num()) {
                $this->addInfo = $res->next();
            }
        }
        
        return $this->addInfo;
    }
    
    public function addSocialInfo() {
        
        if (!count($this->socialInfo)) {
            $query = "SELECT * FROM social_info WHERE politician = '{$this->ID}'";
            $res = $this->QP->get($query, 300);
            if ($res->num()) {
                $this->socialInfo = $res->next();
            }
        }
        
        return $this->socialInfo;
    }
    
}
