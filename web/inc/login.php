<?php

$Fb_app = new Facebook(array(
    'appId'  => $Fb_app_id,
    'secret' => $Fb_app_secret,
));

$Tw_Signin = new TwitterOAuth($Tw_Signin_consumer_key, $Tw_Signin_consumer_secret);
$request_token = $Tw_Signin->getRequestToken("http://$Host/?action=twlogin");

if (isset($request_token['oauth_token'])) {
    $_SESSION['Tw_Signin_oauth_token'] = $token = $request_token['oauth_token'];
    $_SESSION['Tw_Signin_oauth_token_secret'] = $request_token['oauth_token_secret'];

    $twLoginUrl = $Tw_Signin->getAuthorizeURL($token);
}

$fbLoginUrl = $Fb_app->getLoginUrl(
    array(
        "scope" => "email,read_stream",
        "redirect_uri" => "http://{$Host}/?action=fblogin"
    )
);

