<?php

// Need: $Host

class Box {
    private $DB;
    private $QP;
    private $Twitter_name;
    private $Host;
    private $Accepted_intervals;
    
    private $postponed = array();
    
    private $default_flow_pars = array('limit' => 20, 'class' => "detail-left");
    
    function __construct($DB = null, $QP = null, $Twitter_name= null, $Host = null, $Accepted_intervals = array()) {
        if (!$DB) {
            global $DB;
        }
        $this->DB = $DB;
        
        if (!$QP) {
            global $QP;
        }
        $this->QP = $QP;
        
        if (!$Twitter_name) {
            global $Twitter_name;
        }
        $this->Twitter_name = $Twitter_name;
        
        if (!$Host) {
            global $Host;
        }
        $this->Host = $Host;
        
        if (!$Accepted_intervals) {
            global $Accepted_intervals;
        }
        $this->Accepted_intervals = $Accepted_intervals;
        
        if (@$_SESSION['Login']) {
            $this->default_flow_pars['limit'] = 50;
        }
        if (@$_SESSION['Pro']) {
            $this->default_flow_pars['limit'] = 100;
        }
    }
    
    static public function getIntervalInWords($value = 2, $last = false) {
        $label = "$value ore";
        if ($last) {
            $label = "elle ultime $label";
        }
        if ($value > 24) {
            $label = ($value / 24)." giorni";
            if ($last) {
                $label = "egli ultimi $label";
            }
        }
        return $label;
    }
    
    public function politicianWidget($action, $pars) {
        ob_start();
        
        $pars['sub'] = $action;

        echo "<div class='politician-widget' id='postponed-$action'>";
        $this->addPostponedLoader("postponed-$action", $pars);
        echo "</div>";

        return ob_get_clean();
    }
    
    private function getReceivedTweetsPerDay($ID) {
        $query = "SELECT p.tweet
            FROM twitter_politicians p
            -- LEFT JOIN twitter_updates u ON u.id = p.tweet
            WHERE p.politician = '$ID'
            AND p.when_tweet > DATE_SUB(NOW(), INTERVAL 30 DAY)";
        $res = $this->QP->get($query, 24 * 60 * 60);
        return $res->num() / 30;
    }
    
    private function getTweetsPerDay($ID) {
        $query = "SELECT *
            FROM social_updates_new n
            WHERE n.politician = '$ID'
            AND n.created_time > DATE_SUB(NOW(), INTERVAL 30 DAY)";
        $res = $this->QP->get($query, 24 * 60 * 60);
        return $res->num() / 30;
    }

    public function getRecordsCounter($ID, $type) {
        $query = "";
        switch ($type) {
            case "comments":
                $query = "SELECT count(*) as count
                    FROM social_updates_new u
                    INNER JOIN fb_comments c ON c.id_post = u.id
                    LEFT JOIN fb_users us ON us.id = c.user_id
                    WHERE u.politician = '$ID'";
                break;
            case "social":
                $query = "SELECT count(*) as count
                    FROM social_updates_new
                    WHERE politician = '$ID'";
                break;
    	    case "tweets":
                $query = "SELECT count(*) as count
                    FROM twitter_politicians
                    WHERE politician = '$ID'";
                break;
            case "news":
		$query = "SELECT count(*) as count 
		    FROM (SELECT link FROM articles WHERE politician='$ID' GROUP BY title,snippet,newspaper) AS q";
                 break;
            default:
                return 0;
        }

        $res = $this->QP->get($query);
        $r = $res->next();
        return $r['count'];
    }

    public function getPaging ($ID, $type, $page, $max = 20) {
        $nr = $this->getRecordsCounter($ID, $type);
        $paginationDisplay = "<small>Tot. $type: <strong>$nr</strong><br>"; // Initialize the pagination output variable
        if ($nr > 0) {
            $lastPage = ceil($nr / $max);
            // Be sure URL variable $page (page number) is no lower than page 1 and no higher than $lastpage
            if ($page < 1) { // If it is less than 1
                $page = 1; // force if to be 1
            } else if ($page > $lastPage) { // if it is greater than $lastpage
                $page = $lastPage; // force it to be $lastpage's value
            }
            //$paginationDisplay .= "Pg: $page/$lastPage, Rows: " .$nr ."<br>";

            // This creates the numbers to click in between the next and back buttons
            // This section is explained well in the video that accompanies this script
            $centerPages = "";
            $sub1 = $page - 1;
            $sub2 = $page - 2;
            $add1 = $page + 1;
            $add2 = $page + 2;
            if ($page == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $page . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($page == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $page . '</span> &nbsp;';
            } else if ($page > 2 && $page < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $page . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($page > 1 && $page < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $page . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }

            // This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
            if ($lastPage != "1") {
                // This shows the user what page they are on, and the total number of pages
                $paginationDisplay .= '&nbsp;  &nbsp;  &nbsp; Pagina: <strong>' . $page . '</strong> di ' . $lastPage. '</small>&nbsp;  &nbsp;  &nbsp; ';
                // If we are not on page 1 we can place the Back button
                if ($page != 1) {
                    $previous = $page - 1;
                    $paginationDisplay .=  '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?page=1"> &lt;&lt; </a> ';
                }
                // Lay in the clickable numbers display here between the Back and Next links
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                // If we are not on the very last page we can place the Next button
                if ($page != $lastPage) {
                    $nextPage = $page + 1;
                    $paginationDisplay .=  '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?page=' . $lastPage . '"> &gt;&gt; </a> ';
                }
            }
        }

        return $paginationDisplay;
    }

    private function getArticlesPerDay($ID) {
        $query = "SELECT *
            FROM articles a
            WHERE a.politician = '$ID'
            AND a.`when` > DATE_SUB(NOW(), INTERVAL 30 DAY)";
        $res = $this->QP->get($query, 24 * 60 * 60);
        return $res->num() / 30;
    }

    
    public function getPostponed() {
        return $this->postponed;
    }
    
    public function addPostponedLoader($div, $pars) {
       $this->postponed[$div] = json_encode($pars);
    }
    
    private function loadPars($pars, $defaults) {
        foreach ($defaults as $index => $value) {
            if (!isset($pars[$index])) {
                $pars[$index] = $value;
            }
        }
        return $pars;
    }
    
    public function legenda() {
        return "<div class='legenda-box'>
            <span class='legenda legenda1' href='#'>Stampa naz./Agenzia</span>
            <span class='legenda legenda3' href='#'>Stampa locale</span>
            <span class='legenda legenda5' href='#'>Blog</span>
            <span class='legenda legenda6' href='#'>Organo di partito</span>
            <span class='legenda legendat' href='#'>Tweet</span>
            <span class='legenda legendaf' href='#'>Post Facebook</span>
        </div>";
    }
    
    private function titlePart($pars) {
        $title = $pars['title'];
        $subtitle = $pars['subtitle'];
        
        ob_start();
        
        ?>
        <div class='title-part'>
            <h4>
                <?php echo $title; ?>
            </h4>
            <p class='subtitle'>
                <?php echo $subtitle; ?>
            </p>
        </div>
        <?php
        
        return ob_get_clean();
    }
    
    private function embedPart($pars = array()) {
        
        $pars = array_map("urlencode", $pars);
        $link = Link::CustomizeWidget();
        $link .= "#type={$pars['sub']}";
        if (isset($pars['id']) && $pars['id']) {
            $link .= "&id={$pars['id']}";
        }
        return "<a href='$link' class='embed-tag'>embed</a>";
        
        // $json = json_encode($pars);
        // return "<a href='#' onclick='return embedThis(this, $json);' class='embed-tag'>embed</a>";
    }
    
    private function tweetPart($tweet = "", $ID = 0) {
        $text = urlencode($tweet);
        $textjs = htmlentities($tweet, ENT_QUOTES);
        return "<a onclick=\"_gaq.push(['_trackEvent', 'Tweet', '$ID', '$textjs']);\" href='https://twitter.com/intent/tweet?text=$text&related=".$this->Twitter_name."' class='tweet-tag'>twitta</a>";
    }
    
    private function btnGroup($pars) {
        
        $active = $pars['hours'];
        $div = $pars['div'];
        $div = addslashes($div);
        
        ob_start();
        
        echo "<div class='btn-group'>";
        foreach ($this->Accepted_intervals as $value) {
            $label = $this->getIntervalInWords($value);
            $pars['hours'] = $value;
            $json = json_encode($pars);
            echo "<button class='btn btn-mini ".($active == $value ? " active" : "")."' onclick='return postponeLoading(\"$div\", $json);'>$label</button>";
        }
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function trendingPie($pars) {
        ob_start();
                
        $default_pars['type'] = 2;
        $default_pars['title'] = "#trendingtopic";
        $default_pars['subtitle'] = "Le proporzioni dei 10 #trendingtopic delle ultime {$pars['type']} ore";
        $pars = $this->loadPars($pars, $default_pars);
        
        // echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        
        $GC = new GChart($this->DB, $this->QP);
        $GC->trends(array(
            "divid" => "trendingPie",
            "height" => "200px",
            "width" => "100%",
            "type" => $pars['type']
        ));
        
        return ob_get_clean();
    }
    
    function chartNewspapersPie($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['days'] = $this->getBestValue($this->getArticlesPerDay($ID), 90, array(5 => 10, 1 => 30));
        $default_pars['title'] = "Fonti";
        $default_pars['subtitle'] = "Principali fonti che hanno citato $pol negli ultimi {$pars['days']} giorni";
        $pars = $this->loadPars($pars, $default_pars);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        
        $GC = new GChart($this->DB, $this->QP);
        $GC->newspapers(array(
            "divid" => "newspapersPie",
            "politician" => $ID,
            "height" => "200px",
            "width" => "100%",
            "days" => $pars['days']
        ));
        
        return ob_get_clean();
    }
    
    function chartConnectionsPie($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['days'] = $this->getBestValue($this->getArticlesPerDay($ID), 90, array(5 => 10, 1 => 30));
        $default_pars['title'] = "Connessioni";
        $default_pars['subtitle'] = "Politici con cui %s è stato più frequentemente citato negli ultimi %d giorni";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $pol, $pars['days']);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        
        $GC = new GChart($this->DB, $this->QP);
        $GC->connections(array(
            "divid" => "connectionsPie",
            "politician" => $ID,
            "height" => "200px",
            "width" => "100%",
            "days" => $pars['days']
        ));
        
        return ob_get_clean();
    }
    
    function chartTweetsBox($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['days'] = 30;
        $default_pars['title'] = "Tweet ricevuti";
        $default_pars['subtitle'] = "Numero di tweet ricevuti da %s negli ultimi %d giorni";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $pol, $pars['days']);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        
        $GC = new GChart($this->DB, $this->QP);
        $GC->tweets(array(
            "divid" => "containerTweets",
            "politician" => $ID,
            "height" => "200px",
            "width" => "100%",
            "days" => $pars['days']
        ));
        
        return ob_get_clean();
    }
    
    function chartSocialBox($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['days'] = 30;
        $default_pars['title'] = "Post social";
        $default_pars['subtitle'] = "Numero di post social di %s negli ultimi %d giorni";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $pol, $pars['days']);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        
        $GC = new GChart($this->DB, $this->QP);
        $GC->social(array(
            "divid" => "containerSocial",
            "politician" => $ID,
            "height" => "200px",
            "width" => "100%",
            "days" => $pars['days']
        ));
        
        return ob_get_clean();
    }
    
    function chartNewsBox($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['days'] = 30;
        $default_pars['title'] = "Citazioni";
        $default_pars['subtitle'] = "Numero di citazioni di %s negli ultimi %d giorni";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $pol, $pars['days']);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        
        $GC = new GChart($this->DB, $this->QP);
        $GC->citations(array(
            "divid" => "containerCit",
            "politician" => $ID,
            "height" => "200px",
            "width" => "100%",
            "days" => $pars['days']
        ));
        
        return ob_get_clean();
    }
    
    function hashToBox($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        $sex = $P->getSexSuffix();
        
        ob_start();
        
        $default_pars['title'] = "Tendenze";
        $default_pars['hours'] = $this->getBestValue($this->getReceivedTweetsPerDay($ID), 24 * 30, array(100 => 24, 10 => 24 * 7));
        $default_pars['subtitle'] = "Principali hashtag con cui viene twittat%s %s";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $sex, $pol);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        echo $this->btnGroup($pars);
        
        echo "<div class='politician-cloud-inside'>";
        $Cloud = new HashCloud($this->QP);
        $Cloud->setClass("vert-cloud");
        echo $Cloud->piutwittati($ID, $pars['hours']);
        
        /*if (count($Cloud->getLastList()) > 0) {
            $query = "SELECT twitter FROM social_info WHERE politician = '$ID'";
            $res = $this->QP->get($query, 24 * 60 * 60);
            if ($res->num()) {
                $socialInfo = $res->next();
                $txt = "I #trendingtopic con cui hanno twittato @{$socialInfo['twitter']} n". $this->getIntervalInWords($pars['hours'], true) ." sono";
                
                $tweet = Tweet::builtTweetWithHashtags($txt, $Cloud->getLastList(), "http://{$this->Host}/$ID/tweets");
                // echo $tweet;
                echo $this->tweetPart($tweet, $ID);
            }
        }*/
        
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function hashFromBox($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['title'] = "Tendenze";
        $default_pars['hours'] = $this->getBestValue($this->getTweetsPerDay($ID), 24 * 30, array(50 => 24, 10 => 24 * 7));
        $default_pars['subtitle'] = "Principali hashtag usati da %s";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $pol);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        echo $this->btnGroup($pars);
        
        echo "<div class='politician-cloud-inside'>";
        $Cloud = new HashCloud($this->QP);
        $Cloud->setClass("vert-cloud");
        echo $Cloud->piusocial($ID, $pars['hours']);
        
        if (count($Cloud->getLastList()) > 0) {
            $query = "SELECT twitter FROM social_info WHERE politician = '$ID'";
            $res = $this->QP->get($query, 24 * 60 * 60);
            if ($res->num()) {
                $socialInfo = $res->next();
                $txt = "I #trendingtopic di @{$socialInfo['twitter']} n". $this->getIntervalInWords($pars['hours'], true) ." sono";
                
                $tweet = Tweet::builtTweetWithHashtags($txt, $Cloud->getLastList(), "http://{$this->Host}/$ID/tweets");
                // echo $tweet;
                echo $this->tweetPart($tweet, $ID);
            }
        }
        
        echo "</div>";
        
        return ob_get_clean();
    }
    
    private function getBestValue($n, $default, $values) {
        foreach ($values as $index => $value) {
            if ($n > $index) {
                return $value;
            }
        }
        return $default;
    }
    
    function temiBox($pars) {
        
        $ID = $pars['id'];
        $P = new Politician($ID);
        $pol = $P->getName();
        
        ob_start();
        
        $default_pars['title'] = "Argomenti";
        $default_pars['hours'] = $this->getBestValue($this->getArticlesPerDay($ID), 24 * 90, array(100 => 6, 10 => 24, 1 => 24 * 30));
        $default_pars['subtitle'] = "Principali temi trattati dagli articoli che citano %s";
        $pars = $this->loadPars($pars, $default_pars);
        
        $pars['subtitle'] = sprintf($pars['subtitle'], $pol);
        
        echo $this->embedPart($pars);
        echo $this->titlePart($pars);
        echo $this->btnGroup($pars);
        
        echo "<div class='politician-cloud-inside'>";
        $Cloud = new HashCloud($this->QP);
        $Cloud->setClass("vert-cloud");
        echo $Cloud->keywords($ID, $pars['hours']);
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function facebookBox($r) {
        ob_start();
        echo "<div class='item item-facebook' onclick=\"clicky(event, '{$r['link']}');\">";
        echo "<div class='item-post'>";
        
        echo "<div class='item-facebook-dashboard'>";
        $date = time_passed(strtotime($r['created_time']));
        // $date = date("d/m/Y H:i", strtotime($r['created_time']));
        echo "$date";
        echo "</div>";
        
        switch ($r['type2']) {
            case "status":
           	$message = $r['message'];
            break;
            
            case "photo":
            	$message = "<div class='box_social_picture' style=\"background-image: url('{$r['picture']}')\"></div>";
            
            $mparts = array();
            if ($r['message']) {
                $mparts[] = $r['message'];
            }
            if ($r['name']) {
                $mparts[] = "<small>Album: ".utf8_encode($r['name']) ."</small>";
            }
            if ($r['caption']) {
                $mparts[] = "<small>Descrizione: ".utf8_encode($r['caption']) ."</small>";
            }
            $message .= implode("<br />", $mparts);
            
            break;
            
            case "link":
            case "video":
            case "swf":
            if ($r['picture']) {
                $message = "<div class='box_social_picture' style=\"background-image: url('{$r['picture']}')\"></div>";
            }
            
            $mparts = array();
            if ($r['message']) {
                $mparts[] = $r['message'];
            }
            if ($r['link2']) {
            	$mparts[] = "<small><a target='_blank' href='".$r['link2']."'>".utf8_encode($r['name'])."</a></small>";	
	    }
            if ($r['description']) {
                $mparts[] = "<small>".utf8_encode($r['description'])."</small>";
            }
            $message .= implode("<br />", $mparts);
            
            break;
        }
        
        if (!$message) {
            ob_end_clean();
            return;
        }
        
        echo mb_convert_encoding($message,"HTML-ENTITIES","UTF-8");
        
        echo "<p class='shares'>{$r['shares']} condivisioni</p>";
        
        echo "</div>";
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function commentBox($r) {
        ob_start();
	// echo "<div class='item-post'>";

        $comment_id = preg_replace("/^[0-9]+_/", "", $r['comment_id']);
        $permalink = $r['link'];
        if (strpos($r['link'], "?") === false) {
            $permalink = $permalink .= "?comment_id=$comment_id";
        }
        else {
            $permalink = $permalink .= "&comment_id=$comment_id";
        }
        $date = time_passed(strtotime($r['created_time']));

        // echo "<div class='item-facebook-dashboard'>";
        // $date = time_passed(strtotime($r['created_time']));
        // echo "<a target='_blank' href='$permalink'>$date</a>";
        // echo "</div>";
	
	//CG: echo "$date -- ({$r['user_id']} {$r['text']}) $comment_id $permalink<br>";
	//CG: return ob_get_clean();
       
	echo "<div class='item item-facebook' onclick=\"clicky(event, '$permalink');\">";
        echo "<div class='box_social_user_picture' style=\"background-image: url('{$r['user_pic']}');\"></div>";
        
        echo "<div class='social_user_info'>";
        echo "<div class='item-facebook-dashboard'>$date</div>";
        echo "<a target='_blank' href='https://facebook.com/{$r['user_id']}'>{$r['user_name']}</a><br />";
        echo "<span class='username'>{$r['username']}</span>";
        echo "</div>";

        $message_txt = html_entity_decode($r['text']);
        
        switch ($r['type2']) {
            case "status":
            $message = $r['message'];
            break;
            
            case "photo":
            $message = "<a href='{$r['link']}' target='_blank'><div class='box_social_picture' style=\"background-image: url('{$r['picture']}')\"></div></a>";
            
            $mparts = array();
            if ($r['message']) {
                $mparts[] = $r['message'];
            }
            if ($r['name']) {
                $mparts[] = "<small>Album: {$r['name']}</small>";
            }
            if ($r['caption']) {
                $mparts[] = "<small>Descrizione: {$r['caption']}</small>";
            }
            $message .= implode("<br />", $mparts);
            
            break;
            
            case "link":
            case "video":
            case "swf":
            if ($r['picture']) {
                $message = "<a href='{$r['link']}' target='_blank'><div class='box_social_picture' style=\"background-image: url('{$r['picture']}')\"></div></a>";
            }
            
            $mparts = array();
            if ($r['message']) {
                $mparts[] = $r['message'];
            }
            if ($r['link2']) {
                $mparts[] = "<small><a target='_blank' href='{$r['link2']}'>{$r['name']}</a></small>";
            }
            if ($r['description']) {
                $mparts[] = "<small>{$r['description']}</small>";
            }
            $message .= implode("<br />", $mparts);
            
            break;
        }
        
        if (!$message_txt) {
            ob_end_clean();
            return;
        }
        
        echo "<div class='item-facebook-comment'>";
        //echo "<p style='clear: both;'>".html_entity_decode($message_txt)."</p>";
        echo "<p style='clear: both;'>".mb_convert_encoding($message_txt,"HTML-ENTITIES","UTF-8")."</p>";
	echo "<p class='shares'>{$r['likes']} likes</p>";
        echo "<div class='fb-original' style='border-top: 1px solid #3B5997; font-size: .9em; padding: 10px;'>";
	echo mb_convert_encoding($message,"HTML-ENTITIES","UTF-8");
	echo "</div>";
        echo "</div>";

        // echo "</div>";
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function tweetedBox($r, $addClass = "") {
        ob_start();
        
        $retweetMsg = "";
        $pic = $r['userpic'];
        $username = $r['username'];
        $nameu = $r['nameu'];
        if ($r['type2'] == 'retweet') {
            $pic = $r['userpic2'];
            $username = $r['username2'];
            $nameu = $r['nameu2'];
            $retweetMsg = "<p class='retweet'>Retweet di <a target='_blank' href='https://twitter.com/{$r['username']}'>@{$r['username']}</a></p>";
        }
        $date = time_passed(strtotime($r['created_time']));

        echo "<div class='item tweet_received item-twitter $addClass' onclick=\"clicky(event, '{$r['link']}')\">";

        echo "<div class='box_social_user_picture' style=\"background-image: url('{$pic}');\"></div>";
        
        echo "<div class='social_user_info'>";
        echo "<div class='item-tweet-dashboard'>$date";
        if ($Twitter_name != "") {
            echo " |
            <a href='https://twitter.com/intent/tweet?in_reply_to={$r['id']}&via=".$this->Twitter_name."&related=".$this->Twitter_name."' class='answer'>Rispondi</a> |
            <a href='https://twitter.com/intent/retweet?tweet_id={$r['id']}&related=".$this->Twitter_name."' class='retweet'>Retweet</a> |
            <a href='https://twitter.com/intent/favorite?tweet_id={$r['id']}&related=".$this->Twitter_name."' class='favorite'>Preferiti</a>";
        }
        echo "</div>";
        echo "<a target='_blank' href='https://twitter.com/{$username}'>".$nameu."</a><br />";
        echo "<span class='username'>@{$username}</span>";
        echo "</div>";
        
        echo "<div class='item-tweet'>";
        echo $retweetMsg;
        // if ($r['type2'] == "retweet") {
        //     echo "<p class='retweet'>Retweet da <a target='_blank' href='https://twitter.com/{$r['name']}'>@{$r['name']}</a></p>";
        // }
        
        if ($r['picture']) {
            echo "<div class='box_social_picture' style=\"background-image: url('{$r['picture']}');\"></div>";
        }
        
        echo $r['message'];
        echo "</div>";

        
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function twitterBox($r, $exclude_retweets = false) {
        ob_start();
        
        echo "<div class='item item-twitter' onclick=\"clicky(event,'{$r['link']}')\">";
        echo "<div class='item-tweet-dashboard'>";
        $date = time_passed(strtotime($r['created_time']));
        // $date = date("d/m/Y H:i", strtotime($r['created_time']));
        echo $date;
        if ($Twitter_name != "") {
            echo " |
            <a href='https://twitter.com/intent/tweet?in_reply_to={$r['id']}&via=".$this->Twitter_name."&related=".$this->Twitter_name."' class='answer'>Rispondi</a> |
            <a href='https://twitter.com/intent/retweet?tweet_id={$r['id']}&related=".$this->Twitter_name."' class='retweet'>Retweet</a> |
            <a href='https://twitter.com/intent/favorite?tweet_id={$r['id']}&related=".$this->Twitter_name."' class='favorite'>Preferiti</a>";
        }
        echo "</div>";
        
        echo "<div class='item-tweet'>";
        if ($r['type2'] == "retweet") {
            echo "<p class='retweet'>Retweet da <a target='_blank' href='https://twitter.com/{$r['name']}'>@{$r['name']}</a></p>";
        }
        
        if ($r['picture']) {
            echo "<div class='box_social_picture' style=\"background-image: url('{$r['picture']}');\"></div>";
        }
        
        echo $r['message'];
        
        if (!$exclude_retweets) {
            echo "<p class='retweet-received'>{$r['shares']} retweet</p>";
        }
        
        echo "</div>";
        echo "</div>";
        
        return ob_get_clean();
    }
    
    function newsBoxInMailSmall($r, $user = 0) {
        ob_start();
        
        if (!$user && isset($_SESSION['Login'])) {
            $user = $_SESSION['Login'];
        }
        
        $newspaper = $r['newspaper'];
        $title = $r['title'];
        $date = time_passed(strtotime($r['when']));
        
        $hash_link = $r['link'];
        $link = getLinkWithHash($user, $r['nlink'], true);
        $politician = $r['name']." ".$r['surname'];
        
        $img = $r['image_small'];
        
        $politicianLink = "http://{$this->Host}/{$r['politician']}";
        $title = strip_tags($title);
        
        ?>
		<!-- content -->
		<div class="content">
			<!-- Callout Panel -->
			<p class="callout">
			    <?php echo $title; ?>
			    <small><?php echo $date; ?> con <a href='<?php echo $politicianLink; ?>'><?php echo $politician; ?></a></small>
			    <br />
			    <a href='<?php echo $link; ?>'>Leggi la news originale su <em><?php echo $newspaper; ?></em> &raquo;</a>
			</p><!-- /callout panel -->
        </div><!-- /content -->
        <?php
        
        return ob_get_clean();
    }
    
    function newsBoxInMail($r, $user = 0) {
        ob_start();
        
        if (!$user && isset($_SESSION['Login'])) {
            $user = $_SESSION['Login'];
        }
        
        // $picture = "";
        // if (isset($r['picture']) && $r['picture']) {
        //     $picture = rel2abs($r['picture'], $r['link']);
        // }
        
        $snippet = $r['snippet'];
        $snippet = str_replace("&#65533;", "", $snippet);
        $snippet = str_replace("?", "", $snippet);

        $newspaper = $r['newspaper'];
        $title = $r['title'];
        $date = time_passed(strtotime($r['when']));
        
        $hash_link = $r['link'];
        $link = getLinkWithHash($user, $r['nlink'], true);
        
        $summary = utf8_encode($r['summary']);
        $politician = $r['name']." ".$r['surname'];
        
        $img = $r['image_small'];
        
        $politicianLink = "http://{$this->Host}/{$r['politician']}";
        $title = strip_tags($title);
        
        // $title = htmlentities($title);
        // $summary = htmlentities($summary);
        // $newspaper = htmlentities($newspaper);
        // $politician = htmlentities($politician);
        
        ?>
        
        <!-- content -->
		<div class="content">
			
			<table bgcolor="">
				<tr>
					<td class="small" width="20%" style="vertical-align: top; padding-right:10px;">
                       <div style="background-image: url('<?php echo $img; ?>');" class='img-politician-small'></div>
					</td>
					<td>
						<h4><?php echo $title; ?> <nobr><small><?php echo $date; ?> con <a href='<?php echo $politicianLink; ?>'><?php echo $politician; ?></a></small></nobr></h4>
						<p class=""><?php echo $summary; ?></p>
						<a href='<?php echo $link; ?>' class="btn">Leggi la news originale su <em><?php echo $newspaper; ?></em> &raquo;</a>
					</td>
				</tr>
			</table>
		
		</div><!-- /content -->
		
        <hr />
		
        <?php
        
        // echo "<div class='item item-news news-type{$r['type']}'>";
        // 
        // if ($InMail) {
        //     echo "<hr class='no-mail-css' />";
        // }
        // 
        // echo "<div class='item-news-dashboard'>";
        // echo "$date
        //     | <strong>$newspaper</strong>";
        // if (!$InMail) {
        //     echo " | <a target='_blank' href='$link' onclick=\"_gaq.push(['_trackEvent', 'Read', '$hash_link', document.location.href]);\">Leggi</a>";
        //     echo " <span class='summary-link'>| <a href='#' onclick=\"submitSummaryRequest(this, '$hash_link'); return false;\">Sommario</a></span>";
        // }
        // echo "</div>";
        // 
        // echo "<div class='item-news-in'>";
        // $title = "<a target='_blank' href='$link' onclick=\"_gaq.push(['_trackEvent', 'Read', '$hash_link', document.location.href]);\">$title</a>";
        // echo "<p><strong>$title</strong></p>";
        // 
        // if (isset($r['picture']) && $r['picture']) {
        //     $picture = htmlentities($r['picture'], ENT_QUOTES);
        //     echo "<a href='$link' target='_blank'><div class='box_social_picture' style=\"background-image: url('$picture');\"><img src='$picture' class='imgToBeChecked' /></div></a>";
        // }
        // 
        // if ($InMail) {
        //     $summary = utf8_encode($r['summary']);
        //     echo "<p><small>";
        //     // echo "<strong>Riassunto:</strong> ";
        //     echo $summary;
        //     echo "</small></p>";
        // }
        // else {
        //     echo "<small>$snippet</small>";
        // }
        // 
        // echo "</div>";
        // 
        // echo "</div>";
        
        return ob_get_clean();
    }
    
    function groupBox($r) {
        ?>
        <div class='item item-group row-fluid'>
            <div class='span10'>
                <?php echo $r['label']; ?>
            </div>
            <div class='span2'>
                <strong><?php echo $r['num']; ?></strong>
            </div>
        </div>
        <?php
    }
    
    function newsBox($r, $user = 0, $InMail = false) {
        ob_start();
        
        if (!$user && isset($_SESSION['Login'])) {
            $user = $_SESSION['Login'];
        }
        
        $picture = "";
        if (isset($r['picture']) && $r['picture']) {
            $picture = rel2abs($r['picture'], $r['link']);
        }
        
        $snippet = $r['snippet'];
        //$snippet = str_replace("&#65533;", "", $snippet);
        //$snippet = str_replace("?", "", $snippet);

        $newspaper = $r['newspaper'];
        $title = $r['title'];
        $date = time_passed(strtotime($r['when']));
        
        $hash_link = $r['link'];
        $link = getLinkWithHash($user, $r['nlink'], isset($InMail) ? $InMail : false);
        
        echo "<div class='item item-news news-type{$r['type']} divpointer'  onclick=\"clicky(event, '$link');\">";
        
        if ($InMail) {
            echo "<hr class='no-mail-css' />";
        }
        
        echo "<div class='item-news-dashboard'>";
        echo "$date
            | <strong>$newspaper</strong>";
        //if (!$InMail) {
          //  echo " | <a target='_blank' href='$link' onclick=\"_gaq.push(['_trackEvent', 'Read', '$hash_link', document.location.href]);\">Leggi</a>";
            //echo " <span class='summary-link'>| <a href='#' onclick=\"submitSummaryRequest(this, '$hash_link'); return false;\">Sommario</a></span>";
        //}
        echo "</div>";
        
        echo "<div class='item-news-in'>";
        //$title = "<a target='_blank' href='$link' onclick=\"_gaq.push(['_trackEvent', 'Read', '$hash_link', document.location.href]);\">$title</a>";
        echo "<p><strong>$title</strong></p>";
        
        if (isset($r['picture']) && $r['picture']) {
            $picture = htmlentities($r['picture'], ENT_QUOTES);
            echo "<div class='box_social_picture' style=\"background-image: url('$picture');\"><img src='$picture' class='imgToBeChecked' /></div>";
        }
        
        if ($InMail) {
            $summary = utf8_encode($r['summary']);
            echo "<p><small>";
           //  echo "<strong>Riassunto:</strong> ";
            echo $summary;
            echo "</small></p>";
        }
        else {
	   echo mb_convert_encoding("<small>$snippet</small>","HTML-ENTITIES","UTF-8");
        }
        
        echo "</div>";
        echo "</div>";
        
        return ob_get_clean();
    }
    
    public function lastTrending($active = 2, $howmany = 20) {
        ob_start();
        
        echo "<div class='trending-topics'>";
        echo "<div class='btn-group'>";

        foreach ($this->Accepted_intervals as $value) {
            $label = $this->getIntervalInWords($value);
            echo "<button class='btn ".($active == $value ? " active" : "")."' onclick='return updateTrending(this, $value);'>$label</button>";
        }
        echo "</div>";

        echo "<div class='trending-topics-inside'>";
        $Cloud = new HashCloud($this->QP);
        $Cloud->setHowmany($howmany);
        $Cloud->setNoTags("Nessun tag rilevante");
        echo $Cloud->trendinghashes($active);
        echo "</div>";
        
        echo "</div>";
        
        return ob_get_clean();
    }
    
    public static function newsQuery($data) {
        $ID = $data['id'];
        
        $Where = array();
        $Where[] = "politician = '$ID'";
        if ($data['when-from']) {
            $f = addslashes($data['when-from']);
            $Where[] = "`when` >= '$f'";
        }
        if ($data['when-to']) {
            $f = addslashes($data['when-to']);
            $Where[] = "`when` <= '$f'";
        }
        
        $Types = array();
        if ($data['national-press']) {
            $Types[] = "s.type = '1' OR s.type = '2' OR s.type = '4'";
        }
        if ($data['local-press']) {
            $Types[] = "s.type = '3'";
        }
        if ($data['blog-press']) {
            $Types[] = "s.type = '5'";
        }
        if ($data['party-press']) {
            $Types[] = "s.type = '6'";
        }
        if (count($Types)) {
            $Where[] = "(".implode(" OR ", $Types).")";
        }
        
        $orderBy = "`when`";
        $allowed_sorts = array("when", "title", "newspaper");
        if (in_array($data['orderby'], $allowed_sorts)) {
            $orderBy = "`{$data['orderby']}`";
        }
        if (!isset($data['ascdesc']) || $data['ascdesc'] != "asc") {
            $orderBy .= " DESC";
        }
        
        $Limit = "100";
        if (isset($data['limit'])) {
            $Limit = $data['limit'];
        }
        if (isset($data['page'])) {
            $Limit = ($data['page']*$Limit) .",". $Limit;
        }

        $End = "";
        if ($data['group']) {
            switch ($data['group']) {
                case "newspaper":
                $Select = "s.id, s.name label, COUNT(*) num";
                $Where[] = "newspaper != ''";
                $Where[] = "newspaper IS NOT NULL";
                $End = "GROUP BY s.id ORDER BY num DESC";
                break;
                
                case "type":
                $Select = "CASE
                	WHEN s.type = 3 THEN 'Stampa locale'
                	WHEN s.type = 5 THEN 'Blog'
                	WHEN s.type = 6 THEN 'Organo di partito'
                	ELSE 'Stampa nazionale/agenzia'
                	END AS `label`,
                    COUNT(*) num";
                $Where[] = "newspaper != ''";
                $Where[] = "newspaper IS NOT NULL";
                $End = "GROUP BY CASE
                	WHEN s.type = 3 THEN '3'
                	WHEN s.type = 5 THEN '5'
                	WHEN s.type = 6 THEN '6'
                	ELSE '1'
                END ORDER BY num DESC";
                break;
                
                case "day":
                $Select = "DATE_FORMAT(`when`, '%d/%m/%Y') label, COUNT(*) num";
                $End = "GROUP BY YEAR(`when`), MONTH(`when`), DAY(`when`)
                    ORDER BY YEAR(`when`) DESC, MONTH(`when`) DESC, DAY(`when`) DESC";
                break;
                
                case "hour":
                $Select = "DATE_FORMAT(`when`, '%k') label, COUNT(*) num";
                $End = "GROUP BY HOUR(`when`) ORDER BY HOUR(`when`)";
                break;
            }
        }
        else {
            $Select = "v.*, d.url nlink, s.type";
            $End = "ORDER BY $orderBy";
            if ($Limit) {
		        $End .= " LIMIT $Limit";
            }
        }
        
        $Where = implode(" AND ", $Where);
        return "SELECT $Select
            FROM articles v
            LEFT JOIN articles_done d ON d.link = v.link
            LEFT JOIN sites s ON s.name = v.newspaper
            WHERE $Where
            $End";
    }
    
    public static function socialQuery($data = array()) {
        $ID = $data['id'];
        
        $Where = array();
        $Where[] = "s.politician = '$ID'";
        if ($data['when-from']) {
            $f = addslashes($data['when-from']);
            $Where[] = "s.created_time >= '$f'";
        }
        if ($data['when-to']) {
            $f = addslashes($data['when-to']);
            $Where[] = "s.created_time <= '$f'";
        }
        
        $Types = array();
        if ($data['facebook']) {
            $Types[] = "s.type = 'facebook'";
        }
        if ($data['twitter']) {
            $Types[] = "s.type = 'twitter'";
        }
        if (count($Types)) {
            $Where[] = "(".implode(" OR ", $Types).")";
        }
        
        $orderBy = "`created_time`";
        $allowed_sorts = array("created_time", "shares");
        if (in_array($data['orderby'], $allowed_sorts)) {
            $orderBy = "`{$data['orderby']}`";
        }
        if (!isset($data['ascdesc']) || $data['ascdesc'] != "asc") {
            $orderBy .= " DESC";
        }
        
        $Limit = 100;
        if (isset($data['limit'])) {
            $Limit = $data['limit'];
        }
        if (isset($data['page'])) {
            $Limit = ($data['page']*$Limit) .",". $Limit;
        }

        $From = "social_updates_new s
        LEFT JOIN politicians p ON p.id = s.politician";
        $End = "";
        if ($data['group']) {
            switch ($data['group']) {
                case "type":
                $Select = "s.type label, COUNT(*) num";
                $End = "GROUP BY s.type ORDER BY num DESC";
                break;
                
                case "hashtag":
                $Select = "e.tag label, count(*) num";
                $From = "social_updates_new_entities e
                LEFT JOIN social_updates_new s ON (e.tweet = s.id AND s.type = 'twitter')
                LEFT JOIN politicians p ON p.id = s.politician";
                $Where[] = "e.type = 'hashtag'";
               	$End = "GROUP BY LOWER(e.tag) ORDER BY num DESC";
		//CG: $End = "GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) ORDER BY num DESC";
                break;
                
                // case "type":
                // $Select = "CASE
                //  WHEN s.type = 3 THEN 'Stampa locale'
                //  WHEN s.type = 5 THEN 'Blog'
                //  WHEN s.type = 6 THEN 'Organo di partito'
                //  ELSE 'Stampa nazionale/agenzia'
                //  END AS `label`,
                //     COUNT(*) num";
                // $Where[] = "newspaper != ''";
                // $Where[] = "newspaper IS NOT NULL";
                // $End = "GROUP BY CASE
                //  WHEN s.type = 3 THEN '3'
                //  WHEN s.type = 5 THEN '5'
                //  WHEN s.type = 6 THEN '6'
                //  ELSE '1'
                // END ORDER BY num DESC";
                // break;
                
                case "day":
                $Select = "DATE_FORMAT(`created_time`, '%d/%m/%Y') label, COUNT(*) num";
                $End = "GROUP BY YEAR(`created_time`), MONTH(`created_time`), DAY(`created_time`)
                    ORDER BY YEAR(`created_time`) DESC, MONTH(`created_time`) DESC, DAY(`created_time`) DESC";
                break;
                
                case "hour":
                $Select = "DATE_FORMAT(`created_time`, '%k') label, COUNT(*) num";
                $End = "GROUP BY HOUR(`created_time`) ORDER BY HOUR(`created_time`)";
                break;
            }
        }
        else {
            $Select = "s.*, p.name namep, p.surname surnamep";
            $End = "ORDER BY $orderBy";
            if ($Limit) {
                $End .= " LIMIT $Limit";
            }
        }
        
        $Where = implode(" AND ", $Where);
        
        return "SELECT $Select
            FROM $From
            WHERE $Where
            $End";
    }
    
    public static function tweetQuery($ID, $limit = 100) {
        return "SELECT u.*,
                us.picture userpic, us.username, us.name nameu,
                us2.picture userpic2, us2.username username2, us2.name nameu2,
                p.name namep, p.surname surnamep
            FROM twitter_politicians t
            LEFT JOIN twitter_updates u ON u.id = t.tweet
            LEFT JOIN politicians p ON p.id = t.politician
            LEFT JOIN twitter_users us ON us.id = u.user_id
            LEFT JOIN twitter_users us2 ON us2.username = u.name
            WHERE t.politician = '$ID' ORDER BY t.when_tweet DESC
            LIMIT $limit";
    }
    
    public static function commentQuery($ID, $limit = 100) {
        return "SELECT
                u.type2, u.link, u.message, u.picture, u.name, u.caption, u.link2, u.description,
                c.created_time, c.text, c.likes, c.id comment_id, c.user_id,
                us.username, us.name user_name, us.picture user_pic
            FROM social_updates_new u
            INNER JOIN fb_comments c ON c.id_post = u.id
            LEFT JOIN fb_users us ON us.id = c.user_id
            WHERE u.politician = '$ID'
            ORDER BY c.created_time DESC
            LIMIT $limit";
    }
    
    public function socialFlowPostponed($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        //$ID = $data['id'];
        //$limit = $data['limit'];
        if (isset($data['page']) && isset($data['limit'])) {
            $data['limit'] = ($data['page']*$data['limit']) .",". $data['limit'];
        }
        $class = $data['class'];
        
        $data['sub'] = "socialFlow";
        
        if ($class) {
            echo "<div class='$class' id='social-flow-container'>";
            $this->addPostponedLoader("social-flow-container", $data);
            echo "</div>";
        }
    }
    
    public function socialFlow($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        $ID = $data['id'];
        $limit = $data['limit'];
        $class = $data['class'];
        
        ob_start();
        
        $query = $this->socialQuery($data);
        if ($class) {
            echo "<div class='$class'>";
        }
        
        if ($data['group']) {
            $res = $this->QP->get($query, 300);
            while ($r = $res->next()) {
                echo $this->groupBox($r);
            }
        }
        else {
            $i = 0;
            $res = $this->QP->get($query, 300);
            while ($r = $res->next()) {
            
                if ($r['type'] == "twitter") {
                    echo $this->twitterBox($r);
                }
                else {
                    echo $this->facebookBox($r);
                }
            
                if (++$i > $limit) {
                    break;
                }
            }
        }
        
        if ($class) {
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
    public function commentFlowPostponed($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        //$ID = $data['id'];
        //$limit = $data['limit'];
        $class = $data['class'];
        
        $data['sub'] = "commentFlow";
        
        if ($class) {
            echo "<div class='$class' id='comment-flow-container'>";
            //echo "prova ".$data['page']. " " . $this->commentQuery($data['id'],($data['page']*$data['limit']) .",". $data['limit']) ;
	    $this->addPostponedLoader("comment-flow-container", $data);
            echo "</div>";
        }
    }
    
    public function commentFlow($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        $ID = $data['id'];
        $limit = $data['limit'];
        if (isset($data['page'])) {
            $limit = ($data['page']*$limit) .",". $limit;
        }
        $class = $data['class'];
        
        ob_start();
        
        $query = $this->commentQuery($ID, $limit);
	
        if ($class) {
            echo "<div class='$class'>";
        }
        
        //$i = 0;
        $res = $this->QP->get($query, 300);
        while ($r = $res->next()) {
            echo $this->commentBox($r);
            //if (++$i > $limit) {
            //  break;
            //}
        }
        
        if ($class) {
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
    public function tweetFlowPostponed($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        //$ID = $data['id'];
        //$limit = $data['limit'];
        $class = $data['class'];
        
        $data['sub'] = "tweetFlow";
        
        if ($class) {
            echo "<div class='$class' id='tweet-flow-container'>";
            $this->addPostponedLoader("tweet-flow-container", $data);
            echo "</div>";
        }
    }
    
    public function tweetFlow($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        $ID = $data['id'];
        $limit = $data['limit'];
        if (isset($data['page'])) {
            $limit = ($data['page']*$limit) .",". $limit;
        }
        $class = $data['class'];
        
        ob_start();
        
        $query = $this->tweetQuery($ID, $limit);

        if ($class) {
            echo "<div class='$class'>";
        }
        
        $i = 0;
        $res = $this->QP->get($query, 300);
        while ($r = $res->next()) {
            echo $this->tweetedBox($r);
            
            //if (++$i > $limit) {
              //  break;
            //}
        }
        
        if ($class) {
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
    public function newsFlowPostponed($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        //$ID = $data['id'];
        //$limit = $data['limit'];
        $class = $data['class'];

        $data['sub'] = "newsFlow";
        if ($class) {
           echo "<div class='$class' id='news-flow-container'>";
            $this->addPostponedLoader("news-flow-container", $data);
            echo "</div>";
        }
    }
    
    public function newsFlow($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        $limit = $data['limit'];
        $ID = $data['id'];
        $class = $data['class'];

        ob_start();
        
        $query = $this->newsQuery($data);
        if ($class) {
            echo "<div class='$class'>";
        }
        
        if ($data['group']) {
            $res = $this->QP->get($query, 300);
            while ($r = $res->next()) {
                echo $this->groupBox($r);
            }
        }
        else {
            $newsDone = array();
        
            $i = 0;
            $res = $this->QP->get($query, 300);
            while ($r = $res->next()) {
                if (isset($newsDone[$r['title'].$politician])) {
                    continue;
                }
                $newsDone[$r['title'].$politician] = 1;
                echo $this->newsBox($r);
            
                //if (++$i > $limit) {
                //    break;
                //}
            }
        }
        
        if ($class) {
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
    public function allFlowPostponed($data = array()) {
        $data = $this->loadPars($data, $this->default_flow_pars);
        //$ID = $data['id'];
        //$limit = $data['limit'];
        if (isset($data['page']) && isset($data['limit'])) {
            $data['limit'] = ($data['page']*$data['limit']) .",". $data['limit'];
        }
        $class = $data['class'];
        
        $data['sub'] = "allFlow";
        
        if ($class) {
            echo "<div class='$class' id='all-flow-container'>";
            $this->addPostponedLoader("all-flow-container", $data);
            echo "</div>";
        }
    }
    
    public function allFlow($data = array()) {
	$data = $this->loadPars($data, $this->default_flow_pars);
        $ID = $data['id'];
        $limit = $data['limit'];
        $class = $data['class'];
        
        ob_start();
        
        $all = array();
        $newsDone = array();
        $tweetsDone = array();
        // News
        $query = $this->newsQuery($data);
	    $res = $this->QP->get($query, 300);
        while ($r = $res->next()) {
            if (isset($newsDone[$r['title'].$ID])) {
                continue;
            }
            $newsDone[$r['title'].$ID] = 1;
            
            $tmp = array();
            $tmp['timestamp'] = strtotime($r['when']);
            $tmp['box'] = $this->newsBox($r);
            $tmp['type'] = "news";
            $all[] = $tmp;
        }
        
        // Social
        $query = $this->socialQuery($data);
        $res = $this->QP->get($query, 300);
        while ($r = $res->next()) {
            $tmp = array();
            $tmp['type'] = "social";
            if ($r['type'] == "twitter") {
                if (in_array($r['id'], $tweetsDone)) {
                    continue;
                }
                $tweetsDone[] = $r['id'];
                $tmp['timestamp'] = strtotime($r['created_time']);
                $tmp['box'] = $this->twitterBox($r);
                $all[] = $tmp;
            }
            else {
                $tmp = array();
                $tmp['timestamp'] = strtotime($r['created_time']);
                $tmp['box'] = $this->facebookBox($r);
                $all[] = $tmp;
            }
            
        }
        
        $mostAncientSocial = time();
        $mostAncientNews = time();
        
        usort($all, "sortOnTimestamp");
        
        $i = 0;
        foreach ($all as $chiave => $valore) {
            if ($valore['type'] == "social") {
                if ($valore['timestamp'] < $mostAncientSocial) {
                    $mostAncientSocial = $valore['timestamp'];
                }
            }
            if ($valore['type'] == "news") {
                if ($valore['timestamp'] < $mostAncientNews) {
                    $mostAncientNews = $valore['timestamp'];
                }
            }
            
            $i++;
            if ($i > $limit) {
                break;
            }
        }
        
        $Limit = max($mostAncientNews, $mostAncientSocial);
        
        // Tweets
        // $query = "SELECT u.*,
        //         us.picture userpic, us.username, us.name nameu,
        //         us2.picture userpic2, us2.username username2, us2.name nameu2,
        //         p.name namep, p.surname surnamep
        //     FROM twitter_politicians t
        //         LEFT JOIN twitter_updates u ON u.id = t.tweet
        //         LEFT JOIN politicians p ON p.id = t.politician
        //         LEFT JOIN social_info s ON s.politician = p.id
        //         LEFT JOIN twitter_users us ON us.id = u.user_id
        //         LEFT JOIN twitter_users us2 ON us2.username = u.name
        //     WHERE p.id = '$ID'
        //         AND UNIX_TIMESTAMP(`created_time`) > '$Limit'
        //         AND (u.name IS NULL OR u.name != s.twitter)
        //     ORDER BY u.shares DESC
        //     LIMIT 20";
        // $res = $this->QP->get($query, 300);
        // while ($r = $res->next()) {
        //     if (in_array($r['id'], $tweetsDone)) {
        //         continue;
        //     }
        //     if ($r['object_id']) {
        //         if (in_array($r['object_id'], $tweetsDone)) {
        //             continue;
        //         }
        //         $tweetsDone[] = $r['object_id'];
        //     }
        //     
        //     $tmp = array();
        //     $tmp['type'] = "received_tweet";
        //     $tmp['timestamp'] = strtotime($r['created_time']);
        //     $tmp['box'] = $this->tweetedBox($r);
        //     $all[] = $tmp;
        // }
        
        usort($all, "sortOnTimestamp");
        
        if ($class) {
            echo "<div class='$class'>";
        }
        
        $i = 0;
        foreach ($all as $chiave => $valore) {
            
            echo $valore['box'];
            
            $i++;
            if ($i > $limit) {
                break;
            }
        }
        
        if ($class) {
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
}
