<?php

class HashCloud {
    
    private $QP;
    private $class = "";
    private $notags = "Nessun tag rilevante in questo intervallo";
    
    private $howmany = 10;
    private $min = 2;

    private $useTextarea = false;
    
    private $last_list = array();
    
    public function __construct($QP) {
        $this->QP = $QP;
    }
    
    public function setNoTags($txt) {
        $this->notags = $txt;
    }

    public function useTextarea($use = 'myTextArea') {
        $this->useTextarea = $use;
    }
    
    public function setClass($class) {
        $this->class = $class;
    }
    
    public function setHowmany($hm) {
        $this->howmany = $hm;
    }
    
    public function getLastList() {
        return $this->last_list;
    }
    
    private function genericTwitterCloud($query) {
        //echo "Q: $query<br>";
	$resTag = $this->QP->get($query, 300);
        
        ob_start();
        
        $list = array();
        echo "<div class='tag-cloud".($this->class ? " ".$this->class : "")."'>\n";
        
        if ($resTag->num()) {
            while ($r = $resTag->next()) {
                if ($this->useTextarea) {
                    echo "<a href='#' rel='{$r['num']}'>#{$r['tag']}</a>\n";
                }
                else {
                    $url = urlencode($r['tag']);
                    $url = "https://twitter.com/search?q=%23$url&src=hash";
                    $uri = htmlentities($_SERVER['REQUEST_URI'], ENT_QUOTES);
                    $tagOk = htmlentities($r['tag'], ENT_QUOTES);
                    echo "<a title='Twittato {$r['num']} volte' target='_blank' href='$url' rel='{$r['num']}' onclick=\"_gaq.push(['_trackEvent', 'Tag', '$tagOk', '$uri']);\">#{$r['tag']}</a>\n";
                    $list["#".$r['tag']] = $r['num'];
                }
            }
        }
        else {
            echo $this->notags;
        }
        
        echo "</div>";
        
        $this->last_list = $list;
        
        $TagCloud = ob_get_clean();
        return $TagCloud;
    }
    
    public function keywords($politician, $Type = 2, $DateTime = false) {
        global $Months;
        
        if (!$DateTime) {
            $DateTime = time();
        }
        
        ob_start();
        $query = "SELECT p.id politician, t.wikipage tag, t.class, SUM(t.rel) * COALESCE(b1.multiplier, 1) * COALESCE(b2.multiplier, 1) num
            FROM `article_tag` t
                LEFT JOIN `articles` a ON t.link = a.link
                LEFT JOIN tag_blacklisted b1 ON (b1.label = t.wikipage AND b1.type = 'wikipage')
                LEFT JOIN tag_blacklisted b2 ON (b2.label = t.class AND b2.type = 'class')
                LEFT JOIN politicians p ON CONVERT(CONVERT(CONVERT(p.idwiki USING latin1) USING binary) USING utf8) = t.wikipage
            WHERE a.politician = '$politician'
                AND a.`when` > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
                AND a.`when` < FROM_UNIXTIME($DateTime)
            GROUP BY t.wikipage, t.class
            ORDER BY num DESC
            LIMIT 30";
        $query = "SELECT * FROM ($query) t
            WHERE t.num > 3
                AND (politician != '$politician' OR politician IS NULL)
            ORDER BY LOWER(t.tag)
            LIMIT 8";
        $resTag = $this->QP->get($query, 300);
        
        $list = array();
        echo "<div class='tag-cloud".($this->class ? " ".$this->class : "")."'>\n";
        
        if ($resTag->num()) {
            while ($r = $resTag->next()) {
                $txttag = $r['tag'];
                
                // Remove days
                if (preg_match("/^[0-9]+_([a-z]+)$/i", $txttag, $ris)) {
                    if (in_array(strtolower($ris[1]), $Months)) {
                        continue;
                    }
                }
                
                $txttag = str_replace("_", " " , $txttag);
                $txttag = preg_replace("/\(.*\)/", "", $txttag);
                $txttag = trim($txttag);
                $url = urlencode($r['tag']);
            
                if ($r['politician']) {
                    $link = Link::Politician($r['politician'], "news");
                    echo "<a class='internal' href='$link' rel='{$r['num']}'>$txttag</a>\n";
                }
                else {
                    $link = Link::WikiLink($url);
                    echo "<a target='_blank' class='linkable' href='$link' rel='{$r['num']}'>$txttag</a>\n";
                }
                $list[$txttag] = $r['num'];
            }
        }
        else {
            echo $this->notags;
        }
        
        echo "</div>\n";
        
        $this->last_list = $list;
        
        return ob_get_clean();
    }
    
    public function piutwittati($politician, $Type = 2, $DateTime = false) {
        
        if (!$DateTime) {
            $DateTime = time();
        }
        
        $DateTime = $DateTime - ($DateTime % 300);
        $query = "SELECT e.tag tag, COUNT(*) num
            FROM social_updates_new_entities e
                INNER JOIN twitter_updates u ON e.tweet = u.id
                INNER JOIN twitter_politicians p ON p.tweet = u.id
            WHERE p.politician = '$politician'
                AND p.when_tweet > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
                AND p.when_tweet < FROM_UNIXTIME($DateTime)
                AND e.type = 'hashtag'
            GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
            ORDER BY num DESC
            LIMIT {$this->howmany}";
        $query = "SELECT * FROM ($query) t WHERE t.num > {$this->min} ORDER BY LOWER(t.tag)";
        return $this->genericTwitterCloud($query);
    }
    
    public function piusocial($politician, $Type = 2, $DateTime = false) {
        
        if (!$DateTime) {
            $DateTime = time();
        }
        
        $DateTime = $DateTime - ($DateTime % 300);
        $query = "SELECT e.tag tag, COUNT(*) num
            FROM social_updates_new_entities e
                INNER JOIN social_updates_new u ON u.id = e.tweet
            WHERE u.politician = '$politician'
                AND `created_time` > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
                AND `created_time` < FROM_UNIXTIME($DateTime)
                AND e.type = 'hashtag'
            GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
            ORDER BY num DESC
            LIMIT {$this->howmany}";
        $query = "SELECT * FROM ($query) t WHERE t.num > {$this->min} ORDER BY LOWER(t.tag)";
        return $this->genericTwitterCloud($query);
    }
    
    public function trendinghashes($Type = 2, $DateTime = false) {
        if (!$DateTime) {
            $DateTime = time();
        }
        
        $DateTime = $DateTime - ($DateTime % 300);
        
        $query = "SELECT e.tag tag, COUNT(*) num
            FROM social_updates_new_entities e
            LEFT JOIN twitter_politicians p ON p.tweet = e.tweet
            LEFT JOIN politicians pol ON p.politician = pol.id
            WHERE p.politician IS NOT NULL
            AND p.when_tweet > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
            AND p.when_tweet < FROM_UNIXTIME($DateTime)
            AND e.type = 'hashtag'
            GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
            ORDER BY num DESC
            LIMIT {$this->howmany}";
        $query = "SELECT * FROM ($query) t WHERE t.num > {$this->min} ORDER BY LOWER(t.tag)";
        
        return $this->genericTwitterCloud($query);
    }

    public function trendinghashesParty($party, $Type = 2, $DateTime = false) {
        if (!$DateTime) {
            $DateTime = time();
        }
        
        $DateTime = $DateTime - ($DateTime % 300);
        
        $query = "SELECT e.tag tag, COUNT(*) num
            FROM social_updates_new_entities e
            LEFT JOIN twitter_politicians p ON p.tweet = e.tweet
            LEFT JOIN politicians p2 ON p2.id = p.politician
            WHERE p.politician IS NOT NULL
            AND p2.party = '$party'
            AND p.when_tweet > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
            AND p.when_tweet < FROM_UNIXTIME($DateTime)
            AND e.type = 'hashtag'
            GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
            ORDER BY num DESC
            LIMIT {$this->howmany}";
        $query = "SELECT * FROM ($query) t WHERE t.num > {$this->min} ORDER BY LOWER(t.tag)";
        
        return $this->genericTwitterCloud($query);
    }

}
