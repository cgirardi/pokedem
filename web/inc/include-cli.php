<?php

if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

// Set default settings
mb_internal_encoding("utf-8");
setlocale(LC_ALL, 'it_IT.utf8');
date_default_timezone_set('Europe/Rome');

$DateTime = time();

require_once($Folder_inc."/Mysql_connector.class.php");
require_once($Folder_inc."/QueryPreloader.class.php");
require_once($Folder_inc."/HashCloud.class.php");
require_once($Folder_inc."/GChart.class.php");
require_once($Folder_inc."/Box.class.php");
require_once($Folder_inc."/Politician.class.php");
require_once($Folder_inc."/Tweet.class.php");
require_once($Folder_inc."/Link.class.php");
require_once($Folder_inc."/ModuloB.class.php");
require_once($Folder_inc.'/Charts.class.php');

require_once($Folder_inc."/functions.php");
require_once($Folder_inc."/PHPMailer_5.2.4/class.phpmailer.php");

$Accepted_intervals = array();
$Accepted_intervals[] = 2;
$Accepted_intervals[] = 6;
$Accepted_intervals[] = 24;
$Accepted_intervals[] = 24 * 7;
$Accepted_intervals[] = 24 * 30;
$Accepted_intervals[] = 24 * 90;

if ($Is_test) {
	ini_set("error_reporting", E_ALL);
        ini_set("display_errors", "On");
} else {
	ini_set("error_reporting", E_ALL & ~E_DEPRECATED & ~E_NOTICE);
        ini_set("display_errors", "Off");
}

$DB = new Mysql_Connector($db_host, $db_user, $db_pass);
if ($Is_test) {
	$DB->debug = true;
}
$DB->select_db($db_db);
$thisDB = $DB;

//if (!$Is_test) {
//	$DB_2 = new Mysql_Connector($db_host_test, $db_user_test, $db_pass_test);
//	if (!$DB_2->connection) {
//		unset($DB_2);
//	}
//	else {
//		$DB_2->select_db($db_db_test);
//#		$thisDB = $DB_2;
//	}
//}

$QP = new QueryPreloader($thisDB);
$BM = new Box($thisDB, $QP, $Tw_PD_name);

$query = "SET group_concat_max_len = 10000";
$DB->query($query);

$Months = array("", "gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno",
    "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre");

// $query = "SET NAMES WINDOWS-1252";
// $DB->query($query);

$query = "SELECT * FROM sites WHERE active = '1'";
$NumFonti = $DB->querynum($query);

// $Default_User = array();
// $Default_User['max_view'] = 50;
// $Default_User['max_mail'] = 10;
// $Default_User['receive_mail'] = 0;
// $Default_User['enable_social'] = 1;

// $Esteri = array();
// $Esteri['America Meridionale'] = "America Meridionale";
// $Esteri['America Settentrionale'] = "America Settentrionale";
// $Esteri['Europa'] = "Europa";
// $Esteri['Africa'] = "Africa, Asia, Oceania";

// if (get_magic_quotes_gpc()) {
//    function stripslashes_deep($value)
//    {
//        $value = is_array($value) ?
//                    array_map('stripslashes_deep', $value) :
//                    stripslashes($value);

//        return $value;
//    }

//    $_POST = array_map('stripslashes_deep', $_POST);
//    $_GET = array_map('stripslashes_deep', $_GET);
//    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
//    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
// }

$FollowedPoliticians = array();
$NumOfPoliticians = 0;

if (isset($_SESSION['Login']) && $_SESSION['Login']) {
    $FollowedPoliticians = getFollowedPoliticians();
    $NumOfPoliticians = count($FollowedPoliticians);
}

