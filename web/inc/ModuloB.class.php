<?php

###########################################
# Necessaria la classe di FCK
# Necessaria la funzione span()
###########################################

class ModuloB {

	###########################################
	# Variabili della classe
	###########################################

	var $stringa = "";
	var $classe = "";
	var $tab_classe = "";
	var $picc_classe = "";
	var $method = "post";
	var $action = "";
	var $spaziocelle = "3";
	var $hiddenclass = "nascosto";
	var $def;
	var $duecolonne = 1;
	var $enctype = "";
	
	###########################################
	# Costruttore della classe
	###########################################

	function ModuloB($action, $def = array()) {
		$this->action = $action;
		$this->def = $def;
	}

	###########################################
	# Funzioni varie
	###########################################

	function pulisci($testo) {
		return str_replace("'", "&#039;", $testo);
	}
	
	function agg_free($testo) {
		$this->stringa .= $testo."\n";
	}
		
	function agg_free_offset($testo, $id = "", $useAll = false) {
        if (!$useAll) {
            $testo = "<div class='control-group'>
                <div class='offset3 span9'>$testo</div>
                </div>";
        }
        $ret = $testo;
        if ($id) {
            $ret = "<div id='$id'>$ret</div>";
        }
		$this->stringa .= $ret;
	}
		
	function agg_section($title) {
        $ret = "<div class='control-group form-section'>
            <div class='offset3 span9'><h3>$title</h3></div>
        </div>";
		$this->stringa .= $ret;
	}
    
    ### Generic classes
    
    static function generic_select($nome, $array, $testo, $bsSize = 9, $type = "") {
        $stringa = "";
        $stringa .= "<div class='span$bsSize'>";
        $stringa .= "\t<select id='input-$nome' name='".$nome."' class='form-control span$bsSize'"
            . ($type ? " ".$type : "") . ">\n";
        foreach ($array as $indice=>$valore) {
            $stringa .= "\t\t<option value='".$indice."'";
            if (is_array($testo)) {
                $stringa .= (in_array($indice, $testo) ? " selected" : "");
            }
            else {
                $stringa .= ($testo == $indice ? " selected" : "");
            }
            $stringa .= ">".($valore !== "" ? $valore : "[".l(97)."]")."</option>\n";
        }
        $stringa .= "\t</select>\n";
        $stringa .= "</div>";
        return $stringa;
    }

	static function generic_text_number($nome, $value, $bsSize = 3, $decimali = 2, $step = 1) {
	    if (!$value) {
	        $value = 0;
        }
	    return "<div class='col-sm-$bsSize input-group'>
            <input class='form-control hasSpinedit' type='text' name='$nome' id='input-$nome'>
            <script type='text/javascript' charset='utf-8'>
                $('#input-$nome').spinedit({
                    minimum: 0,
                    maximum: 500000000,
                    step: $step,
                    value: $value,
                    numberOfDecimals: $decimali
                });
            </script>
        </div>";
    }

	###########################################
	# Funzioni per i campi con le tabelle
	###########################################
	
	function agg_text($nome, $titolo, $testo = "", $placeholder = "", $piccolino = "") {
		if (!$testo && $this->def[$nome]) {
			$testo = $this->def[$nome];
		}
		
		$value = $this->pulisci($testo);
		$placeholder = $this->pulisci($placeholder);
		
		$this->stringa .= "
		<div class='control-group'>
            <label for='input-$nome' class='span3 control-label'>$titolo</label>
            <div class='span9'>
                <input type='text' name='$nome' class='span7 form-control' id='input-$nome' placeholder='$placeholder' value='$value'>
            </div>
        </div>
        ";
	}

    function agg_politician($nome, $titolo, $testo = "", $placeholder = "", $piccolino = "") {
        if (!$testo && $this->def[$nome]) {
            $testo = $this->def[$nome];
        }
        
        $value = $this->pulisci($testo);
        $placeholder = $this->pulisci($placeholder);

        
        $this->stringa .= "
        <div class='control-group'>
            <label class='span3 control-label'>$titolo</label>
            <div class='span6 politician-controls'>
                <input type='text' class='span4 form-control inputPolitician' placeholder='Digita il nome...'>
                <span class='help-inline'><a class='inputPolitician_cancel' href='#' data-callback='cancelWidget' onclick='return cancelPolitician(this);'>Cambia</a></span>
                <input type='hidden' class='inputPolitician_hidden' name='{$nome}' value='{$testo}'>
            </div>
        </div>
        ";
    }

	function agg_check($nome, $titolo, $testo = "") {
		if (!$testo && $this->def[$nome]) {
			$testo = $this->def[$nome];
		}
		
		$value = $this->pulisci($testo);
		$placeholder = $this->pulisci($placeholder);
		$checked = $value ? " checked='checked'" : "";
		
		$this->stringa .= "
		<div class='control-group'>
            <div class='offset3 checkbox'>
                <label>
                    <input name='$nome' type='checkbox'$checked>
                    $titolo
                </label>
            </div>
        </div>
        ";
	}

	function agg_select($nome, $titolo, $array = array(), $testo = "", $placeholder = "", $piccolino = "") {
		if (!$testo && $this->def[$nome]) {
			$testo = $this->def[$nome];
		}
		
		$value = $this->pulisci($testo);
		$placeholder = $this->pulisci($placeholder);
		
		$this->stringa .= "
		<div class='control-group'>
            <label for='input-$nome' class='span3 control-label'>$titolo</label>";
        $this->stringa .= ModuloB::generic_select($nome, $array, $testo, 9, $type);
		$this->stringa .= "
        </div>
        ";
	}
	
	// function agg_text_number($nome, $titolo, $testo = "", $decimali = 2, $step = 1) {
	// 	if (!$testo && $this->def[$nome]) {
	// 		$testo = $this->def[$nome];
	// 	}
		
	// 	$value = $this->pulisci($testo);
	// 	$placeholder = $this->pulisci($placeholder);
		
	// 	if (!$value) {
	// 	    $value = 0;
	//     }
		
	// 	$this->stringa .= "
	// 	<div class='control-group'>
 //            <label for='input-$nome' class='span3 control-label'>$titolo</label>
 //            <div class='span3 input-group'>
 //                <input class='form-control hasSpinedit' type='text' name='$nome' id='input-$nome'>
 //                <script type='text/javascript' charset='utf-8'>
 //                    $('#input-$nome').spinedit({
 //                        minimum: 0,
 //                        maximum: 500000000,
 //                        step: $step,
 //                        value: $value,
 //                        numberOfDecimals: $decimali
 //                    });
 //                </script>
 //            </div>
 //        </div>
 //        ";
	// }

	// function agg_text_date($nome, $titolo, $testo = "", $placeholder = "", $piccolino = "") {
	// 	if (!$testo && $this->def[$nome]) {
	// 		$testo = $this->def[$nome];
	// 	}
		
	// 	$value = $this->pulisci($testo);
	// 	$placeholder = $this->pulisci($placeholder);
		
	// 	// data-date='1979-09-16T05:25:07Z'
		
	// 	$value_ok = "";
	// 	$value_c = "";
	// 	if ($value) {
 //            $value_ok = date("d F Y - h:i a", strtotime($value));
	// 	    $value_c = date("c", strtotime($value));
	//     }
	// 	$this->stringa .= "
	// 	<div class='control-group'>
 //            <label for='input-$nome' class='span3 control-label'>$titolo</label>
 //            <div class='input-group date form_datetime span5' id='datetime-$nome'
 //                    data-date-format='dd MM yyyy - HH:ii p' data-link-field='input-$nome'>
 //                <input class='form-control' type='text' value='$value_ok' readonly>
 //                <span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>
 //    			<span class='input-group-addon'><span class='glyphicon glyphicon-th'></span></span>
 //            </div>
 //    		<input type='hidden' name='$nome' id='input-$nome' value='$value' /><br/>
 //        </div>
 //        <script type='text/javascript' charset='utf-8'>
 //        $('#datetime-$nome').datetimepicker({
 //            weekStart: 1,
 //            todayBtn: 1,
 //            autoclose: 1,
 //            todayHighlight: 1,
 //            startView: 2,
 //            showMeridian: 1,
 //            language: '{$_SESSION['Language']}'
 //        });
 //        var d = new Date();
 //        var dstr = d.toISOString();
 //        $('#datetime-$nome').data({date: new Date(".($value_c ? "'$value_c'" : "").")});
 //        $('#datetime-$nome').datetimepicker({update: new Date()});
 //        // $('#datetime-$nome').datetimepicker().children('input').val(dstr);
 //        </script>
 //        ";
	// }

	function agg_textarea($nome, $titolo, $testo = "", $placeholder = "", $piccolino = "") {
		if (!$testo && $this->def[$nome]) {
			$testo = $this->def[$nome];
		}
		
		$value = $this->pulisci($testo);
		$placeholder = $this->pulisci($placeholder);
		
		$this->stringa .= "
		<div class='control-group'>
            <label for='input-$nome' class='span3 control-label'>$titolo</label>
            <div class='span9'>
                <textarea class='form-control span9' name='$nome' rows='5' id='input-$nome' placeholder='$placeholder'>$value</textarea>
            </div>
        </div>
        ";
	}

	function agg_submit($edit = "") {
	    $btn1 = "<button type='submit' class='btn btn-primary'>Invia</button>";
		$this->stringa .= "
	    <div class='control-group'>
          <div class='offset3 span9'>
            $btn1
            $edit
          </div>
        </div>
        ";
    }
    
	###########################################
	# Funzioni di output
	###########################################
	
	function scrivi_html() {
		echo $this->retrieve_html();
	}
	
	function retrieve_html() {
		$tmpstringa = "<form class='form-horizontal' role='form' method='".$this->method."' action='".$this->action."'"
			. ($this->enctype ? " enctype='".$this->enctype."'" : "").">\n"
			. $this->stringa
			. "</form>\n";
		return $tmpstringa;
	}

}

?>