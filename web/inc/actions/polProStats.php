<?php

$DB->debug = true;
try {
    
    $data = array();
    if (isset($_REQUEST['pol'])) {
        foreach ($_REQUEST as $index => $value) {
            $data[$index] = addslashes($value);
        }
        $polID = $data['pol'];
    }
    else {
        foreach ($_REQUEST as $index => $value) {
            if (preg_match("/^query-(.*)/", $index, $r)) {
                $data[$r[1]] = $value;
            }
        }
        $polID = $data['id'];
    }
    
    if (!isProEnabled($polID)) {
        throw new Exception("Account non abilitato");
    }
    
    if ($data['when-from'] && !strtotime($data['when-from'])) {
        throw new Exception("Campo 'da' non valido");
    }
    if ($data['when-to'] && !strtotime($data['when-to'])) {
        throw new Exception("Campo 'a' non valido");
    }

    // Politician
    if (!$polID) {
        throw new Exception("Inserire un politico");
    }
    $P = new Politician($polID);
    $info = $P->addSocialInfo();
    $polName = $P->getName();
    $polTwitter = $info['twitter'];
    
    if (!$polTwitter) {
        throw new Exception("Politico senza Twitter");
    }
    
    $Parties = array();
    $query = "SELECT * FROM parties WHERE coalition = '0'";
    $DB->query($query);
    while ($r = $DB->fetch()) {
        $Parties[$r['id']] = $r['shortname'];
    }

    $query = "SELECT party FROM politicians WHERE id = '$polID'";
    $r = $DB->queryfetch($query);
    $Party = $r['party'];
    if (!$Party) {
        // wlog("Politico senza partito");
        $Party = 0;
    }

    // Settimana con cui confrontare i dati
    // (retweet, ecc.)
    $secondsInWeek = 60 * 60 * 24 * 7;
    $secondsInTwoDays = 60 * 60 * 24 * 2;
    $WeekID = $data['week'];
    // $WeekID = 9;
    if ($WeekID) {
        $query = "SELECT * FROM weeks
            WHERE id = '$WeekID'";
        if ($DB->querynum($query)) {
            $WeekR = $DB->fetch();
            $WeekID = $WeekR['id'];
        }
    }
    if (!$WeekID) {
        $WeekR = array();
    
        $query = "SELECT * FROM weeks
            ORDER BY startdate DESC
            LIMIT 1";
        if ($DB->querynum($query)) {
            $WeekR = $DB->fetch();
        }
        $WeekID = $WeekR['id'];
    }
    if (empty($WeekR)) {
        throw new Exception("Settimana non valida");
    }
    
    // $startTime = $WeekR['startdate'];
    $startTime = date("Y-m-d H:i:s", time() - $secondsInWeek);
    $hasFrom = false;
    if ($data['when-from']) {
        $startTime = $data['when-from'];
        $hasFrom = true;
    }
    
    $startTS = strtotime($startTime);
    
    if ($hasFrom) {
        $endTS = time();
    }
    else {
        $endTS = min(time(), $startTS + $secondsInWeek - 1);
    }
    $endTime = date("Y-m-d H:i:s", $endTS);
    if ($data['when-to']) {
        $endTime = $data['when-to'];
        $endTS = strtotime($endTime);
    }
    
    $startTimePos = $startTime;
    if ($endTS - $startTS < $secondsInTwoDays) {
        $startTimePos = date("Y-m-d H:i:s", $endTS - $secondsInTwoDays + 1);
    }
    
    $WeekTxt = getTimeTxt($startTS, $endTS);

    // Temporary table

    $query = "CREATE TEMPORARY TABLE `table_ok` (
      `id` bigint NOT NULL,
      `username` varchar(200) COLLATE utf8_bin NOT NULL,
      `num` int(11) NOT NULL DEFAULT '0',
      `good` boolean NOT NULL DEFAULT '1',
      PRIMARY KEY (`id`)
    ) ENGINE=MEMORY";
    $DB->query($query);

    $query = "ALTER TABLE `table_ok` ADD INDEX(`username`)";
    $DB->query($query);
    $query = "ALTER TABLE `table_ok` ADD INDEX(`num`)";
    $DB->query($query);

    // $query = "TRUNCATE TABLE table_ok";
    // $DB->query($query);
    // $query = "TRUNCATE TABLE table_no";
    // $DB->query($query);

    // Utenti che hanno retwittato almeno una volta il politico
    $query = "INSERT INTO table_ok SELECT u.id, u.username, COUNT(*) num, 1
    	FROM twitter_updates t
        LEFT JOIN twitter_users u ON t.user_id = u.id
        WHERE type2 = 'retweet' AND t.name = '$polTwitter'
        AND u.username != '$polTwitter'
        AND t.created_time >= '$startTimePos' AND t.created_time <= '$endTime'
        -- AND ADDDATE(t.created_time, INTERVAL 30 DAY) > NOW()
        GROUP BY u.id
        ORDER BY num DESC";
    $DB->query($query, 2);

    // Utenti che hanno utilizzato gli hashtag almeno una volta
    $query = "SELECT t.id, t.username, COUNT(*) num
        FROM twitter_updates u
        JOIN social_updates_new_entities e ON e.tweet = u.id
        JOIN twitter_politicians p ON u.id = p.tweet
        LEFT JOIN twitter_users t ON t.id = u.user_id
        WHERE p.politician = '$polID'
        AND u.created_time >= '$startTimePos' AND u.created_time <= '$endTime'
        AND t.username != '$polTwitter'
        -- AND ADDDATE(u.created_time, INTERVAL 30 DAY) > NOW()
        AND e.type = 'hashtag'
        AND e.tag = ANY(SELECT tag FROM (SELECT e.tag, COUNT(*) num
            FROM social_updates_new_entities e
            LEFT JOIN social_updates_new u ON u.id = e.tweet
            WHERE u.politician = '$polID'
                AND u.created_time >= '$startTimePos' AND u.created_time <= '$endTime'
                -- AND ADDDATE(u.created_time, INTERVAL 30 DAY) > NOW()
                AND e.type = 'hashtag'
                AND u.type = 'twitter'
            GROUP BY e.tag
            ORDER BY num DESC) t1 WHERE num > 1)
        GROUP BY t.id
        ORDER BY num DESC;";
    $DB->query($query, 2);
    while ($r = $DB->fetch_a(2)) {

        $query = "INSERT INTO table_ok (id, username, num)
            VALUES ('{$r['id']}', '{$r['username']}', '{$r['num']}')
            ON DUPLICATE KEY UPDATE num = num + VALUES(num)";
        $DB->query($query);

        // $query = "SELECT * FROM table_ok WHERE id = '{$r['id']}'";
        // if ($DB->querynum($query)) {
        //     $query = "UPDATE table_ok SET num = num + '{$r['num']}' WHERE id = '{$r['id']}'";
        //     $DB->query($query);
        // }
        // else {
        //     $DB->queryinsert("table_ok", $r);
        // }
    }

    // DEBUG $query
        // SELECT u.username, r.*, 1 - (r.party_16/r.total) ratio
        // FROM twitter_politicians p
        // LEFT JOIN twitter_updates t ON t.id = p.tweet
        // INNER JOIN (
        //     SELECT *
        //     FROM attivisti_rt
        //     WHERE total > 5
        //     AND week = 8
        //     AND 1 - (party_16/total) > 0.9
        // ) r ON r.user_id = t.user_id
        // LEFT JOIN twitter_users u ON u.id = t.user_id
        // LEFT OUTER JOIN table_ok ON t.user_id = table_ok.id
        // WHERE p.politician = '2001'
        // AND when_tweet >= '2014-02-17 00:00:00'
        // AND when_tweet < '2014-02-24 00:00:00'
        // GROUP BY t.user_id
        // ORDER BY ratio DESC
    
    if ($Party) {
        $query = "CREATE TEMPORARY TABLE `tmp_attivisti` ENGINE=MEMORY AS
            SELECT *
            FROM attivisti_rt
            WHERE total > 5
            AND week = '$WeekID'
            AND 1 - (party_$Party/total) > 0.9";
        $DB->query($query);

        $query = "ALTER TABLE `tmp_attivisti` ADD INDEX(`user_id`)";
        $DB->query($query);

        $query = "CREATE TEMPORARY TABLE `table_no` ENGINE=MEMORY AS
            SELECT DISTINCT u.id, u.username, COUNT(*) num
            FROM twitter_politicians p
            LEFT JOIN twitter_updates t ON t.id = p.tweet
            INNER JOIN tmp_attivisti r ON r.user_id = t.user_id
            LEFT JOIN twitter_users u ON u.id = t.user_id
            LEFT OUTER JOIN table_ok ON t.user_id = table_ok.id
            WHERE p.politician = '$polID'
            AND u.username != '$polTwitter'
            AND when_tweet >= '$startTime'
            AND when_tweet <= '$endTime'
            GROUP BY u.id";
        try {
            $DB->query($query);
            
            $query = "ALTER TABLE `table_no` ADD INDEX(`username`)";
            $DB->query($query);
            $query = "ALTER TABLE `table_no` ADD INDEX(`num`)";
            $DB->query($query);
        }
        catch (Exception $e) {
            $Party = 0;
        }
    }
    
    $queryUserOk = "SELECT r.*, u.*, tmp.num
        FROM table_ok tmp
        LEFT JOIN twitter_users u ON u.id = tmp.id
        LEFT JOIN attivisti_rt r ON (r.user_id = tmp.id AND r.week = '$WeekID')
        ORDER BY tmp.num DESC";
    $queryUserNo = "SELECT r.*, u.*, tmp.num
        FROM table_no tmp
        LEFT JOIN twitter_users u ON u.id = tmp.id
        LEFT JOIN attivisti_rt r ON (r.user_id = tmp.id AND r.week = '$WeekID')
        ORDER BY tmp.num DESC";
    $query = "SELECT id FROM table_ok";
    $nOk = $DB->querynum($query);
    if ($Party) {
        $query = "SELECT id FROM table_no";
        $nNo = $DB->querynum($query);
    }
    
    $queryHashOk = "SELECT e.tag, COUNT(*)/$nOk num
        FROM social_updates_new_entities e
        LEFT JOIN twitter_updates t ON t.id = e.tweet
        INNER JOIN table_ok tmp ON tmp.id = t.user_id
        INNER JOIN twitter_politicians p ON (p.politician = '$polID' AND p.tweet = t.id)
        WHERE t.created_time >= '$startTime'
        AND LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) != ALL (SELECT tag FROM neutral_tags)
        AND t.created_time <= '$endTime'
        AND e.type = 'hashtag'
        GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
        ORDER BY num DESC";
    $queryHashNo = "SELECT e.tag, COUNT(*)/$nNo num
        FROM social_updates_new_entities e
        LEFT JOIN twitter_updates t ON t.id = e.tweet
        INNER JOIN table_no tmp ON tmp.id = t.user_id
        INNER JOIN twitter_politicians p ON (p.politician = '$polID' AND p.tweet = t.id)
        WHERE t.created_time >= '$startTime'
        AND LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8)) != ALL (SELECT tag FROM neutral_tags)
        AND t.created_time <= '$endTime'
        AND e.type = 'hashtag'
        GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
        ORDER BY num DESC";
    
    $query = "CREATE TEMPORARY TABLE `table_tags` (
      `tag` varchar(200) COLLATE utf8_bin NOT NULL,
      `numok` double NOT NULL DEFAULT '1',
      `numno` double NOT NULL DEFAULT '1',
      `difference` double NOT NULL DEFAULT '0',
      `ratio` double NOT NULL DEFAULT '0',
      PRIMARY KEY (`tag`)
    ) ENGINE=MEMORY";
    $DB->query($query);
    
    // $query = "TRUNCATE TABLE table_tags";
    // $DB->query($query);
    
    $query = "INSERT INTO table_tags (tag, numok)
        SELECT LOWER(CONVERT(CONVERT(CONVERT(tag USING latin1) USING binary) USING utf8)), num + 1
        FROM ($queryHashOk) tmp";
    $DB->query($query);
    
    if ($Party) {
        $query = "INSERT INTO table_tags (tag, numno)
            SELECT LOWER(CONVERT(CONVERT(CONVERT(tag USING latin1) USING binary) USING utf8)), num + 1
            FROM ($queryHashNo) tmp
            ON DUPLICATE KEY UPDATE numno = VALUES(numno)";
        $DB->query($query);
    }
    
    $query = "UPDATE table_tags SET difference = numok - numno";
    $DB->query($query);
    $query = "UPDATE table_tags SET ratio = numok / numno";
    $DB->query($query);
    
    $queryHashOk = "SELECT tag, numok num FROM table_tags WHERE ratio > 1.05 ORDER BY ratio DESC";
    $queryHashNo = "SELECT tag, numno num FROM table_tags WHERE ratio < 0.95 ORDER BY ratio";
    
    if ($_REQUEST['export']) {
        switch ($_REQUEST['type']) {
            case "userok":
            giveExport($queryUserOk);
            break;
            
            case "userno":
            if ($Party) {
                giveExport($queryUserNo);
            }
            break;
            
            case "hashok":
            giveExport($queryHashOk);
            break;
            
            case "hashno":
            if ($Party) {
                giveExport($queryHashNo);
            }
            break;
        }
    }
    
    ob_start();
                
    echo "<p>Dati relativi all'intervallo: $WeekTxt</p>";
    
    echo "<hr />";
    
    echo "<div class='row-fluid'>";
    echo "<div class='span8'>";
    echo "<a href='?action=ajax&amp;sub=polProStats&amp;pol=$polID&amp;export=1&amp;type=userok' class='btn btn-warning btn-submit-form pull-right'><i class='icon-white icon-list-alt'></i> Esporta</a>";
    echo "<h4>Utenti pi&ugrave; favorevoli".($_SESSION['Debug'] ? " [$nOk]" : "")."</h4>";
    if ($DB->querynum($queryUserOk . " LIMIT 100")) {
        echo "<div class='list-users'>";
        echo "<table class='table table-striped'>";
        echo "<thead>";
        echo "<tr><th>Username</th><th>Nome</th><th>Follower</th><th title=\"Numero di tweet favorevoli da parte dell'utente\">#</th><th>Partito</th></tr>";
        echo "</thead>";
        echo "<tbody>";
        while ($r = $DB->fetch()) {
            
            $party = "n.d.";
            if ($r['total'] > 5) {
                foreach ($r as $index => $value) {
                    if (preg_match("/^party_([0-9]+)$/", $index, $ris)) {
                        $ratio = $value / $r['total'];
                        if ($ratio > .5) {
                            $party = $Parties[$ris[1]];
                        }
                    }
                }
            }
            
            echo "<tr>";
            echo "<td>
                <!--<a onclick=\"_gaq.push(['_trackEvent', 'Pro', 'AttivistiFromOk', '{$r['username']}']);\" href='http://$Host/attivisti/{$r['username']}' target='_blank'>{$r['username']}</a>-->
                <a title=\"Elenco dei tweet inviati da {$r['username']} nell'intervallo selezionato, filtrabile per utenti citati, retweet e hashtag\" onclick=\"_gaq.push(['_trackEvent', 'Pro', 'AttivistiFromOk', '{$r['username']}']);\" href='#' class='link-to-user' data-user='{$r['id']}'>{$r['username']}</a>
                <a title=\"Apri l'account Twitter di {$r['username']} in un altro tab\" onclick=\"_gaq.push(['_trackEvent', 'Pro', 'TwitterFromOk', '{$r['username']}']);\" href='http://twitter.com/{$r['username']}' target='_blank'><img style='height: 20px;' src='img/tweet.png'></a>
            </td>";
            echo "<td>{$r['name']}</td>";
            echo "<td>{$r['followers']}</td>";
            echo "<td>{$r['num']}</td>";
            echo "<td>{$party}</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
    else {
        echo "<p>Non ci sono dati</p>";
    }
    echo "</div>";
    echo "<div class='span4'>";
    echo "<a href='?action=ajax&amp;sub=polProStats&amp;pol=$polID&amp;export=1&amp;type=hashok' class='btn btn-warning btn-submit-form pull-right'><i class='icon-white icon-list-alt'></i> Esporta</a>";
    echo "<h4>Hashtag favorevoli <span title='Elenco hashtag utilizzati con accezione positiva verso il politico'><i class='fa fa-question-circle'></i></span></h4>";
    if ($DB->querynum($queryHashOk . " LIMIT 10")) {
        echo "<table class='table table-striped'>";
        echo "<thead>";
        echo "<tr><th>Tag</th><th>confidenza</th></tr>";
        echo "</thead>";
        echo "<tbody>";
        while ($r = $DB->fetch()) {
            echo "<tr>";
            echo "<td>".htmlentities($r['tag'], ENT_QUOTES, "ISO-8859-1")."</td>";
            echo "<td>{$r['num']}</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    }
    else {
        echo "<p>Non ci sono dati</p>";
    }
    echo "</div>";
    echo "</div>";
    
    echo "<hr />";
    
    echo "<div class='row-fluid'>";
    if ($Party) {
        echo "<div class='span8'>";
        echo "<a href='?action=ajax&amp;sub=polProStats&amp;pol=$polID&amp;export=1&amp;type=userno' class='btn btn-warning btn-submit-form pull-right'><i class='icon-white icon-list-alt'></i> Esporta</a>";
        echo "<h4>Utenti pi&ugrave; ostili".($_SESSION['Debug'] ? " [$nNo]" : "")."</h4>";
        if ($DB->querynum($queryUserNo . " LIMIT 100")) {
            echo "<div class='list-users'>";
            echo "<table class='table table-striped'>";
            echo "<thead>";
            echo "<tr><th>Username</th><th>Nome</th><th>Follower</th><th title=\"Numero di tweet ostili da parte dell'utente\">#</th><th>Partito</th></tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($r = $DB->fetch()) {
                
                $party = "n.d.";
                if ($r['total'] > 10) {
                    // $parties = array();
                    foreach ($r as $index => $value) {
                        if (preg_match("/^party_([0-9]+)$/", $index, $ris)) {
                            $ratio = $value / $r['total'];
                            // $parties[$ris[1]] = $ratio;
                            if ($ratio > .7) {
                                $party = $Parties[$ris[1]];
                            }
                        }
                    }
                }
                
                echo "<tr>";
                echo "<td>
                    <!--<a onclick=\"_gaq.push(['_trackEvent', 'Pro', 'AttivistiFromNo', '{$r['username']}']);\" href='http://$Host/attivisti/{$r['username']}' target='_blank'>{$r['username']}</a>-->
                    <a title=\"Elenco dei tweet inviati dall'utente nell'intervallo selezionato, filtrabile per utenti citati, retweet e hashtag\" onclick=\"_gaq.push(['_trackEvent', 'Pro', 'AttivistiFromNo', '{$r['username']}']);\" href='#' class='link-to-user' data-user='{$r['id']}'>{$r['username']}</a>
                    <a title=\"Apri l'account Twitter di {$r['username']} in un altro tab\" onclick=\"_gaq.push(['_trackEvent', 'Pro', 'TwitterFromNo', '{$r['username']}']);\" href='http://twitter.com/{$r['username']}' target='_blank'><img style='height: 20px;' src='img/tweet.png'></a>
                </td>";
                echo "<td>{$r['name']}</td>";
                echo "<td>{$r['followers']}</td>";
                echo "<td>{$r['num']}</td>";
                echo "<td>{$party}</td>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        }
        else {
            echo "<p>Non ci sono dati</p>";
        }
        echo "</div>";
        echo "<div class='span4'>";
        echo "<a href='?action=ajax&amp;sub=polProStats&amp;pol=$polID&amp;export=1&amp;type=hashno' class='btn btn-warning btn-submit-form pull-right'><i class='icon-white icon-list-alt'></i> Esporta</a>";
        echo "<h4>Hashtag ostili <span title='Elenco hashtag utilizzati con accezione negativa verso il politico'><i class='fa fa-question-circle'></i></span></h4>";
        if ($DB->querynum($queryHashNo . " LIMIT 10")) {
            echo "<table class='table table-striped'>";
            echo "<thead>";
            echo "<tr><th>Tag</th><th>confidenza</th></tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($r = $DB->fetch()) {
                echo "<tr>";
                echo "<td>".htmlentities($r['tag'], ENT_QUOTES, "ISO-8859-1")."</td>";
                echo "<td>{$r['num']}</td>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
        }
        else {
            echo "<p>Non ci sono dati</p>";
        }
        echo "</div>";
    }
    else {
        echo "<p>Non ci sono dati ostili per $polName</p>";
    }
    echo "</div>";
    
    ?>
    
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('.link-to-user').each(function() {
                $(this).click(function() {
                    var user = $(this).data('user');
                    var data = $('.form-pro').serializeObject();
                    data['action'] = 'twitterUserInfo';
                    data['id'] = user;
                    var queryString = $.param(data);
                    
                    window.open('?' + queryString, '_blank');
                    window.focus();
                    
                    // console.log(queryString);
                    return false;
                });
                // $(this).click(function() {
                //     sendProData("userInfo", '.btn-submit-form', false, { "sub": "userInfo", "id": user });
                //     return false;
                // });
            });
        });
        
    </script>
    
    <?php
    
    $data = ob_get_clean();
    // $data = htmlentities( (string) $data, ENT_QUOTES, 'utf-8', FALSE);
    // $data = utf8_encode($data);
    // $data = htmlspecialchars($data, ENT_COMPAT, 'ISO-8859-1');
    // $data = utf8_decode($data);
    // $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');
    
    $mess['ok'] = true;
    $mess['data'] = $data;
    exit(json_encode($mess));
}
catch (Exception $e) {
    $mess['ok'] = false;
    $mess['error'] = $e->getMessage();
    exit(json_encode($mess));
}
