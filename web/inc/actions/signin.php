<?php

$back = Link::Signin();
if ($_SESSION['Login']) {
    back($back, "");
}

$_SESSION['Form'] = $_POST;
$v = $_POST;
array_map("trim", $v);

$missing = array();
if (!$v['name']) {
    array_push($missing, 'inserire il nome');
}
if (!$v['surname']) {
    array_push($missing, 'inserire il cognome');
}
if (!$v['email']) {
    array_push($missing, "inserire l'indirizzo email");
} else if (!valemail($v['email'])) {
    array_push($missing, "l'indirizzo e-mail non è valido");
}
if (!$v['pass']) {
    array_push($missing, 'inserire la password');
} else if (strlen($v['pass']) < 6) {
    array_push($missing, 'la password deve essere lunga almeno 6 caratteri');
}
if ($v['pass'] != $v['pass2']) {
    array_push($missing, 'le due password non coincidono');
}

if(count($missing) > 0) {
    back($back, join(", ", $missing));
}

$email = addslashes($v['email']);
$query = "SELECT * FROM users WHERE email = '$email'";
if ($DB->querynum($query)) {
    e("address_already_registered", "", "", $email);
    back($back, "L'indirizzo e-mail fornito risulta gi&agrave; registrato");
}

$activation_code = generatePassword(8);

$dati = array();
// $dati['name'] = utf8_decode($v['name']);
// $dati['surname'] = utf8_decode($v['surname']);
$dati['name'] = $v['name'];
$dati['surname'] = $v['surname'];
$dati['email'] = $v['email'];
$dati['password'] = sha1($v['pass']);
$dati['active'] = 0;
$dati['activation'] = $activation_code;
$DB->queryinsert("users", $dati);

$ID = $DB->last_id;

l("user_registered", "", "", $v['email'], $DB->last_id);

$activation_link = "http://$Host/?action=activation&id=$ID&code=$activation_code";

// Send mail with the password
$mail = new PHPMailer();
$mail->SetFrom($settings["email"], $settings["title"]);
// $mail->AddAddress($v['email'], utf8_decode($v['name'])." ".utf8_decode($v['surname']));
$mail->AddAddress($v['email'], $v['name']." ".$v['surname']);
$mail->Subject = "Conferma registrazione";

$txt = sprintf(file_get_contents("templates/mail-ok.txt"), $v['email'], $activation_link, $activation_link);
$mail->AltBody = $txt;
$txt = nl2br($txt);
$txt = sprintf(file_get_contents("templates/mail.html"), $txt);

$mail->IsHTML(true);
//$mail->AddEmbeddedImage("templates/pokedem-mail.png", "pokedem-image", "pokedem-mail.png");
$mail->AddEmbeddedImage($settings["image"], "pokedem-image", $settings["image"]);
$mail->MsgHTML($txt);

if (!$mail->Send()) {
    back($back, "Errore nell'invio del messaggio di posta elettronica ({$mail->ErrorInfo})");
}


header("Location: ".Link::Registered());
exit();
