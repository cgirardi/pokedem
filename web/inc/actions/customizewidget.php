<?php

ob_start();

?>

<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function() {
        var type = $.url().fparam("type");
        var politician = $.url().fparam("id");
        
        if (type != undefined) {
            
            _gaq.push(['_trackEvent', 'CustomizeWidget', type]);
            
            $("#widgetType").val(type);
            if ($("#widgetType").val() != '') {
                $("#widgetType").trigger('change');
            }
            
            var topDiv = $('.widget-hidden');
            if ($(topDiv).find('.inputPolitician').parents('.control-group').is(':visible') && politician != undefined) {
                loadPolitician(politician, $('.widget-hidden').find('.inputPolitician'));
            }
            
            loadWidget('.widget-hidden div');
        }
    });
</script>
<h2>Widget personalizzati</h2>
<p>
    Se ti piace un particolare contenuto di <?= $settings['title'] ?>, puoi inserirlo all'interno del tuo sito/blog, in modo che tutti i tuoi utenti possano vederlo.
    La scelta è ampia: classifiche, tendenze, news, statistiche, grafici.
</p>
<p>
    <form onsubmit='return false;' class='form-inline'>
        Per iniziare, scegli il tipo di widget che vuoi includere:
        <select size='1' id='widgetType' onchange='return selectFirstStepWidget(this);'>
            <option value='' selected='selected'>[Seleziona]</option>
            <option value='allFlow'>Riepilogo</option>
           	<option value='piucitati'>Classifica #piùcitati</option>
            <option value='piusocial'>Classifica #piùsocial</option>
            <option value='piutwittati'>Classifica #piùtwittati</option>
            <!-- <option value='piuattivi'>Classifica #piùattivi</option> -->
            <option value='tendenze'>Tendenze</option>            
             <option value='temiBox'>Tendenze (news)</option>
            <option value='hashFromBox'>Tendenze (post social)</option>
            <option value='hashToBox'>Tendenze (tweet ricevuti)</option>
            <option value='newsFlow'>News</option>
            <option value='socialFlow'>Post social</option>
            <option value='tweetFlow'>Tweet ricevuti</option>
            <option value='chartNewsBox'>Grafico citazioni</option>
            <option value='chartSocialBox'>Grafico post social</option>
            <option value='chartTweetsBox'>Grafico tweet ricevuti</option>
            <option value='chartNewspapersPie'>Grafico fonti più frequenti</option>
            <option value='chartConnectionsPie'>Grafico connessioni più frequenti</option>
        </select>
    </form>
</p>

<div class='widget-hidden'>
    <hr />
    <div class='span6'>
        <h3>Impostazioni</h3>
        <hr />
        <form class="form-horizontal">
            
            <div class="control-group">
              <label class="control-label" for="inputPolitician">Nome<span style='color: red;'>*</span></label>
              <div class="controls politician-controls">
                <input type="text" class="inputPolitician" data-callback='loadWidget' placeholder='Digita il nome...'>
                <span class="help-inline"><a class='inputPolitician_cancel' href='#' data-callback='cancelWidget' onclick='return cancelPolitician(this);'>Cambia</a></span>
                <input type="hidden" class="inputPolitician_hidden" name='inputPolitician_hidden' value=''>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="inputWidth-1">Larghezza</label>
              <div class="controls">
                <input type="text" class="inputWidth" id="inputWidth-1" value='300' onchange='return loadWidget(this);'>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="inputHeight-1">Altezza</label>
              <div class="controls">
                <input type="text" id="inputHeight-1" class="inputHeight" value='300' onchange='return loadWidget(this);'>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="inputHeaderColor-1">Colore testata</label>
              <div class="controls">
                <select id='inputHeaderColor-1' class='inputHeaderColor' onchange='return loadWidget(this);'>
                    <option value='orange' selected='selected'>Arancione</option>
                    <option value='white'>Bianco</option>
                    <option value='black'>Nero</option>
                </select>
              </div>
            </div>
            
            <hr />
            
            <div class="control-group">
              <label class="control-label">Colore testo</label>
              <div class="controls">
                  <div class="input-append color colorpickami" data-color="#000000">
                    <input type="text" class="span2 inputTextColor" value='#000000' readonly />
                    <span class="add-on"><i></i></span>
                </div>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Colore link</label>
              <div class="controls">
                  <div class="input-append color colorpickami" data-color="#0088cc">
                    <input type="text" class="span2 inputLinkColor" value='#0088cc' readonly />
                    <span class="add-on"><i></i></span>
                </div>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Colore sfondo</label>
              <div class="controls">
                  <div class="input-append color colorpickami" data-color="#ffffff">
                    <input type="text" class="span2 inputBgColor" value='#ffffff' readonly />
                    <span class="add-on"><i></i></span>
                </div>
              </div>
            </div>
            
            <div class="control-group">
              <div class="controls">
                  <button class='btn' onclick='return loadWidget(this);'>Aggiorna colori</button>
              </div>
            </div>
            
            <hr />
            
            <div class='embed-code'>
                <p>
                    Codice da incorporare
                </p>
                <textarea onclick='$(this).select();'></textarea>
            </div>
        </form>
        <!-- <input type="text" class="span2 colorpickami" /> -->
    </div>
              
    <div class='span5'>
        <h3>
            <div class='widget-loading'>
            </div>
            Anteprima
            <div class='widget-loading'>
                <img src='img/loading-circle-ball.gif' />
            </div>
        </h3>
        <hr />
        <div class='widget-iframe-here'>
        </div>
        <p class='widget-no'>
            Completare i campi contrassegnati da <span style='color: red;'>*</span> per visualizzare l'anteprima del widget.
        </p>
    </div>
</div>

<!-- <div class='widget-hidden row' id='widget-generic'>
    Generic
</div> -->

<?php

$Text = ob_get_clean();

$Include = "tpl/dashboard.php";

