<?php

ob_start();

echo "<h3>#trendingtopic</h3>";

echo "<div id='sidebar'>";
echo $BM->politicianWidget("trendingPie", array("type" => $settings["rate_default"]));
echo "</div>";

$json = array(
    "sub" => "trendingTopics",
    "title" => "Trending topics delle ultime due ore"
);
$json = json_encode($json);

$new = "_gaq.push(['_trackEvent', 'Tendenze', 'Tendenze', document.location.href]);";
echo "<script type='text/javascript' charset='utf-8'>
$(document).ready(function() {
    $new
});
</script>";

echo "<div id='mainbar'>";
$link = Link::CustomizeWidget();
$link .= "#type=tendenze";
echo "<p class='pol-subtitle-small'>Vuoi i #trendingtopic sul tuo sito?
    <a href='$link' class='embed-tag-inline'>embed</a></p>";
echo $BM->lastTrending($settings["rate_default"]);
echo "<p>Questa cloud elenca i principali hashtag utilizzati su Twitter.</p>";
echo "</div>";

$Text = ob_get_clean();
$Include = "tpl/dashboard.php";
