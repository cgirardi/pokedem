<?php

$Sub = addslashes($_REQUEST['sub']);
$Chart = null;

$valid_types = array();

$PartyFilter = false;

switch ($Sub) {
	case 'piucitati':
		$PageTitle = $uimsg["piucitati_title"];

		$link = Link::CustomizeWidget();
		$link .= "#type=piucitati";

		$AfterStatsEmbed = "<p class='after_stats_embed'>".$uimsg["piucitati_subtitle"] ."</p>";
	    $AfterStatsLink = Link::Fonti();
	    $AfterStatsTitle = "Dati estratti da <a href='$AfterStatsLink'>$NumFonti media digitali</a> %s e aggiornati %s";

	    $Hashtag = "#piùcitati";

		$Chart = new PiuCitati();
		break;
	
	case 'piucommentati':
		$PageTitle = $uimsg["piucommentati_title"];
		
		$AfterStatsEmbed = "<p class='after_stats_embed'>".$uimsg["piucommentati_subtitle"] ."</p>";
	    $AfterStatsTitle = "Dati estratti da Facebook %s e aggiornati %s";

	    $Hashtag = "#piùcommentati";

	    $Chart = new PiuCommentati();
		$valid_types = $Chart->getValidTypes();
		break;
	
	case 'piusocial':
		$PageTitle = $uimsg["piusocial_title"];
		
		$link = Link::CustomizeWidget();
		$link .= "#type=piusocial";

		$AfterStatsEmbed = "<p class='after_stats_embed'>".$uimsg["piusocial_subtitle"] ."</p>";
	    $AfterStatsTitle = "Dati estratti da Twitter e Facebook %s e aggiornati %s";

		$Hashtag = "#piùsocial";

		$Chart = new PiuSocial();
		break;
		
	case 'piutwittati':
		$PageTitle = $uimsg["piutwittati_title"];

		$link = Link::CustomizeWidget();
		$link .= "#type=piutwittati";

		$AfterStatsEmbed = "<p class='after_stats_embed'>".$uimsg["piutwittati_subtitle"] ."</p>";
	    $AfterStatsTitle = "Dati estratti da Twitter %s e aggiornati %s";

	    $Hashtag = "#piùtwittati";

		$Chart = new PiuTwittati();
		break;
		
	case 'piuattivi':
		$PageTitle = $uimsg["piuattivi_title"];

		$AfterStatsEmbed = "<p class='after_stats_embed'>".$uimsg["piuattivi_subtitle"] ."</p>";
	    $AfterStatsTitle = "Dati estratti da Twitter %s e aggiornati %s";

	    $Hashtag = "#piùattivi";

		$Chart = new PiuAttivi();
		break;
		
	default:
		echo "Pagina non esistente";
		exit();
		break;
}

if ($link != "") {
	$AfterStatsTitle .= "<p class='after_stats_embed'>Vuoi inserire i ".$Hashtag." sul tuo sito? Clicca sul tasto <a href='$link' class='embed-tag-inline'>embed</a></p>";
}
$valid_types = $Chart->getValidTypes();
$DateTime = 0;
$LastUpdate = "in tempo reale";

if (preg_match("/^[0-9]+$/", $_REQUEST['datetime'])) {
    if ($_REQUEST['datetime'] <= time()) {
        $DateTime = $_REQUEST['datetime'];
        $LastUpdate = "alle <strong>".date("H:i", $DateTime)."</strong> del giorno <strong>".date("d/m/Y", $DateTime)."</strong>";
        $LastUpdate = "<span style='color: red;'>$LastUpdate</span>";
    }
}

$MetaFB = array();
$MetaFB['title'] = "I $Hashtag alle ".date("H:i", ($DateTime ? $DateTime : $DateTime));
$MetaFB['url'] = Link::Chart($Sub, $Type, ($DateTime ? $DateTime : $DateTime));
$descFB = array();

if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], $valid_types)) {
    $Type = $_REQUEST['type'];
    $_SESSION['lastHours'] = $Type;
} 
if (!isset($Type)) {
    if (!$_SESSION['lastHours']) {
	    $Type = $settings["rate_default"];
    } else {
	    $Type = $_SESSION['lastHours'];
    } 
}

$TwUrl = Link::Chart($Sub, $Type, $DateTime ? $DateTime : time());
$TwUrl_dt = Link::Chart($Sub, $Type, $DateTime ? $DateTime : 0);

$Party = 0;
if (is_numeric($_REQUEST['party'])) {
    $Party = $_REQUEST['party'];
}

$FiltersForParties = array();
if ($PartyFilter) {

	$parties = array();
	$active = false;

	$selected = "";
	if (!$Party) {
		$selected = "active";
		$active = true;
	}
	$FiltersForParties[] = "<a href='$TwUrl_dt' class='btn $selected'>[Tutti]</a>";

	$query = "SELECT * FROM tmp_partyeu WHERE disabled = '0'";
	$DB->query($query);

	while ($r = $DB->fetch()) {
		$selected = "";
		if ($Party == $r['id']) {
			$selected = "active";
			$active = true;
		}
		$link = $TwUrl_dt."?party={$r['id']}";
		$FiltersForParties[] = "<a href='$link' class='btn $selected'>{$r['party']}</a>";
	}

	if (!$active) {
		$PartyFilter = false;
	}

}

ob_start();

echo "<table class='table table-striped table-list-statistics'>";
echo "<tbody>";

// --

$Period = Chart::getTextForType($Type, true);
$AfterStatsTitle = sprintf($AfterStatsTitle, $Period, $LastUpdate);

$Chart->setType($Sub);
$Chart->setDefaultImage($settings["image_default"]);

if ($Is_test) {
	$rows = $Chart->run($Type, $DateTime);
}
else {
	$rows = $Chart->get($Type, $DateTime);
}

$pos = 0;

if ($Party && $PartyFilter) {
	foreach ($rows as $r) {
	    $pos++;

	    if ($r['partyid'] != $Party) {
	        continue;
	    }

	    if ($r['pos'] <= 3) {
	        $descFB[] = "$pos. {$r['name']} {$r['surname']}";
	    }

	    echo $Chart->printPoliticianRow($r, $pos, $Hashtag, $Period, $TwUrl);
	}

	$Infinity = false;
}
else {
	foreach ($rows as $r) {
	    $pos++;

	    if ($r['pos'] <= 3) {
	        $descFB[] = "$pos. {$r['name']} {$r['surname']}";
	    }

	    echo $Chart->printPoliticianRow($r, $pos, $Hashtag, $Period, $TwUrl);

	    if ($pos >= 30) {
	        break;
	    }
	}

	$Infinity = count($rows) > 30;
}

// --

echo "</tbody>";
echo "</table>";

if ($UsersLoginAccess == 1 && !$_SESSION['Login']) {
    $Infinity = false;
    echo "<p>Questa versione è limitata a visualizzare le prime 30 posizioni. Ne vuoi vedere di più? Registrati gratuitamente cliccando <a href='#' onclick='return triggerLogin(event);'>qui</a>.</p>";
}

$Text = ob_get_clean();

$MetaFB['description'] = implode("\r\n", $descFB);

// l("statistics");
$Include = $Folder_web."/tpl/all-stats.php";
$Active = $Sub;

