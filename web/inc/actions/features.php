<?php

ob_start();

?>

<h2>Registrarsi su <?= $settings["title"] ?>, perché...</h2>
<p>
    Se vuoi consultare un maggior numero di contenuti e attivare le funzionalità avanzate, puoi effettuare la registrazione gratuita sul nostro sito,
    cliccando sul tasto "Accedi" nel menu in alto.
</p>

<div class='row page-features'>
    <div class='span8'>
        <p class='title'>
            <i class="fa fa-quote-left"></i><br />
            Sei interessato a strumenti di analisi<br />
            e monitoraggio dei media online?
        </p>
        <table class='table'>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Non registrato</th>
                    <th>Registrato</th>
                    <th>Pro</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>News</td>
                    <td>ultime 20</td>
                    <td>ultime 50</td>
                    <td>tutte</td>
                </tr>
                <tr>
                    <td>Post social</td>
                    <td>ultimi 20</td>
                    <td>ultimi 50</td>
                    <td>tutti</td>
                </tr>
                <tr>
                    <td>Tweet ricevuti</td>
                    <td>ultimi 20</td>
                    <td>ultimi 50</td>
                    <td>tutti</td>
                </tr>
                <tr>
                    <td>Classifiche</td>
                    <td>primi 30</td>
                    <td>tutti</td>
                    <td>tutti</td>
                </tr>
                <tr>
                    <td>Entità seguite</td>
                    <td><i class="icon-remove"></i></td>
                    <td>fino a 10</td>
                    <td>illimitati</td>
                </tr>
                <tr>
                    <td>Notifiche settimanali</td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-ok"></i></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Notifiche giornaliere</td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-ok"></i></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Notifiche real-time</td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Filtri su utenti social <span title='Per ciascun utente Twitter/Facebook, &egrave; possibile filtrare i tweet in base agli hashtag usati e/o gli utenti retwittati'><i class='fa fa-question-circle'></i></span></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Profilazione utenti social <span title='Accesso al database di <?= $settings["title"] ?>'><i class='fa fa-question-circle'></i></span></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Strumenti di <em>intelligence</em> <span title='Utenti/hashtag che stanno supportando o danneggiando'><i class='fa fa-question-circle'></i></span></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-remove"></i></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr class='info'>
                    <td>&nbsp;</td>
                    <td>gratis</td>
                    <td>gratis</td>
                    <td>A richiesta</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><a class='btn btn-info' onclick="return triggerLogin(event || window.event);">Accedi o<br />registrati</a></td>
                    
                    <td><a href='http://<?= $settings["server"] ?>/blog/?p=101' class='btn btn-info'>Maggiori<br />informazioni</a></td>
                </tr>
                <!-- $($('.dropdown-toggle')[1]).trigger('click') -->
            </tbody>
        </table>
    </div>

    <div class='span4'>
        <p class='title'>
            <i class="fa fa-bullhorn"></i><br />
            Vuoi<br />
            aumentare la tua visibilit&agrave;?
        </p>
        <table class='table'>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Pagina pubblica consultabile</td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Presenza nelle classifiche</td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Presenza nei tweet di <?php echo $Tw_PD_name;?></td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Monitoraggio dei media online</td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr>
                    <td>Monitoraggio dei social network</td>
                    <td><i class="icon-ok"></i></td>
                </tr>
                <tr class='info'>
                    <td>&nbsp;</td>
                    <td>A richiesta</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><a href='http://<?= $settings["server"] ?>/blog/?p=89' class='btn btn-info'>Maggiori<br />informazioni</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php

$Text = ob_get_clean();

$Include = "tpl/dashboard.php";

