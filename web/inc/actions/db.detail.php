<?php

$ID = addslashes($_REQUEST['id']);
// Needed: $DB, $BM, $QP

// ---

$P = new Politician($ID);

$Active = $P->isActive();
$InCharts = $P->isInCharts();

if ($InCharts && !$Active && !$_SESSION['ProAll'] && !isset($_SESSION['ProPol'][$ID])) {
    echo $P->getPageHeader(true);

    echo "<hr />";
    echo "<div class='row-fluid'>";

    $socialInfo = $P->addSocialInfo();
    $ss = $P->getSexSuffix();

    $class = "span6 offset3";

    if ($socialInfo['twitter']) {
        $class = "span6";
        $TText = "Car$sex ";

        $TwUrl = Link::Politician($P->getID());
        $TText = "@{$socialInfo['twitter']} vorrei seguirti su ".$settings["title"] .", quando attivi il tuo profilo? $TwUrl";
        // $TButton_def = sprintf($TButtonUrl, urlencode($TText), urlencode($TwUrl));

        ?>

        <div class="span6 page-features">
            <div class='grey-box'>
                <p class="title">
                    <i class="fa fa-comment"></i><br>
                    Vorresti<br />
                    avere pi&ugrave; informazioni<br />
                    su <?php echo $P->getName(); ?>?
                </p>
                <p class='title'>
                    <!-- <?php echo $TButton_def; ?> Twittaglielo! -->
                    <a href="https://twitter.com/intent/tweet?via=".$Tw_PD_name."&amp;related=".$Tw_PD_name."&amp;text=<?php echo urlencode($TText); ?>">
                        <i class="fa fa-twitter" style='font-size: 1.2em; display: inline;'></i> Twittaglielo!
                    </a>
                </p>
            </div>
        </div>
        <?php
    }

    ?>

    <div class="<?php echo $class; ?> page-features">
        <div class='grey-box'>
            <p class="title">
                <i class="fa fa-user"></i><br>
                Sei <?php echo $P->getName(); ?><br />
                e vuoi farti conoscere meglio<br >
                dai tuoi elettori?
            </p>
            <p class='title'>
                <a href='http://<?= $settings["server"] ?>/blog/?p=89'>Maggiori informazioni</a>
                &#183;
                <a href='<?php echo Link::Features(); ?>'>Offerte</a>
            </p>
        </div>
    </div>

    <?php

    echo "</div>";
    // exit("Pagina in allestimento");
}
else {
    echo $P->getPageHeader();

    $SS = $_REQUEST['ss'];
    echo Politician::getTabs($SS, $ID);
    echo "<hr class='clear' />\n";

    $NumNews = 20;
    $ShowRegister = true;
    if ($_SESSION['Login']) {
        $NumNews = 50;
        $ShowRegister = false;
    }
    if ($_SESSION['Pro']) {
        $NumNews = 500;
        $ShowRegister = false;
    }

    $Page = 1;
    if ($_REQUEST['page']) {
        //$_SESSION['page'] = $_REQUEST['page'];
        $Page = $_REQUEST['page'];
    } 
    //else {
    //    if ($_SESSION['page']) {
    //        $Page = $_SESSION['page'];
    //    }
    //}

    switch ($SS) {
        
        // case "bio":
        // $Meta = $P->getMetaBio();
        // echo $P->getPageBio();
        // break;
        
        case "considerati":
        case "tweets":
        $Meta = $P->getMetaTweet();
        echo $P->getPageTweet($NumNews, $Page);
        break;
        
        case "comments":
        $Meta = $P->getMetaComments();
        echo $P->getPageComments($NumNews, $Page);
        break;
        
        case "social":
        $Meta = $P->getMetaSocial();
        echo $P->getPageSocial($NumNews, $Page);
        break;
        
        case "news":
        $Meta = $P->getMetaNews();
        echo $P->getPageNews($NumNews, $NumFonti, $Page);
        break;
        
        case "pro":
        echo $P->getPagePro();
        break;
        
        case "profb":
        echo $P->getPageProFB();
        break;
        
        default:
        header("Location: http://$Host/$ID/news");
        exit();
        // $Meta = $P->getMetaAll();
        // echo $P->getPageAll($NumNews, $NumFonti);
        break;
        
    }
}
