<?php 

if (!$_SESSION['Pro']) {
    $Text = "Non si hanno i permessi necessari, eseguire il login.";
    $Include = "tpl/dashboard.php";
    exit($Text);
}

$TornaBase = "/?action=proTwitter";
$TornaHome = "/?";

$ProTwitterTipi = array();
$ProTwitterTipi['select_random_one'] = "Selezione aleatoria di tweet da elenco";
$ProTwitterTipi['api_wordpress'] = "Post aleatorio da Wordpress";
$ProTwitterTipi['retweet'] = "Retweet di altro utente";
$ProTwitterTipi['massive_tweet'] = "Tweet massiccio";

$ProTwitterRetweetTipi = array();
$ProTwitterRetweetTipi['politicians'] = "Account di politici";
$ProTwitterRetweetTipi['attivisti'] = "Account di attivisti";

ob_start();
echo "<h2>Account Twitter</h2>";

switch ($_REQUEST['sub']) {
    case "twitterlogin":

        // echo "<pre>";

        $Tw_Signin = new TwitterOAuth($Tw_SigninPro_consumer_key, $Tw_SigninPro_consumer_secret, $_SESSION['Tw_SigninPro_oauth_token'], $_SESSION['Tw_SigninPro_oauth_token_secret']);
        $access_token = $Tw_Signin->getAccessToken($_REQUEST['oauth_verifier']);
        unset($_SESSION['Tw_SigninPro_oauth_token']);
        unset($_SESSION['Tw_SigninPro_oauth_token_secret']);
        // print_r($access_token);

        $UserConnection = new TwitterOAuth($Tw_SigninPro_consumer_key, $Tw_SigninPro_consumer_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
        // print_r($UserConnection);

        $api = "https://api.twitter.com/1.1/users/show.json?user_id={$access_token['user_id']}";
        $stuff = $UserConnection->get($api);
        $image = "";
        if (property_exists($stuff, "profile_image_url")) {
            $image = $stuff->profile_image_url;
        }

        // print_r($stuff);

        // exit();

        if ($access_token && $access_token['oauth_token'] && $access_token['oauth_token_secret'] && $image) {
            // TODO: check existence!

            $data = array();
            $data['user'] = $_SESSION['Login'];
            $data['tw_userid'] = $access_token['user_id'];
            $data['tw_username'] = $access_token['screen_name'];
            $data['oauth_access_token'] = $access_token['oauth_token'];
            $data['oauth_access_token_secret'] = $access_token['oauth_token_secret'];
            $data['use_twitter'] = 1;
            $data['tw_picture'] = $image;

            $DB->queryinsert("pro_twitteraccounts", $data);

            unset($_SESSION['Tw_SigninPro_oauth_token']);
            unset($_SESSION['Tw_SigninPro_oauth_token_secret']);

            back($TornaBase, "Utente inserito correttamente", 1);
        }
        else {
            back($TornaBase, "Errore di autenticazione");
        }

        break;

    case 'off':
    case 'on':
        $r = checkTwitterAccount($_REQUEST['id']);

        $DB->queryupdate("pro_twitteraccounts", array("active" => ($_REQUEST['sub'] == "on")), array("id" => $r['id']));
        back($TornaBase);
        break;
    
    case "add2":
        $_SESSION['form'] = $_REQUEST;
        $fields = check_twitteraccount("{$TornaBase}&sub=add");

        // @include_once($Folder_inc."/twitteroauth/twitteroauth/twitteroauth.php");
        $connection = new TwitterOAuth($fields['consumer_key'], $fields['consumer_secret'], $fields['oauth_access_token'], $fields['oauth_access_token_secret']);
        // $stuff = $connection->get("https://api.twitter.com/1.1/account/settings.json");
        // $stuff = $connection->get("https://api.twitter.com/1.1/statuses/home_timeline.json");
        $stuff = $connection->get("https://api.twitter.com/1.1/account/verify_credentials.json");

        if (property_exists($stuff, "errors") && !empty($stuff->errors)) {
            $msg = is_array($stuff->errors) ? $stuff->errors[0]->code." - ".$stuff->errors[0]->message : $stuff->errors;
            back("{$TornaBase}&sub=add", $msg);
        }

        // echo "<pre>";
        // print_r($stuff);
        // exit();

        $fields['tw_username'] = $stuff->screen_name;
        $fields['tw_picture'] = $stuff->profile_image_url;
        $fields['user'] = $_SESSION['Login'];
        $fields['active'] = 0;
        unset($fields['action']);
        unset($fields['sub']);

        if (!$DB->queryinsert("pro_twitteraccounts", $fields)) {
            back($TornaBad, "Inserimento non riuscito");
        }

        unset($_SESSION['form']);
        back($TornaBase, "Account inserito correttamente", 1);

        break;

    case "add":
        echo "<h3>Aggiungi account</h3>";
        echo form_twitteraccount("{$TornaBase}&sub=add2");
        break;

    case 'del':
        $r = checkTwitterAccount($_REQUEST['id']);

        $query = "UPDATE pro_twitteraccounts SET deleted = '1' WHERE id = '{$r['id']}'";
        $DB->query($query);

        back($TornaBase, "Account eliminato correttamente", 1);
        break;

    // Sub stuff

    case "preaddsub":
        $r = checkTwitterAccount($_REQUEST['id']);

        echo "<h3>Aggiungi operazione</h3>";
        // echo form_operation_select_random_one("{$TornaBase}&id={$r['id']}&sub=addsub2");
        echo form_operation_add("{$TornaBase}&id={$r['id']}&sub=addsub");
        break;

    case 'offsub':
    case 'onsub':
        $r = checkTwitterOperation($_REQUEST['id']);

        $DB->queryupdate("pro_scheduled", array("active" => ($_REQUEST['sub'] == "onsub")), array("id" => $r['id']));
        back($TornaBase);
        break;
    
    case 'delsub':
        $r = checkTwitterOperation($_REQUEST['id']);

        $query = "DELETE FROM pro_scheduled WHERE id = '{$r['id']}'";
        $DB->query($query);

        back($TornaBase);
        break;

    case "addsub2":
        $r = checkTwitterAccount($_REQUEST['id']);
        $Type = checkTwitterOperationType($_REQUEST['type']);
        $TornaBad = "{$TornaBase}&id={$r['id']}&sub=addsub&type={$Type}";

        $_SESSION['form'] = $_REQUEST;

        if ($Type == "massive_tweet") {

            $txt = $_REQUEST['tweet_text'];
            if (!$txt) {
                back($TornaBad, "Testo non inserito");
            }

            $tweet_expire = 2;
            if (preg_match("/$[0-9]+^/i", $_REQUEST['tweet_expire'])) {
                $tweet_expire = $_REQUEST['tweet_expire'];
            }

            $tweet_send_on_expiration = 0;
            if ($_REQUEST['tweet_send_on_expiration']) {
                $tweet_send_on_expiration = 1;
            }

            $attivisti = $_REQUEST['attivisti'];
            if (!is_array($attivisti) || !count($attivisti)) {
                back($TornaBad, "Non ci sono utenti selezionati");
            }

            $data = array();
            $data['id_account'] = $r['id'];
            $data['label'] = $Type;
            $data['interval'] = 0;
            $data['options'] = serialize($_REQUEST);
            $data['active'] = 0;

            if (!$DB->queryinsert("pro_scheduled", $data)) {
                back($TornaBad, "Inserimento non riuscito");
            }
            $ID = $DB->last_id;

            foreach ($attivisti as $user) {
                if (!preg_match("/^[0-9]+$/i", $user)) {
                    continue;
                }

                $query = "SELECT * FROM twitter_users WHERE id = '$user'";
                if (!$DB->querynum($query)) {
                    continue;
                }
                $rUser = $DB->fetch();

                $tweet = str_replace("@#user", "@{$rUser['username']}", $txt);
                if (strlen($tweet) > 140) {
                    continue;
                }

                $data = array();
                $data['schedule'] = $ID;
                $data['user'] = $rUser['username'];
                $data['userid'] = $rUser['id'];
                $data['text'] = $tweet;
                $data['send_on_expiration'] = $tweet_send_on_expiration ? 1 : 0;
                $data['expire_mins'] = $tweet_expire * 60 * 24;

                $DB->queryinsert("pro_scheduled_tweets", $data);
            }

            unset($_SESSION['form']);
            back($TornaBase, "Operazione inserita correttamente", 1);
            // print_r($_REQUEST);
        }
        else {
            if (!function_exists("check_operation_$Type")) {
                back($TornaBad, "Errore");
            }
            $fields = call_user_func("check_operation_$Type", $TornaBad);

            // print_r($fields);
            // exit();

            if (!isset($fields) || !count($fields)) {
                back($TornaBad, "Errore generico");
            }

            $data = array();
            $data['id_account'] = $r['id'];
            $data['label'] = $Type;
            $data['interval'] = $fields['interval'];
            $data['options'] = serialize($fields);
            $data['active'] = 0;

            if (!$DB->queryinsert("pro_scheduled", $data)) {
                back($TornaBad, "Inserimento non riuscito");
            }

            unset($_SESSION['form']);
            back($TornaBase, "Operazione inserita correttamente", 1);
        }

        break;

    case "addsub":
        $r = checkTwitterAccount($_REQUEST['id']);
        $Type = checkTwitterOperationType($_REQUEST['type']);

        $txt = $ProTwitterTipi[$Type];

        echo "<h3>Aggiungi operazione: $txt</h3>";
        if (!function_exists("form_operation_$Type")) {
            back($TornaBad, "Errore");
        }
        echo call_user_func("form_operation_$Type", "{$TornaBase}&sub=addsub2&type={$Type}&id={$r['id']}");

        break;

    default:
        echo "<p><a href='{$TornaBase}&sub=add' class='btn btn-success'>Inserisci nuovo account</a></p>";
        $query = "SELECT * FROM pro_twitteraccounts WHERE deleted = '0' AND user = '{$_SESSION['Login']}'";

        if ($DB->querynum($query, 2)) {
            while ($r = $DB->fetch_a(2)) {
                $active = $r['active'] ? 'success' : 'error';
                $actions = array();
                // $actions[] = "<a title='Modifica chiavi' class='btn btn-warning' href='#'><i class='icon-edit icon-white'></i></a>";
                if ($r['active']) {
                    $actions[] = "<a title='Disattiva' class='btn btn-inverse' href='{$TornaBase}&sub=off&id={$r['id']}'><i class='icon-off icon-white'></i></a>";
                }
                else {
                    $actions[] = "<a title='Riattiva' class='btn' href='{$TornaBase}&sub=on&id={$r['id']}'><i class='icon-off'></i></a>";
                }
                $actions[] = "<a title='Elimina account' class='a-confirm btn btn-danger' href='{$TornaBase}&sub=del&id={$r['id']}'><i class='icon-ban-circle icon-white'></i></a>";
                $actions[] = "<a title='Aggiungi operazione' class='btn btn-success' href='{$TornaBase}&sub=preaddsub&id={$r['id']}'><i class='icon-calendar icon-white'></i> <i class='icon-plus icon-white'></i></a>";

                $actionTxt = implode(" ", $actions);

                echo "<div class='media twitter-account-pro $active'>";
                echo "<a target='_blank' class='pull-left' href='http://www.twitter.com/{$r['tw_username']}'><img class='img-polaroid media-object' src='{$r['tw_picture']}' /></a>";
                echo "<div class='media-body'>";
                echo "<div class='row-fluid'>
                        <div class='pull-left username'><a target='_blank' class='pull-left' href='http://www.twitter.com/{$r['tw_username']}'>@{$r['tw_username']}</a></div>
                        <div class='pull-right'>$actionTxt</div>
                    </div>";
                $query = "SELECT * FROM pro_scheduled WHERE id_account = '{$r['id']}'";
                if ($DB->querynum($query, 4)) {
                    echo "<table class='table table-condensed table-bordered table-striped table-twitter-pro-sub'>
                        <thead>
                            <tr>
                                <th>
                                    Tipo
                                </th>
                                <th>
                                    Testo
                                </th>
                                <th>
                                    Frequenza (minuti)
                                </th>
                                <th>
                                    Ultimo avvio
                                </th>
                                <th>
                                    Attivo
                                </th>
                                <th>
                                    Azioni
                                </th>
                            </tr>
                        </thead>";
                    while ($rs = $DB->fetch(4)) {
                        $Opts = unserialize($rs['options']);
                        $last = "-";
                        if ($rs['last']) {
                            $last = date("d/m/Y H:i", strtotime($rs['last']));
                        }
                        $active = $rs['active'] ? "s&igrave;" : "no";

                        $actions = array();
                        // $actions[] = "<a title='Modifica operazione' class='btn btn-mini btn-warning' href='#'><i class='icon-edit icon-white'></i></a>";
                        $actions[] = "<a title='Elimina operazione' class='a-confirm btn btn-mini btn-danger' href='{$TornaBase}&sub=delsub&id={$rs['id']}'><i class='icon-ban-circle icon-white'></i></a>";
                        if ($rs['active']) {
                            $actions[] = "<a title='Disattiva' class='btn btn-mini btn-inverse' href='{$TornaBase}&sub=offsub&id={$rs['id']}'><i class='icon-off icon-white'></i></a>";
                        }
                        else {
                            $actions[] = "<a title='Riattiva' class='btn btn-mini' href='{$TornaBase}&sub=onsub&id={$rs['id']}'><i class='icon-off'></i></a>";
                        }
                        $actionTxt = implode(" ", $actions);

                        if ($rs['label'] == "massive_tweet") {
                            $tw = primi($Opts['tweet_text'], 35);

                            $query = "SELECT (`pro_scheduled_tweets`.`sent` IS NOT NULL) sent, COUNT(*) num
                                FROM `pro_scheduled_tweets`
                                WHERE schedule = '{$rs['id']}'
                                GROUP BY (`pro_scheduled_tweets`.`sent` IS NULL)";
                            $toBeSent = 0;
                            $alreadySent = 0;
                            $DB->query($query);
                            while ($rSent = $DB->fetch()) {
                                if ($rSent['sent']) {
                                    $alreadySent = $rSent['num'];
                                }
                                else {
                                    $toBeSent = $rSent['num'];
                                }
                            }
                            $tw .= " - Inviati: $alreadySent/".($toBeSent + $alreadySent);

                            $query = "SELECT * FROM pro_scheduled_tweets WHERE schedule = '{$rs['id']}' ORDER BY sent DESC LIMIT 1";
                            $rLast = $DB->queryfetch($query);
                            if ($rLast['sent']) {
                                $last = date("d/m/Y H:i", strtotime($rLast['sent']));
                            }

                            echo "<tr>
                                <td>{$rs['label']}</td>
                                <td>{$tw}</td>
                                <td>{$rs['interval']}</td>
                                <td>{$last}</td>
                                <td>{$active}</td>
                                <td>{$actionTxt}</td>
                            </tr>";
                        }
                        else {
                            echo "<tr>
                                <td>{$rs['label']}</td>
                                <td>{$Opts['label']}</td>
                                <td>{$rs['interval']}</td>
                                <td>{$last}</td>
                                <td>{$active}</td>
                                <td>{$actionTxt}</td>
                            </tr>";
                        }
                    }
                    echo "</table>";

                }
                else {
                    echo "<p>Non ci sono operazioni impostate.</p>";
                }
                echo "</div>";
                echo "</div>";
            }
        }
        else {
            echo "<p>Non ci sono account Twitter configurati.</p>";
        }

        break;
}

$Text = ob_get_clean();
$Include = "tpl/dashboard.php";
unset($_SESSION['form']);

function check_twitteraccount($torna, $v = array()) {
    if (empty($v)) {
        $v = $_SESSION['form'];
    }

    $v = array_map("trim", $v);

    if (!$v['consumer_key']) {
        back($torna, "Inserire il campo 'consumer_key'");
    }
    if (!$v['consumer_secret']) {
        back($torna, "Inserire il campo 'consumer_secret'");
    }
    if (!$v['oauth_access_token']) {
        back($torna, "Inserire il campo 'oauth_access_token'");
    }
    if (!$v['oauth_access_token_secret']) {
        back($torna, "Inserire il campo 'oauth_access_token_secret'");
    }

    return $v;
}

function form_twitteraccount($stringa, $v = array()) {
    global $TornaBase, $Host;
    global $Tw_SigninPro_consumer_key, $Tw_SigninPro_consumer_secret;

    if (empty($v)) {
        $v = $_SESSION['form'];
    }

    $Tw_Signin = new TwitterOAuth($Tw_SigninPro_consumer_key, $Tw_SigninPro_consumer_secret);

    $url = "http://$Host/?action=proTwitter&sub=twitterlogin";
    $request_token = $Tw_Signin->getRequestToken($url);

    $_SESSION['Tw_SigninPro_oauth_token'] = $token = $request_token['oauth_token'];
    $_SESSION['Tw_SigninPro_oauth_token_secret'] = $request_token['oauth_token_secret'];

    $twLoginUrl = $Tw_Signin->getAuthorizeURL($token);

    $modulo = new ModuloB($stringa, $v);
    $modulo->agg_text("consumer_key", "Consumer key:");
    $modulo->agg_text("consumer_secret", "Consumer secret:");
    $modulo->agg_text("oauth_access_token", "OAuth access token:");
    $modulo->agg_text("oauth_access_token_secret", "OAuth access token secret:");
    $modulo->agg_submit("<a class='btn btn-danger' href='$TornaBase'>Annulla</a> <a class='btn btn-info' href='$twLoginUrl'><i class='fa fa-twitter'></i> Autorizza account Twitter</a>");
    return $modulo->retrieve_html();
}

function form_operation_add($stringa, $v = array()) {
    global $ProTwitterTipi, $TornaBase;

    if (empty($v)) {
        $v = $_SESSION['form'];
    }

    $modulo = new ModuloB($stringa, $v = array());
    $modulo->agg_select("type", "Tipo:", $ProTwitterTipi);
    $modulo->agg_submit("<a class='btn btn-danger' href='$TornaBase'>Annulla</a>");
    return $modulo->retrieve_html();
}

function check_operation_select_random_one($torna, $v = array()) {
    if (empty($v)) {
        $v = $_SESSION['form'];
    }

    $v['interval'] = trim($v['interval']);
    if (!$v['interval']) {
        back($torna, "Inserire un intervallo");
    }
    if (!preg_match("/^[0-9]+$/", $v['interval'])) {
        back($torna, "Intervallo non valido (non numerico)");
    }
    if ($v['interval'] <= 0) {
        back($torna, "Intervallo non valido (negativo)");
    }

    if (!trim($v['label'])) {
        back($torna, "Inserire una descrizione");
    }

    if (!trim($v['tweets'])) {
        back($torna, "Campo non compilato");
    }

    $tmp_tweets = explode("\n", $v['tweets']);
    $tmp_tweets = array_map("trim", $tmp_tweets);
    $tmp_tweets = array_filter($tmp_tweets);

    if (empty($tmp_tweets)) {
        back($torna, "Non ci sono tweet");
    }

    if (count($tmp_tweets) > 100) {
        back($torna, "Ci sono piu' di 100 tweet");
    }
    $v['tweets'] = $tmp_tweets;

    return $v;
}

function form_operation_select_random_one($stringa, $v = array()) {
    global $TornaBase;

    if (empty($v)) {
        $v = $_SESSION['form'];
    }
    if (!isset($v['interval'])) {
        $v['interval'] = 60;
    }

    ob_start();

    echo "<p>Inserire i tweet da inviare, uno per riga. Le righe vuote saranno ignorate. Si possono inserire al massimo 100 tweet.</p>";

    $modulo = new ModuloB($stringa, $v);
    $modulo->agg_text("label", "Descrizione breve:");
    $modulo->agg_text("interval", "Intervallo di riproduzione:");
    $modulo->agg_textarea("tweets", "Tweets:");
    $modulo->agg_submit("<a class='btn btn-danger' href='$TornaBase'>Annulla</a>");
    echo $modulo->retrieve_html();

    return ob_get_clean();
}

function check_operation_api_wordpress($torna, $v = array()) {
    if (empty($v)) {
        $v = $_SESSION['form'];
    }

    $v['interval'] = trim($v['interval']);
    if (!$v['interval']) {
        back($torna, "Inserire un intervallo");
    }
    if (!preg_match("/^[0-9]+$/", $v['interval'])) {
        back($torna, "Intervallo non valido (non numerico)");
    }
    if ($v['interval'] <= 0) {
        back($torna, "Intervallo non valido (negativo)");
    }

    if (!trim($v['label'])) {
        back($torna, "Inserire una descrizione");
    }

    if (!trim($v['url'])) {
        back($torna, "Campo URL non compilato");
    }

    return $v;
}

function form_operation_api_wordpress($stringa, $v = array()) {
    global $TornaBase;
    
    if (empty($v)) {
        $v = $_SESSION['form'];
    }
    if (!isset($v['interval'])) {
        $v['interval'] = 60;
    }

    ob_start();

    echo "<p>Inserire l'URL dell'indirizzo JSON dove trovare i post Wordpress. Il JSON deve restituire le variabili
        <strong>twitter</strong> (nei <em>custom_field</em>), <strong>url</strong> e <strong>id</strong>.</p>";

    $modulo = new ModuloB($stringa, $v);
    $modulo->agg_text("label", "Descrizione breve:");
    $modulo->agg_text("interval", "Intervallo di riproduzione:");
    $modulo->agg_text("url", "URL blog Wordpress:");
    $modulo->agg_submit("<a class='btn btn-danger' href='$TornaBase'>Annulla</a>");
    echo $modulo->retrieve_html();

    return ob_get_clean();
}

function check_operation_retweet($torna, $v = array()) {
    global $ProTwitterRetweetTipi;

    if (empty($v)) {
        $v = $_SESSION['form'];
    }

    $v['interval'] = trim($v['interval']);
    if (!$v['interval']) {
        back($torna, "Inserire un intervallo");
    }
    if (!preg_match("/^[0-9]+$/", $v['interval'])) {
        back($torna, "Intervallo non valido (non numerico)");
    }
    if ($v['interval'] <= 0) {
        back($torna, "Intervallo non valido (negativo)");
    }

    if (!trim($v['label'])) {
        back($torna, "Inserire una descrizione");
    }

    if ($v['min_retweet'] && (!is_numeric($v['min_retweet']) || $v['min_retweet'] < 0)) {
        back($torna, "Numero di RT non valido");
    }
    if ($v['min_retweet_to_follow'] && (!is_numeric($v['min_retweet_to_follow']) || $v['min_retweet_to_follow'] < 0)) {
        back($torna, "Numero di RT non valido");
    }

    if (!isset($ProTwitterRetweetTipi[$v['type2']])) {
        back($torna, "Tipo non valido");
    }

    $politician = "";
    if (isset($v['politician']) && $v['politician'][0]) {
        $politician = $v['politician'][0];
    }
    else {
        unset($v['politician']);
    }

    $party = 0;
    if (isset($v['party']) && $v['party'][0]) {
        $party = $v['party'][0];
    }
    else {
        unset($v['party']);
    }

    // print_r($v);
    // exit();

    if (!$politician && !$party) {
        back($torna, "Selezionare il partito o il politico");
    }

    if ($v['type2'] == 'attivisti' && !$party) {
        back($torna, "Con gli attivisti deve essere scelto un partito");
    }

    if ($v['type2'] == 'attivisti') {
        unset($v['politician']);
    }

    $v['type'] = $v['type2'];
    unset($v['type2']);

    $v['follow_after_reweet'] = $v['follow_after_reweet'] ? 1 : 0;

    return $v;
}

function form_operation_massive_tweet($stringa) {
    global $TornaBase, $DB, $QP;

    ?>

    <style type="text/css">

    #tweet_textarea_counter {
        font-size: .7em;
        text-align: right;
    }

    .pro-tweet-container {
        padding: 0 10px;
    }

    .row-pro-clouds .vert-cloud {
        width: 100%;
        padding: 20px 10px;
        background-color: #eee;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #pro-party-results {
        height: 600px;
        overflow: auto;
        background-color: #ddd;
        margin-top: 20px;
        line-height: 1em;
        padding: 10px;
    }

/*    #pro-party-results .pro-twitter-single-user {
        padding: 5px;
        position: relative;
        height: 40px;
    }

    #pro-party-results .pro-twitter-single-user .check {
        position: absolute;
        top: 15px;
        left: 5px;
    }

    #pro-party-results .pro-twitter-single-user .image {
        height: 40px;
        width: 40px;
        background-size: cover;
        background-color: #ddd;
        position: absolute;
        top: 5px;
        left: 25px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #pro-party-results .pro-twitter-single-user .username {
        position: absolute;
        top: 7px;
        left: 70px;
    }

    #pro-party-results .pro-twitter-single-user .additional-info {
        position: absolute;
        top: 23px;
        left: 70px;
        font-size: .7em;
    }*/

    .row-pro-clouds h4 {
        text-align: center;
    }

    table.pro-twitter-table {
        font-size: .75em;
        width: 100%;
        table-layout: fixed;
    }

    table.pro-twitter-table .image {
        height: 20px;
        width: 20px;
        background-size: cover;
        background-color: #ddd;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    table.pro-twitter-table td {
        overflow: hidden;
    }

    table.pro-twitter-table .location,
    table.pro-twitter-table .name {
        font-size: .8em;
    }

    #pro-party-results p {
        font-size: .8em;
    }

    #pro-party-results p input {
        font-size: .8em;
        height: 12px;
        width: 30px;
    }

    #sex_select {
        width: 50px;
    }


    </style>

    <script type="text/javascript">

    function addTextToTextArea(textareaID, text) {
        var caretPos = document.getElementById(textareaID).selectionStart;
        var textAreaTxt = jQuery("#" + textareaID).val();
        var myChar = textAreaTxt.substring(caretPos - 1, caretPos);
        if (caretPos != 0 && myChar != ' ') {
            text = ' ' + text;
        }
        jQuery("#" + textareaID).val(textAreaTxt.substring(0, caretPos) + text + textAreaTxt.substring(caretPos));
        setCaretToPos(textareaID, caretPos + text.length);
        return false;
    }

    function setSelectionRange(textareaID, selectionStart, selectionEnd) {
        var input = document.getElementById(textareaID);
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        }
        else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    }

    function setCaretToPos(input, pos) {
        setSelectionRange(input, pos, pos);
    }

    var party = null;
    var region = null;
    var orderby = null;
    var desc = false;
    var sex = "";

    function searchUsersPro() {
        var div = $("#pro-party-results");
        div.block({message: "<img src='img/loadinfo.net.gif' />", css: {border: 0, background: "transparent"}});

        $.ajax({
            url: "/",
            dataType: 'json',
            data: {
                action: 'ajax',
                sub: 'listUsersByParty',
                party: party,
                orderby: orderby,
                desc: (desc ? "yes" : ""),
                sex: sex,
                region: region
            },
            success: function (data) {
                if (data.ok) {
                    div.html(data.data);
                    renewLinks();
                }
                else {
                    alert(data.error);
                }
            },
            complete: function() {
                div.unblock();
            }
        });

    }

    function renewLinks() {
        $("a.pro-twitter-filter").on("click", function() {
            orderbyNew = $(this).data("sort");

            if (orderby == orderbyNew) {
                desc = !desc;
            }
            else {
                orderby = orderbyNew;
                desc = false;
            }
            searchUsersPro();

            return false;
        });

        $("#pro-twitter-selectall").on("click", function() {
            $(this).parents("div").find("input:checkbox").prop("checked", "checked");
            return false;
        });

        $("#pro-twitter-deselectall").on("click", function() {
            $(this).parents("div").find("input:checkbox").prop("checked", "");
            return false;
        });

        $("#pro-twitter-selectsome").on("click", function() {
            var value = $("#pro-twitter-selectsome-input").val();
            var intValue = parseInt(value);
            if (intValue == Number.NaN || intValue <= 0) {
                alert("Inserire un numero valido");
                return false;
            }

            $(this).parents("div").find("input:checkbox").each(function(index, element) {
                if (index < intValue) {
                    $(element).prop("checked", "checked");
                }
                else {
                    $(element).prop("checked", "");
                }
            });
        });
    }

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function charLeft(id, max) {
        var txt = $(id).val();
        var left = max - txt.length;

        var count = (txt.split("@#user").length - 1);
        left = left - (count * 15);

        return left;
    }


    $(document).ready(function(){

        $("#pro-tweet-form").on("submit", function() {
            var formStuff = $(this).serializeObject();

            var users = formStuff['attivisti[]'];
            if (users == undefined || users.length == 0) {
                alert("Non ci sono utenti selezionati");
                return false;
            }

            var expiration = parseInt(formStuff.tweet_expire);
            if (expiration == Number.NaN || expiration <= 0) {
                alert("Inserire una scadenza valida");
                return false;
            }

            var txt = formStuff.tweet_text;
            if (txt == undefined || txt.length < 10) {
                alert("Testo non presente o troppo breve");
                return false;
            }

            var left = charLeft('#tweet_textarea', 140);
            if (left < 0) {
                alert("Testo troppo lungo");
                return false;
            }

            // console.log($(this).serializeObject());
            return true;
        });

        $(".row-pro-clouds a, code.add-to-textarea").on("click", function() {
            var text = $(this).text();
            addTextToTextArea("tweet_textarea", text);
            $("#tweet_textarea").trigger("keyup");
            return false;
        });

        $('#tweet_textarea').keyup(function () {
            var left = charLeft(this, 140);
            $('#tweet_textarea_counter').text('Caratteri rimasti: ' + left);
        });

        renewLinks();

        $("#btn_list_users_by_party").on("click", function() {
            party = $("#party_select").val();
            sex = $("#sex_select").val();
            region = $("#region_select").val();

            if (party == null || party == '' || party == 0) {
                alert("Selezionare un partito");
                return;
            }

            orderby = null;
            desc = false;

            searchUsersPro();

            return false;
        });

    });

    </script>

    <form class='form-inline' method='post' action='<?php echo "{$stringa}"; ?>' id='pro-tweet-form'>
    <div class='row-fluid'>
        <div class='span6'>
            <div class='pro-tweet-container'>
                <fieldset>
                    <label for='tweet_textarea'>
                        Tweet da inviare<br />
                        Usare <code class='add-to-textarea'>@#user</code> come segnaposto per l'utente destinatario.
                    </label>
                    <textarea id='tweet_textarea' rows='5' style='width: 100%;' name='tweet_text'></textarea>
                    <div id='tweet_textarea_counter'>Caratteri rimasti: 140</div>
                </fieldset>
                <div class='row-pro-clouds'>
                    <h4>Miei hashtag</h4>
                        <?php
                        $Cloud = new HashCloud($QP);
                        $Cloud->setClass("vert-cloud");
                        $Cloud->useTextArea("boh");
                        echo $Cloud->piutwittati(3582, 168);
                        ?>
                    <h4>Hashtag partito</h4>
                        <?php
                        $Cloud = new HashCloud($QP);
                        $Cloud->setClass("vert-cloud");
                        $Cloud->useTextArea("boh");
                        echo $Cloud->trendinghashesParty(38, 168);
                        ?>
                    <h4>Trending topic</h4>
                        <?php
                        $Cloud = new HashCloud($QP);
                        $Cloud->setClass("vert-cloud");
                        $Cloud->useTextArea("boh");
                        echo $Cloud->trendinghashes(2);
                        ?>
                </div>
            </div>
        </div>
        <div class='span6'>
            <label for='party_select'>
                Partito:
            </label>
            <select id='party_select'>
                <option value=''>[Seleziona]</option>
                <?php
                $query = "SELECT * FROM parties WHERE important = '1'";
                $DB->query($query, $DBid);
                while ($r = $DB->fetch($DBid)) {
                    echo "<option value='{$r['id']}'>{$r['goodname']}</option>";
                }
                ?>
                <!-- <option value='M5S'>M5S</option> -->
            </select>
            <label for='sex_select'>
                Sesso:
            </label>
            <select id='sex_select'>
                <option value=''></option>
                <option value='F'>F</option>
                <option value='M'>M</option>
            </select><br />
            <label for='region_select'>
                Regione:
            </label>
            <select id='region_select'>
                <option value=''>[Seleziona]</option>
                <?php
                $query = "SELECT * FROM georegions ORDER BY name";
                $DB->query($query, $DBid);
                while ($r = $DB->fetch($DBid)) {
                    echo "<option value='{$r['id']}'>{$r['name']}</option>";
                }
                ?>
                <option disabled='disabled'>──────────</option>
                <option value='8,9,12,19'>Nord-ovest</option>
                <option value='5,6,17,20'>Nord-est</option>
                <option value='7,10,16,18'>Centro</option>
                <option value='1,2,3,4,11,13'>Sud</option>
                <option value='14,15'>Isole</option>
            </select>
            <button type="button" class="btn" id="btn_list_users_by_party">Cerca</button>
            <div id='pro-party-results'>
            </div>
        </div>
    </div>
    <div class='row-fluid' style='margin-top: 20px;'>
        <div class='span6'>
            <div class='pro-tweet-container'>
                <label for='tweet_textarea'>
                    Scadenza dei tweet (in giorni):
                </label>
                <input title='Giorni di attesa per inviare il tweet all&#39;utente in modo che l&#39;invio avvenga poco dopo un suo tweet'
                    type='text' value='5' name='tweet_expire' id='tweet_expire' />
            </div>
        </div>
        <div class='span6'>
            <div class='pro-tweet-container'>
                <label class="checkbox">
                    <input title='Selezionare questa casella per inviare il tweet all&#39;utente anche dopo la scadenza, senza aspettare un suo tweet'
                    type="checkbox" name='tweet_send_on_expiration' id='tweet_send_on_expiration'> Invio massiccio dopo la scadenza
                </label>
            </div>
        </div>
    </div>
    <div class='row-fluid' style='margin-top: 20px;'>
        Attenzione! Premendo il tasto "Invia", l'utente si assume la totale responsabilità dell'invio di tweet agli utenti selezionati.
        PokeDem non è in alcun modo responsabile del contenuto dei tweet inviati.
    </div>
    <div class='row-fluid' style='margin-top: 20px;'>
        <div class='offset10 span2'>
            <input type='submit' class='btn bnt-success' value='Invia' />
        </div>
    </div>
    </form>

    <?php
}

function form_operation_retweet($stringa, $v = array()) {
    global $TornaBase, $DB, $ProTwitterRetweetTipi, $Default_rt_min_rt, $Default_min_rt_to_follow;
    $DBid = md5("form_operation_retweet");
    
    if (empty($v)) {
        $v = $_SESSION['form'];
    }
    if (!isset($v['interval'])) {
        $v['interval'] = 60;
    }
    if (!isset($v['min_retweet'])) {
        $v['min_retweet'] = $Default_rt_min_rt;
    }
    if (!isset($v['min_retweet_to_follow'])) {
        $v['min_retweet_to_follow'] = $Default_min_rt_to_follow;
    }

    ob_start();

    // echo "<p>Inserire l'URL dell'indirizzo JSON dove trovare i post Wordpress. Il JSON deve restituire le variabili
    //     <strong>twitter</strong> (nei <em>custom_field</em>), <strong>url</strong> e <strong>id</strong>.</p>";

    $Parties =array();
    $Parties[0] = "[Nessun partito selezionato]";
    $query = "SELECT * FROM parties WHERE important = '1'";
    $DB->querynum($query, $DBid);
    while ($r = $DB->fetch($DBid)) {
        $Parties[$r['id']] = $r['goodname'];
    }

    // print_r($v);

    $modulo = new ModuloB($stringa, $v);
    $modulo->agg_text("label", "Descrizione breve:");
    $modulo->agg_text("interval", "Intervallo di riproduzione:");
    $modulo->agg_select("type2", "Tipo:", $ProTwitterRetweetTipi);

    $modulo->agg_text("min_retweet", "Minimo numero di RT presenti:");
    $modulo->agg_check("follow_after_reweet", "Segui dopo il RT");
    $modulo->agg_text("min_retweet_to_follow", "Minimo numero di RT per follow:");

    $politician = "";
    if (isset($v['politician']) && $v['politician'][0]) {
        $politician = $v['politician'][0];
    }
    $modulo->agg_politician("politician[]", "Politico:", $politician);

    $party = 0;
    if (isset($v['party']) && $v['party'][0]) {
        $party = $v['party'][0];
    }
    $modulo->agg_select("party[]", "Partito:", $Parties, $party);

    // $modulo->agg_text("url", "URL blog Wordpress:");
    $modulo->agg_submit("<a class='btn btn-danger' href='$TornaBase'>Annulla</a>");
    echo $modulo->retrieve_html();

    return ob_get_clean();
}

function checkTwitterOperationType($Type) {
    global $ProTwitterTipi, $TornaHome;
    if (!isset($ProTwitterTipi[$Type])) {
        back($TornaHome, "Errore");
    }

    return $Type;
}

function checkTwitterAccount($id) {
    global $TornaHome;

    $r = exists("pro_twitteraccounts", $id, $TornaHome);
    checkUser($r['user']);

    return $r;
}

function checkTwitterOperation($id) {
    global $TornaHome;
    $r = exists("pro_scheduled", $id, $TornaHome);
    checkTwitterAccount($r['id_account']);
    return $r;
}

function checkUser($id) {
    global $TornaHome;

    if ($id != $_SESSION['Login']) {
        back($TornaHome, "Errore di permessi");
    }
}
