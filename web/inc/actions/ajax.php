	<?php

	ini_set("memory_limit", "8192M");
	$Not_allowed_ajax_calls = array("pertinence", "changeState", "notification");

	if (!$_SESSION['Login'] && in_array($_REQUEST['sub'], $Not_allowed_ajax_calls)) {
	    $_SESSION['Message'] = getMessage("Area riservata: per accedere, occorre essere registrati");
	    header("Location: ".Link::Login());
	    exit();
	}

	ob_start();
	$mess = array();

	// global $MaxNumOfPoliticians;
global $DB;
	switch ($_REQUEST['sub']) {
	    
	    case "charts":
	    $Sub = addslashes($_REQUEST['which']);
	    switch ($Sub) {
		case 'piucitati':
		    $Hashtag = "#piùcitati";
		    $Chart = new PiuCitati();
		    break;

		case 'piusocial':
		    $Hashtag = "#piùsocial";
		    $Chart = new PiuSocial();
		    break;
		    
		case 'piutwittati':
		    $Hashtag = "#piùtwittati";
		    $Chart = new PiuTwittati();
		    break;
		    
		/*
		case 'piuattivi':
		    $Hashtag = "#piùattivi";
		    $Chart = new PiuAttivi();
		    break;

		case 'piucitatiEU':
		    $Hashtag = "#piùcitati #Europee2014";
		    $Chart = new PiuCitatiEU();
		    break;
		
		case 'piusocialEU':
		    $Hashtag = "#piùsocial #Europee2014";
		    $Chart = new PiuSocialEU();
		    break;
		    
		case 'piutwittatiEU':
		    $Hashtag = "#piùtwittati #Europee2014";
		    $Chart = new PiuTwittatiEU();
		    break;
		*/    
		default:
		    echo "Pagina non esistente";
		    exit();
		    break;
	    }

	    $valid_types = $Chart->getValidTypes();
	    if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], $valid_types)) {
		$Type = $_REQUEST['type'];
	    }
	    if (!isset($Type)) {
		$Type = 2;
	    }
	    $TwUrl = Link::Chart($Sub, $Type, time());

	    if ($Type <= 24) {
		$Period = "nelle ultime $Type ore";
	    }
	    else {
		$days = $Type / 24;
		$Period = "negli ultimi $days giorni";
	    }

	    $DateTime = 0;

	    if (preg_match("/^[0-9]+$/", $_REQUEST['datetime'])) {
		if ($_REQUEST['datetime'] <= time()) {
		    $DateTime = $_REQUEST['datetime'];
		}
	    }

	    $Start = 0;
	    if ($_REQUEST['start'] && preg_match("/^[0-9]+$/", $_REQUEST['start'])) {
		$Start = $_REQUEST['start'];
	    }

	    $Chart->setType($Sub);
	    $Chart->setDefaultImage($settings["image_default"]);

	    if ($Is_test) {
		    $rows = $Chart->run($Type, $DateTime);
	    } else {
		    $rows = $Chart->get($Type, $DateTime);
	    }

	    $Party = 0;
	    if (is_numeric($_REQUEST['party'])) {
		    $Party = $_REQUEST['party'];
	    }

	    ob_start();

	    if ($Party) {
		$pos = 0;

		foreach ($rows as $r) {
		    $pos++;

		    if ($r['partyid'] != $Party) {
			continue;
		    }

		    // if ($pos <= $Start) {
		    //     continue;
		    // }

		    echo $Chart->printPoliticianRow($r, $pos, $Hashtag, $Period, $TwUrl);

		    // if ($pos >= 30 + $Start) {
		    //     break;
		    // }
		}

		$continue = false;
	    }
	    else {
		$pos = 0;

		foreach ($rows as $r) {
		    $pos++;

		    if ($pos <= $Start) {
			continue;
		    }

		    echo $Chart->printPoliticianRow($r, $pos, $Hashtag, $Period, $TwUrl);

		    if ($pos >= 30 + $Start) {
			break;
		    }
		}

		$continue = (count($rows) > (30 + $Start));
	    }

	    $mess['ok'] = true;
	    $mess['data'] = ob_get_clean();
	    $mess['next'] = $Start + 30;
	    $mess['continue'] = $continue;
	    // $mess['num'] = $N;
	    // $mess['oldnum'] = $oldnum;
	    // $mess['oldpos'] = $oldpos;
	    // $mess['query'] = $query;
	    
	    exit(json_encode($mess));
	    break;


	    case "listUsersByParty":
		$mess['ok'] = false;

		if (!$_SESSION['Pro']) {
		    $mess['error'] = "Non si hanno i permessi necessari, eseguire il login.";
		    exit(json_encode($mess));
		}

		$Party = $_REQUEST['party'];
		if (!is_numeric($Party)) {
		    $mess['error'] = "Partito non valido.";
		    exit(json_encode($mess));
		}

		$query = "SELECT * FROM parties WHERE id = '$Party' AND important = '1'";
		if (!$DB->querynum($query)) {
		    $mess['error'] = "Partito non valido (imp).";
		    exit(json_encode($mess));
		}

		$orderby = "";
		if (isset($_REQUEST['orderby']) && preg_match("/^[a-z_]+$/i", $_REQUEST['orderby'])) {
		    $orderby = $_REQUEST['orderby'];
		}
		$desc = $_REQUEST['desc'] ? "DESC" : "";

		$sex = "";
		if (isset($_REQUEST['sex']) && preg_match("/^(M|F)$/i", $_REQUEST['sex'])) {
		    $sex = $_REQUEST['sex'];
		}

		$region = "";
		if (isset($_REQUEST['region']) && preg_match("/^[0-9,]+$/i", $_REQUEST['region'])) {
		    $region = $_REQUEST['region'];
		}

		if (!$orderby) {
		    $orderby = "id";
		}

		$Max = 1000;

		$Where = array();
		$Where[] = "a.party_$Party / a.total > .5";
		$Where[] = "a.party_$Party > 1";
		if ($sex) {
		    $Where[] = "u.sex = '$sex'";
		}
		if ($region) {
		    $Where[] = "u.region IN ($region)";
		}
		$WhereStr = implode(" AND ", $Where);

		$query = "SELECT DISTINCT u.id, u.username, u.name, u.picture, u.followers, u.location, u.sex,
		    SUM( a.party_$Party * a.followers * ( 1 / EXP( (
			SELECT MAX( id ) 
			FROM weeks ) - week ) )
			) influence
		    FROM attivisti_rt a
		    INNER JOIN twitter_users u ON u.id = a.user_id
		    WHERE $WhereStr
		    GROUP BY u.id
		    ORDER BY $orderby $desc";
		$mess['num'] = $DB->querynum($query);
		$mess['query'] = $query;

		ob_start();
		// echo "<p>$query</p>";

		$headers = array();
		$headers[] = "<th width='20%'><a title='Clicca qui per ordinare i risultati in base al nome utente Twitter' class='pro-twitter-filter' href='javascript: void(0)' data-sort='username'>Username</a></th>";
		$headers[] = "<th width='20%'><a title='Clicca qui per ordinare i risultati in base al nome' class='pro-twitter-filter' href='javascript: void(0)' data-sort='name'>Nome</a></th>";
		$headers[] = "<th width='10%'><a title='Clicca qui per ordinare i risultati in base al numero di follower' class='pro-twitter-filter' href='javascript: void(0)' data-sort='followers'>Foll.</a></th>";
		$headers[] = "<th width='10%'><a title='Clicca qui per ordinare i risultati in base al valore di influenza che ha l&#39;utente rispetto al partito' class='pro-twitter-filter' href='javascript: void(0)' data-sort='influence'>Infl.</a></th>";
		$headers[] = "<th width='5%'><a title='Clicca qui per ordinare i risultati in base al sesso dell&#39;utente' class='pro-twitter-filter' href='javascript: void(0)' data-sort='sex'>S</a></th>";
		$headers[] = "<th width='25%'><a title='Clicca qui per ordinare i risultati in base alla regione di provenienza' class='pro-twitter-filter' href='javascript: void(0)' data-sort='location'>Luogo</a></th>";

		echo "<p>
		    Risultati trovati: {$mess['num']} - Visualizzati: ".min($mess['num'], $Max)."
		    | Seleziona:
		    <a title='Seleziona tutti gli utenti visualizzati' href='javascript: void(0)' id='pro-twitter-selectall'>tutti</a> -
		    <a title='Deseleziona tutti gli utenti' href='javascript: void(0)' id='pro-twitter-deselectall'>nessuno</a> -
		    <a title='Seleziona solo i primi utenti della lista visualizzata' href='javascript: void(0)' id='pro-twitter-selectsome'>primi</a> <input id='pro-twitter-selectsome-input' type='text' value='100' />
		    </p>";
		$i = 0;
		echo "<table class='table table-condensed pro-twitter-table'>";
		echo "<thead><tr>
		    <th width='5%'>&nbsp;</th>
		    <th width='5%'>&nbsp;</th>
		    ".implode("\n", $headers)."
		    </tr></thead>";
		while ($r = $DB->fetch()) {
		    $i++;

		    if (!$r['sex'] || $r['sex'] == "N") {
			$r['sex'] = "";
		    }
			$image = $r['picture'];
			//if ($image == "") {
        		   $image = $settings["image_default"];
        		//}
        	
		    echo "<tr>
			<td><input title='Usa questa casella per inviare il tweet all&#39;utente' type='checkbox' id='attivista_{$r['id']}' name='attivisti[]' value='{$r['id']}' /></td>
			<td><div class='image' style='background-image: url(".$image.");'></div></td>
			<td class='username'><a title='Profilo Twitter ufficiale dell&#39;utente' target='_blank' href='http://www.twitter.com/{$r['username']}'>{$r['username']}</a></td>
			<td class='name'>{$r['name']}</td>
			<td class='followers'>".thousandsCurrencyFormat($r['followers'])."</td>
			<td class='influence'>".thousandsCurrencyFormat($r['influence'])."</td>
			<td class='sex'>{$r['sex']}</td>
			<td class='location'>{$r['location']}</td>
			</tr>";

		    // echo "<div class='pro-twitter-single-user'>
		    //         <div class='check'><input type='checkbox' /></div>
		    //         <div class='image' style='background-image: url(\"{$r['picture']}\")'></div>
		    //         <span class='username'>{$r['name']} ({$r['username']})</span>
		    //         <span class='additional-info'>Followers: {$r['followers']}</span>
		    //     </div>";

		    if ($i >= $Max) {
			break;
		    }
		}
		echo "</table>";

		$mess['ok'] = true;
		$mess['data'] = ob_get_clean();
		exit(json_encode($mess));

		break;


	    case "tweetUserInfo":
		$mess['ok'] = false;

		if (!$_SESSION['Pro']) {
		    $mess['error'] = "Non si hanno i permessi necessari, eseguire il login.";
		    exit(json_encode($mess));
		}
		
		$data = array();
		foreach ($_REQUEST as $index => $value) {
		    if (preg_match("/^query-(.*)/", $index, $r)) {
			$data[$r[1]] = $value;
		    }
		}
		$data = array_map("addslashes", $data);
		
		$userID = getUserByNameId($data);
		if (!$userID) {
		    $mess['error'] = "Utente non valido.";
		    exit(json_encode($mess));
		}

		$data['use-filter'] = false;

		$mess['ok'] = true;
		$mess['data'] = getTwitterUserInfo($userID, $data);

		exit(json_encode($mess));
		break;

	    // Deprecated
	    case "userInfo":
		
		if (!isProEnabled()) {
		    exit();
		}
		
		ob_start();
		
		$userID = addslashes($_REQUEST['id']);
		if (!is_numeric($userID)) {
		    exit();
		}
	    
		$data = array();
		foreach ($_REQUEST as $index => $value) {
		    if (preg_match("/^query-(.*)/", $index, $r)) {
			$data[$r[1]] = $value;
		    }
		}
		$data = array_map("addslashes", $data);
		
		?>
		
		<p>
		    <a href='#' class='link-back'><i class='fa fa-arrow-left'></i> Torna indietro</a>
		</p>

		<script type="text/javascript" charset="utf-8">
		    $('.link-back').click(function() {
			$('.btn-submit-form').click();
			return false;
		    });
		</script>
		
		<?php
		
		$data['use-filter'] = false;
		echo getTwitterUserInfo($userID, $data);
		
		$mess['ok'] = true;
		$mess['data'] = ob_get_clean();
		exit(json_encode($mess));
	    
		break;
	    
	    case "polProStats":
		include("polProStats.php");
		break;
	    
	    case "polProStatsFB":
		include("polProStatsFB.php");
		break;
	    
	    case "newsFlow":
	    case "tweetFlow":
	    case "commentFlow":
	    case "socialFlow":
	    case "allFlow":
		if ($_REQUEST['sub'] == "newsFlow" || $_REQUEST['sub'] == "commentFlow") {
                     $DB->forceEncoding("utf8mb4");
                }
		$data = array();
		foreach ($_REQUEST as $index => $value) {
		    $data[$index] = addslashes($value);
		}
		
		if (!$_SESSION['Login']) {
		    $data['limit'] = 20;
		}
		if ($_SESSION['Login'] &&  !$_SESSION['Pro']) {
		    $data['limit'] = 50;
		}
		if (isset($data['class'])) {
		    unset($data['class']);
		}
		$mess['ok'] = true;
                $mess['data'] = $BM->$_REQUEST['sub']($data);
	  	exit(json_encode($mess));
	    break;
	    
	    case "proQuery":
	    
		try {
		    
		    $data = array();
		    
		    foreach ($_REQUEST as $index => $value) {
			if (preg_match("/^query-(.*)/", $index, $r)) {
			    $data[$r[1]] = $value;
			}
		    }
		    
		    if (!isProEnabled($data['id'])) {
			throw new Exception("Utente non abilitato");
		    }
		    $P = new Politician($data['id']);
		
		    switch ($_REQUEST['page']) {
			case "news":
			if ($data['when-from'] && !strtotime($data['when-from'])) {
			    throw new Exception("Campo 'da' non valido");
			}
			if ($data['when-to'] && !strtotime($data['when-to'])) {
			    throw new Exception("Campo 'a' non valido");
			}

			if ($_REQUEST['export']) {
			    $data['limit'] = 0;
			    $query = $BM->newsQuery($data);
			    giveExport($query);
			}
			else {
			    $mess['ok'] = true;
			    $mess['html'] = $BM->newsFlow($data);
                }
                break;
                
                case "social":
                if ($data['when-from'] && !strtotime($data['when-from'])) {
                    throw new Exception("Campo 'da' non valido");
                }
                if ($data['when-to'] && !strtotime($data['when-to'])) {
                    throw new Exception("Campo 'a' non valido");
                }
                
                if ($_REQUEST['export']) {
                    $data['limit'] = 0;
                    $query = $BM->newsQuery($data);
                    giveExport($query);
                }
                else {
                    $mess['ok'] = true;
                    $mess['html'] = $BM->socialFlow($data);
                    // $mess['query'] = str_replace("\n", " ", $BM->socialQuery($data));
                }
                break;
            }
            
            exit(json_encode($mess));
        }
        catch (Exception $e) {
            $mess['ok'] = false;
            $mess['error'] = $e->getMessage();
            exit(json_encode($mess));
        }
        break;
    
    case "forgetNews":
        $_SESSION['forgetNews'] = 1;
        $mess['ok'] = true;
        exit(json_encode($mess));
        break;
    
    case "getPoliticianById":
        $ID = addslashes($_REQUEST['id']);
        if (!is_numeric($ID)) {
            $mess['ok'] = false;
            $mess['error'] = "ID non valido";
            
            exit(json_encode($mess));
        }
        
        $P = new Politician($ID);
        $mess['name'] = $P->getName();
        $mess['ok'] = true;
        exit(json_encode($mess));
        
        break;
        
    case "notification":
        $ID = addslashes($_REQUEST['id']);
        $remove = ($_REQUEST['remove'] == "true" ? 1 : 0);
        $type = addslashes($_REQUEST['type']);
        
        if (!is_numeric($ID)) {
            $mess['ok'] = false;
            $mess['error'] = "ID non valido (1)";
            exit(json_encode($mess));
        }
        
        if (!isset($FollowedPoliticians[$ID])) {
            $mess['ok'] = false;
            $mess['error'] = "ID non valido (2)";
            exit(json_encode($mess));
        }
        
        if (!isset($_SESSION['Login']) || !$_SESSION['Login']) {
            $mess['ok'] = false;
            $mess['error'] = "Utente non valido";
            exit(json_encode($mess));
        }
        
        $types = array(1, 2, 3);
        if (!in_array($type, $types)) {
            $mess['ok'] = false;
            $mess['error'] = "Invalid type";
            exit(json_encode($mess));
        }
        
        $field = "";
        switch ($type) {
            case 1: $field = "notification_real";
            if (!$_SESSION['Pro']) {
                $mess['ok'] = false;
                $mess['error'] = "User is not Pro";
                exit(json_encode($mess));
            }
            break;
            
            case 2: $field = "notification_day"; break;
            case 3: $field = "notification_week"; break;
        }
        
        if (!$field) {
            // This should never happen
            $mess['ok'] = false;
            $mess['error'] = "Invalid field";
            exit(json_encode($mess));
        }
        
        $data = array();
        $data[$field] = (isset($remove) && $remove) ? 0 : 1;
        $DB->queryupdate("user_politician", $data, array("user" => $_SESSION['Login'], "politician" => $ID));
        
        // $mess['ok'] = false;
        // $mess['error'] = $DB->W_last_query;
        // exit(json_encode($mess));
        
        $tick = false;
        $query = "SELECT * FROM user_politician WHERE user = '{$_SESSION['Login']}' AND politician = '{$ID}'";
        if ($DB->querynum($query)) {
            $R = $DB->fetch();
            $tick = $R[$field] ? true : false;
        }
        
        $mess['ok'] = true;
        $mess['tick'] = $tick;
        exit(json_encode($mess));
        
        break;
    
    case "trendingPie":
        $type = addslashes($_REQUEST['type']);
    
        if (!is_numeric($type)) {
            $mess['ok'] = false;
            $mess['error'] = "Invalid type";
            exit(json_encode($mess));
        }
    
        $mess['ok'] = true;
        $mess['data'] = $BM->$_REQUEST['sub']($_REQUEST);
    
        exit(json_encode($mess));
        break;
    
    case "temiBox":
    case "chartNewsBox":
    case "chartNewspapersPie":
    case "chartConnectionsPie":
    case "hashFromBox":
    case "chartSocialBox":
    case "hashToBox":
    case "chartTweetsBox":
    
        $ID = addslashes($_REQUEST['id']);
    
        if (!is_numeric($ID)) {
            $mess['ok'] = false;
            $mess['error'] = "Invalid ID";
            exit(json_encode($mess));
        }
    
        $mess['ok'] = true;
        $mess['data'] = $BM->$_REQUEST['sub']($_REQUEST);
    
        exit(json_encode($mess));
        break;
    
    
    case "updatetrending":
        $type = $_REQUEST['type'];
        if (!preg_match("/^[0-9]+$/", $type)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect type";
            exit(json_encode($mess));
        }
    
        $mess['ok'] = true;
        $Cloud = new HashCloud($QP);
        $Cloud->setHowmany(20);
        $Cloud->setNoTags("Nessun tag rilevante");
        $mess['data'] = $Cloud->trendinghashes($type);
    
        exit(json_encode($mess));
        break;
    
    
    case "updatekeywords":
        $politician = $_REQUEST['politician'];
        if (!preg_match("/^[0-9]+$/", $politician)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect politician";
            exit(json_encode($mess));
        }
        $type = $_REQUEST['type'];
        if (!preg_match("/^[0-9]+$/", $type)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect type";
            exit(json_encode($mess));
        }
    
        $mess['ok'] = true;
    
        $Cloud = new HashCloud($QP);
        $Cloud->setClass("vert-cloud");
        $mess['data'] = $Cloud->keywords($politician, $type);
    
        exit(json_encode($mess));
    
        break;
    
    
    case "updatesocialtags":
        $politician = $_REQUEST['politician'];
        if (!preg_match("/^[0-9]+$/", $politician)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect politician";
            exit(json_encode($mess));
        }
        $type = $_REQUEST['type'];
        if (!preg_match("/^[0-9]+$/", $type)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect type";
            exit(json_encode($mess));
        }
    
        $mess['ok'] = true;
    
        $Cloud = new HashCloud($QP);
        $Cloud->setClass("vert-cloud");
        $mess['data'] = $Cloud->piusocial($politician, $type);
    
        exit(json_encode($mess));
    
        break;
    
    
    case "updatetags":
        $politician = $_REQUEST['politician'];
        if (!preg_match("/^[0-9]+$/", $politician)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect politician";
            exit(json_encode($mess));
        }
        $type = $_REQUEST['type'];
        if (!preg_match("/^[0-9]+$/", $type)) {
            $mess['ok'] = false;
            $mess['error'] = "Incorrect type";
            exit(json_encode($mess));
        }
    
        $mess['ok'] = true;
    
        $Cloud = new HashCloud($QP);
        $Cloud->setClass("vert-cloud");
        $mess['data'] = $Cloud->piutwittati($politician, $type);
    
        exit(json_encode($mess));
    
        break;
    
    
    case "update_infty_piusocial":
        if ($_REQUEST['start'] && preg_match("/^[0-9]+$/", $_REQUEST['start'])) {
            $Start = $_REQUEST['start'];
        }
        require_once($Folder_inc."/actions/social_stats.php");
    
        $N = $DB->querynum($query);
    
        $pos = $Start;
    
        $oldnum = 0;
        $oldpos = 0;
        if ($_REQUEST['oldnum'] && preg_match("/^[0-9]+$/", $_REQUEST['oldnum'])) {
            $oldnum = $_REQUEST['oldnum'];
        }
        if ($_REQUEST['oldpos'] && preg_match("/^[0-9]+$/", $_REQUEST['oldpos'])) {
            $oldpos = $_REQUEST['oldpos'];
        }
    
        while ($r = $DB->fetch()) {
            $pos++;
        
            if ($oldnum == $r['num']) {
                $newpos = $oldpos;
            }
            else {
                $newpos = $oldpos = $pos;
            }
            $oldnum = $r['num'];
        
            if ($pos <= 3) {
                $descFB[] = "$pos. {$r['name']} {$r['surname']}";
            }
        
            $Twitter = "@".$r['twitter'];
            if (!$Twitter || strlen($Twitter) == 1) {
                $Twitter = $r['name']." ".$r['surname'];
            }
        
            if (strlen($Twitter) > 25) {
                $Twitter = $r['surname'];
            }
        
            if ($newpos == 1) {
                $TText = "$Twitter è il rappresentante in parlamento #piùsocial $Period";
            }
            elseif ($newpos == 2) {
                $TText = "$Twitter è secondo in classifica tra i parlamentari #piùsocial $Period";
            }
            elseif ($newpos == 3) {
                $TText = "$Twitter è medaglia di bronzo tra i parlamentari #piùsocial $Period";
            }
            elseif ($newpos <= 10) {
                $TText = "$Twitter è nella top-ten dei parlamentari #piùsocial $Period";
            }
            else {
                $TText = "$Twitter è nella posizione $newpos dei parlamentari #piùsocial $Period";
            }
            $TButton_def = sprintf($TButton, urlencode($TText));
        
            $img = "";
            if (isset($OldPositions[$r['politician']])) {
                $old = $OldPositions[$r['politician']];
                if ($old > $pos) {
                    // Up
                    $img = "arrows/up.png";
                    $glyph = "&and;";
                    $badge = "badge badge-success";
                }
                elseif ($old < $pos) {
                    // Down
                    $img = "arrows/down.png";
                    $glyph = "&or;";
                    $badge = "badge badge-important";
                }
                else {
                    // None
                    $img = "arrows/no.png";
                    $glyph = "=";
                    $badge = "badge";
                }
            }
            else {
                // New entry
                // $img = "arrows/new.png";
                $img = "arrows/up.png";
                $glyph = "NEW";
                $badge = "badge badge-warning";
            }
        
            $party = $r['party'];
            $party = str_replace("_", " ", $party);
            $party = preg_replace("/\(.*\)/", "", $party);
            $house = $r['house'];
            $house = str_replace("_", " ", $house);
        
            // Hide button
            // $TButton_def = "";
        
            // echo "<tr id='pos{$pos}' onmouseover='return caricaTwitter(this, \"{$Twitter}\", \"{$Period}\", \"social\", $newpos);' onmouseout='return nascondiTwitter(this);'>";
            $link = Link::Politician($r['politician'], "social");
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos'>$newpos</td>";
            //$image = $r['image'];
	    //if ($image == "") {
        	$image = $settings["image_default"];
            //}
            echo "<td class='statistics-img'><img style='background-image: url(".$image.");' class='img-politician-small' /></td>";
            echo "<td class='statistics-politician'><a href=\"$link\">{$r['name']} {$r['surname']}</a><br />
                <small>{$house} - {$party}</small></td>";
            echo "<td class='statistics-tweet'>$TButton_def</td>";
            echo "<td class='statistics-num'>{$r['num']} <img src='img/$img' class='img-arrow' /></td>";
        }
    
        $mess['ok'] = true;
        $mess['data'] = ob_get_clean();
        $mess['num'] = $N;
        $mess['oldnum'] = $oldnum;
        $mess['oldpos'] = $oldpos;
        $mess['query'] = $query;
    
        exit(json_encode($mess));
        break;
    
    
    case "update_infty_piutwittati":
        if ($_REQUEST['start'] && preg_match("/^[0-9]+$/", $_REQUEST['start'])) {
            $Start = $_REQUEST['start'];
        }
        require_once($Folder_inc."/actions/considerati_stats.php");
    
        $N = $DB->querynum($query);
    
        $pos = $Start;
    
        $oldnum = 0;
        $oldpos = 0;
        if ($_REQUEST['oldnum'] && preg_match("/^[0-9]+$/", $_REQUEST['oldnum'])) {
            $oldnum = $_REQUEST['oldnum'];
        }
        if ($_REQUEST['oldpos'] && preg_match("/^[0-9]+$/", $_REQUEST['oldpos'])) {
            $oldpos = $_REQUEST['oldpos'];
        }
    
        while ($r = $DB->fetch()) {
            $pos++;
        
            if ($oldnum == $r['num']) {
                $newpos = $oldpos;
            }
            else {
                $newpos = $oldpos = $pos;
            }
            $oldnum = $r['num'];
        
            if ($pos <= 3) {
                $descFB[] = "$pos. {$r['name']} {$r['surname']}";
            }
        
            $Twitter = "@".$r['twitter'];
            if (!$Twitter || strlen($Twitter) == 1) {
                $Twitter = $r['name']." ".$r['surname'];
            }
        
            if (strlen($Twitter) > 25) {
                $Twitter = $r['surname'];
            }
        
            if ($newpos == 1) {
                $TText = "$Twitter è al primo posto dei rappresentanti #piùtwittati $Period";
            }
            elseif ($newpos == 2) {
                $TText = "$Twitter è secondo in classifica tra i parlamentari #piùtwittati $Period";
            }
            elseif ($newpos == 3) {
                $TText = "$Twitter è medaglia di bronzo tra i parlamentari #piùtwittati $Period";
            }
            elseif ($newpos <= 10) {
                $TText = "$Twitter è nella top-ten dei parlamentari #piùtwittati $Period";
            }
            else {
                $TText = "$Twitter è nella posizione $newpos dei parlamentari #piùtwittati $Period";
            }
            $TButton_def = sprintf($TButton, urlencode($TText));
        
            $img = "";
            if (isset($OldPositions[$r['politician']])) {
                $old = $OldPositions[$r['politician']];
                if ($old > $pos) {
                    // Up
                    $img = "arrows/up.png";
                    $glyph = "&and;";
                    $badge = "badge badge-success";
                }
                elseif ($old < $pos) {
                    // Down
                    $img = "arrows/down.png";
                    $glyph = "&or;";
                    $badge = "badge badge-important";
                }
                else {
                    // None
                    $img = "arrows/no.png";
                    $glyph = "=";
                    $badge = "badge";
                }
            }
            else {
                // New entry
                // $img = "arrows/new.png";
                $img = "arrows/up.png";
                $glyph = "NEW";
                $badge = "badge badge-warning";
            }
        
            $party = $r['party'];
            $party = str_replace("_", " ", $party);
            $party = preg_replace("/\(.*\)/", "", $party);
            $house = $r['house'];
            $house = str_replace("_", " ", $house);
        
        	//$image = $r['image'];
		//if ($image == "") {
        		$image = $settings["image_default"];
        	//}
            // Hide button
            // $TButton_def = "";
        
            $link = Link::Politician($r['politician'], "tweets");
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos'>$newpos<br />$TButton_def</td>";
            echo "<td class='statistics-img'><img style='background-image: url(".$image.");' class='img-politician-small' /></td>";
            echo "<td class='statistics-politician'>$firsttime<a href=\"$link\">{$r['name']} {$r['surname']}</a><br />
                <small>{$house} - {$party}$tagtxt</small><!--<div class='fadew'></div>--></td>";
            echo "<td class='statistics-num'>{$r['num']} <img src='img/$img' class='img-arrow' /></td>";
            echo "</tr>";
        
        }
    
        $mess['ok'] = true;
        $mess['data'] = ob_get_clean();
        $mess['num'] = $N;
        $mess['oldnum'] = $oldnum;
        $mess['oldpos'] = $oldpos;
        $mess['query'] = $query;
    
        exit(json_encode($mess));
        break;
    
    
    case "update_infty_piucitati":
    
        if ($_REQUEST['start'] && preg_match("/^[0-9]+$/", $_REQUEST['start'])) {
            $Start = $_REQUEST['start'];
        }
        require_once($Folder_inc."/actions/statistics.php");
    
        $N = $DB->querynum($query);
    
        $pos = $Start;
    
        $oldnum = 0;
        $oldpos = 0;
        if ($_REQUEST['oldnum'] && preg_match("/^[0-9]+$/", $_REQUEST['oldnum'])) {
            $oldnum = $_REQUEST['oldnum'];
        }
        if ($_REQUEST['oldpos'] && preg_match("/^[0-9]+$/", $_REQUEST['oldpos'])) {
            $oldpos = $_REQUEST['oldpos'];
        }
    
        while ($r = $DB->fetch()) {
            $pos++;
        
            if ($oldnum == $r['num']) {
                $newpos = $oldpos;
            }
            else {
                $newpos = $oldpos = $pos;
            }
            $oldnum = $r['num'];
        
            if ($pos <= 3) {
                $descFB[] = "$pos. {$r['name']} {$r['surname']}";
            }
        
            $Twitter = "@".$r['twitter'];
            if (!$Twitter || strlen($Twitter) == 1) {
                $Twitter = $r['name']." ".$r['surname'];
            }
        
            if (strlen($Twitter) > 25) {
                $Twitter = $r['surname'];
            }
        
            if ($newpos == 1) {
                $TText = "$Twitter è al primo posto dei #piùcitati $Period";
            }
            elseif ($newpos == 2) {
                $TText = "$Twitter è medaglia d'argento dei #piùcitati $Period";
            }
            elseif ($newpos == 3) {
                $TText = "$Twitter è sul gradino più basso del podio dei #piùcitati $Period";
            }
            elseif ($newpos <= 10) {
                $TText = "$Twitter è nella top-ten dei parlamentari #piùcitati $Period";
            }
            else {
                $TText = "$Twitter è alla $newpos posizione dei parlamentari #piùcitati $Period";
            }
            $TButton_def = sprintf($TButton, urlencode($TText));
        
            $img = "";
            $glyph = "";
            $badge = "";
            if (isset($OldPositions[$r['politician']])) {
                $old = $OldPositions[$r['politician']];
                if ($old > $pos) {
                    // Up
                    $img = "arrows/up.png";
                    $glyph = "&and;";
                    $badge = "badge badge-success";
                }
                elseif ($old < $pos) {
                    // Down
                    $img = "arrows/down.png";
                    $glyph = "&or;";
                    $badge = "badge badge-important";
                }
                else {
                    // None
                    $img = "arrows/no.png";
                    $glyph = "=";
                    $badge = "badge";
                }
            }
            else {
                // New entry
                // $img = "arrows/new.png";
                $img = "arrows/up.png";
                $glyph = "NEW";
                $badge = "badge badge-warning";
            }
        
            $party = $r['party'];
            $party = str_replace("_", " ", $party);
            $party = preg_replace("/\(.*\)/", "", $party);
            $house = $r['house'];
            $house = str_replace("_", " ", $house);
        
        	//$image = $r['image'];
		//if ($image == "") {
        		$image = $settings["image_default"];
        	//}
            // Hide button
            // $TButton_def = "";
        
            // echo "<tr id='pos{$pos}' onmouseover='return caricaTwitter(this, \"{$Twitter}\", \"{$Period}\", \"citati\", $newpos);' onmouseout='return nascondiTwitter(this);'>";
            $link = Link::Politician($r['politician'], "news");
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos'>$newpos<br />$TButton_def</td>";
            echo "<td class='statistics-img'><img style='background-image: url(".$image.");' class='img-politician-small' /></td>";
            echo "<td class='statistics-politician'>$firsttime<a href=\"$link\">{$r['name']} {$r['surname']}</a><br />
                <small>{$house} - {$party}</small></td>";
            // echo "<td class='statistics-tweet'></td>";
            echo "<td class='statistics-num'>{$r['num']} <img src='img/$img' class='img-arrow' /></td>";
            // echo "<td class='statistics-num'>";
            // echo "&nbsp;";
            // echo "</td>";
            echo "</tr>";
        }
    
        $mess['ok'] = true;
        $mess['data'] = ob_get_clean();
        $mess['num'] = $N;
        $mess['oldnum'] = $oldnum;
        $mess['oldpos'] = $oldpos;
    
        exit(json_encode($mess));
        break;
    
    case "pertinence":
        $link = $_REQUEST['link'];
        $politician = $_REQUEST['politician'];
    
        if (!$link) {
            $mess['ok'] = false;
            $mess['errno'] = 1;
            exit(json_encode($mess));
        }
        if (!$politician) {
            $mess['ok'] = false;
            $mess['errno'] = 2;
            exit(json_encode($mess));
        }
    
        $query = "SELECT * FROM pertinence_notifications WHERE link = '".addslashes($link)."' AND politician = '".addslashes($politician)."' AND user = '{$_SESSION['Login']}'";
        if ($DB->querynum($query)) {
            $mess['ok'] = false;
            $mess['errno'] = 3;
            exit(json_encode($mess));
        }
    
        $dati = array();
        $dati['politician'] = $politician;
        $dati['link'] = $link;
        $dati['user'] = $_SESSION['Login'];
        $DB->queryinsert("pertinence_notifications", $dati);
    
        $mess['ok'] = true;
        // $mess['msg'] = "Segnalazione inviata correttamente";
    
        exit(json_encode($mess));
        break;
    
    
    
    case "getpoliticianbyname":
        $pol_text = $_REQUEST['q'];
        $pol_text = trim($pol_text);
        $mess['ok'] = true;
        $mess['pol'] = array();
        if (strlen($pol_text) > 0) {
            // $pol_text = addslashes($pol_text);
            $pol_text = preg_replace("/((^|[^a-z])[a-z]{1,3})\\s+/i", "$1_", $pol_text);
            $parts = preg_split("/\\s+/", $pol_text);
            $Where = array();
            foreach ($parts as $part) {
                $part = str_replace("_", " ", $part);
                $part = addslashes($part);
                $Where[] = "(p.name LIKE '%$part%' OR p.surname LIKE '%$part%')";
            }

            $WhereOR = array();
            if (!$_SESSION['ProAll']) {
                $WhereOR[] = "p.active = 1";
                $WhereOR[] = "p.in_charts = 1";
                if (count($_SESSION['ProPol'])) {
                    foreach ($_SESSION['ProPol'] as $pol => $value) {
                        $WhereOR[] = "p.id = '$pol'";
                    }
                }
            }

            if (count($WhereOR)) {
                $Where[] = "(".implode(" OR ", $WhereOR).")";
            }

            $WhereDef = implode(" AND ", $Where);
        
            $query = "SELECT p.*, pa.goodname, r.description, i.image_small FROM politicians p
                LEFT JOIN parties pa ON pa.id = p.party
                LEFT JOIN politician_info i ON i.politician = p.id
                LEFT JOIN roles r ON r.id = p.role
                WHERE $WhereDef
                ORDER BY p.surname, p.name";
            if ($DB->querynum($query)) {
                while ($r = $DB->fetch()) {
                    $res = array();
                    $res['id'] = $r['id'];
                    $res['name'] =  $r['name']." ".$r['surname'];
                    if ($r['in_charts'] && !$r['active'] && !$_SESSION['ProAll'] && !isset($_SESSION['ProPol'][$r['id']])) {
                        $res['image'] = "";
                    } else {
                        $res['image'] = $r['image_small'];
                    }
                    if ($res['image'] == "") {
                        $res['image'] = $settings["image_default"];
                    }
                    // $res['party'] = Politician::getGoodDescription($r['goodname'], $r['description']);
                    // $res['party'] = $r['description'];
                    $res['party'] = "";
                    $mess['pol'][] = $res;
                }
            }


            if ($_SESSION['Debug']) {
                $mess['query'] = $query;
            }
        }

        exit(json_encode($mess));
        break;
    
    
    
    case "changeState":
        $user = $_SESSION['Login'];
        $ID = addslashes($_REQUEST['id']);
    
        if (!is_numeric($ID)) {
            $mess['ok'] = false;
            $mess['error'] = "ID non valido";
            exit(json_encode($mess));
        }
    
        $query = "SELECT * FROM user_politician WHERE user = '$user' AND politician = '$ID'";
        if ($DB->querynum($query)) {
        
            // Remove
            try {
                unfollowPolitician($user, $ID);
                $mess['ok'] = true;
                $pol = new Politician($ID);
                $mess['data'] = $pol->followLink();
            } catch (Exception $e) {
                $mess['ok'] = false;
                $mess['error'] = $e->getMessage();
            }
        }
        else {
        
            // Add
            try {
                followPolitician($user, $ID);
                $mess['ok'] = true;
                $pol = new Politician($ID);
                $mess['data'] = $pol->followLink();
            } catch (Exception $e) {
                $mess['ok'] = false;
                $mess['error'] = $e->getMessage();
            }
        }
    
        exit(json_encode($mess));
        break;
    
    
    
    case "summary":
        try {
        
            ob_start();
            $hash = addslashes($_REQUEST['hash']);
            $query = "SELECT * FROM `articles_done` WHERE `link` = '$hash'";
        
            $DB->query("SET CHARACTER SET utf8");
        
            if ($DB->querynum($query)) {
                $r = $DB->fetch();
                $summary = $r['summary'];
                $link = $r['url'];
                if (!$summary) {
                    $mess['ok'] = false;
                    $mess['error'] = "Summary is empty";
                    exit(json_encode($mess));
                }
            
                $DB->query("SET CHARACTER SET latin1");
                $query = "SELECT a.`when`, a.title, p.*
                    FROM `articles` a
                    LEFT JOIN politicians p ON p.id = a.politician
                    WHERE a.`link` = '$hash'";
                $DB->query($query);
            
                $time = null;
                $title = null;
                $participants = array();
            
                while ($r = $DB->fetch()) {
                    $time = date("H:i d/m/Y", strtotime($r['when']));
                    $title = $r['title'];
                
                    if ($r['name']) {
                        $participants[] = "<a href='/{$r['id']}'>{$r['name']} {$r['surname']}</a>";
                    }
                }
            
                if (!$time || !$title) {
                    $mess['ok'] = false;
                    $mess['error'] = "Cannot find title/datetime";
                    exit(json_encode($mess));
                }
            
                echo "<h4>{$title}</h4>";
                echo "<p class='summary-text'>{$summary}</p>";
                echo "Con: ".implode(", ", $participants);
                echo "<hr />";
                echo "<p>Fonte: <em>{$r['newspaper']} <small>($time)</small></em> - <a target='_blank' href='$link'>Leggi articolo originale</a></p>";
            }
            else {
                $mess['ok'] = false;
                $mess['error'] = "Cannot find news";
                exit(json_encode($mess));
            }
        
            $mess['ok'] = true;
            $mess['data'] = ob_get_clean();
        } catch (Exception $e) {
            $mess['ok'] = false;
            $mess['error'] = $e->getMessage();
        }
        exit(json_encode($mess));
        break;
    
    default:
        $mess['ok'] = false;
        $mess['error'] = "Richiesta errata";
        exit(json_encode($mess));
    
}

$Text = ob_get_clean();
