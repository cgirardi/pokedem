<?php

$allowed_subs = array("joint", "detail", "trace_link", "select_politician");

if (!$_SESSION['Login'] && !in_array($_REQUEST['sub'], $allowed_subs)) {
    $_SESSION['Message'] = getMessage("Area riservata: per accedere, occorre essere registrati");
    header("Location: ".Link::Login());
    exit();
}

ob_start();

switch ($_REQUEST['sub']) {
    
    
    
    case "trace_link":
    $hash = $_REQUEST['hash'];
    $link = $_REQUEST['link'];
    $user = $_SESSION['Login'];
    if (getHashForLink($user, $link) == $hash) {
        $dati = array();
        $dati['link'] = $link;
        $dati['user'] = $user;
        $DB->queryreplace("user_clicks", $dati);
    }
    
    header("Location: ".$link);
    exit();
    break;
    
    
    
    case "joint":
    
    $id = $_REQUEST['id1'];
    $query = "SELECT * FROM politicians WHERE id = '".addslashes($id)."'";
    if (!$DB->querynum($query)) {
        e("dashboard_joint", "", "Error loading politician 1 $id");
        back(Link::Dashboard(), "Errore");
        break;
    }
    
    header("Location: /$id");
    break;
    
    
    
    case "detail":
    include($Folder_inc."/actions/db.detail.php");
    break;
    
    
    
    case "unfollow":
    $id = addslashes($_REQUEST['id']);
    try {
        unfollowPolitician($_SESSION['Login'], $id);
    } catch(Exception $e) {
        e("dashboard_unfollow", $e);
        back(Link::Dashboard(), "Si &egrave; verificato un errore.");
    }
    back(Link::Dashboard(), "Operazione eseguita correttamente", "alert-success");
    break;
    
    
    
    case "follow":
    if ($NumOfPoliticians >= $MaxNumOfPoliticians && !$_SESSION['LoginRow']['debug']) {
        e("dashboard_follow", "Too many politicians");
        back(Link::Dashboard(), "Hai già selezionato dieci rappresentanti, devi eliminarne uno per continuare.");
        break;
    }
    
    $id = $_REQUEST['id'];
    try {
        followPolitician($_SESSION['Login'], $id, false);
        back(Link::Dashboard(), "Rappresentante selezionato correttamente", "alert-success");
    } catch(Exception $e) {
        e("dashboard_follow", $e);
        back(Link::Dashboard(), "Si &egrave; verificato un errore.");
    }
    break;
    
    
    
    default:
    
    l("page_dashboard");
    ?>
    <p>
        Benvenuta/o <strong><?php echo $_SESSION['LoginRow']['name']; ?></strong>!
    </p>
    <?php
    
    $followedPoliticians = array();
    $query = "SELECT politician
        FROM user_politician
        WHERE user = '{$_SESSION['Login']}'
        ORDER BY rank";
    $numPoliticians = $DB->querynum($query);
    while ($r = $DB->fetch()) {
        $followedPoliticians[] = $r['politician'];
    }
    
    if ($numPoliticians == 0) {
        echo "
        <p>
            Non hai ancora scelto alcun rappresentante.<br />
            Cosa aspetti? Cercane uno usando il campo di testo in alto nella pagina.
        </p>
        ";
    }
    else {
        $activePolitician = $_REQUEST['id'];
        if (!in_array($activePolitician, $followedPoliticians)) {
            $activePolitician = $followedPoliticians[0];
        }
        
        $useOther = ($numPoliticians > 5);
        
        echo "<ul class='nav nav-tabs tabs-dashboard'>\n";
        $activeShown = false;
        $i = 0;
        foreach ($followedPoliticians as $ID) {
            $i++;
            
            if ($useOther && ($i == 5)) {
                ?><li class="dropdown<?php echo (!$activeShown ? " active" : ""); ?>">
                  <a class="dropdown-toggle"
                     data-toggle="dropdown"
                     href="#" title='Visualizza altri parlamentari seguiti'>
                      Altri
                      <b class="caret"></b>
                    </a>
                  <ul class="dropdown-menu">
                <?php
            }
            
            $active = ($ID == $activePolitician);
            $activeShown = $active ? true : $activeShown;
            $P = new Politician($ID);
            echo "<li".($active ? " class='active'" : "")."><a href='".Link::PoliticianInDashboard($ID)."'>{$P->getName()}</a></li>";
            
            if ($useOther && $i == $numPoliticians) {
                ?>  </ul>
                </li>
                <?php
            }
        }
        echo "</ul>\n";
        
        $P = new Politician($activePolitician);
        echo $P->getPageHeader();

        $SS = $_REQUEST['ss'];
        echo Politician::getTabs($SS, $activePolitician, false);
        echo "<hr class='clear' />\n";
        
        $NumNews = 20;
        $ShowRegister = true;
        if ($_SESSION['Login']) {
            $NumNews = 50;
            $ShowRegister = false;
        }
        if ($_SESSION['Pro']) {
            $NumNews = 500;
            $ShowRegister = false;
        }

        $Page = 1;
        if ($_REQUEST['page']) {
            //$_SESSION['page'] = $_REQUEST['page'];
            $Page = $_REQUEST['page'];
        } 
	//else {
        //    if ($_SESSION['page']) {
        //        $Page = $_SESSION['page'];
        //    }
        //}

        switch ($SS) {

            // case "bio":
            // echo $P->getPageBio();
            // break;

            case "considerati":
            case "tweets":
            echo $P->getPageTweet($NumNew, $Page);
            break;

            case "social":
            echo $P->getPageSocial($NumNew, $Page);
            break;

            case "news":
            echo $P->getPageNews($NumNew, $NumFonti, $Page);
            break;

            case "pro":
            echo $P->getPagePro();
            break;

            default:
            header("Location: http://$Host/dashboard/$activePolitician/news");
            exit();
            // echo $P->getPageAll($NumFonti);
            break;

        }
        
    }
    
}

$Text = ob_get_clean();

$Include = "tpl/dashboard.php";
