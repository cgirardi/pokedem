<?php

ini_set("memory_limit", "4096M");
if (!isset($Start)) {
    $Start = 0;
}

$DateTime = time();

$query = "SELECT * FROM twitter_updates WHERE UNIX_TIMESTAMP(`created_time`) <= '$DateTime' ORDER BY `created_time` DESC LIMIT 1";
$r = $DB->queryfetch($query);
$LastUpdateTime = strtotime($r['created_time']);
$LastUpdate = "alle <strong>".date("H:i")."</strong> del giorno <strong>".date("d/m/Y")."</strong>";

if (preg_match("/^[0-9]+$/", $_REQUEST['datetime'])) {
    if ($_REQUEST['datetime'] <= $DateTime) {
        $DateTime = $_REQUEST['datetime'];
        $LastUpdateTime = $DateTime;
        $LastUpdate = "alle <strong>".date("H:i", $DateTime)."</strong> del giorno <strong>".date("d/m/Y", $DateTime)."</strong>";
        $LastUpdate = "<span style='color: red;'>$LastUpdate</span>";
    }
}

$positions = 30;
if ($_REQUEST['positions'] && preg_match("/^[0-9]+$/", $_REQUEST['positions'])) {
    $positions = $_REQUEST['positions'];
}

$valid_types = array(2, 6, 24, 168, 720, 2160);
if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], $valid_types)) {
    $Type = $_REQUEST['type'];
}
if (!isset($Type)) {
    $Type = 2;
}
$TwUrl = Link::Chart("piuattivi", $Type, $DateTime);
$DateTimeBefore = $LastUpdateTime - 60 * 60 * $Type;

// Preloading stuff
$DateTimeBefore = $DateTimeBefore - ($DateTimeBefore % 300);
$DateTime = $DateTime - ($DateTime % 300);

if ($Type <= 24) {
    $Period = "nelle ultime $Type ore";
}
else {
    $days = $Type / 24;
    $Period = "negli ultimi $days giorni";
}

if (!isset($Where)) {
    $Where = array("p.in_charts = '1'");
}
$WhereDef = implode(" AND ", $Where);

$queryBefore = "SELECT us.id, us.username, us.name, us.description, us.followers, COUNT(*) num
    FROM twitter_politicians t
    LEFT JOIN politicians p ON p.id = t.politician
    LEFT JOIN twitter_updates u ON u.id = t.tweet
	LEFT JOIN twitter_users us ON us.id = u.user_id
    WHERE
        t.when_tweet > DATE_SUB(FROM_UNIXTIME($DateTimeBefore), INTERVAL $Type HOUR)
        AND t.when_tweet < FROM_UNIXTIME($DateTimeBefore)
        AND us.id != ALL(SELECT id FROM twitter_blacklisted WHERE active = '1')
        AND LOWER(us.username) != ALL(SELECT LOWER(twitter) FROM social_info)
        AND $WhereDef
	GROUP BY u.user_id
	ORDER BY num DESC
";
$res = $QP->get($queryBefore);
$OldPositions = array();
$pos = 0;
while ($r = $res->next()) {
    $pos++;
    
    // if ($r['max_social'] > $pos || $r['max_social'] == 0) {
    //     $DB->queryupdate("politicians", array("max_social" => $pos), array("id" => $r['politician']), 1);
    // }
    
    $OldPositions[$r['id']] = $pos;
}

$query = "SELECT us.id, us.username, us.name, us.description, us.followers, us.following, us.tweets, us.picture, COUNT(*) num
    FROM twitter_politicians t
    LEFT JOIN politicians p ON p.id = t.politician
    LEFT JOIN twitter_updates u ON u.id = t.tweet
	LEFT JOIN twitter_users us ON us.id = u.user_id
    WHERE
        t.when_tweet > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
        AND t.when_tweet < FROM_UNIXTIME($DateTime)
        AND us.id != ALL(SELECT id FROM twitter_blacklisted WHERE active = '1')
        AND LOWER(us.username) != ALL(SELECT LOWER(twitter) FROM social_info)
        AND $WhereDef
	GROUP BY u.user_id
	ORDER BY num DESC
    LIMIT $Start, $positions";

