<?php

if (!isset($Start)) {
    $Start = 0;
}

$DateTime = time();

$query = "SELECT * FROM social_updates_new WHERE UNIX_TIMESTAMP(`created_time`) <= '$DateTime' ORDER BY `created_time` DESC LIMIT 1";
$r = $DB->queryfetch($query);
$LastUpdateTime = strtotime($r['created_time']);
$LastUpdate = "alle <strong>".date("H:i")."</strong> del giorno <strong>".date("d/m/Y")."</strong>";

if (preg_match("/^[0-9]+$/", $_REQUEST['datetime'])) {
    if ($_REQUEST['datetime'] <= $DateTime) {
        $DateTime = $_REQUEST['datetime'];
        $LastUpdateTime = $DateTime;
        $LastUpdate = "alle <strong>".date("H:i", $DateTime)."</strong> del giorno <strong>".date("d/m/Y", $DateTime)."</strong>";
        $LastUpdate = "<span style='color: red;'>$LastUpdate</span>";
    }
}

$positions = 30;
if ($_REQUEST['positions'] && preg_match("/^[0-9]+$/", $_REQUEST['positions'])) {
    $positions = $_REQUEST['positions'];
}

$valid_types = array(2, 6, 24, 168, 720, 2160);
if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], $valid_types)) {
    $Type = $_REQUEST['type'];
}
if (!isset($Type)) {
    $Type = 2;
}
$TwUrl = Link::Chart("piusocial", $Type, $DateTime);
$DateTimeBefore = $LastUpdateTime - 60 * 60 * $Type;

// Preloading stuff
$DateTimeBefore = $DateTimeBefore - ($DateTimeBefore % 300);
$DateTime = $DateTime - ($DateTime % 300);

if ($Type <= 24) {
    $Period = "nelle ultime $Type ore";
}
else {
    $days = $Type / 24;
    $Period = "negli ultimi $days giorni";
}

if (!isset($Where)) {
    $Where = array("p.in_charts = '1'");
}
$WhereDef = implode(" AND ", $Where);

$queryBefore = "SELECT s.politician, COUNT(*) num, p.name, p.surname, i.image, pt.name party, h.name house, p.max_social
    FROM `social_updates_new` s
    LEFT JOIN politicians p ON p.id = s.politician
    LEFT JOIN politician_info i ON i.politician = s.politician
    LEFT JOIN parties pt ON p.party = pt.id
    LEFT JOIN houses h ON h.id = p.house
    WHERE
        `created_time` > DATE_SUB(FROM_UNIXTIME($DateTimeBefore), INTERVAL $Type HOUR)
        AND `created_time` < FROM_UNIXTIME($DateTimeBefore)
        AND $WhereDef
    GROUP BY politician
    ORDER BY num DESC";
    // LIMIT ".($positions * 2);
$res = $QP->get($queryBefore);
$OldPositions = array();
$pos = 0;
while ($r = $res->next()) {
    $pos++;
    
    if ($r['max_social'] > $pos || $r['max_social'] == 0) {
        $DB->queryupdate("politicians", array("max_social" => $pos), array("id" => $r['politician']), 1);
    }
    
    $OldPositions[$r['politician']] = $pos;
}

$query = "SELECT s.politician, COUNT(*) num, p.name, p.surname, i.image, pt.name party,
        h.name house, soc.twitter, p.max_social, pt.goodname, r.description
    FROM `social_updates_new` s
    LEFT JOIN politicians p ON p.id = s.politician
    LEFT JOIN politician_info i ON i.politician = s.politician
    LEFT JOIN social_info soc ON soc.politician = s.politician
    LEFT JOIN roles r ON r.id = p.role
    LEFT JOIN parties pt ON p.party = pt.id
    LEFT JOIN houses h ON h.id = p.house
    WHERE
        `created_time` > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
        AND `created_time` < FROM_UNIXTIME($DateTime)
        AND $WhereDef
    GROUP BY politician
    ORDER BY num DESC
    LIMIT $Start, $positions";
