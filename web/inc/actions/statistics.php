<?php

if (!isset($Start)) {
    $Start = 0;
}

// date_default_timezone_set('Europe/Rome');
$DateTime = time();

$query = "SELECT * FROM articles WHERE UNIX_TIMESTAMP(`when`) <= '$DateTime' ORDER BY `when` DESC LIMIT 1";
$r = $DB->queryfetch($query);
$LastUpdateTime = strtotime($r['when']);
$LastUpdate = "alle <strong>".date("H:i")."</strong> del giorno <strong>".date("d/m/Y")."</strong>";

if (preg_match("/^[0-9]+$/", $_REQUEST['datetime'])) {
    if ($_REQUEST['datetime'] <= $DateTime) {
        $DateTime = $_REQUEST['datetime'];
        $LastUpdateTime = $DateTime;
        $LastUpdate = "alle <strong>".date("H:i", $DateTime)."</strong> del giorno <strong>".date("d/m/Y", $DateTime)."</strong>";
        $LastUpdate = "<span style='color: red;'>$LastUpdate</span>";
    }
}

$positions = 30;
if ($_REQUEST['positions'] && preg_match("/^[0-9]+$/", $_REQUEST['positions'])) {
    $positions = $_REQUEST['positions'];
}

$valid_types = array(2, 6, 24, 168, 720, 2160);
if (isset($_REQUEST['type']) && in_array($_REQUEST['type'], $valid_types)) {
    $Type = $_REQUEST['type'];
}
if (!isset($Type)) {
    $Type = 2;
}
$TwUrl = Link::Chart("piucitati", $Type, $DateTime);
$DateTimeBefore = $LastUpdateTime - 60 * 60 * $Type;

// Preloading stuff
$DateTimeBefore = $DateTimeBefore - ($DateTimeBefore % 300);
$DateTime = $DateTime - ($DateTime % 300);

if ($Type <= 24) {
    $Period = "nelle ultime $Type ore";
}
else {
    $days = $Type / 24;
    $Period = "negli ultimi $days giorni";
}

if (!isset($Where)) {
    $Where = array("p.in_charts = '1'");
}
$WhereDef = implode(" AND ", $Where);

$queryBefore = "SELECT a.politician, COUNT(DISTINCT a.title) num, p.name, p.surname, i.image, pt.name party, h.name house, p.max_citati
    FROM articles a
    LEFT JOIN politicians p ON p.id = a.politician
    LEFT JOIN politician_info i ON i.politician = a.politician
    LEFT JOIN parties pt ON p.party = pt.id
    LEFT JOIN houses h ON h.id = p.house
    WHERE a.`when` > DATE_SUB(FROM_UNIXTIME($DateTimeBefore), INTERVAL $Type HOUR)
        AND a.`when` < FROM_UNIXTIME($DateTimeBefore)
        AND $WhereDef
    GROUP BY a.politician ORDER BY num DESC";
    // LIMIT 0, ".($positions * 2);
$res = $QP->get($queryBefore);
$OldPositions = array();
$pos = 0;
while ($r = $res->next()) {
    $pos++;
    
    if ($r['max_citati'] > $pos || $r['max_citati'] == 0) {
        $DB->queryupdate("politicians", array("max_citati" => $pos), array("id" => $r['politician']), 1);
    }
    
    $OldPositions[$r['politician']] = $pos;
}
// echo "<p>$queryBefore</p>";
    
$query = "SELECT a.politician, COUNT(DISTINCT a.title) num, p.name, p.surname, i.image, pt.name party,
        h.name house, soc.twitter, p.max_citati, pt.goodname, r.description,
        GROUP_CONCAT(DISTINCT a.title ORDER BY a.title SEPARATOR '<br />') titles
    FROM articles a
    LEFT JOIN politicians p ON p.id = a.politician
    LEFT JOIN politician_info i ON i.politician = a.politician
    LEFT JOIN social_info soc ON soc.politician = a.politician
    LEFT JOIN roles r ON r.id = p.role
    LEFT JOIN parties pt ON p.party = pt.id
    LEFT JOIN houses h ON h.id = p.house
    WHERE a.`when` > DATE_SUB(FROM_UNIXTIME($DateTime), INTERVAL $Type HOUR)
        AND a.`when` < FROM_UNIXTIME($DateTime)
        AND $WhereDef
    GROUP BY a.politician ORDER BY num DESC
    LIMIT $Start, $positions";
// echo "<p>$query</p>";
