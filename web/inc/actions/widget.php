<?php

// ini_set("error_reporting", E_ALL & ~E_NOTICE);
// ini_set("display_errors", "On");
ini_set("memory_limit", "2048M");

$Width = 200;
$Height = 300;

if (isset($_REQUEST['width']) && is_numeric($_REQUEST['width'])) {
    $Width = round($_REQUEST['width']);
    if ($Width < 200) {
        $Width = 200;
    }
    if ($Width > 1000) {
        $Width = 1000;
    }
}

if (isset($_REQUEST['height']) && is_numeric($_REQUEST['height'])) {
    $Height = round($_REQUEST['height']);
    if ($Height < 300) {
        $Height = 300;
    }
    if ($Height > 1000) {
        $Height = 1000;
    }
}

$Height -= 40;

$IsMini = false;
if ($Width < 300) {
    $IsMini = true;
}

ob_start();

$hdColor = $_REQUEST['hdColor'];
$bgColor = $_REQUEST['bgColor'];
$fgColor = $_REQUEST['fgColor'];
$lnColor = $_REQUEST['lnColor'];

echo '<style type="text/css" media="screen">';
if ($hdColor) {
    switch ($hdColor) {
        case "black":
        echo "#widget-header, #widget-header-mini { background-color: {$_REQUEST['hdColor']} !important; }";
        if ($hdColor == "white") {
            echo "#widget-header *, #widget-header-mini * { color: #f89625 !important; }";
        }
        break;
        
        case "white":
        echo "#widget-header, #widget-header-mini { background-color: {$_REQUEST['hdColor']} !important; }";
        if ($hdColor == "white") {
            echo "#widget-header *, #widget-header-mini * { color: black !important; }";
        }
        break;
    }
}
echo $fgColor ? "#widget-div * { color: $fgColor !important; }" : "";
echo $lnColor ? "#widget-div a { color: $lnColor !important; }" : "";
echo $bgColor ? "#widget-div { background-color: $bgColor !important; }" : "";

echo '</style>';

echo "<div style='position: fixed; top: 0;'><a href='". $settings["server"] ."'><img src='". $settings["image"] ."' height='40'></a></div>";

$type = $_REQUEST['type'];

switch ($type) {
    
    case "trendingTopics":
    case "tendenze":
    $Occhiello = "Tendenze";
    $Title = "#trendingtopic";
    $Link = Link::Tendenze();
    echo "<div class='politician-widget' style='margin-top: 20px;'>";
    echo $BM->lastTrending(2);
    echo "</div>";
    break;
    
    case "newsFlow":
    case "socialFlow":
    case "tweetFlow":
    case "allFlow":
    
    $Occhiello = "";
    switch ($type) {
        case "newsFlow":
        $Occhiello = "Ultime news";
        break;
        
        case "socialFlow":
        $Occhiello = "Interventi social";
        break;
        
        case "tweetFlow":
        $Occhiello = "Tweet ricevuti";
        break;
        
        case "allFlow":
        $Occhiello = "Ultime news";
        break;
        
    }
    $ID = addslashes($_REQUEST['id']);
    if (!is_numeric($ID)) {
        exit("ID non valido");
    }

    $query = "SELECT * FROM politicians WHERE id = '$ID'";
    if (!$DB->querynum($query)) {
        exit("ID non valido");
    }
    $R = $DB->fetch();
    
    $Title = "{$R['name']} {$R['surname']}";
    $Link = Link::Politician($ID);
    
    echo $BM->$type(array("id" => $ID, "limit" => 20, "class" => "detail-widget"));
    
    echo "<p class='widget-p'>
        <a href='http://". $settings["server"] ."' target='_blank'>Vuoi leggere più notizie?<br />
        Registrati</a>
    </p>";
    
    break;
    
    case "temiBox":
    case "chartNewsBox":
    case "chartNewspapersPie":
    case "chartConnectionsPie":
    case "hashFromBox":
    case "chartSocialBox":
    case "hashToBox":
    case "chartTweetsBox":
    
    $Occhiello = "Statistiche";
    $ID = addslashes($_REQUEST['id']);
    if (!is_numeric($ID)) {
        exit("ID non valido");
    }
    
    $query = "SELECT * FROM politicians WHERE id = '$ID'";
    if (!$DB->querynum($query)) {
        exit("ID non valido");
    }
    $R = $DB->fetch();
    $pol = "{$R['name']} {$R['surname']}";
    
    $Title = $pol;
    $Link = Link::Politician($ID);
    
    echo "<div class='politician-widget' id='politician-widget'>";
    echo $BM->$type(array("id" => $ID, "div" => 'politician-widget', "sub" => $type));
    echo "</div>";
    break;
    
    
    case "piucitati":
    case "piusocial":
    case "piutwittati":
    // case "piuattivi":
    
    $Occhiello = "Classifica";
    $subLink = "";
    
    switch ($type) {
        case "piucitati":
        $Title = "I #piùcitati";
        require_once($Folder_inc."/actions/statistics.php");
        $img_field = "image";
        $img_class = "politician";
        $subLink = "news";
        break;
        
        case "piusocial":
        $Title = "I #piùsocial";
        require_once($Folder_inc."/actions/social_stats.php");
        $img_field = "image";
        $img_class = "politician";
        $subLink = "social";
        break;
        
        case "piutwittati":
        $Title = "I #piùtwittati";
        require_once($Folder_inc."/actions/considerati_stats.php");
        $img_field = "image";
        $img_class = "politician";
        $subLink = "tweets";
        break;
        
        // case "piuattivi":
        // $Title = "I #piùattivi";
        // require_once($Folder_inc."/actions/attivisti_stats.php");
        // $img_field = "picture";
        // $img_class = "attivisti";
        // break;
    }
    
    $Link = Link::Chart($type, 0);
    $DB->query($query);
    
    $pos = 0;
    
    echo "<table class='table table-striped table-list-statistics'>";
    echo "<tbody>";
    
    $oldnum = 0;
    $oldpos = 0;
    
    while ($r = $DB->fetch()) {
        $pos++;
        
        if ($oldnum == $r['num']) {
            $newpos = $oldpos;
        }
        else {
            $newpos = $oldpos = $pos;
        }
        $oldnum = $r['num'];
        
        $img = "";
        if (isset($OldPositions[$r['politician']])) {
            $old = $OldPositions[$r['politician']];
            if ($old > $pos) {
                // Up
                $img = "arrows/up.png";
                $glyph = "&and;";
                $badge = "badge badge-success";
            }
            elseif ($old < $pos) {
                // Down
                $img = "arrows/down.png";
                $glyph = "&or;";
                $badge = "badge badge-important";
            }
            else {
                // None
                $img = "arrows/no.png";
                $glyph = "=";
                $badge = "badge";
            }
        }
        else {
            // New entry
            // $img = "arrows/new.png";
            $img = "arrows/up.png";
            $glyph = "NEW";
            $badge = "badge badge-warning";
        }
        
        $party = $r['party'];
        $party = str_replace("_", " ", $party);
        $party = preg_replace("/\(.*\)/", "", $party);
        $house = $r['house'];
        $house = str_replace("_", " ", $house);
        
        $linkPol = Link::Politician($r['politician'], $subLink);
        $thumb = $r[$img_field];
        if ($thumb == "") {
        	$thumb = $settings["image_default"];
        }
        
        if ($Width >= 600) {
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos'>$newpos</td>";
            echo "<td class='statistics-img'><img style='background-image: url($thumb);' class='img-$img_class-small' /></td>";
            echo "<td class='statistics-politician'><a href=\"$linkPol\">{$r['name']} {$r['surname']}</a><br />
                <small>{$house} - {$party}</small></td>";
            echo "<td class='statistics-num'>{$r['num']} <img src='img/$img' class='img-arrow' /></td>";
            echo "</tr>";
        }
        elseif ($Width >= 400) {
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos'>$newpos</td>";
            echo "<td class='statistics-img'><img style='background-image: url($thumb);' class='img-$img_class-small' /></td>";
            echo "<td class='statistics-politician'><a href=\"$linkPol\">{$r['name']} {$r['surname']}</a></td>";
            echo "<td class='statistics-num'>{$r['num']} <img src='img/$img' class='img-arrow' /></td>";
            echo "</tr>";
        }
        elseif ($Width >= 300) {
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos-small'>$newpos</td>";
            echo "<td class='statistics-img-small'><img style='background-image: url($thumb);' class='img-$img_class-small' /></td>";
            echo "<td class='statistics-politician-small'><a href=\"$linkPol\">{$r['name']} {$r['surname']}</a></td>";
            echo "<td class='statistics-num-small'>{$r['num']} <img src='img/$img' class='img-arrow-small-widget' /></td>";
            echo "</tr>";
        }
        else {
            echo "<tr id='pos{$pos}'>";
            echo "<td class='statistics-pos-mini'>$newpos</td>";
            echo "<td class='statistics-img-mini'><img style='background-image: url($thumb);' class='img-$img_class-small' /></td>";
            echo "<td class='statistics-politician-mini'><a href=\"$linkPol\">{$r['name']} {$r['surname']}</a></td>";
            echo "<td class='statistics-num-mini'>{$r['num']} <img src='img/$img' class='img-arrow-mini-widget' /></td>";
            echo "</tr>";
        }
    }
    
    echo "</tbody>";
    echo "</table>";
    
    break;
}

$Body = ob_get_clean();
include("tpl/index-widget.php");

if (isset($Google_analytics_ID) && $Google_analytics_ID !="") { ?>
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', <?php echo $Google_analytics_ID; ?>]);
      _gaq.push(['_setDomainName', <?php echo $Host; ?>]);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);
      _gaq.push(['_setAllowAnchor', true]);

      <?php if ($Is_test) { ?>
          window['ga-disable-<?php echo $Google_analytics_ID; ?>'] = true;
      <?php } ?>
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
<?php
}

exit();
