<?php

class GChart {
    
    private $DB;
    private $QP;
    
    private $defaults = array(
        "days" => 30,
        "height" => "300px",
        "width" => "500px",
        "divid" => "chartContainer",
        "limit_connections" => 10,
        "limit_newspapers" => 10,
    );
    
    public function __construct($DB, $QP = null) {
        $this->DB = $DB;
        $this->QP = $QP;
    }
    
    private function firstDateJS($days) {
        $date = strtotime('-'. ($days - 1) .' days');
        return date("Y", $date).", ".(date("n", $date) - 1).", ".date("j", $date);
    }
    
    private function createEmptyArray($days) {
        $stats = array();
        for ($i = $days - 1; $i >= 0; $i--) {
            $date = strtotime('-'. $i .' days');
            $stats[date("Y-m-d", $date)] = 0;
        }
        return $stats;
    }
    
    private function createArrayFromQuery($query, $days, $fieldTime = "when", $fieldNum = "num") {
        $stats = $this->createEmptyArray($days);
        $res = $this->QP->get($query, 300);
        
        while ($r = $res->next()) {
            $when = substr($r[$fieldTime], 0, 10);
            if (isset($stats[$when])) {
                $stats[$when] = $r[$fieldNum];
            }
        }
        
        return $stats;
    }
    
    private function createLabelArrayFromQuery($query, $tot = 0, $prependChar = "", $fieldText = "label", $fieldNum = "num") {
        $count = 0;
        $stats = array();
        $res = $this->QP->get($query, 300);
        
        while ($r = $res->next()) {
            $label = substr($r[$fieldText], 0, 15);
            $stats[] = "['$prependChar".addslashes($label)."', {$r[$fieldNum]}]";
            $count += $r[$fieldNum];
        }
        
        $alt = $tot - $count;
        if ($tot > 0 && max(0, $alt) > 0) {
            $stats[] = "['Altri', $alt]";
        }
        return $stats;
    }
    
    private function loadPars($pars) {
        foreach ($this->defaults as $index => $value) {
            if (!isset($pars[$index])) {
                $pars[$index] = $value;
            }
        }
        return $pars;
    }
    
    private function createDiv($pars) {
        $txt = "";
        $txt .= "<div id='{$pars['divid']}' style='height: {$pars['height']}; width: {$pars['width']}; position: relative;'>\n";
        $txt .= "</div>\n";
        return $txt;
    }
    
    private function chartXYdate($values, $firstDate, $pars) {
        echo $this->createDiv($pars);
        
        ?>
        
        <script type="text/javascript" charset="utf-8">
            $(function () {
                    $('#<?php echo $pars['divid']; ?>').highcharts({
                        title: {
                            text: ''
                        },
                        tooltip: {
                            dateTimeLabelFormats: {
                                day: '%a, %e %b %Y'
                            }
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        legend: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: ''
                            },
                            min: 0,
                            plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                        },
                        chart: {
                            backgroundColor: null
                        },
                        series: [{
                            name: '<?php echo addslashes($pars['yaxis']); ?>',
                            pointInterval: 24 * 3600 * 1000,
                            pointStart: Date.UTC(<?php echo $firstDate; ?>),
                            data: [
                                <?php echo $values; ?>
                            ]
                        }]
                    });
                });
        </script>

        <?php
    }
    
    private function chartPie($values, $pars) {
        
        echo $this->createDiv($pars);
        
        ?>
        
        <script type="text/javascript" charset="utf-8">
            $(function () {
                $('#<?php echo $pars['divid']; ?>').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        backgroundColor: null
                    },
                    legend: {
                        align: 'left',
                        layout: 'vertical'
                    },
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '<?php echo addslashes($pars['title']); ?>',
                        data: [
                            <?php echo $values; ?>
                        ]
                    }]
                });
            });
        </script>

        <?php
    }
    
    public function citations($pars) {
        
        $pars = $this->loadPars($pars);
        $firstDate = $this->firstDateJS($pars['days']);
        
        if (!isset($pars['politician']) || !preg_match("/^[0-9]+$/", $pars['politician'])) {
            return false;
        }
        
        if (!isset($pars['title'])) {
            $pars['title'] = "Notizie";
        }
        if (!isset($pars['yaxis'])) {
            $pars['yaxis'] = "Notizie";
        }
        
        $query = "SELECT `when`, COUNT(*) num FROM articles
            WHERE politician = '{$pars['politician']}'
                AND NOW() < DATE_ADD(`when`, INTERVAL {$pars['days']} DAY)
            GROUP BY YEAR(`when`), MONTH(`when`), DAY(`when`)";
        $stats = $this->createArrayFromQuery($query, $pars['days']);
        $values = implode(", ", $stats);
        
        $this->chartXYdate($values, $firstDate, $pars);
    }
    
    public function tweets($pars) {
        
        $pars = $this->loadPars($pars);
        $firstDate = $this->firstDateJS($pars['days']);
        
        if (!isset($pars['politician']) || !preg_match("/^[0-9]+$/", $pars['politician'])) {
            return false;
        }
        
        if (!isset($pars['title'])) {
            $pars['title'] = "Tweet ricevuti";
        }
        if (!isset($pars['yaxis'])) {
            $pars['yaxis'] = "Tweet";
        }
        
        $query = "SELECT  `created_time` `when`, COUNT( * ) num
            FROM twitter_updates u
                LEFT JOIN twitter_politicians p ON p.tweet = u.id
            WHERE p.politician =  '{$pars['politician']}'
                AND NOW( ) < DATE_ADD(  p.when_tweet , INTERVAL {$pars['days']} DAY ) 
            GROUP BY YEAR(  p.when_tweet ) , MONTH(  p.when_tweet ) , DAY(  p.when_tweet )";
        $stats = $this->createArrayFromQuery($query, $pars['days']);
        $values = implode(", ", $stats);
        
        $this->chartXYdate($values, $firstDate, $pars);
    }
    
    public function social($pars) {
        
        $pars = $this->loadPars($pars);
        $firstDate = $this->firstDateJS($pars['days']);
        
        if (!isset($pars['politician']) || !preg_match("/^[0-9]+$/", $pars['politician'])) {
            return false;
        }
        
        if (!isset($pars['title'])) {
            $pars['title'] = "Post social";
        }
        if (!isset($pars['yaxis'])) {
            $pars['yaxis'] = "Post";
        }
        
        $query = "SELECT  `created_time` `when`, COUNT( * ) num
            FROM social_updates_new u
            WHERE u.politician =  '{$pars['politician']}'
                AND NOW( ) < DATE_ADD(  `created_time` , INTERVAL {$pars['days']} DAY ) 
            GROUP BY YEAR(  `created_time` ) , MONTH(  `created_time` ) , DAY(  `created_time` )";
        $stats = $this->createArrayFromQuery($query, $pars['days']);
        $values = implode(", ", $stats);
        
        $this->chartXYdate($values, $firstDate, $pars);
    }
    
    public function trends($pars) {
        $pars = $this->loadPars($pars);
        
        if (!isset($pars['type']) || !preg_match("/^[0-9]+$/", $pars['type'])) {
            return false;
        }
        
        $query = "SELECT e.tag label, COUNT(*) num
            FROM social_updates_new_entities e
            LEFT JOIN twitter_updates u ON e.tweet = u.id
            LEFT JOIN twitter_politicians p ON p.tweet = u.id
            WHERE p.politician IS NOT NULL
            AND p.when_tweet > DATE_SUB(NOW(), INTERVAL {$pars['type']} HOUR)
            AND p.when_tweet < NOW()
            AND e.type = 'hashtag'
            GROUP BY LOWER(CONVERT(CONVERT(CONVERT(e.tag USING latin1) USING binary) USING utf8))
            ORDER BY num DESC
            LIMIT 10";
        $stats = $this->createLabelArrayFromQuery($query, 0, "#");
        // echo $query;
        $values = implode(", ", $stats);
        
        $this->chartPie($values, $pars);
    }
    
    public function connections($pars) {
        $pars = $this->loadPars($pars);
        
        if (!isset($pars['politician']) || !preg_match("/^[0-9]+$/", $pars['politician'])) {
            return false;
        }
        
        if (!isset($pars['title'])) {
            $pars['title'] = "Connessioni";
        }
        
        $query = "SELECT a.link
            FROM articles a
            LEFT JOIN politicians p ON p.id = a.politician
            WHERE a.link = ANY(
                SELECT link FROM articles WHERE politician = '{$pars['politician']}'
                )
                AND NOW( ) < DATE_ADD(  `when` , INTERVAL {$pars['days']} DAY ) 
                AND politician != '{$pars['politician']}'";
        $res = $this->QP->get($query, 300);
        $tot = $res->num();
        
        $query = "SELECT politician, COUNT(*) num, CONCAT(SUBSTRING(p.name, 1, 1), '. ', p.surname) label
            FROM articles a
            LEFT JOIN politicians p ON p.id = a.politician
            WHERE a.link = ANY(
                SELECT link FROM articles WHERE politician = '{$pars['politician']}'
                )
                AND NOW( ) < DATE_ADD(  `when` , INTERVAL {$pars['days']} DAY ) 
                AND politician != '{$pars['politician']}'
            GROUP BY politician
            ORDER BY num DESC
            LIMIT {$pars['limit_connections']}";
        $stats = $this->createLabelArrayFromQuery($query, $tot);
        $values = implode(", ", $stats);
        
        $this->chartPie($values, $pars);
    }
    
    public function newspapers($pars) {
        $pars = $this->loadPars($pars);
        
        if (!isset($pars['politician']) || !preg_match("/^[0-9]+$/", $pars['politician'])) {
            return false;
        }
        
        if (!isset($pars['title'])) {
            $pars['title'] = "Fonti";
        }
        
        $query = "SELECT link
            FROM `articles`
            WHERE politician = '{$pars['politician']}'
                AND `when` > date_sub(NOW(), INTERVAL {$pars['days']} day)";
        $res = $this->QP->get($query, 300);
        $tot = $res->num();
        
        $query = "SELECT newspaper label, COUNT(*) num
            FROM `articles`
            WHERE politician = '{$pars['politician']}'
                AND `when` > date_sub(NOW(), INTERVAL {$pars['days']} day)
            GROUP BY newspaper
            ORDER BY num DESC
            LIMIT {$pars['limit_newspapers']}";
        $stats = $this->createLabelArrayFromQuery($query, $tot);
        $values = implode(", ", $stats);
        
        $this->chartPie($values, $pars);
    }

    public function newspaperTypes($pars) {
        $DBid = mt_rand(100, getrandmax());
        
        $pars = $this->loadPars($pars);
        $firstDate = $this->firstDateJS($pars['days']);
        
        if (!isset($pars['politician']) || !preg_match("/^[0-9]+$/", $pars['politician'])) {
            return false;
        }
        
        if (!isset($pars['title'])) {
            $pars['title'] = "Tipologia fonti";
        }
        
        $query = "SELECT t.name label, COUNT(*) num
            FROM `articles` a
                LEFT JOIN sites s ON s.name = a.newspaper
                LEFT JOIN site_types t ON t.id = s.type
            WHERE politician = '{$pars['politician']}'
                AND `when` > date_sub(NOW(), INTERVAL {$pars['days']} day)
            GROUP BY s.type
            ORDER BY num DESC";
        $stats = $this->createLabelArrayFromQuery($query);
        $values = implode(", ", $stats);
        
        $this->chartPie($values, $pars);
    }
    
}
