<?php

class Tweet {
    
    public static $tweet_max = 140;
    public static $url_length = 22;
    
    static public function builtTweetWithHashtags($text, $hashtags, $url, $glue = "") {
        $final = trim($text);
        $final .= " ";
        $final .= trim($glue);
        
        $final = trim($final);
        
        $Max_len_tweet = Tweet::$tweet_max - Tweet::$url_length - 1;
        
        $hash = false;
        
        arsort($hashtags);
        foreach ($hashtags as $tag => $value) {
            $TText_test = $final . " " . $tag;
            if (strlen($TText_test) < $Max_len_tweet) {
                $hash = true;
                $final = $TText_test;
            }
        }
        
        if (!$hash) {
            // Che si fa?
        }
        
        $final .= " " . $url;
        return $final;
    }
}