<?php


if (get_magic_quotes_gpc()) {
   function stripslashes_deep($value)
   {
       $value = is_array($value) ?
                   array_map('stripslashes_deep', $value) :
                   stripslashes($value);
 
       return $value;
   }
 
   $_POST = array_map('stripslashes_deep', $_POST);
   $_GET = array_map('stripslashes_deep', $_GET);
   $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
   $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
}

function thousandsCurrencyFormat($num) {
    $x = round($num);
    $x_number_format = number_format($x);
    $x_array = explode(',', $x_number_format);
    $x_parts = array('k', 'm', 'b', 't');
    $x_count_parts = count($x_array) - 1;
    $x_display = $x;
    $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
    $x_display .= $x_parts[$x_count_parts - 1];
    return $x_display;
}

function getSettings($ID = 0) {
    global $DB, $Default_User;
    $DBid = md5("getSettings");
    if ($ID == 0) {
        $ID = $_SESSION['Login'];
    }
    $query = "SELECT * FROM user_settings WHERE user = '$ID'";
    if ($DB->querynum($query, $DBid)) {
        return $DB->fetch($DBid);
    }
    else {
        return $Default_User;
    }
}

function setLogin($userid, $remember = false) {
    global $Host;
    
    $_SESSION['Login'] = $userid;
    
    l("login");
    
    // if ($remember) {
    //     global $DB;
    //     $DBid = md5("setLogin");
    //     
    //     $query = "SELECT * FROM users WHERE id = '$userid'";
    //     $r = $DB->queryfetch($query, $DBid);
    //     
    //     setcookie('username', $r['email'], time() + 60 * 60 * 24 * 365);
    //     setcookie('password', sha1($r['password']), time() + 60 * 60 * 24 * 365);
    // }
    // else {
    //     setcookie('username', '', time() - 10);
    //     setcookie('password', '', time() - 10);
    // }
}

function getMessage($text, $type = "alert-error") {
    $Message = "";
    $Message .= "<div class='alert $type'>";
    $Message .= "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
    $Message .= $text;
    $Message .= "</div>";
    return $Message;
}

function valemail($indirizzo) {
	$regex = '^[\._a-z0-9-]+@[\._a-z0-9-]+\.[a-z]+$';
	return eregi($regex, $indirizzo);
}

function back($backurl, $message = "", $class = "alert-error") {
    global $Is_test;
    
    if ($message) {
        $_SESSION['Message'] = getMessage($message, $class);
    }
    if ($Is_test) {
        ?>
        <div style='background-color: red; color: white; padding: 10px;'>
            AMBIENTE DI TEST
            <a href='<?php echo $backurl; ?>' style='color: white;'>CONTINUA</a>
        </div>
        <?php
    }
    else {
        header("Location: $backurl");
    }
    exit();
}

function generatePassword($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

function strip_underscore($text) {
    return str_replace("_", " ", $text);
}

function primi($stringa, $quanti = 50) {
    $stringa = trim(html_entity_decode($stringa));
	$stringa = (strlen($stringa) > $quanti ? substr($stringa, 0, $quanti)." [...]" : $stringa);
	return $stringa;
}

function buildBaseString($baseURI, $method, $params) {
    $r = array();
    ksort($params);
    foreach($params as $key=>$value){
        $r[] = "$key=" . rawurlencode($value);
    }
    return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
}

function buildAuthorizationHeader($oauth) {
    $r = 'Authorization: OAuth ';
    $values = array();
    foreach($oauth as $key=>$value)
        $values[] = "$key=\"" . rawurlencode($value) . "\"";
    $r .= implode(', ', $values);
    return $r;
}

function getHashForLink($user, $link) {
    $secret = "I topi non avevano nipoti";
    return sha1($user.$secret.$link);
}

function getLinkWithHash($user, $link, $mail = false) {
    global $Host;
    $hash = getHashForLink($user, $link);
    
    if ($mail) {
        return "http://$Host/?action=trace_link&amp;user=".$user."&amp;link=".urlencode($link)."&amp;hash=".$hash;
    }
    else {
        return "?action=dashboard&amp;sub=trace_link&amp;link=".urlencode($link)."&amp;hash=".$hash;
    }
}

function e($action, $politician = "", $info = "", $email = "", $user = "") {
    l($action, $politician, $info, $email, $user, "error");
}

function getSetting($name, $value = false) {
    global $DB; $DBid = md5("getSetting");
    $query = "SELECT * FROM settings WHERE name = '".addslashes($name)."'";
    if ($DB->querynum($query, $DBid)) {
        $r = $DB->fetch($DBid);
        $value = $r['value'];
    }
    return $value;
}

function setSetting($name, $value) {
    global $DB; $DBid = md5("setSetting");
    $data = array("name" => $name, "value" => $value);
    $DB->queryinsertodku("settings", $data, array(), $DBid);
    return $value;
}

function tlog($action, $username, $info = "", $id = "") {
    global $DB; $DBid = md5("log");
    $dati = array();
    $dati['action'] = $action;
    $dati['username'] = $username;
    if ($info) {
        $dati['info'] = $info;
    }
    if ($id && is_numeric($id)) {
        $dati['is_tweet'] = $id;
    }
    
    $DB->queryinsert("twitter_pd_log", $dati, $DBid);
}

function l($action, $politician = "", $info = "", $email = "", $user = "", $type = "info") {
    global $DB; $DBid = md5("log");
    
    $referer = "";
    
    if ($_SERVER['HTTP_REFERER']) {
        $referer = $_SERVER['HTTP_REFERER'];
        $parts = parse_url($referer);
        $referer = $parts['host'];
    }
    
    $dati = array();
    $dati['action'] = $action;
    $dati['type'] = $type;
    $dati['referer'] = $referer;
    if ($user) {
        $dati['user'] = $user;
    }
    else {
        if ($_SESSION['Login']) {
            $dati['user'] = $_SESSION['Login'];
        }
    }
    if ($politician) {
        $dati['politician'] = $politician;
    }
    if ($email) {
        $dati['email'] = $email;
    }
    if ($info) {
        $dati['info'] = $info;
    }
    $dati['ip'] = $_SERVER['REMOTE_ADDR'];
    
    $DB->queryinsert("log", $dati, $DBid);
}

function rel2abs($rel, $base)
{
    /* return if already absolute URL */
    if (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;

    /* queries and anchors */
    if ($rel[0]=='#' || $rel[0]=='?') return $base.$rel;

    /* parse base URL and convert to local variables:
       $scheme, $host, $path */
    extract(parse_url($base));

    /* remove non-directory element from path */
    $path = preg_replace('#/[^/]*$#', '', $path);

    /* destroy path if relative url points to root */
    if ($rel[0] == '/') $path = '';

    /* dirty absolute URL */
    $abs = "$host$path/$rel";

    /* replace '//' or '/./' or '/foo/../' with '/' */
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for($n=1; $n>0; $abs=preg_replace($re, '/', $abs, -1, $n)) {}

    /* absolute URL is ready! */
    return $scheme.'://'.$abs;
}

function getFollowedPoliticians($user_id = 0) {
	global $DB;
	
	if (!$user_id) {
	    $user_id = $_SESSION['Login'];
    }
	
	$FollowedPoliticians = array();
	
	$query = "SELECT * FROM user_politician WHERE user = '{$user_id}'";
	$DB->querynum($query);
    while ($r = $DB->fetch()) {
        $FollowedPoliticians[$r['politician']] = array();
        $FollowedPoliticians[$r['politician']]['real'] = $r['notification_real'];
        $FollowedPoliticians[$r['politician']]['day'] = $r['notification_day'];
        $FollowedPoliticians[$r['politician']]['week'] = $r['notification_week'];
    }
    
    return $FollowedPoliticians;
}

function followPolitician($user_id, $politician_id) {
    global $DB, $MaxNumOfPoliticians, $FollowedPoliticians;
    
    $check_max_num_reached = $_SESSION['Debug'] || $_SESSION['Pro'];
    
    $followed_politicians_count = count($FollowedPoliticians);
    if ($check_max_num_reached && $followed_politicians_count >= $MaxNumOfPoliticians) {
        throw new Exception("Raggiunto numero massimo di politici.");
    }

    $query = "SELECT * FROM politicians WHERE id = '".addslashes($politician_id)."'";
    if (!$DB->querynum($query)) {
        throw new Exception("Politico sconosciuto: $politician_id");
    }
    $r = $DB->fetch();

    l("follow_politician", $politician_id);

    $house = $r['house'];
    $dati = array();
    $dati['user'] = $user_id;
    $dati['house'] = $house;
    $dati['politician'] = $politician_id;
    $DB->queryinsert("user_politician", $dati);
    $FollowedPoliticians[$politician_id] = true;
    
    return;
    // $query = "SELECT DISTINCT politician FROM user_politician WHERE user = '{$user_id}'";
    // return $DB->querynum($query);
}

function unfollowPolitician($user_id, $politician_id) {
    global $DB, $FollowedPoliticians;
    
    l("unfollow_politician", $id);
    $query = "DELETE FROM user_politician WHERE user = '{$user_id}' AND politician = '$politician_id'";
    $DB->query($query);
    unset($FollowedPoliticians[$politician_id]);
    
    return;
    // $query = "SELECT DISTINCT politician FROM user_politician WHERE user = '{$user_id}'";
    // return $DB->querynum($query);
}

function wlog($text, $type = "INFO") {
    echo "[$type] ".date("Y-m-d H:i:s")." $text\n";
}

function time_passed($timestamp){
    //type cast, current time, difference in timestamps
    if (!is_numeric($timestamp)) {
        $timestamp = strtotime($timestamp);
    }
    else {
        $timestamp = (int) $timestamp;
    }
    $current_time   = time();
    $diff           = $current_time - $timestamp;
    
    //intervals in seconds
    $intervals      = array (
        'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
    );
    
    //now we just find the difference
    if ($diff < 10)
    {
        return 'adesso';
    }    

    if ($diff < 60)
    {
        return $diff == 1 ? $diff . ' secondo fa' : $diff . ' secondi fa';
    }        

    if ($diff >= 60 && $diff < $intervals['hour'])
    {
        $diff = floor($diff/$intervals['minute']);
        return $diff == 1 ? 'un minuto fa' : $diff . ' minuti fa';
    }        

    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
    {
        $diff = floor($diff/$intervals['hour']);
        return $diff == 1 ? "un'ora fa" : $diff . ' ore fa';
    }    

    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
    {
        $diff = floor($diff/$intervals['day']);
        return $diff == 1 ? 'un giorno fa' : $diff . ' giorni fa';
    }    

    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
    {
        $diff = floor($diff/$intervals['week']);
        return $diff == 1 ? 'una settimana fa' : $diff . ' settimane fa';
    }    

    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
    {
        $diff = floor($diff/$intervals['month']);
        return $diff == 1 ? 'un mese fa' : $diff . ' mesi fa';
    }    

    if ($diff >= $intervals['year'])
    {
        $diff = floor($diff/$intervals['year']);
        return $diff == 1 ? 'un anno fa' : $diff . ' anni fa';
    }
}

function exists($t, $id, $torna, $msg = "Id non esistente", $where = "") {
    global $DB;
    $Di = crc32("esiste");
    
    $query = "SELECT * FROM $t WHERE id = '".addslashes($id)."'".($where ? " AND ".$where : "");
    if (!$DB->querynum($query, $Di)) {
        torna($torna, $msg);
    }
    else {
        return $DB->fetch($Di);
    }
}

function sortOnTimestamp($a, $b) {
    return $a['timestamp'] <= $b['timestamp'];
}

function str_lreplace($search, $replace, $subject) {
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}

function giveExport($query) {
    global $Folder_inc, $DB;
    $DBid = md5("giveExport");
    
    require_once($Folder_inc."/PHPExcel.php");

    $objPHPExcel = new PHPExcel();

    $first = true;
    $DB->query($query, $DBid);
    $row = 1;
    while ($r = $DB->fetch_a($DBid)) {
        if ($first) {
            $i = 0;
            foreach ($r as $index => $value) {
                $letter = chr(65 + ($i++));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter.'1', $index, PHPExcel_Cell_DataType::TYPE_STRING);
            }
            $first = false;
        }

        $row++;
        $i = 0;
        foreach ($r as $index => $value) {
            $letter = chr(65 + ($i++));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit($letter.$row, $value, PHPExcel_Cell_DataType::TYPE_STRING);
        }
    }

    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="export.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    exit();
}

function getTwitterUserInfo($userID, $data) {
    global $DB, $BM, $Host;
    $DBid = md5("getTwitterUserInfo");
    
    //TODO: $DBid is not used!
    
    ob_start();
    
    if ($data['when-from'] && !strtotime($data['when-from'])) {
        throw new Exception("Campo 'da' non valido");
    }
    if ($data['when-to'] && !strtotime($data['when-to'])) {
        throw new Exception("Campo 'a' non valido");
    }
    
    $timeArray = array();
    
    $secondsInWeek = 60 * 60 * 24 * 7;
    
    $startTS = time() - $secondsInWeek;
    $endTS = time();
    $timeArray[] = "created_time > DATE_SUB(NOW(), INTERVAL 7 DAY)";
    
    if ($data['when-from']) {
        $startTime = $data['when-from'];
        $timeArray = array();
        $timeArray[] = "created_time > '{$data['when-from']}'";
        $startTS = strtotime($data['when-from']);
        if ($data['when-to']) {
            $endTime = $data['when-to'];
            $timeArray[] = "created_time < '{$data['when-to']}'";
            $endTS = strtotime($data['when-to']);
        }
    }
    
    $Tpl = array();
    $Tpl['party'] = "n.d.";
    $Tpl['interval'] = getTimeTxt($startTS, $endTS);
    $Tpl['party_conf'] = 0;
    
    $Parties = array();
    $query = "SELECT * FROM parties WHERE coalition = '0'";
    $DB->query($query);
    while ($r = $DB->fetch()) {
        $Parties[$r['id']] = $r['shortname'];
    }
    
    $query = "SELECT a.*, u.*
        FROM twitter_users u
        LEFT JOIN attivisti_rt a ON (
            a.user_id = u.id
            AND (a.week = (SELECT id FROM weeks ORDER BY startdate DESC LIMIT 1))
        )
        WHERE id = '$userID'";
    
    if (!$DB->querynum($query)) {
        exit("Utente non trovato");
    }
    $rU = $DB->fetch();

    if ($rU['total'] > 5) {
        foreach ($rU as $index => $value) {
            if (preg_match("/^party_([0-9]+)$/", $index, $ris)) {
                $ratio = $value / $rU['total'];
                if ($ratio > .5) {
                    $Tpl['party'] = $Parties[$ris[1]];
                    $Tpl['party_conf'] = round(100 * $ratio);
                }
            }
        }
    }
    
    $query = "SELECT *
        FROM attivisti_rt a
        WHERE user_id = '$userID'";
    $OtherParties = array();
    $OtherParties['total'] = 0;
    $DB->query($query, $DBid);
    while ($r = $DB->fetch_a($DBid)) {
        $OtherParties['total'] += $r['total'];
        foreach ($r as $field => $value) {
            if (preg_match("/^party_([0-9]+)/", $field, $ris)) {
                if (!isset($OtherParties[$Parties[$ris[1]]])) {
                    $OtherParties[$Parties[$ris[1]]] = 0;
                }
                $OtherParties[$Parties[$ris[1]]] += $value;
            }
        }
    }

    // print_r($OtherParties);
    // exit();

    if ($OtherParties['total']) {
        foreach ($OtherParties as $key => $value) {
            $OtherParties[$key] = $value / $OtherParties['total'];
        }
    }

    unset($OtherParties['total']);
    arsort($OtherParties);

    $OtherPartiesTxt = array();
    foreach ($OtherParties as $key => $value) {
        if ($key == $Tpl['party'] || $value <= 0) {
            continue;
        }
        $OtherPartiesTxt[] = "$key (".number_format($value).")";
    }
    $Tpl['otherparties'] = "";
    if (count($OtherPartiesTxt)) {
        $Tpl['otherparties'] = "Altri partiti: ".implode(", ", $OtherPartiesTxt);
    }

    // Load twitter(s)
    $query = "SELECT s.twitter, s.politician
        FROM social_info s
        WHERE s.twitter IS NOT NULL AND s.twitter != ''";
    $Twitters = array();
    $DB->query($query);
    while ($r = $DB->fetch()) {
        $Twitters[strtolower($r['twitter'])] = $r['politician'];
    }
    
    $Tpl['username'] = $rU['username'];
    $Tpl['name'] = $rU['name'];
    $Tpl['description'] = $rU['description'];
    $Tpl['picture'] = $rU['picture'];
    $Tpl['followers'] = $rU['followers'];
    $Tpl['following'] = $rU['following'];
    $Tpl['last_tweet'] = $rU['last_tweet'];
    
    // $Tpl['poltwitter'] = $socialInfo['twitter'];
    // $Tpl['polname'] = $P->getName();
    
    $Tpl['retweets_no'] = 0;
    
    $Tpl['tweets'] = array();
    
    $Tpl['retweets'] = array();
    $Tpl['ctweets'] = array();
    $Tpl['htweets'] = array();
    $idTag = 0;
    
    $timePart = implode(" AND ", $timeArray);
    $query = "SELECT t.*, p.politician, pol.surname surnamep, pol.name namep, e.tag, e.type entity_type,
            us.picture userpic, us.username, us.name nameu,
            us2.picture userpic2, us2.username username2, us2.name nameu2
        FROM twitter_updates t
        LEFT OUTER JOIN twitter_politicians p ON p.tweet = t.id
        LEFT OUTER JOIN social_updates_new_entities e ON e.tweet = t.id
        LEFT JOIN twitter_users us ON us.id = t.user_id
        LEFT JOIN twitter_users us2 ON us2.username = t.name
        LEFT JOIN politicians pol ON pol.id = p.politician
        WHERE t.user_id = '{$rU['id']}'
        AND $timePart
        ORDER BY created_time DESC";
    $DB->query($query);
    while ($r = $DB->fetch()) {
        
        if (!isset($Tpl['tweets'][$r['id']])) {
            $Tpl['tweets'][$r['id']] = array();
            $Tpl['tweets'][$r['id']]['row'] = $r;
            $Tpl['tweets'][$r['id']]['classes'] = array();
            
            if (!$r['type2']) {
                $Tpl['retweets_no'] += $r['shares'];
            }
        }
        
        // Citations
        if ($r['politician'] && ($r['type2'] != "retweet" || $r['politician'] != $Twitters[strtolower($r['name'])])) {
            if (!isset($Tpl['ctweets'][$r['politician']])) {
                $Tpl['ctweets'][$r['politician']] = array();
            }
            $Tpl['ctweets'][$r['politician']][$r['id']] = true;
            
            $Tpl['tweets'][$r['id']]['classes']['cite_'.$r['politician']] = true;
        }
        
        // Hashtags
        if ($r['entity_type'] == "hashtag") {
            $thisID = 0;
            $r['tag'] = strtolower($r['tag']);
            if (!isset($Tpl['htweets'][$r['tag']])) {
                $thisID = $idTag++;
                $Tpl['htweets'][$r['tag']] = array();
                $Tpl['htweets'][$r['tag']]['id'] = $thisID;
                $Tpl['htweets'][$r['tag']]['count'] = 0;
            }
            else {
                $thisID = $Tpl['htweets'][$r['tag']]['id'];
            }
            $Tpl['htweets'][$r['tag']]['count']++;
            $Tpl['tweets'][$r['id']]['classes']['hash_'.$thisID] = true;
        }
        
        // Retweets
        if ($r['type2'] == "retweet") {
            $name = strtolower($r['name']);
            if (!isset($Tpl['retweets'][$name])) {
                $Tpl['retweets'][$name] = array();
                $Tpl['retweets'][$name]['ispol'] = false;
                $Tpl['retweets'][$name]['name'] = $r['nameu2'];
                $Tpl['retweets'][$name]['tweets'] = array();
                if (isset($Twitters[$name])) {
                    $Tpl['retweets'][$name]['ispol'] = $Twitters[$name];
                }
            }
            $Tpl['retweets'][$name]['tweets'][$r['id']] = true;
            
            $Tpl['tweets'][$r['id']]['classes']['retw_'.$name] = true;
        }
    }
    
    if ($data['use-filter']) {
        include("tpl/user-tweets-filter.php");
        include("tpl/user-tweets-header.php");
    }
    include("tpl/user-tweets.php");
    
    return ob_get_clean();
    
    // echo "<pre>";
    // print_r($Tpl);
    // echo "</pre>";
    
    // $Text = ob_get_clean();
    // $mess['ok'] = true;
    // $mess['data'] = ob_get_clean();
    // exit(json_encode($mess));
}

function getUserByNameId($data = array()) {
    global $DB;
    $DBid = md5("getUserByNameId");

    if (isset($data['id']) &&  is_numeric($data['id'])) {
        return $data['id'];
    }

    if (isset($data['username'])) {
        $u = addslashes($data['username']);
        $u = strtolower($u);
        $query = "SELECT id FROM twitter_users WHERE LOWER(username) = '$u'";
        if ($DB->querynum($query, $DBid)) {
            $r = $DB->fetch($DBid);
            return $r['id'];
        }
    }

    return false;
}

function getTimeTxt($startTS, $endTS) {
    $m1 = date("n", $startTS);
    $m2 = date("n", $endTS);

    if ($m1 == $m2) {
        $d1 = strftime("%e", $startTS);
        $d2 = strftime("%e", $endTS);
        $mName = strftime("%B", $startTS);
        if ($d1 == $d2) {
            $WeekTxt = "$d2 $mName";
        }
        else {
            $WeekTxt = "dal $d1 al $d2 $mName";
        }
    }
    else {
        $d1 = strftime("%e %B", $startTS);
        $d2 = strftime("%e %B", $endTS);
        $WeekTxt = "dal $d1 al $d2";
    }
    
    return $WeekTxt;
}

function isProEnabled($polID = 0) {
    return $_SESSION['Pro'] &&
        (!$polID || $_SESSION['ProAll'] || $_SESSION['ProPol'][$polID]);
}

// function primi($stringa, $quanti = 50) {
//     $stringa = trim(html_entity_decode($stringa));
//     $stringa = (strlen($stringa) > $quanti ? substr($stringa, 0, $quanti)." [...]" : $stringa);
//     return htmlentities($stringa);
// }
