<?php

class Link {
    
    static public function Simple($word) {
        global $Host;
        $ret = "http://$Host/$word";
        return $ret;
    }
    
    // ---
    
    static public function Home() {
        return Link::Simple("");
    }
    
    static public function Login() {
        return Link::Simple("login");
    }
    
    static public function Logout() {
        return Link::Simple("logout");
    }
    
    static public function Lostpass() {
        return Link::Simple("lostpass");
    }
    
    static public function Signin() {
        return Link::Simple("signin");
    }
    
    static public function Registered() {
        return Link::Simple("registered");
    }
    
    // ---
    
    static public function Dashboard() {
        return Link::Simple("dashboard");
    }
    
    static public function Pro() {
        return Link::Simple("pro");
    }
    
    static public function Fonti() {
        return Link::Simple("fonti");
    }
    
    static public function Contatti() {
        return Link::Simple("contatti");
    }
    
    static public function Random() {
        return Link::Simple("random");
    }
    
    static public function Tendenze() {
        return Link::Simple("tendenze");
    }
    
    static public function CustomizeWidget() {
        return Link::Simple("customizewidget");
    }
    
    static public function Features() {
        return Link::Simple("features");
    }
    
    static public function Chart($which, $type = 2, $datetime = 0) {
        $ret = Link::Simple($which);
        if ($type) {
            $ret .= "/$type";
        }
        if ($datetime) {
            if (!$type) {
                $ret .= "/2";
            }
            $ret .= "/$datetime";
        }
        return $ret;
    }
    
    static public function Politician($id, $sub = "") {
        $ret = Link::Simple($id);
        if ($sub) {
            $ret .= "/$sub";
        }
        return $ret;
    }
    
    static public function PoliticianInDashboard($id, $sub = "") {
        $ret = Link::Dashboard();
        $ret .= "/$id";
        if ($sub) {
            $ret .= "/$sub";
        }
        return $ret;
    }
    
    // ---
    
    static public function WikiLink($page, $lang = 'it') {
        return "http://$lang.wikipedia.org/wiki/$page";
    }
}