<?php
require_once 'Predis/Autoloader.php';

class QueryPreloader {
    
    private $predis;
    private $DB;
    private $log = false;

    public function __construct($DB) {
        $this->DB = $DB;
        try {
            Predis\Autoloader::register();
            $this->predis = new Predis\Client();
            $this->predis->ping();
        } catch (Exception $e) {
            error_log("WARNING! Redis has some problems: " .$e->getMessage());
        }
    }
    
    public function get($query, $expire = 300) {
        $DBid = mt_rand(100, getrandmax());
        
        $res = array();
        $loaded = false;

        if ($predis && $this->predis->exists($query) && $this->predis->get($query)) {
            // Loaded
            // echo "Loaded $query<br />\n";
            $loaded = true;
            $res = json_decode($this->predis->get($query), true);
        } else {
            $this->DB->query($query, $DBid);
            while ($r = $this->DB->fetch($DBid)) {
                $res[] = $r;
            }
            // Saved
            // echo "Saved $query<br />\n";
            // echo json_encode($res);
            if ($predis) {
                $this->predis->set($query, json_encode($res));
                if ($expire) {
                    $this->predis->expire($query, $expire);
                }
            }
        }

        // if ($this->log) {
        //     $data = array();
        //     $data['query'] = $query;
        //     $data['status'] = $loaded ? 1 : 0;
        //     $data['ip'] = $_SERVER['REMOTE_ADDR'];
        //     $this->DB->queryinsert("log_cache", $data, $DBid);
        // }
        
        // print_r($res);
        return new QueryPreloaderResource($res, $loaded);
    }
}

class QueryPreloaderResource {
    
    private $index = 0;
    private $json = array();
    private $loaded = false;
    
    public function __construct($json, $loaded = false) {
        $this->index = 0;
        $this->json = $json;
        $this->loaded = $loaded;
    }
    
    public function getJson() {
        return $this->json;
    }
    
    public function isLoaded() {
        return $this->loaded;
    }
    
    public function num() {
        return count($this->json);
    }
    
    public function next() {
        $ret = false;
        
        if (isset($this->json[$this->index])) {
            $ret = $this->json[$this->index];
            $this->index++;
        }
        
        return $ret;
    }
}

