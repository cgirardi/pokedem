<?php $Data = $_SESSION['form'];
if (isset($Data['test_fail_time']) && $Data['test_fail_time'] != "0000-00-00 00:00:00") {
    echo "<div class='alert alert-error' id='test_fail_time'><button type='button' class='close' data-dismiss='alert'>&times;</button>Il test è fallito il giorno {$Data['test_fail_time']} (similarità al ". number_format(strlen($Data['test_output'])/strlen($Data['test_checked_output']) * 100, 1)."%)</div>";
}
?>
<div class='row'>
    <form class="form-horizontal" method="post" action="?action=<?php echo $Action; ?>">
        <div class="control-group">
                <label class="label" for="id_name">Name</label>
                <input class='span4' name="name" maxlength="100" placeholder="Newspaper name" type="text" class="input-large" id="id_name"
                    value="<?php if (isset($Data['name'])) echo htmlentities($Data['name'], ENT_QUOTES); ?>" />

                <label class="label" for="id_type">Type</label>
                <select class='span4' name="type">
                    <option value='0'>[Non selezionato]</option>
                    <option value='1' <?php if ($Data['type'] == 1) { echo "selected='selected'";} ?>>Stampa nazionale cartaceo</option>
                    <option value='2' <?php if ($Data['type'] == 2) { echo "selected='selected'";} ?>>Stampa nazionale online</option>
                    <option value='3' <?php if ($Data['type'] == 3) { echo "selected='selected'";} ?>>Stampa locale</option>
                    <option value='4' <?php if ($Data['type'] == 4) { echo "selected='selected'";} ?>>Agenzia stampa</option>
                    <option value='5' <?php if ($Data['type'] == 5) { echo "selected='selected'";} ?>>Blog</option>
                    <option value='6' <?php if ($Data['type'] == 6) { echo "selected='selected'";} ?>>Organo di partito</option>
                </select>
                <!-- <input class='span10' name="name" maxlength="100" placeholder="Newspaper name" type="text" class="input-large" id="id_name"
                    value="<?php if (isset($Data['name'])) echo htmlentities($Data['name'], ENT_QUOTES); ?>" /> -->

        </div>
        <div class="control-group">
            <label class="label" for="id_rss">RSSs</label>
            <div class="control">
                <textarea rows='5' class='span10' name="rss" class="input-large" id="id_rss"><?php if (isset($Data['rss'])) echo htmlentities($Data['rss'], ENT_QUOTES); ?></textarea>
            </div>
        </div>
        <div class='control-group'>
            <div class="btn-group-vertical">
                <div class='btn-group-vertical'>
                    <label class="label" for="id_pattern">HTML Patterns</label><br>
                    <textarea rows='5' class='span5' name="pattern" id="id_pattern"><?php if (isset($Data['pattern'])) echo htmlentities($Data['pattern'], ENT_QUOTES); ?></textarea>
                </div>
                <div class='btn-group-vertical'>
                    <label class="label" for="id_sections">Section Patterns</label><br>
                    <textarea rows='5' class='span5' name="sections" id="id_sections"><?php if (isset($Data['sections'])) echo htmlentities($Data['sections'], ENT_QUOTES); ?></textarea>
                </div>
            </div>
        </div>
        <div class='control-group'>
            <div class="btn-group-vertical">
                 <div class='btn-group-vertical'>
                    <label class="label" for="id_images">Image Patterns</label> <br>
                    <textarea rows='5' class='span5' name="images" id="id_images"><?php if (isset($Data['images'])) echo htmlentities($Data['images'], ENT_QUOTES); ?></textarea>
                 </div>
                 <div class='btn-group-vertical'>
                    <label class="label" for="id_tags">Tag Patterns</label><br>
                    <textarea rows='5' class='span5' name="tags" id="id_tags"><?php if (isset($Data['tags'])) echo htmlentities($Data['tags'], ENT_QUOTES); ?></textarea>
                 </div>
            </div>
        </div>
        <div class="control-group">
            <label class="label">Encoding</label>
            <select class='span4' name="encoding" id="id_encoding">
                <option value=''>[Auto detect]</option>
                <option value='UTF8' <?php if ($Data['encoding'] == "UTF8") { echo "selected='selected'";} ?>>UTF-8</option>
                <option value='ISO8859-1' <?php if ($Data['encoding'] == "ISO8859-1") { echo "selected='selected'";} ?>>ISO8859-1</option>
                <option value='ASCII' <?php if ($Data['encoding'] == "ASCII") { echo "selected='selected'";} ?>>ASCII</option>
            </select>
        </div>
        <div class="control-group">
            <label class="label">User-Agent</label>
            <select class='span4' name="user_agent" id="id_user_agent">
                <option value='phone'>Phone</option>
                <option value='pc' <?php if ($Data['user_agent'] == "pc") { echo "selected='selected'";} ?>>PC</option>
            </select>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Submit form" class="btn btn-primary" />  <a target="_new" href="http://www.w3schools.com/cssref/css_selectors.asp">CSS selector reference</a>
            </div>
        </div>
        <div class="control-group">
            <label class="label" for="id_name">URL</label>
                <input class='span9' name="url" maxlength="200" placeholder="URL" type="text" class="input-large" id="id_url" value="<?php if (isset($Data['url'])) echo htmlentities($Data['url'], ENT_QUOTES); ?>" />
                <input type="button" value="View code" class="btn btn-info" onclick='return launchViewer(this,<?php echo $ID ?>);' />
                <input type="button" value="Test" class="btn btn-info" onclick='return launchTest(this,<?php echo $ID ?>);' />

        </div>
    </form>
</div>
<div class='row testrow'>
    <h3>Risultato del test</h3>
    <div id='testcontent'></div>
</div>
