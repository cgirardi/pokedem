<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
    <title>Administration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    
    <style type="text/css">
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
      
      .hide {
          display: none;
      }
      
      .testrow {
          display: none;
          background-color: #ddd;
          padding: 10px;
      }
      
    </style>
    
  </head>
  <body>
      
      <div id="modal-from-dom" class="modal hide fade">
          <div class="modal-header">
            <a href="#" class="close">&times;</a>
            <h3>Delete Newspaper</h3>
          </div>
          <div class="modal-body">
            <p>You are about to delete a newspaper from the DB, this procedure is irreversible.</p>
            <p>Do you want to proceed?</p>
          </div>
          <div class="modal-footer">
            <a href="?action=delete&amp;id=" class="btn btn-danger">Yes</a>
            <a href="javascript:$('#modal-from-dom').modal('hide')" class="btn btn-secondary">No</a>
          </div>
      </div>
      
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li><a href="?">Home</a></li>
                <!-- <li><a href="?action=test">Test</a></li> -->
                <li><a href="?action=keywords">Keywords</a></li>
                <li><a href="?action=logout">Logout</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
          <div class="row">
              <?php echo $Message; ?>
              <h1><?php if ($Title) { ?><a class='btn' href='?'>Back</a> <?php } echo $Title; ?></h1>
              <?php echo $Text; ?>
          </div>
      </div>

      <!-- Le javascript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="http://code.jquery.com/jquery.js"></script>
      <script src="../js/bootstrap.min.js"></script>
      
      <script type='text/javascript'>
      $('#modal-from-dom').bind('show', function() {
          var id = $(this).data('id'),
              removeBtn = $(this).find('.btn-danger'),
              href = removeBtn.attr('href');
      
          removeBtn.attr('href', href.replace(/&id=\d*/, '&id=' + id));
      });
      // .modal({ backdrop: true });

      $('.confirm-delete').click(function(e) {
          e.preventDefault();
          var id = $(this).data('id');
          $('#modal-from-dom').data('id', id).modal('show');
      });

      function launchViewer(el, id) {
         var form = $(el).parents('form');
         var url = form.find('#id_url').val();
         var ua = "";
         var elt = document.getElementById("id_user_agent");
         if (elt.selectedIndex != -1)
            ua = elt.options[elt.selectedIndex].value;

         window.open("?action=view&ua="+ua+"&url="+encodeURIComponent(url), "htmlviewer");
      }

      function launchTest(el, id) {
          var form = $(el).parents('form');
          $('.testrow').show();
          
          var url = form.find('#id_url').val();
          var pattern = form.find('#id_pattern').val();
          var images = form.find('#id_images').val();
          var tags = form.find('#id_tags').val();
          var sections = form.find('#id_sections').val();

          var ua = "phone";
          var elt = document.getElementById("id_user_agent");
          if (elt.selectedIndex != -1)
            ua = elt.options[elt.selectedIndex].value;

          var encoding = "";
          var elt = document.getElementById("id_encoding");
          if (elt.selectedIndex != -1)
            encoding = elt.options[elt.selectedIndex].value;

          $.ajax({
              url: '?action=test&id='+id,
              method: 'post',
              dataType: 'html',
              data: {
                  url: url,
                  pattern: pattern,
                  images: images,
                  tags: tags,
                  sections: sections,
                  encoding: encoding,
                  ua: ua
              },
              success: function(data) {
                  $('#test_fail_time').hide();
                  $('#testcontent').html(data);
              }
          });
          
          return false;
      }
      </script>
      
  </body>
</html>