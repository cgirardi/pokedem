<?php

if (get_magic_quotes_gpc()) {
   $_POST = array_map('stripslashes_deep', $_POST);
   $_GET = array_map('stripslashes_deep', $_GET);
   $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
   $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
}

function torna($url = "?", $messaggio = "", $bene = 0) {
	if ($messaggio) {
		$_SESSION['mess'] = $messaggio;
		$_SESSION['messok'] = $bene;
	}
	header("Location: ".$url);
	exit();
}

function check_newspaper($torna, $v = array()) {
    if (!count($v)) {
        $v = $_SESSION['form'];
    }
    if (!$v['name']) {
        torna($torna, "Name field is mandatory");
    }
    if (!$v['rss']) {
        torna($torna, "RSS field is mandatory");
    }
    
    return $v;
}

function esiste($t, $id, $torna, $msg = "Id non esistente", $where = "") {
	global $DB;
	$Di = crc32("esiste");
	
	$query = "SELECT * FROM $t WHERE id = '".addslashes($id)."'".($where ? " AND ".$where : "");
	if (!$DB->querynum($query, $Di)) {
		torna($torna, $msg);
	}
	else {
		return $DB->fetch($Di);
	}
}

function absolute_url($txt, $base_url){ 
  $needles = array('href="', 'src="', 'background="'); 
  $new_txt = ''; 
  if(substr($base_url,-1) != '/') $base_url .= '/'; 
  $new_base_url = $base_url; 
  $base_url_parts = parse_url($base_url); 

  foreach($needles as $needle){ 
    while($pos = strpos($txt, $needle)){ 
      $pos += strlen($needle); 
      if(substr($txt,$pos,7) != 'http://' && substr($txt,$pos,8) != 'https://' && substr($txt,$pos,6) != 'ftp://' && substr($txt,$pos,9) != 'mailto://'){ 
        if(substr($txt,$pos,1) == '/') $new_base_url = $base_url_parts['scheme'].'://'.$base_url_parts['host']; 
        $new_txt .= substr($txt,0,$pos).$new_base_url; 
      } else { 
        $new_txt .= substr($txt,0,$pos); 
      } 
      $txt = substr($txt,$pos); 
    } 
    $txt = $new_txt.$txt; 
    $new_txt = ''; 
  } 
  return $txt; 
}

