<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
      <div class="container">
          <div class="row">
              <div class="offset4 span4">
                  <form class="form-horizontal" method="post" action="?action=login">
                      <fieldset>
                          <legend>User login</legend>
                          <div class="control-group">
                              <label class="control-label" for="id_username">Username</label>
                              <div class="controls">
                                  <input name="username" maxlength="100" placeholder="Enter your username..." type="text" class="input-large" id="id_username" />
                              </div>
                          </div>
                          <div class="control-group">
                              <label class="control-label" for="id_password">Password</label>
                              <div class="controls">
                                  <input name="password" maxlength="100" placeholder="Enter your password..." type="password" class="input-large" id="id_password" />
                              </div>
                          </div>
                          <div class="control-group">
                              <div class="controls">
                                  <input type="submit" value="Login" class="btn btn-primary" />
                              </div>
                          </div>
                      </fieldset>
                  </form>
              </div>
          </div>
      </div>

      <!-- Le javascript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="http://code.jquery.com/jquery.js"></script>
      <script src="../js/bootstrap.min.js"></script>
      
  </body>
</html>
