<link rel="stylesheet" href="../css/github.css">
<script src="../js/jquery.js"></script>
<script src="../js/highlight.pack.js"></script>
<script>
hljs.initHighlightingOnLoad();
</script>

<?php
$resp ="";
if (isset($url)) {
  $ua = PHONE_UA;
  if (isset($_REQUEST['ua']) && $_REQUEST['ua'] == "pc") {
     $ua = PC_UA;
  }
  $htmlName = tempnam(sys_get_temp_dir(), "html");
  $command = "$Folder_scripts/../api/pageparser.sh -f $htmlName -a '$ua' -u '$url'";
  echo "URL: <a target='_new' href='$url'>$url</a>";
  $res = shell_exec($command);
  //if ($res != "") {
  //  echo "<br><br>Used command: $command<br><font color=red>$res</font>";
  //}
  if (file_exists($htmlName)) {
    $html = file_get_contents($htmlName);
    if (strlen($html) > 0) {
        $htmlspecialcharsResult = htmlspecialchars($html, ENT_QUOTES, "UTF-8");
        if (strlen($htmlspecialcharsResult) == 0) {
            $htmlspecialcharsResult = htmlspecialchars($html, ENT_QUOTES, "ISO-8859-1");
            if (strlen($htmlspecialcharsResult) == 0) {
                $htmlspecialcharsResult = $html;
            }
        }
    }
    //unlink($htmlName);
?>

<pre>
<code class="html" id="code">
<?php
    echo $htmlspecialcharsResult;
?>
</code>
</pre>

<?php
   } else {
      print "Download of the page $url failed.";
   }
} else {
  print "URL is no valid";
}
?>

