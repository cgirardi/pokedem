<?php
//header('Content-Type: text/html; charset=UTF-8');
//setlocale(LC_ALL, "en_US.UTF-8");
define("PC_UA", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");
define("PHONE_UA", "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53");

if (getenv('PD_CONFIG_FILE')) {
    $Config = parse_ini_file(getenv('PD_CONFIG_FILE'));

    foreach ($Config as $index => $value) {
        $$index = $value;
    }
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

if ($Folder_inc[0] != "/") {
    $Folder_inc = '../'.$Folder_inc;
}
include($Folder_inc.'/include.php');
require_once("inc/functions.php");

$Action = $_REQUEST['action'];
// echo "## $Action ".$_POST['username']."==$Admin_User && ".$_POST['password']."==$Admin_Password";

if ($Action == "login") {
    if ($_POST['username'] == $Admin_User && $_POST['password'] == $Admin_Password) {
        $_SESSION['AdminLogin'] = 1;
    }
    torna("?", "");
}
if ($Action == "logout") {
    unset($_SESSION['AdminLogin']);
}

if (!$_SESSION['AdminLogin']) {
    include("inc/login.php");
    exit();
}

ob_start();
switch ($Action) {
    case "keywords":
        $sub = $_REQUEST['sub'];

        if($sub) {
            //echo "HERE";
            $error = 0;
            try {
                $keyword = $_REQUEST['keyword'];
                if(!$keyword) {
                    throw new Exception('Keyword not specified.');
                }
                if($sub == 'delete') {
                    $query = "DELETE FROM blacklisted_keywords WHERE keyword = '$keyword'";
                    $DB->query($query);

                } else if($sub == 'insert') {
                    $query = "INSERT INTO blacklisted_keywords (keyword, created) VALUES ('$keyword', now())";
                    $DB->query($query);
                }
            } catch(Exception $e) {
                $error = 1;
            }
            torna('?action=keywords', $error ? $e->getMessage() : "Operation successful.", $error);
            break;
        }

        $query = "SELECT * FROM blacklisted_keywords";
        $DB->query($query);

?>
        <h1>Blacklisted Keywords</h1>

        <form class="form-inline" action=".">
        <fieldset>
        <legend>Add Blacklisted Keyword</legend>
        <input name="keyword" type="text" placeholder="keyword"/>
        <input name="action" type="hidden" value="keywords"/>
        <input name="sub" type="hidden" value="insert"/>
        <button type="submit" class="btn">Add</button>
        </fieldset>
        </form>

        <table class="table table-striped">
            <thead>
                <tr><th>Keyword</th><th>Creation Date</th><tr>
            </thead>
            <tbody>

<?php
        while ($r = $DB->fetch()) {
            echo "<tr><td>{$r['keyword']}</td><td>{$r['created']}</td><td><a class=\"btn btn-small btn-danger\" href=\"?action=keywords&sub=delete&keyword=".urlencode($r['keyword'])."\">Delete</a></td></tr>\n";
        }
?>
        </tbody>
<?php
        break;

    case "test":
        if (isset($_REQUEST['url']) && ($_REQUEST['pattern'] || $_REQUEST['images'])) {
            $url = $_REQUEST['url'];

            if (substr($url, 0, 4) != "http") {
                exit();
            }

            $articleName = tempnam(sys_get_temp_dir(), "article");
            $imagesName = tempnam(sys_get_temp_dir(), "images");
            $htmlName = tempnam(sys_get_temp_dir(), "html");
            $tagsName = tempnam(sys_get_temp_dir(), "tags");
            $sectName = tempnam(sys_get_temp_dir(), "sect");

            $article = fopen($articleName, "w");
            fwrite($article, $_REQUEST['pattern']);
            fclose($article);

            $images = fopen($imagesName, "w");
            fwrite($images, $_REQUEST['images']);
            fclose($images);

            //la pagine viene scaricata da PageParser
            //file_put_contents($htmlName, file_get_contents($url));
            $tags = fopen($tagsName, "w");
            fwrite($tags, $_REQUEST['tags']);
            fclose($tags);

            $sections = fopen($sectName, "w");
            fwrite($sections, $_REQUEST['sections']);
            fclose($sections);

            $ua = $_REQUEST['ua'];
            $encoding_option="";
	    if (isset($_REQUEST['encoding'])) {
		$encoding = $_REQUEST['encoding'];
	        if ($encoding != "") {
		   $encoding_option = " -encoding $encoding";
	        }
	    }
            //AGGIUNGERE $ua
            // echo "<div class='row'>\n";
            // java -cp HTMLdownload.jar:HTMLdownload_utils.jar org.fbk.cit.hlt.htmldownload.util.PageParser -f /tmp/repubblica.txt -p /tmp/repubblica-pattern.txt -i /tmp/repubblica-img.txt
            $command = "$Folder_scripts/../api/pageparser.sh -o json ".$encoding_option." -f $htmlName -p $articleName -i $imagesName -t $tagsName -s $sectName -u '$url' 2> /dev/null";
            $res = shell_exec($command);

            $json = json_decode($res);

            echo "<p>\n";
            echo "<strong>Command:</strong> $command";
            echo "</p>\n";


            echo "<p><strong>Section:</strong>\n";
            echo $json->{'sections'};
            echo "</p>\n";

            echo "<p><strong>Article:</strong>\n";
            echo $json->{'body'};
            echo "</p>\n";

            echo "<p><strong>Tags:</strong>\n";
            echo $json->{'tags'};
            echo "</p>\n";

            echo "<p>\n";
            $img = $json->{'image'};
            if ($img) {
                if ($R['basepath']) {
                    $img = rel2abs($img, $R['basepath']);
                }
                else {
                    $img = rel2abs($img, $url);
                }
            }
            $imgok = htmlentities($img, ENT_QUOTES, "UTF-8");
            echo "<strong>Image:</strong><br /><img src='$imgok' />";
            echo "</p>\n";

            // echo "</div>\n";

            unlink($htmlName);
            unlink($imagesName);
            unlink($articleName);
            unlink($tagsName);
            unlink($sectName);
            //system("/bin/chmod -R 777 /tmp/*");

            $ID=addslashes($_REQUEST['id']);
            $data["pattern"] = $_REQUEST['pattern'];
            $data["images"] = $_REQUEST['images'];
            $data["tags"] = $_REQUEST['tags'];
            $data["sections"] = $_REQUEST['sections'];
            $data["url"] = $url;
            if ($json->{'body'} != "" || $json->{'sections'} != "" || $json->{'tags'} != "" || $json->{'image'} != "") {
                $data["test_output"] = $res;
            } else {
                $data["test_output"] = "";
            }
            //$data["test_time"] = time();
            //$data["test_fail_time"] = "";
            $data["user_agent"] = $ua;
            $data["encoding"] = $encoding;
            $DB->queryupdate("sites", $data, array("id" => $ID));

            if ($data["test_output"] != "") {
                $DB->query("UPDATE sites SET test_time=current_timestamp(), test_fail_time=null WHERE id='$ID'");
            } else {
                $DB->query("UPDATE sites SET test_time=null, test_fail_time=current_timestamp() WHERE id='$ID'");
            }
            exit();
        }

?>
        
        <div class='row'>
            <form class="form-horizontal" method="post" action="?action=<?php echo $Action; ?>">
                <div class="control-group">
                    <label class="control-label" for="id_name">URL</label>
                    <div class="controls">
                        <input class='span10' name="url" placeholder="Enter URL here" type="text" class="input-large" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="id_type">Newspaper</label>
                    <div class="controls">
                        <select class='span10' name="newspaper">
                            <option value='0'>[Non selezionato]</option>
                            <?php
                            $query = "SELECT * FROM sites ORDER BY name";
                            $DB->query($query);
                            while ($r = $DB->fetch()) {
                                echo "<option value='{$r['id']}'>{$r['name']}</option>\n";
                            }
                            ?>
                        </select>
                        <!-- <input class='span10' name="name" maxlength="100" placeholder="Newspaper name" type="text" class="input-large" id="id_name"
                            value="<?php if (isset($Data['name'])) echo htmlentities($Data['name'], ENT_QUOTES); ?>" /> -->
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Check" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        <?php
        break;

    case "delete":
        $Torna = "?";
        $ID = addslashes($_REQUEST['id']);
        $_SESSION['form'] = esiste("sites", $ID, $Torna, "ID not found");

        $query = "DELETE FROM sites WHERE id = '$ID'";
        $DB->query($query);
        torna($Torna, "Record successfully deleted", 1);
        break;

    case "enable":
        $Torna = "?";
        $ID = addslashes($_REQUEST['id']);
        $_SESSION['form'] = esiste("sites", $ID, $Torna, "ID not found");

        $query = "UPDATE sites SET active = CASE WHEN active=1 THEN 0 ELSE 1 END WHERE id = '$ID'";
        $DB->query($query);
        torna($Torna, "", 1);
        break;

    case "edit2":
        $Torna = "?";
        $ID = addslashes($_REQUEST['id']);
        $_SESSION['form'] = esiste("sites", $ID, $Torna, "ID not found");

        $Torna = "?action=edit&id={$ID}";
        //$TornaOk = "?";
        $_SESSION['form'] = $_POST;
        $data = check_newspaper($Torna);
        if (isset($data['user_agent']) && $data['user_agent'] == "pc") {
            $data['user_agent'] = PC_UA;
        } else {
            $data['user_agent'] = PHONE_UA;
        }
        //unset($data['url']);
        $DB->query("UPDATE sites SET test_time=NULL, test_fail_time=NULL WHERE id='$ID'");
        $DB->queryupdate("sites", $data, array("id" => $ID));
        torna($Torna, "Newspaper successfully modified", 1);
        break;

    case "edit":
        $Torna = "?";
        $ID = addslashes($_REQUEST['id']);
        $fields = esiste("sites", $ID, $Torna, "ID not found");
        //$fields = array_map("utf8_decode", $fields);
        $_SESSION['form'] = $fields;

        $Title = "Edit newspaper";
        $Action = "edit2&id={$ID}";
        include("inc/form.php");
        break;

    case "add2":
        $Torna = "?action=add";
        $TornaOk = "?";
        $_SESSION['form'] = $_POST;
        $data = check_newspaper($Torna);
        if (isset($data['user_agent']) && $data['user_agent'] == "pc") {
            $data['user_agent'] = PC_UA;
        } else {
            $data['user_agent'] = PHONE_UA;
        }
        //unset($data['url']);
        $DB->queryinsert("sites", $data);
        torna($TornaOk, "Newspaper successfully added", 1);
        break;

    case "add":
        $Title = "Add newspaper";
        $Action = "add2";
        include("inc/form.php");
        break;

    case "view":
        $ID = $_REQUEST['id'];
        $url = $_REQUEST['url'];
        $ua = PC_UA;
        include("htmlviewer.php");
        exit;
    default:
        echo "<h1>Newspapers management</h1>";
        echo "<p><a class='btn btn-primary' href='?action=add'>Add newspaper</a></p>";
        $query = "SELECT id,name,active,url,test_fail_time FROM sites ORDER BY name";
        if ($DB->querynum($query)) {
            echo "<table class='table table-striped'>";
            // echo "<thead>";
            // echo "<tr><th>Name</th><th>&nbsp;</th></tr>";
            // echo "</thead>";
            echo "<tbody>";
            while ($r = $DB->fetch()) {
                $activeNews = "Off";
                if ($r['active'] == 1) {
                    $activeNews = "On";
                }
                echo "<tr><td>{$r['name']}</td><td><a class='btn btn-danger btn-small confirm-delete' href='#' data-id='{$r['id']}'>Delete</a><a class='btn btn-small' href='?action=edit&id={$r['id']}'>Edit";
                if ($r['url'] == "" ) {
                    echo "<img src='../img/test_unavailable.png' title='test unavailable'>";
                } else if ($r['test_fail_time'] != "" ) {
                    echo "<img src='../img/exclamation.png' title='test failed ({$r['test_fail_time']})'>";
                } else {
                    echo "<img src='../img/accept.png' title='test passed'>";
                }

                echo "</a></td><td align=left><div title='Active/Disactive this source' class='Switch Round $activeNews' onclick=\"javascript:enableSource('{$r['id']}');\"><div class='Toggle'></div></div></td></tr>";
            }
            echo "</tbody>";
            echo "</table>";
        }
        else {
            echo "<p>No newspapers</p>";
        }

        // Compile pattern
        // $F = fopen("$Folder/news-list.txt", "w");
        // $query = "SELECT * FROM sites ORDER BY name";
        // $DB->query($query);
        // while ($r = $DB->fetch()) {
        //     fwrite($F, $r['name']);
        //     fwrite($F, "|");
        //     fwrite($F, $r['id']);
        //
        //     file_put_contents("$Folder/rss/{$r['id']}.txt", $r['rss']);
        //
        //     if (trim($r['pattern'])) {
        //         file_put_contents("$Folder/pattern/{$r['id']}.txt", $r['pattern']);
        //     }
        //     else {
        //         fwrite($F, "|");
        //         fwrite($F, "false");
        //     }
        //
        //     fwrite($F, "\n");
        // }
        // fclose($F);
}
$Text = ob_get_clean();

$Message = "";
if (isset($_SESSION['mess']) && $_SESSION['mess']) {
    $class = "alert-error";
    if (isset($_SESSION['messok']))
        $class = $_SESSION['messok'] == 1 ? 'alert-error' : 'alert-success';
    $Message .= "<div class='alert $class'>";
    $Message .= "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
    $Message .= $_SESSION['mess'];
    $Message .= "</div>";
}

include("inc/index.php");

unset($_SESSION['mess']);
unset($_SESSION['messok']);
unset($_SESSION['form']);

?>

<style>
/*------------------------------------------------*/
.Switch {
position: relative;
display: inline-block;
font-size: 1.6em;
font-weight: bold;
color: #ccc;
text-shadow: 0px 1px 1px rgba(255,255,255,0.8);
height: 18px;
padding: 6px 6px 5px 6px;
border: 1px solid #ccc;
border: 1px solid rgba(0,0,0,0.2);
border-radius: 4px;
background: #ececec;
box-shadow: 0px 0px 4px rgba(0,0,0,0.1), inset 0px 1px 3px 0px rgba(0,0,0,0.1);
cursor: pointer;
}

body.IE7 .Switch { width: 78px; }

.Switch span { display: inline-block; width: 35px; }
.Switch span.On { color: #33d2da; }

.Switch .Toggle {
position: absolute;
top: 1px;
width: 37px;
height: 25px;
border: 1px solid #ccc;
border: 1px solid rgba(0,0,0,0.3);
border-radius: 4px;
background: #fff;
background: -moz-linear-gradient(top,  #ececec 0%, #ffffff 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ececec), color-stop(100%,#ffffff));
background: -webkit-linear-gradient(top,  #ececec 0%,#ffffff 100%);
background: -o-linear-gradient(top,  #ececec 0%,#ffffff 100%);
background: -ms-linear-gradient(top,  #ececec 0%,#ffffff 100%);
background: linear-gradient(top,  #ececec 0%,#ffffff 100%);

box-shadow: inset 0px 1px 0px 0px rgba(255,255,255,0.5);
z-index: 999;

-webkit-transition: all 0.15s ease-in-out;
-moz-transition: all 0.15s ease-in-out;
-o-transition: all 0.15s ease-in-out;
-ms-transition: all 0.15s ease-in-out;
}

.Switch.On .Toggle { left: 2%; }
.Switch.Off .Toggle { left: 54%; }


/* Round Switch */
.Switch.Round {
padding: 0px 20px;
border-radius: 40px;
}

body.IE7 .Switch.Round { width: 1px; }

.Switch.Round .Toggle {
border-radius: 40px;
width: 14px;
height: 14px;
}

.Switch.Round.On .Toggle { left: 3%; background: #4fc845; }
.Switch.Round.Off .Toggle { left: 58%; }
</style>

<script>
$(document).ready(function() {
	// Switch toggle
	$('.Switch').click(function() {
		$(this).toggleClass('On').toggleClass('Off');
	});

});

function enableSource (id) {
    $.ajax({
        url: '?action=enable&id='+id,
        method: 'get',
        dataType: 'html'
    });
    return false;
}
</script>


