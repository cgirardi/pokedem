<div class="row" id="login-form">
    <div class="span4 offset4">
      <div class="well">
        <legend>Login</legend>
        <form method="POST" action="?action=login" accept-charset="UTF-8">
            <input class="span3" placeholder="Indirizzo e-mail" type="text" name="email">
            <input class="span3" placeholder="Password" type="password" name="password"> 
            <!-- <label class="checkbox">
                <input type="checkbox" name="remember"> Ricordami
            </label> -->
            <button class="btn-info btn" type="submit">Accedi</button>
            <a class="btn-warning btn" href="<?php echo Link::Lostpass(); ?>">Password dimenticata</a>
            <hr />
            <p style='font-size: 100%;'>
                <a href='?action=signin_form'>Non sei ancora registrato? Fallo ora!</a>
            </p>
            <p>
                <a href="<?php echo $fbLoginUrl; ?>" class="btn btn-large" id='btn-fb'><img class='logo-social' src='img/fb-small.png' /> Accedi con Facebook</a>
            </p>
        </form>    
      </div>
    </div>
</div>
