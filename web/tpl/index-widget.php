<?php

$CSS[] = "css/widget.css?version=4";

?><!DOCTYPE html>
<html lang="en" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    <base target='_blank' />
    <?php include("head.php"); ?>
</head>
  <body<?php echo $IsMini ? " id='body-mini'" : ""; ?>>
      <div id='widget-header<?php echo $IsMini ? "-mini" : ""; ?>'>
          <a href='http://<?= $settings["server"] ?>' id='link-home'></a>
          <div>
              <?php
              if ($Occhiello) {
                  echo "<small>$Occhiello<br /></small>";
              }
              echo "<a href='$Link'>$Title</a>";
              ?>
          </div>
      </div>
      <div style='width: <?php echo $Width; ?>px; height: <?php echo $Height; ?>px;' id='widget-div'>
          <?php echo $Body; ?>
      </div>

    <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39414879-2']);
    _gaq.push(['_setDomainName', '<?= $settings["server"] ?>']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    </script>
    
    <script type="text/javascript" charset="utf-8" src='js/widget.js?version=2'></script>
    
  </body>
</html>
