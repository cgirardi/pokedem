<div class='row'>
    <div class='span6'>
        <h2 id='h2-chi-siamo'>
            Chi siamo
        </h2>
        <p>
            
        </p>
    </div>
    
    <div class='span6'>
        <h2 id='h2-tecnologia'>
            Tecnologia
        </h2>
        <p>
            Questo sito è basato su una tecnologia stato dell'arte 
            (<a href='http://www.machinelinking.com/'>Machine Linking</a>)
            che permette di seguire i nostri 
            rappresentati sul Web con una precisione altissima e 
            garantendo massima copertura delle notizie.
        </p>
        <p>
            Molteplici sorgenti, siti di informazione, blog e 
            social network (Facebook e Twitter), sono analizzati
            in tempo reale: se il rappresentante &egrave; menzionato,
            tutti gli iscritti vengono notificati nel modo che 
            preferiscono.
            <!-- , email o 
            direttamente con post sul social network preferito. -->
        </p>
        <p>
            La nostra tecnologia permette agli utenti con un solo 
            colpo d’occhio di capire di cosa si parla e di decidere se 
            leggere l’articolo intero, condividerlo o archiviarlo.
        </p>
    </div>
</div>

<div class='row'>
    <div class='span6'>
        <h2 id='h2-contatti'>
            Contatti
        </h2>
        <p>
            Machine Linking s.r.l.<br />
            Via Sommarive, 18<br />
            38123 Trento<br />
            info@machinelinking.com
        </p>
    </div>
    
    <div class='span6'>
        <h2 id='h2-lavora-con-noi'>
            Lavora con noi
        </h2>
        <p>
            Machine Linking è 
            un'azienda in continua 
            crescita.<br />
            Invia il tuo CV o 
            consulta le posizioni aperte.
        </p>
<!--         <h2 id='h2-il-nostro-team'>
            Il nostro team
        </h2>
        <table class='table table-striped' id='table-staff'>
            <tbody>
                <tr>
                    <td class='table-image'><img src='img/staff/paolo.jpg' border='0' /></td>
                    <td>Paolo Lombardi<br /><small>Fondatore e Presidente</small></td>
                </tr>
                <tr>
                    <td class='table-image'><img src='img/staff/claudio.jpg' border='0' /></td>
                    <td>Claudio Giuliano<br /><small>Fondatore e Amministratore Delegato</small></td>
                </tr>
                <tr>
                    <td class='table-image'><img src='img/staff/michele.jpg' border='0' /></td>
                    <td>Michele Mostarda<br /><small>Fondatore e Sviluppatore</small></td>
                </tr>
                <tr>
                    <td class='table-image'><img src='img/staff/alessio.jpg' border='0' /></td>
                    <td>Alessio Palmero Aprosio<br /><small>Sviluppatore</small></td>
                </tr>
            </tbody>
        </table>
 -->    </div>
</div>
