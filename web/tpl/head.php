  <base href="http://<?php echo $Host; ?>/" />
  <meta charset="utf-8">
  <?php if (isset($Meta)) { ?>
      <title><?= htmlentities($Meta['title'], ENT_QUOTES, "utf-8") ?> - <?= $settings['title'] ?></title>
      <meta name="description" content="<?= htmlentities($Meta['description'], ENT_QUOTES) ?>">
      <meta name="keywords" content="<?= htmlentities($Meta['keywords'], ENT_QUOTES) ?>">
  <? } else { ?>
      <title><?= $settings['title'] ?></title>
      <meta name="description" content="<?= $settings["description"] ?>">
  <? } ?>

  <script src="http://code.jquery.com/jquery.js"></script>
  
  <!-- Le styles -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/isotope.css" rel="stylesheet" />
  <link href="css/auth-buttons.css" rel="stylesheet" />
  <link href="ui/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/bootstrap-colorpicker.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" type="text/css" charset="utf-8">
  <?php
  if (isset($CSS) && is_array($CSS)) {
      foreach ($CSS as $s) {
          echo "<link href='$s' rel='stylesheet' />";
      }
  }
  ?>
  
  <link rel="icon" href="/favicon.ico" type="image/x-icon" />
  <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Fauna+One' rel='stylesheet' type='text/css'>
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  
  <meta property="og:title" content="<?= htmlentities($Meta['title'], ENT_QUOTES, "utf-8") ?>" />
  <? if (!empty($MetaFB)) { ?>
      <meta property="og:site_name" content="<?= $settings["title"] ?>" />
      <meta property="og:type" content="website" />
      <meta property="og:image" content="<?= $settings["image"] ?>" />
      <meta property="og:description" content="<?php echo $MetaFB['description']; ?>" />
  <? } else { ?>
      <meta property="og:site_name" content="<?= $settings["title"] ?>" />
      <meta property="og:type" content="website" />
      <meta property="og:image" content="<?= $settings["image"] ?>" />
      <meta property="og:description" content="<?= $settings["description"] ?>" />
  <? } ?>
