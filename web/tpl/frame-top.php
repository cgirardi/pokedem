<?php

array_unshift($CSS, "css/styles.css?version=".md5_file("css/styles.css"));

?><!DOCTYPE html>
<html lang="en" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    <?php include("head.php"); ?>
</head>
  <body class='top-only'>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a target="_top" href="<?php echo Link::Home(); ?>" id='brand'><img src='img/title-h70.png' class='title-brand' /></a>
          <ul class="nav" id='menu-ul'>
            <!-- <li><span>In questo articolo:</span></li> -->
            <? foreach ($Citations as $value): ?>
              <li><?php echo $value; ?><li>
            <? endforeach; ?>
          </ul>
        </div>
      </div>
    </div>

    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-39414879-5']);
      _gaq.push(['_setDomainName', '<?= $settings["server"] ?>']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);
      _gaq.push(['_setAllowAnchor', true]);

      <?php if ($Is_test) { ?>
          window['ga-disable-UA-39414879-5'] = true;
      <?php } ?>
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
  </body>
</html>
