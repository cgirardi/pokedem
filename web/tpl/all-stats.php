<div class="hero-unit">
    <? if (!$_SESSION['Login']) { ?>
    <!-- <p class='before_stats'>
        Vuoi <b>scoprire</b>, <b>seguire</b> e <b>spronare</b> i tuoi rappresentanti nel Parlamento italiano?<br />
        Registrati gratuitamente cliccando sul link "Accedi" in alto.
    </p> -->
    <? } ?>
    
    <script type="text/javascript" charset="utf-8">
        jQuery(document).ready(function() {
            _gaq.push(['_trackEvent', 'Chart', '<?php echo $Active; ?>', document.location.href]);
        });
    </script>
    
    <h2 class='title_stats'><?php echo $PageTitle; ?></h2>
    
    <p class='after_stats_title'>
        <?php echo $AfterStatsTitle; ?>
    </p>
    
    <p class='after_stats_embed'>
        <?php echo $AfterStatsEmbed; ?>
    </p>

    <?php if ($PartyFilter): ?>
    <div class='party_filter'>
        Filtra partito:
        <div class="btn-group">
            <?php echo implode("\n", $FiltersForParties); ?>
        </div>
    </div>
    <?php endif; ?>
    
    <div class='verticaltabs tabbable tabs-left'>
    	<ul class="nav nav-tabs">
            <? foreach ($valid_types as $thisType): ?>
                <li class='<?php echo $Type == $thisType ? "active" : ""; ?>'><a href='<?php echo Link::Chart($Active, $thisType); ?>'><?php echo ucfirst(Chart::getTextForType($thisType)); ?></a></li>
            <? endforeach; ?>
    	</ul>
    </div>
            
    <div class='neartabs'>
    
    <?php echo $Text; ?>
    
    <div class='loading_stats'>
        <img src='img/ajax-loader.gif' />
    </div>
    
    </div>
    <hr class='clear' />
    <?php if ($Infinity) { ?>
    <script type="text/javascript">
    
    var datetime = <?php echo $DateTime; ?>;
    var type = <?php echo $Type; ?>;
    var startPosition = 30;
    
    var loadingBottom = false;
    
    $(document).ready(function() {
        $(window).scroll(function() {
            if ($('body').height() <= ($(window).height() + $(window).scrollTop()) && !loadingBottom) {
                
                loadingBottom = true;
                $('.loading_stats').show();

                $.ajax({
                    url: "?",
                    data: {
                        action: "ajax",
                        sub: "charts",
                        which: '<?php echo $Active; ?>',
                        type: type,
                        start: startPosition,
                        datetime: datetime
                    },
                    dataType: "json",
                    success: function(data) {
                        $('.loading_stats').hide();
                        $('.table-list-statistics').append(data.data);
                        twitter();
                        startPosition = data.next;
                        if (data.continue) {
                            loadingBottom = false;
                        }
                    }
                });
                
            }
        });
    });    
    </script>
    <?php } ?>
</div>
