<div id='user-tweets-content'>
    <p>
        Elenco dei tweet di argomento politico inviati da <?php echo $Tpl['username']; ?> nell'intervallo: <?php echo $Tpl['interval']; ?>
    </p>

    <div id='sidebar' class='bar-small'>
        <div class='isotope-filters'>
            <p>
                <a title='Annulla filtri e visualizza tutti i tweet di @<?php echo $Tpl['username']; ?>' href='#' class='reset-filter' data-filter='*'><span><i class='fa fa-twitter'></i></span> Tutti i tweet</a>
            </p>
            <p>
                <!-- oppure filtra -->
                <div class="form-horizontal">
                    <div class="btn-group" id='type-twitter-user-filter'>
                        <button class="btn" data-class='hashtag'>#</button>
                        <button class="btn" data-class='mention'>@</button>
                        <button class="btn" data-class='retweet'><i class='fa fa-retweet'></i></button>
                    </div>
                    <input id="input-twitter-user-filter" type="text" placeholder="Filtra" />
                </div>
            </p>
            <script type="text/javascript">
                // $(document).ready(function() {
                //     $('#sidebar').stickyfloat({ duration: 400 });
                // });
            </script>
            <div id='div-table-twitter-user-filter'>
                <table class='sortable filterable table table-striped' id='table-twitter-user-filter'>
                    <thead>
                        <tr>
                            <th class='fa-cell'>
                                &#xf02e;
                            </th>
                            <th class='fa-cell'>
                                &#xf012;
                            </th>
                            <th>
                                Testo
                            </th>
                            <th class='fa-cell unsortable'>
                                &#xf0c1;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        if (count($Tpl['htweets'])) {
                            foreach ($Tpl['htweets'] as $tag => $info) {
                                $id = $info['id'];
                                $name = $tag;
                                $num = $info['count'];
                                echo "<tr class='hashtag-row'>
                                    <td><span>#</span></td>
                                    <td>{$num}</td>
                                    <td class='name'><a title=\"Mostra solo i tweet in cui @{$Tpl['username']} ha usato #$name\" class='apply-filter' href='#' data-filter='.hash_{$id}'>#{$name}</a></td>
                                    <td>&nbsp;</td>
                                    </tr>";
                            }
                        }

                        if (count($Tpl['ctweets'])) {
                            foreach ($Tpl['ctweets'] as $politician => $info) {
                                $tmpP = new Politician($politician);

                                $id = $politician;
                                $name = $tmpP->getName();
                                $num = count($info);
                                echo "<tr class='mention-row'>
                                    <td><span>@</span></td>
                                    <td>{$num}</td>
                                    <td class='name'><a title=\"Mostra solo i tweet in cui @{$Tpl['username']} ha menzionato $name\" class='apply-filter' href='#' data-filter='.cite_{$id}'>{$name}</a></td>
                                    <td><a title=\"Vai alla pagina di $name\" target='_blank' href='http://$Host/$id'><i class='fa fa-pokedem'></i></a></td>
                                    </tr>";
                            }
                        }

                        if (count($Tpl['retweets'])) {
                            foreach ($Tpl['retweets'] as $twitter => $info) {
                                $id = $twitter;
                                $name = $info['name'];
                                $num = count($info['tweets']);
                                $add = "&nbsp;";
                                if ($info['ispol']) {
                                    $add = " <a title=\"Vai alla pagina di $name\" target='_blank' href='http://$Host/{$info['ispol']}'><i class='fa fa-pokedem'></i></a>";
                                }
                                echo "<tr class='retweet-row'>
                                    <td><span><i class='fa fa-retweet'></i></span></td>
                                    <td>{$num}</td>
                                    <td class='name'><a title=\"Mostra solo i retweet fatti da @{$Tpl['username']} su tweet postati da $name\" class='apply-filter' href='#' data-filter='.retw_{$id}'>{$name}</a></td>
                                    <td>{$add}</td>
                                    </tr>";
                            }
                        }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id='mainbar' class='bar-small'>
        <div class='detail-left isotope'>
            <?php
            
            foreach ($Tpl['tweets'] as $tw) {
                $classes = implode(" ", array_keys($tw['classes']));
                echo $BM->tweetedBox($tw['row'], $classes);
            }
            
            ?>
        </div>
    </div>
    <hr class="clear">
</div>
