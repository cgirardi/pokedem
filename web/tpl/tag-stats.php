<div class="hero-unit">
    <? if (!$_SESSION['Login']) { ?>
    <p class='before_stats'>
        Vuoi <b>scoprire</b>, <b>seguire</b> e <b>spronare</b> i tuoi rappresentanti nel Parlamento italiano?<br />
        <a href='?'>Registrati gratuitamente</a>!
    </p>
    <? } ?>
    
    <h2><?php echo $PageTitle; ?></h2>

</div>
