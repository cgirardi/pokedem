<div class="row" id="login-form">
    <div class="span4 offset4">
<div class="well">
  	<legend>Nuova registrazione</legend>
    <form accept-charset="UTF-8" action="?action=signin" method="post">
		<input class="span3" name="name" placeholder="Nome" type="text" value="<?php echo isset($_SESSION['Form']['name']) ? $_SESSION['Form']['name'] : ""; ?>">
		<input class="span3" name="surname" placeholder="Cognome" type="text" value="<?php echo isset($_SESSION['Form']['surname']) ? $_SESSION['Form']['surname'] : ""; ?>">
        <input class="span3" name="email" placeholder="Indirizzo e-mail" type="text" value="<?php echo isset($_SESSION['Form']['email']) ? $_SESSION['Form']['email'] : ""; ?>">
        <!-- <input class="span3" name="email2" placeholder="Ripeti indirizzo e-mail" type="text" value="<?php echo isset($_SESSION['Form']['email2']) ? $_SESSION['Form']['email2'] : ""; ?>"> -->
        <input class="span3" name="pass" placeholder="Password" type="password" value="<?php echo isset($_SESSION['Form']['pass']) ? $_SESSION['Form']['pass'] : ""; ?>">
        <input class="span3" name="pass2" placeholder="Ripeti password" type="password" value="<?php echo isset($_SESSION['Form']['pass2']) ? $_SESSION['Form']['pass2'] : ""; ?>">
        <!-- <input class="span3" name="password" placeholder="Password" type="password">  -->
        <button class="btn btn-info" type="submit">Registrati gratuitamente</button>
    </form>
    <p class='conditions'>
        Cliccando sul tasto "Registrati gratuitamente" qui sopra, accetti i <a href='http://www.machinelinking.com/wp/terms-of-service/'>Termini di Servizio</a>
        e la <a href='https://www.iubenda.com/privacy-policy/588415'>Privacy policy</a>.
        Dopo la registrazione, ti sar&agrave; inviata una mail di conferma con una password provvisoria che potrai cambiare dopo il primo accesso.
    </p>
    <p>
        Usa un social network
    </p>
    <p>
        <a class='btn-auth btn-facebook' href="<?php echo $fbLoginUrl; ?>">Entra con Facebook</a>
    </p>
    <p>
        <a class='btn-auth btn-twitter' href="<?php echo $twLoginUrl; ?>">Entra con Twitter</a>
    </p>
    <p class='conditions'>
        Accedendo a Pokedem, accetti i <a href="http://www.machinelinking.com/wp/terms-of-service/">Termini di Servizio</a>
        e la <a href="https://www.iubenda.com/privacy-policy/588415">Privacy policy</a>.
    </p>
</div>
</div>
</div>