<div class="row" id="lostpass-form">
    <div class="span4 offset4">
<div class="well">
  	<legend>Password dimenticata</legend>
    <form accept-charset="UTF-8" action="<?php echo Link::Lostpass(); ?>" method="post">
        <input class="span3" name="email" placeholder="Indirizzo e-mail" type="text" value="<?php echo isset($_SESSION['Form']['email']) ? $_SESSION['Form']['email'] : ""; ?>">
        <button class="btn btn-info" type="submit">Inviami una nuova password</button>
    </form>
  	<p>
  	    Inserire il proprio indirizzo e-mail nella casella qui sopra per ricevere una nuova password.<br />
  	    Gli utenti che hanno eseguito la registrazione tramite i social network (Facebook, Twitter, Google Plus, ecc.)
  	    potranno resettare la password utilizzando i relativi servizi messi a disposizione dai social network.
    </p>
</div>
</div>
</div>