<div>
    <p style='float: right; width: 400px;'>
        Follower: <strong><?php echo $Tpl['followers']; ?></strong><br />
        Following: <strong><?php echo $Tpl['following']; ?></strong><br />
        Tendenza politica: <strong><?php echo $Tpl['party']; ?></strong>
        <?php if ($Tpl['party_conf']): ?>
            (confidenza <?php echo $Tpl['party_conf']; ?>%)
        <?php endif; ?>
        <?php if ($Tpl['otherparties']): ?>
            <span title='<?php echo htmlentities($Tpl['otherparties'], ENT_QUOTES); ?>'><i class='fa fa-plus-circle'></i></span>
        <?php endif; ?>
        <br />
        Retweet ottenuti: <strong><?php echo $Tpl['retweets_no']; ?></strong><br />
        <!-- Ultimo tweet politico: <strong><?php echo $Tpl['followers']; ?></strong><br /> -->
    </p>
    
    <p style='float: left;'>
        <a target='_blank' href='http://www.twitter.com/<?php echo $Tpl['username']; ?>'><img class='attivista-pic' src='<?php echo $Tpl['picture']; ?>'></a>
    </p>

    <h2 style='padding: 0 0 0 100px; margin: 0;'>
        <?php echo $Tpl['name']; ?>
    </h2>
    <h4 style='padding: 2px 0 0 100px; margin: 0; width: 600px;'>
        <a target='_blank' href='http://www.twitter.com/<?php echo $Tpl['username']; ?>'><?php echo "@".$Tpl['username']; ?></a>
    </h4>
    <h4 style='padding: 2px 0 0 100px; margin: 0; width: 600px;'>
        <?php echo $Tpl['description']; ?>
    </h4>
</div>
<hr class="clear-visible">