<?php

function PolRetweet($WeeklyUserInfo, $i) {
    global $Host;
    ob_start();
    ?>
    <div class='span4 span-box'>
        <div class="pol-title">
            <span style="margin-left: 6px; background-image: url(<?php echo $WeeklyUserInfo['most_retw_pol_pic_'.$i]; ?>);" class="img-politician-small"></span>
            <h2><a onclick="_gaq.push(['_trackEvent', 'Attivisti', 'PolRetweet', document.location.href]);" href='http://<?php echo $Host; ?>/<?php echo $WeeklyUserInfo['most_retw_pol_id_'.$i]; ?>'><?php echo $WeeklyUserInfo['most_retw_pol_name_'.$i]; ?></a></h2>
            <!-- <p class="description">@<?php echo $WeeklyUserInfo['most_retw_pol_twitter_'.$i]; ?>, ritwittat<?php echo $WeeklyUserInfo['most_retw_pol_sex_'.$i]; ?>
                <?php echo $WeeklyUserInfo['most_retw_pol_qty_'.$i]; ?> volt<?php echo $WeeklyUserInfo['most_retw_pol_qty_'.$i] > 1 ? "e" : "a"; ?></p> -->
            <p class="description"><a href='https://twitter.com/<?php echo $WeeklyUserInfo['most_retw_pol_twitter_'.$i]; ?>'>@<?php echo $WeeklyUserInfo['most_retw_pol_twitter_'.$i]; ?></a>,
                <?php echo $WeeklyUserInfo['most_retw_pol_qty_'.$i]; ?> retweet</p>
        </div>
    </div>
    <?php
    return ob_get_clean();
}

function PolTweet($WeeklyUserInfo, $i) {
    global $Host;
    ob_start();
    ?>
    <div class='span4 span-box'>
        <div class="pol-title">
            <span style="margin-left: 6px; background-image: url(<?php echo $WeeklyUserInfo['most_cited_pol_pic_'.$i]; ?>);" class="img-politician-small"></span>
            <h2><a onclick="_gaq.push(['_trackEvent', 'Attivisti', 'PolTweet', document.location.href]);" href='http://<?php echo $Host; ?>/<?php echo $WeeklyUserInfo['most_cited_pol_id_'.$i]; ?>'><?php echo $WeeklyUserInfo['most_cited_pol_name_'.$i]; ?></a></h2>
            <!-- <p class="description">twittat<?php echo $WeeklyUserInfo['most_cited_pol_sex_'.$i]; ?>
                <?php echo $WeeklyUserInfo['most_cited_pol_qty_'.$i]; ?> volt<?php echo $WeeklyUserInfo['most_cited_pol_qty_'.$i] > 1 ? "e" : "a"; ?></p> -->
            <p class="description"><?php echo $WeeklyUserInfo['most_cited_pol_qty_'.$i]; ?> tweet</p>
        </div>
    </div>
    <?php
    return ob_get_clean();
}

function MostRetweet($WeeklyUserInfo, $i) {
    global $BM, $Tw_PD_name;
    ob_start();
    ?>
    <div class='span4 span-box'>
        <?php echo $BM->twitterBox($WeeklyUserInfo['most_retw_'.$i], true); ?>
        <div class='item-retweet'>
            <!-- (ritwittato <?php echo $WeeklyUserInfo['most_retw_shares_'.$i]; ?> volt<?php echo $WeeklyUserInfo['most_retw_shares_'.$i] > 1 ? "e" : "a"; ?>) -->
            <?php echo $WeeklyUserInfo['most_retw_shares_'.$i]; ?> retweet
        </div>
    </div>
    <?php
    return ob_get_clean();
}

$Color = "f89625";
$titleCoccarda = "";
if ($IsPolitician) {
    $Color = "259cf8";
    $titleCoccarda = " title='Questo numero indica la posizione nella classifica dei #pi&ugrave;social della settimana'";
}

?>

<?php if ($IsPolitician) { ?>
    <div class='attivista-page attivista-politician'>
<?php } else { ?>
    <div class='attivista-page'>
<?php } ?>
    
    <?php if (is_numeric($WeeklyUserInfo['position']) && $WeeklyUserInfo['position'] <= 100) { ?>
    <div class='attivista-coccarda'>
        <span<?php echo $titleCoccarda; ?>><?php echo $WeeklyUserInfo['position']; ?></span>
    </div>
    <?php } ?>

    <div class='attivista-top'>
        <?php if ($IsPolitician) { ?>
            <p><a href='/<?php echo $IsPolitician; ?>'><img class='attivista-pic' src='<?php echo $UserR['picture']; ?>'></a></p>
        <?php } else { ?>
            <p><a target='_blank' href='http://www.twitter.com/<?php echo $UserR['username']; ?>'><img class='attivista-pic' src='<?php echo $UserR['picture']; ?>'></a></p>
        <?php } ?>

        <h2><?php echo $ThisTitleBr; ?></h2>
        <h4><?php echo $WeekTxt; ?></h4>
        <p>
            <a href="#"
                data-app-id="138958742943135"
                data-link="<?php echo htmlentities($ThisLink, ENT_QUOTES); ?>"
                data-picture="/img/fb-bigg-squared.png"
                data-name="<?php echo htmlentities($ThisTitle, ENT_QUOTES); ?>"
                data-description="<?php echo htmlentities($ThisDescription, ENT_QUOTES); ?>"
                class="facebook-share btn-auth btn-facebook"
                title="Condividi su Facebook">Condividi su Facebook</a>
            <a href="https://twitter.com/intent/tweet?url=<?php echo urlencode($ThisLink); ?>&amp;text=<?php echo urlencode($ThisTitle); ?>&amp;via=<?php echo $Tw_PD_name; ?>"
                class="twitter btn-auth btn-twitter"
                title="Condividi su Twitter">Condividi su Twitter</a>
        </p>
    </div>

    <hr />

    <div class='row-fluid'>
        <div class='span4'>
            <div class="infobox-container"> 

            <div class="triangle-l"></div> 
            <div class="triangle-r"></div> 

            <div class="infobox"> 
            <h3><span>Tweet</span></h3> 
            <p class='num'><?php echo $WeeklyUserInfo['tw_to_pol']; ?></p>  
            <p class='avg'>Media utenti: <?php echo number_format($WeekR['average'], 0); ?></p>
            </div> 

            </div>
        </div>

        <div class='span4'>
            <div class="infobox-container"> 

            <div class="triangle-l"></div> 
            <div class="triangle-r"></div> 

            <div class="infobox"> 
            <h3><span>I retwittati</span></h3> 
            <p class='num'><?php echo $WeeklyUserInfo['rt_of_pol']; ?></p>  
            <p class='avg'>Media utenti: <?php echo number_format($WeekR['avg_rt_pol'], 0); ?></p>
            </div>

            </div>
        </div>

        <div class='span4'>
            <div class="infobox-container"> 

            <div class="triangle-l"></div> 
            <div class="triangle-r"></div> 

            <div class="infobox">
            <h3><span>Retweet ricevuti</span></h3> 
            <p class='num'><?php echo $WeeklyUserInfo['rt_received']; ?></p>  
            <p class='avg'>Media utenti: <?php echo number_format($WeekR['avg_rt'], 0); ?></p>
            </div>

            </div>
        </div>
    </div>
    
    <?php if ($_SESSION['Login']): ?>
    <h3>Numero di tweet per giorno</h3>
    
    <div class='row-fluid'>
        <div class='span2'>
        </div>
        
        <div class='span8 well'>
            <div id='containerWeek' style='height: 200px; width: 100%; position: relative;'></div>
            <script type="text/javascript" charset="utf-8">
                $(function () {
                        $('#containerWeek').highcharts({
                            title: {
                                text: ''
                            },
                            tooltip: {
                                dateTimeLabelFormats: {
                                    day: '%a, %e %b %Y'
                                }
                            },
                            xAxis: {
                                type: 'datetime'
                            },
                            legend: {
                                enabled: false
                            },
                            yAxis: {
                                title: {
                                    text: ''
                                },
                                min: 0,
                                plotLines: [{
                                    value: 0,
                                    width: 1,
                                    color: '#808080'
                                }]
                            },
                            chart: {
                                backgroundColor: null
                            },
                            series: [{
                                name: 'Tweet',
                                pointInterval: 24 * 3600 * 1000,
                                pointStart: Date.UTC(<?php echo $StartDateJS; ?>),
                                width: 2,
                                color: '#<?php echo $Color; ?>',
                                data: [
                                    <?php echo $WeeklyUserInfo['tweet_on_day_0']; ?>,
                                    <?php echo $WeeklyUserInfo['tweet_on_day_1']; ?>,
                                    <?php echo $WeeklyUserInfo['tweet_on_day_2']; ?>,
                                    <?php echo $WeeklyUserInfo['tweet_on_day_3']; ?>,
                                    <?php echo $WeeklyUserInfo['tweet_on_day_4']; ?>,
                                    <?php echo $WeeklyUserInfo['tweet_on_day_5']; ?>,
                                    <?php echo $WeeklyUserInfo['tweet_on_day_6']; ?>
                                ]
                            }]
                        });
                    });
            </script>
        </div>

        <div class='span2'>
        </div>
    </div>

    <h3>I tweet di <?php echo $UserR['name']; ?> che hanno avuto più retweet</h3>

    <div class='row-fluid attivista-piu-retwittati'>
        <?php
        
        switch ($WeeklyUserInfo['most_retw']) {
            case 0:
            echo "<p>Nessun retweet.</p>";
            break;
            case 1:
            echo "<div class='span4'></div>";
            echo MostRetweet($WeeklyUserInfo, 1);
            echo "<div class='span4'></div>";
            break;
            case 2:
            echo "<div class='span2'></div>";
            echo MostRetweet($WeeklyUserInfo, 1);
            echo MostRetweet($WeeklyUserInfo, 2);
            echo "<div class='span2'></div>";
            break;
            case 3:
            echo MostRetweet($WeeklyUserInfo, 1);
            echo MostRetweet($WeeklyUserInfo, 2);
            echo MostRetweet($WeeklyUserInfo, 3);
            break;
        }
        ?>
    </div>
    
    <h3>I più retwittati da <?php echo $UserR['name']; ?></h3>

    <div class='row-fluid attivista-pol-retwittati'>
        <?php
        
        switch ($WeeklyUserInfo['most_retw_pol']) {
            case 0:
            echo "<p>Nessun retweet.</p>";
            break;
            case 1:
            echo "<div class='span4'></div>";
            echo PolRetweet($WeeklyUserInfo, 1);
            echo "<div class='span4'></div>";
            break;
            case 2:
            echo "<div class='span2'></div>";
            echo PolRetweet($WeeklyUserInfo, 1);
            echo PolRetweet($WeeklyUserInfo, 2);
            echo "<div class='span2'></div>";
            break;
            case 3:
            echo PolRetweet($WeeklyUserInfo, 1);
            echo PolRetweet($WeeklyUserInfo, 2);
            echo PolRetweet($WeeklyUserInfo, 3);
            break;
        }
        ?>
    </div>
    
    <h3>I più menzionati da <?php echo $UserR['name']; ?></h3>

    <div class='row-fluid attivista-pol-twittati'>
        <?php
        
        switch ($WeeklyUserInfo['most_cited_pol']) {
            case 0:
            echo "<p>Nessun tweet.</p>";
            break;
            case 1:
            echo "<div class='span4'></div>";
            echo PolTweet($WeeklyUserInfo, 1);
            echo "<div class='span4'></div>";
            break;
            case 2:
            echo "<div class='span2'></div>";
            echo PolTweet($WeeklyUserInfo, 1);
            echo PolTweet($WeeklyUserInfo, 2);
            echo "<div class='span2'></div>";
            break;
            case 3:
            echo PolTweet($WeeklyUserInfo, 1);
            echo PolTweet($WeeklyUserInfo, 2);
            echo PolTweet($WeeklyUserInfo, 3);
            break;
        }
        ?>
    </div>

    <?php else: ?>
    <div class='attivisti-blur'>
        <div class="row-fluid">
          <div class="offset3 span6 login-request">
            <h3>Vuoi saperne di pi&ugrave;?<br />
                Registrati gratis su PokeDem.</h3>
            <p>
                Se hai già un account Twitter/Facebook o se sei già<br />
                registrato, puoi eseguire il login usando il tasto<br />
                "Accedi" in alto a destra.
            </p>
            <p>
                Potrai vedere:
            </p>
            <ul>
                <li>tweet inviati giorno per giorno</li>
                <li>i tuoi tweet di maggior successo</li>
                <li>chi hai twittato di più</li>
                <li>chi hai retwittato di più</li>
            </ul>
          </div>
        </div>
    </div>
    <?php endif; ?>
    
    <hr />
    
    <div class='note'>
        <strong>Nota.</strong>
        I dati raccolti riguardano i tweet dell'utente o inviati a uno delle entità monitorate da PokeDem.
        I valori medi sono calcolati sulla base dei primi 1000 utenti per numero di tweet.
        I calcoli si basano sulle API di Twitter e sono conteggiati <em>una tantum</em> alla fine della settimana considerata, pertanto ogni retweet successivo non sar&agrave; conteggiato.
    </div>
    
    <!-- <pre>
    <?php print_r($WeeklyUserInfo); ?>
    </pre> -->

</div>
