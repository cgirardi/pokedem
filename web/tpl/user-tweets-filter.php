<div class='well'>
    <form class="form-inline form-pro" role="form" action="?">
        <input type="hidden" name="action" value="twitterUserInfo" />

    <p><strong>Informazioni utente</strong></p>

    <div class="form-group row-fluid">
        <div class='span2'>
            Utente:
        </div>
        
        <div class='span10'>
            <input name='query-username' type="text" value="<?php echo $Tpl['username']; ?>" id='input-username'>
        </div>
    </div>
    
    <div class="form-group row-fluid">

        <div class='span2'>
            Periodo:
        </div>
        
        <div class='span4'>
            <label for="input-when-from" class="col-sm-3 control-label">Da:</label>
            <div class="input-append date form_datetime" data-link-field='input-field-from'>
                <input type="text" value="" readonly id='input-when-from'>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" name="query-when-from" id="input-field-from" value="<?php echo $data['when-from']; ?>">
        </div>

        <div class='span4'>
            <label for="input-when-to" class="col-sm-3 control-label">a:</label>
            <div class="input-append date form_datetime" data-link-field='input-field-to'>
                <input type="text" value="" readonly id='input-when-to'>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" name="query-when-to" id="input-field-to" value="<?php echo $data['when-to']; ?>">
        </div>

        <div class="span2 text-right">
            <button class="btn btn-primary" type="submit">Applica filtro</button>
        </div>

    </div>

    </form>
</div>
