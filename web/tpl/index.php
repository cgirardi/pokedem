<?php
//header("Content-type: text/html; charset=utf-8");
 
$_SESSION['last_page'] = $_SERVER['REQUEST_URI'];

if (isset($ShowNews['actions']) && in_array($Action, $ShowNews['actions'])) {
    $ShowNews = false;
}

array_unshift($CSS, "css/styles.css?version=".md5_file("css/styles.css"));

?><!DOCTYPE html>
<html lang="en" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    <script type="text/javascript" charset="utf-8" src="js/jquery.min.js"></script>

    <?php include("head.php"); ?>
</head>
  <body>
      <?php
      
      if ($Is_test) {
          ?>
          <div style='width: 150px; height: 300px; position: fixed; z-index: 3000; top: 0; right: 0;'>
              <img src='img/test-1.png' style='width: 150px;' />
          </div>
          <?php
      }
      // if ($_SESSION['Login'] && $_SESSION['Login'] == 1) {
      //     print_r($_SESSION);
      // }
      
      ?>

            <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">

                    <a class="brand" href="<?php echo Link::Home(); ?>" id='brand'><img src='<?= $settings["image"] ?>' class='title-brand' /></a>
                <div class="nav-collapse collapse">
                  <ul class="nav" id='menu-ul'>
                      <li class="dropdown<?php echo ($Active == "piucitati" || $Active == "piusocial" || $Active == "piutwittati" || $Active == "piuattivi") ? " active" : ""; ?>">
                          <a class="dropdown-toggle" href="#" data-toggle="dropdown">Classifiche <strong class="caret"></strong></a>
                          <ul class="dropdown-menu">
                              <li<?php echo $Active == "piucitati" ? " class='active'" : ""; ?>><a href="<?php echo Link::Chart('piucitati', 0); ?>">I #piùcitati</a></li>
                              <li<?php echo $Active == "piusocial" ? " class='active'" : ""; ?>><a href="<?php echo Link::Chart('piusocial', 0); ?>">I #piùsocial</a></li>
                              <li<?php echo $Active == "piutwittati" ? " class='active'" : ""; ?>><a href="<?php echo Link::Chart('piutwittati', 0); ?>">I #piùtwittati</a></li>
                              <!-- <li<?php echo $Active == "piucommentati" ? " class='active'" : ""; ?>><a href="<?php echo Link::Chart('piucommentati', 0); ?>">I #piùcommentati</a></li> 
                              <li<?php echo $Active == "piuattivi" ? " class='active'" : ""; ?>><a href="<?php echo Link::Chart('piuattivi', 0); ?>">Gli utenti #piùattivi</a></li> -->
	                         </ul>
                      </li>
                  </ul>
                  <!-- <input type='text' class='span3' name='name' autocomplete='off' placeholder='Inizia a digitare il nome' id='politician-name-field' /> -->
                  <form class="navbar-search pull-left" id='search-form' onsubmit='return false;'>
                    <input type='text' class='search-query' name='name' autocomplete='on' placeholder='<?php echo $uimsg["search_name"]; ?>' id='politician-name-field' />
                  </form>
                  <ul class="nav" id='menu-ul'>
		    <?php if (isset($uimsg["iamlucky"]) && $uimsg["iamlucky"] != "") { ?>
                      <li><a onclick="_gaq.push(['_trackEvent', 'Random', 'Random', document.location.href]);" href="<?php echo Link::Random(); ?>"><?php echo $uimsg["iamlucky"]; ?></a></li>
                    <?php } ?>   
		 </ul>
                  <?php
                  include("tpl/login-top.php");
                  echo $LoginArea;
                  ?>
                </div>

            </div>
            </div>
    </div>
    
    <div class="container">
        <?php
        
        if (is_array($ShowNews)) {
            $template = file_get_contents($Folder_web."/tpl/news.php");
            echo sprintf($template, $ShowNews['url'], $ShowNews['onclick'], $ShowNews['title']);
        }
        
        $Settings = getSettings();
        echo $_SESSION['Message'];
        if ($Include) require($Include);
        
        ?>
      <hr class='clear-visible' />

      <footer>
        <p>
            &copy; Copyright <?= $settings['copyright_year'] ?> <a href="<?= $settings['copyright_link'] ?>" target="_blank"><?= $settings['copyright_label'] ?></a>
            | <a href="<?= $settings['privacy_link'] ?>" class="iubenda-white iub-legal-only iubenda-embed" title="Privacy Policy" target="_blank">Privacy</a>
        </p>
      </footer>

    </div> <!-- /container -->
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <script type="text/javascript" charset="utf-8" src="js/bootstrap.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/sortable.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/bootbox.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/canvasjs.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.tagcloud.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.blockUI.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.showHtml.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/purl.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/bootstrap-datetimepicker.it.js"></script>
    
    <script type="text/javascript" charset="utf-8" src="js/highcharts/highcharts.js"></script>
    <script type="text/javascript" charset="utf-8" src="ui/jquery-ui-1.10.4.custom.min.js"></script>

    <!-- <script type="text/javascript" charset="utf-8" src="js/plusone.js"></script> -->
    <script type="text/javascript">
    window.server = '<?= $settings["server"] ?>';
    </script>
    <script type="text/javascript" charset="utf-8" src="js/custom.js?version=<?php echo md5_file("js/custom.js") ?>"></script>

    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-39414879-1']);
      _gaq.push(['_setDomainName', '<?= $settings['server'] ?>']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);
      _gaq.push(['_setAllowAnchor', true]);

      <?php if ($Is_test) { ?>
          window['ga-disable-<?php echo $Google_analytics_ID; ?>'] = true;
      <?php } ?>
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
       
 <?php if (isset($Google_analytics_ID) && $Google_analytics_ID !="" && !$Is_test) { ?>
     <script type="text/javascript">

       var _gaq = _gaq || [];
       _gaq.push(['_setAccount', '<?= $Google_analytics_ID ?>']);
       _gaq.push(['_setDomainName', '<?= $Host ?>']);
       _gaq.push(['_setAllowLinker', true]);
       _gaq.push(['_trackPageview']);
       _gaq.push(['_setAllowAnchor', true]);

       (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
       })();

     </script>

  <?php } ?>

 
    <?php 
    	if ($UsersLoginAccess) {
    		include("helptab.php");
    	}
    ?>
    <link rel="stylesheet" href="http://bxslider.com/lib/jquery.bxslider.css" type="text/css" />
        
    <script type="text/javascript">var addthis_config = {"data_track_addressbar": false};</script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5151c23407294e62"></script>
    
    <script type="text/javascript" charset="utf-8">
        <?php

        $p = $BM->getPostponed();
        foreach (array_reverse($p) as $div => $ajax) {
            $div = addslashes($div);
            echo "postponeLoading('$div', $ajax);\n";
        }
        
        ?>
    </script>
    
  </body>
</html>
