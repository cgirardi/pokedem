<?php

ob_start();

if ($_SESSION['Login']) {
  if ($_SESSION['Pro']) {
    ?>
    <div class="navbar-form pull-right" id='tastini_login'>
        <ul id='menu-login' class='pull-right nav'>
          <li class='dropdown<?php echo ($_REQUEST['action'] == "twitterUserInfo" || ($_REQUEST['action'] == "dashboard" && ($_REQUEST['sub'] != "select_politician" && $_REQUEST['sub'] != "settings" && $_REQUEST['sub'] != "followed")) ? " active" : "") ?>'>
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">Strumenti PRO <strong class="caret"></strong></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo Link::Pro(); ?>">Informazioni utente</a></li>
              <li><a href="<?php echo Link::Dashboard(); ?>">Dashboard</a></li>
              <li><a href="/?action=twitterUserInfo">Info utente Twitter</a></li>
            </ul>
          </li>
          <li><a href='<?php echo Link::Logout(); ?>'>Logout</a></li>
          <li<?php echo $Active == "chi_siamo" ? " class='active'" : ""; ?>><a href="<?php echo Link::Contatti(); ?>">Contatti</a></li>
          <li><a onclick="_gaq.push(['_trackEvent', 'Blog', 'Blog', document.location.href]);" href="http://<?= $settings["server"] ?>/blog">Blog</a></li>
        </ul>
    </div>
    <?php
  }
  else {
    ?>
    <div class="navbar-form pull-right" id='tastini_login'>
        <ul id='menu-login' class='pull-right nav'>
    <?php
    echo "
        <li><a ".($_REQUEST['action'] == "dashboard" && ($_REQUEST['sub'] != "select_politician" && $_REQUEST['sub'] != "settings" && $_REQUEST['sub'] != "followed") ? "class='active'" : "")." href='".Link::Dashboard()."'>Dashboard</a></li>
        <li><a href='".Link::Logout()."'>Logout</a></li>
        ";
    ?>
            <li<?php echo $Active == "chi_siamo" ? " class='active'" : ""; ?>><a href="<?php echo Link::Contatti(); ?>">Contatti</a></li>
            <li><a onclick="_gaq.push(['_trackEvent', 'Blog', 'Blog', document.location.href]);" href="http:/<?= $settings["server"] ?>/blog">Blog</a></li>
        </ul>
    </div>
    <?php
  }
}
else if ($UsersLoginAccess != 0) {
    ?>
    <div class="navbar-form pull-right" id='tastini_login'>
        <ul id='menu-login' class='pull-right nav'>
            <li class="dropdown" id='accedi-li'>
              <a class="dropdown-toggle" href="#" data-toggle="dropdown" id='accedi-button'>Accedi <strong class="caret"></strong></a>
              <div class="dropdown-menu" id='login-menu'>
                  <div id='login-menu-content'>
                      <h4>Accedi</h4>
                      <div id='login-menu-content-left'>
                          <p>
                              Usa un social network
                          </p>
                          <p>
                              <a class='btn-auth btn-facebook' href="<?php echo $fbLoginUrl; ?>">Entra con Facebook</a>
                          </p>
                          <p>
                              <a class='btn-auth btn-twitter' href="<?php echo $twLoginUrl; ?>">Entra con Twitter</a>
                          </p>
                          <p class='conditions'>
                              Accedendo a <?= $settings["title"] ?>, accetti i <a href="http://www.machinelinking.com/wp/terms-of-service/">Termini di Servizio</a>
                              e la <a href="https://www.iubenda.com/privacy-policy/588415">Privacy policy</a>.
                          </p>
                      </div>
                      <div id='login-menu-content-right'>
                          <p>
                              Usa il tuo account <strong><?= $settings["title"] ?></strong>
                          </p>
                          <form method="POST" action="?action=login" accept-charset="UTF-8">
                              <input class="span3" placeholder="Indirizzo e-mail" type="text" name="email"><br />
                              <input class="span3" placeholder="Password" type="password" name="password"><br />
                              <!-- <label class="checkbox">
                                  <input type="checkbox" name="remember"> Ricordami
                              </label> -->
                              <button class="btn-info btn" type="submit">Accedi</button>
                              <a class="btn-warning btn" href="<?php echo Link::Lostpass(); ?>">Password dimenticata</a>
                          </form>
                      </div>
                      <hr class='clear' />
                      <p id='p-register'>
                          <a href='<?php echo Link::Signin(); ?>'><i class='icon-user'></i> Non sei ancora registrato? Fallo ora!</a>
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          <a href='<?php echo Link::Features(); ?>'><i class='icon-list'></i> Versioni a confronto</a>
                      </p>
                      <hr class='clear' />
                  </div>
              </div>
            </li>
            <!-- <li><a href="#">Blog</a></li> -->
            <li<?php echo $Active == "chi_siamo" ? " class='active'" : ""; ?>><a href="<?php echo Link::Contatti(); ?>">Contatti</a></li>
            <li><a onclick="_gaq.push(['_trackEvent', 'Blog', 'Blog', document.location.href]);" href="http://<?= $settings["server"] ?>/blog">Blog</a></li>
        </ul>
    </div>
    <?php
}

$LoginArea = ob_get_clean();
