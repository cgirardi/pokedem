<?php

// exit("Sito in manutenzione, riprovare piu' tardi");

$configFile = realpath("../config.txt");
if (file_exists($configFile)) {
    $Config = parse_ini_file($configFile);

    foreach ($Config as $index => $value) {
        $$index = $value;
    }
}
if (!isset($Included_config)) {
    exit("Unable to load configuration file\n");
}

include($Folder_inc.'/include.php');
include("lang.php");

if (isset($_SESSION['Login']) && $_SESSION['Login']) {
    $query = "SELECT * FROM users WHERE id = '{$_SESSION['Login']}'";
    if ($DB->querynum($query)) {
        $_SESSION['LoginRow'] = $DB->fetch();
        $_SESSION['Pro'] = $_SESSION['LoginRow']['pro'];
        $_SESSION['ProAll'] = $_SESSION['LoginRow']['proall'];
        $_SESSION['Debug'] = $_SESSION['LoginRow']['debug'];
        
        $_SESSION['ProPol'] = array();
        $query = "SELECT * FROM pro_pol WHERE user = '{$_SESSION['Login']}'";
        $DB->query($query);
        while ($r = $DB->fetch()) {
            $_SESSION['ProPol'][$r['politician']] = true;
        }
    }
}

if (isset($_REQUEST['action'])) {
    $Action = $_REQUEST['action'];
} else {
    $Action = "";
}
$Active = "";

// Bad!
if ($Action != "twlogin") {
    require_once($Folder_inc."/login.php");
}

// -- News stuff

//require($Folder_blog."/wp-blog-header.php");
date_default_timezone_set('Europe/Rome');
$ShowNews = false;

/*if (!$_SESSION['forgetNews']) {
    // if (!$_SESSION['Login']) {
    //     $ShowNews = array();
    //     $ShowNews['url'] = "http://<?= $settings["server"] ?>/features";
    //     $ShowNews['actions'] = array("features", "signin_form");
    //     $ShowNews['title'] = "Vuoi avere accesso a più contenuti? Registrati gratuitamente o esegui il login cliccando il tasto \"Accedi\" in alto a destra.";
        
    //     $uri = htmlentities($_SERVER['REQUEST_URI'], ENT_QUOTES);
    //     $ShowNews['onclick'] = "_gaq.push(['_trackEvent', 'News', 'Reg', '$uri']);";
    // }
    // else {
        function filter_where($where = '') {
            $where .= " AND post_date > DATE_SUB(NOW(), INTERVAL 30 DAY)";
            return $where;
        }
        add_filter('posts_where', 'filter_where');

        query_posts('category_name=nuove-feature&showposts=5&orderby=rand');
    
        if ( have_posts() ) : while ( have_posts() ) : the_post();
            $c = get_post_meta($post->ID, "tweet", true);
            $l = get_permalink($post->ID);
            if ($c && $l) {
                $ShowNews = array();
                $ShowNews['url'] = $l;
                $ShowNews['actions'] = array();
                $ShowNews['title'] = $c;
                $uri = htmlentities($_SERVER['REQUEST_URI'], ENT_QUOTES);
                $ShowNews['onclick'] = "_gaq.push(['_trackEvent', 'NewsWP', '{$post->ID}', '$uri']);";
            }
        endwhile; endif;
    // }
}
*/

// The stripslashes_deep needs to be applied due to Wordpress.
// It is already defined, therefore it is not needed.
// See http://codex.wordpress.org/Function_Reference/stripslashes_deep

 function stripslashes_deep($value) {
    $value = is_array($value) ?
        array_map('stripslashes_deep', $value) :
        stripslashes($value);

    return $value;
 }

$_POST = array_map('stripslashes_deep', $_POST);
$_GET = array_map('stripslashes_deep', $_GET);
$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
$_COOKIE = array_map('stripslashes_deep', $_COOKIE);

// -- News stuff (end)

switch ($Action) {
    
    case "primariepd":
    include($Folder_inc."/actions/primariepd.php");
    break;
    
    case "features":
    include($Folder_inc."/actions/features.php");
    break;
    
    case "tendenze":
    include($Folder_inc."/actions/tendenze.php");
    break;
    
    case "clearsession":
    session_start();
    session_destroy();
    break;
    
    case "ajax":
    require_once($Folder_inc."/actions/ajax.php");
    exit();
    break;
    
    case "chart":
    require_once($Folder_inc."/actions/charts.php");
    break;
    
    case "dashboard":
    require_once($Folder_inc."/actions/dashboard.php");
    break;
    
    case "signin":
    require_once($Folder_inc."/actions/signin.php");
    break;
    
    case "widget":
    include($Folder_inc."/actions/widget.php");
    break;
    
    case "customizewidget":
    include($Folder_inc."/actions/customizewidget.php");
    break;

    case "prohome":
    ob_start();

    echo "<h2>Informazioni account</h2>";

    if ($_SESSION['ProAll']) {
        echo "<p>Tipo di account: <strong>Premium</strong></p>";
        echo "<p>&Egrave; possibile:</p>";
        echo "<ul>";
        echo "<li>accedere alle schede degli account Twitter che parlando di politica, con filtri di ricerca avanzati e analisi sulla possibile fede politica;</li>";
        echo "<li>come per gli account gratuiti, visualizzare le schede pubbliche dei politici;</li>";
        echo "<li>seguire illimitati politici per averli sempre pronti nella dashboard;</li>";
        echo "<li>attivare notifiche settimanali, giornaliere o real time per ricevere via e-mail le news sui politici seguiti;</li>";
        echo "<li>visualizzare tutte le schede dei politici in modalit&agrave; PRO (con filtri avanzati, nessun limite di accesso all'archivio, statistiche su utenti Twitter e hashtag favorevoli/ostili).</li>";
        echo "</ul>";
    }
    elseif ($_SESSION['Pro']) {
        echo "<p>Tipo di account: <strong>Pro</strong></p>";
        echo "<p>&Egrave; possibile:</p>";
        echo "<ul>";
        echo "<li>accedere alle schede degli account Twitter che parlando di politica, con filtri di ricerca avanzati e analisi sulla possibile fede politica;</li>";
        echo "<li>come per gli account gratuiti, visualizzare le schede pubbliche dei politici;</li>";
        echo "<li>seguire illimitati politici per averli sempre pronti nella dashboard;</li>";
        echo "<li>attivare notifiche settimanali, giornaliere o real time per ricevere via e-mail le news sui politici seguiti.</li>";
        echo "</ul>";

        if (count($_SESSION['ProPol'])) {
            echo "<p>Inoltre questo account &egrave; abilitato a vedere le seguenti schede di politici in modalit&agrave; Pro (con filtri avanzati, nessun limite di accesso all'archivio, statistiche su utenti Twitter e hashtag favorevoli/ostili):</p>";
            echo "<ul>";
            $query = "SELECT * FROM politicians WHERE id IN (".implode(", ", array_keys($_SESSION['ProPol'])).")";
            $DB->query($query);
            while ($r = $DB->fetch()) {
                echo "<li><a href='http://$Host/{$r['id']}'>{$r['name']} {$r['surname']}</a></li>";
            }
            echo "</ul>";
        }
    }
    elseif ($_SESSION['Login']) {
        echo "<p>Tipo di account: <strong>Free</strong></p>";
        echo "<p>&Egrave; possibile:</p>";
        echo "<ul>";
        echo "<li>visualizzare le schede pubbliche dei politici.</li>";
        echo "<li>seguire fino a 10 politici per averli sempre pronti nella dashboard;</li>";
        echo "<li>attivare notifiche settimanali o giornaliere via e-mail le news sui politici seguiti.</li>";
        echo "</ul>";

        echo "<p>Vuoi di pi&ugrave;? Consulta la pagina con gli <a href='".Link::Features()."'>abbonamenti Pro e Premium</a>.</p>";
    }
    else {
        echo "<p>Tipo di account: <strong>Nessuno</strong></p>";
        echo "<p>Eseguire il login per avere informazioni sull'account.</p>";
    }

    $Text = ob_get_clean();
    $Include = "tpl/dashboard.php";
    break;

    case "readmail":

    ob_start();
    if (isset($_REQUEST['id'])) {
        $ID = addslashes($_REQUEST['id']);
        $query = "SELECT * FROM email_read WHERE id = '$ID'";
        if ($DB->querynum($query)) {
            $query = "UPDATE email_read SET `read` = NOW() WHERE id = '$ID'";
            $DB->query($query);
        }
    }
    ob_end_clean();

    header('Content-Type: image/png');
    echo file_get_contents("img/title-mail.png");
    exit();
    break;
    
    
    
    case "random":
    $query = "SELECT id FROM politicians WHERE active = '1' ORDER BY RAND() LIMIT 1";
    $r = $DB->queryfetch($query);
    header("Location: http://$Host/{$r['id']}");
    break;



    case "proTwitter":
    include($Folder_inc."/actions/proTwitter.php");
    break;
    
    
    
    case "proTwitterSearch":
    include($Folder_inc."/actions/proTwitterSearch.php");
    break;
    
    
    
    case "twitterUserInfo":
    
    if (!$_SESSION['Pro']) {
        $Text = "Non si hanno i permessi necessari, eseguire il login.";
        $Include = "tpl/dashboard.php";
        break;
    }
    
    ob_start();
    
    // $userID = addslashes($_REQUEST['id']);
    // if (!is_numeric($userID)) {
    //     exit();
    // }

    $data = array();
    foreach ($_REQUEST as $index => $value) {
        if (preg_match("/^query-(.*)/", $index, $r)) {
            $data[$r[1]] = $value;
        }
    }
    if (isset($_REQUEST['id'])) {
        $data['id'] = $_REQUEST['id'];
    }
    $data = array_map("addslashes", $data);

    $userID = getUserByNameId($data);
    if (!$userID) {
        $Tpl = array("username" => $data['username']);
        include("tpl/user-tweets-filter.php");
        if (empty($data)) {
            // echo "<p>Compilare i campi in al.</p>";
        }
        else {
            echo "<p>Utente non trovato.</p>";
        }
    }
    else {
        $data['use-filter'] = true;
        echo getTwitterUserInfo($userID, $data);
    }
    
    $Text = ob_get_clean();
    $Include = "tpl/dashboard.php";
    break;
    
    
    
    case "attivisti":
    ob_start();
    
    // print_r($_REQUEST);
    
    $User = addslashes($_REQUEST['user']);
    $Week = addslashes($_REQUEST['week']);

    $query = "SELECT u.*, b.active
        FROM twitter_users u
        LEFT JOIN twitter_blacklisted b ON b.id = u.id
        WHERE LOWER(u.username) = LOWER('{$User}')";
    if (!$DB->querynum($query)) {
        echo "<p>L'utente selezionato [$User] non è presente nei nostri sistemi.</p>";
        $Text = ob_get_clean();
        $Include = "tpl/dashboard.php";
        break;
    }
    $UserR = $DB->fetch();
    $UserID = $UserR['id'];

    // Blacklisted
    if ($UserR['active']) {
        echo "<p>L'utente selezionato [$User] non è presente nei nostri sistemi.</p>";
        $Text = ob_get_clean();
        $Include = "tpl/dashboard.php";
        break;
    }

    $WeekR = array();
    
    if (!$Week) {
        $query = "SELECT * FROM weeks
            ORDER BY startdate DESC
            LIMIT 1";
        if ($DB->querynum($query)) {
            $WeekR = $DB->fetch();
        }
    }
    else {
        $query = "SELECT * FROM weeks
            WHERE id = '$Week'";
        if ($DB->querynum($query)) {
            $WeekR = $DB->fetch();
        }
    }
    
    if (empty($WeekR)) {
        echo "<p>Errore: utente o settimana non validi.</p>";
        $Text = ob_get_clean();
        $Include = "tpl/dashboard.php";
        break;
    }
    $WeekID = $WeekR['id'];
    
    $IsPolitician = false;
    $query = "SELECT politician FROM social_info WHERE LOWER(twitter) = LOWER('{$User}')";
    if ($DB->querynum($query)) {
        $r = $DB->fetch();
        $IsPolitician = $r['politician'];
    }
    
    $ThisLink = "http://$Host/attivisti/$User/$WeekID";
    $ThisTitle = "La settimana politica su Twitter di {$UserR['name']}";
    $ThisTitleBr = "La settimana politica su Twitter<br />di {$UserR['name']}";
    
    $secondsInDay = 60 * 60 * 24;
    $secondsInWeek = $secondsInDay * 7;
    
    $IDweek = $WeekR['id'];
    
    $start = $WeekR['startdate'];
    $startTS = strtotime($WeekR['startdate']);
    $end = date("Y-m-d H:i:s", $startTS + $secondsInWeek - 1);
    
    $StartDateJS = "";
    $StartDateJS .= date("Y", $startTS);
    $StartDateJS .= ", ";
    $StartDateJS .= date("n", $startTS) - 1;
    $StartDateJS .= ", ";
    $StartDateJS .= date("j", $startTS);
    
    $WeekTxt = "";
    $m1 = date("n", $startTS);
    $m2 = date("n", $startTS + $secondsInWeek - 1);
    
    if ($m1 == $m2) {
        $d1 = strftime("%e", $startTS);
        $d2 = strftime("%e", $startTS + $secondsInWeek - 1);
        $mName = strftime("%B", $startTS);
        $WeekTxt = "dal $d1 al $d2 $mName";
    }
    else {
        $d1 = strftime("%e %B", $startTS);
        $d2 = strftime("%e %B", $startTS + $secondsInWeek - 1);
        $WeekTxt = "dal $d1 al $d2";
    }
    
    $query = "SELECT * FROM weekly_user_info
        WHERE user = '$UserID' AND week = '$WeekID'";
    if (!$DB->querynum($query)) {

        $Position = "1000+";
        $query = "SELECT * FROM weekly_charts WHERE username = '$User'";
        if ($IsPolitician) {
            $query = "SELECT * FROM weekly_charts_pol WHERE username = '$User'";
        }
        if ($DB->querynum($query)) {
            $r = $DB->fetch();
            $Position = $r['realposition'];

            $WeeklyUserInfo['rt_received'] = $r['retweet'];
            $WeeklyUserInfo['tw_to_pol'] = $r['num'];
            // $WeeklyUserInfo['rt_of_pol'] = $r['retweet_pol'];
        }
        $WeeklyUserInfo['position'] = $Position;
        // wlog("Position: ".($Position ? $Position : "1000+"));

        if (!isset($WeeklyUserInfo['rt_of_pol'])) {
            $query = "SELECT * FROM twitter_updates u
                WHERE u.user_id = '$UserID'
                AND u.created_time >= '$start'
                AND u.created_time <= '$end'
                AND u.type2 = 'retweet'
                AND u.name = ANY (SELECT twitter FROM social_info)";
            $WeeklyUserInfo['rt_of_pol'] = $DB->querynum($query);
        }
        // wlog("Retweets of politicians: ".$DB->querynum($query));
        
        // TODO: check!
        // Values are different (stores in the database, calculated here)
        if (!isset($WeeklyUserInfo['tw_to_pol'])) {
            $query = "SELECT * FROM twitter_updates
                WHERE user_id = '$UserID'
                AND created_time >= '$start'
                AND created_time <= '$end'";
            $WeeklyUserInfo['tw_to_pol'] = $DB->querynum($query);
        }
        // wlog("Tweets to politicians: ".$DB->querynum($query));
        
        if (!$WeeklyUserInfo['rt_received']) {
            $WeeklyUserInfo['rt_received'] = "?";
        }
        if (!$WeeklyUserInfo['tw_to_pol']) {
            $WeeklyUserInfo['tw_to_pol'] = "?";
        }

        if ($_SESSION['Login']) {
            $query = "SELECT * FROM social_info WHERE twitter = '$User'";
            if ($DB->querynum($query)) {
                $Rp = $DB->fetch();
                $query = "SELECT SUM(u.shares) total
                    FROM social_updates_new u
                    WHERE u.politician = '{$Rp['politician']}'
                    AND u.created_time >= '$start'
                    AND u.created_time <= '$end'
                    AND u.type2 IS NULL";
                $WeeklyUserInfo['rt_received'] = 0;
                if ($DB->querynum($query)) {
                    $r = $DB->fetch();
                    $WeeklyUserInfo['rt_received'] = $r['total'] ? $r['total'] : 0;
                }
            }
            else {
                // $query = "SELECT object_id id, MAX(shares) shares
                //     FROM twitter_updates t
                //     WHERE object_id = ANY(
                //         SELECT id
                //         FROM twitter_updates
                //         WHERE user_id = '$UserID' AND created_time >= '$start' AND created_time <= '$end'
                //         AND type2 IS NULL
                //     )
                //     GROUP BY object_id
                //     ORDER BY shares DESC";
                $query = "SELECT u1.object_id id, MAX(u1.shares) shares
                    FROM twitter_updates u1
                    LEFT JOIN twitter_updates u2 ON u1.object_id = u2.id
                    WHERE u2.user_id = '$UserID' AND u2.created_time >= '$start' AND u2.created_time <= '$end'
                    AND u2.type2 IS NULL
                    AND u1.created_time >= '$start'
                    AND u1.created_time <= '$end'
                    GROUP BY u1.object_id
                    ORDER BY shares DESC";
                // $query = "SELECT SUM(u.shares) total
                //     FROM twitter_updates u
                //     WHERE u.user_id = '$UserID'
                //     AND u.created_time >= '$start'
                //     AND u.created_time <= '$end'
                //     AND u.type2 IS NULL";
                $WeeklyUserInfo['rt_received'] = 0;
                if ($DB->querynum($query)) {
                    $i = 0;
                    $WeeklyUserInfo['most_retw'] = 0;
                    while ($r = $DB->fetch()) {
                        $i++;
                        if ($i <= 3) {
                            $WeeklyUserInfo['most_retw_'.$i] = $r['id'];
                            $WeeklyUserInfo['most_retw_shares_'.$i] = $r['shares'];
                            $WeeklyUserInfo['most_retw'] = $i;
                        }
                        $WeeklyUserInfo['rt_received'] += $r['shares'];
                    }
                }
            }
            // wlog("Retweets received: ".$r['total']);
            
            for ($i = 0; $i < 7; $i++) {
                $newStart = date("Y-m-d H:i:s", $startTS + $i * $secondsInDay);
                $newEnd = date("Y-m-d H:i:s", $startTS + ($i + 1) * $secondsInDay - 1);
                $query = "SELECT * FROM twitter_updates
                    WHERE user_id = '$UserID'
                    AND created_time >= '$newStart'
                    AND created_time <= '$newEnd'";
                $WeeklyUserInfo['tweet_on_day_'.$i] = $DB->querynum($query);
                // wlog("$newStart: ".$DB->querynum($query));
            }

            $query = "SELECT * FROM social_info WHERE twitter = '$User'";
            if ($DB->querynum($query)) {
                $Rp = $DB->fetch();
                $query = "SELECT u.*
                    FROM social_updates_new u
                    WHERE u.politician = '{$Rp['politician']}'
                    AND u.created_time >= '$start'
                    AND u.created_time <= '$end'
                    AND u.shares > 0
                    AND u.type2 IS NULL
                    ORDER BY u.shares DESC
                    LIMIT 3";
                $WeeklyUserInfo['most_retw'] = $DB->querynum($query);
                $i = 0;
                while ($r = $DB->fetch()) {
                    $i++;
                    $mess = strip_tags($r['message']);
                    $mess = str_replace("\n", " ", $mess);
                    $WeeklyUserInfo['most_retw_'.$i] = $r['id'];
                    $WeeklyUserInfo['most_retw_shares_'.$i] = $r['shares'];
                    // wlog("{$mess} [{$r['shares']}]");
                }
            }
            // else {
            //     $query = "SELECT u.*
            //         FROM twitter_updates u
            //         WHERE u.user_id = '$UserID'
            //         AND u.created_time >= '$start'
            //         AND u.created_time <= '$end'
            //         AND u.shares > 0
            //         AND u.type2 IS NULL
            //         ORDER BY u.shares DESC
            //         LIMIT 3";
            //     $WeeklyUserInfo['most_retw'] = $DB->querynum($query);
            //     $i = 0;
            //     while ($r = $DB->fetch()) {
            //         $i++;
            //         $mess = strip_tags($r['message']);
            //         $mess = str_replace("\n", " ", $mess);
            //         $WeeklyUserInfo['most_retw_'.$i] = $r['id'];
            //         $WeeklyUserInfo['most_retw_shares_'.$i] = $r['shares'];
            //         // wlog("{$mess} [{$r['shares']}]");
            //     }
            // }

            $query = "SELECT name, COUNT(*) num
                FROM `twitter_updates`
                WHERE created_time >= '$start'
                    AND created_time <= '$end'
                    AND user_id = '$UserID'
                    AND type2 = 'retweet'
                    AND name = ANY (SELECT twitter FROM social_info)
                GROUP BY name
                ORDER BY num DESC
                LIMIT 3";
            $WeeklyUserInfo['most_retw_pol'] = $DB->querynum($query, 5);
            $i = 0;
            while ($r = $DB->fetch(5)) {
                $i++;
                $WeeklyUserInfo['most_retw_pol_twitter_'.$i] = $r['name'];
                $WeeklyUserInfo['most_retw_pol_qty_'.$i] = $r['num'];
                $query = "SELECT politician FROM social_info WHERE twitter = '{$r['name']}'";
                $rP = $DB->queryfetch($query);
                $WeeklyUserInfo['most_retw_pol_id_'.$i] = $rP['politician'];
                // wlog("{$r['name']} [{$r['num']}]");
            }

            $query = "SELECT p.id, p.surname, p.name, COUNT(*) num
                FROM twitter_politicians t
                LEFT JOIN twitter_updates u ON u.id = t.tweet
             LEFT JOIN politicians p ON p.id = t.politician
                WHERE when_tweet > '$start'
                    AND when_tweet < '$end'
                    AND u.user_id = '$UserID'
                 AND type2 IS NULL
                    GROUP BY politician
                ORDER BY num DESC
                LIMIT 3";
            $WeeklyUserInfo['most_cited_pol'] = $DB->querynum($query);
            $i = 0;
            while ($r = $DB->fetch()) {
                $i++;
                $WeeklyUserInfo['most_cited_pol_id_'.$i] = $r['id'];
                $WeeklyUserInfo['most_cited_pol_qty_'.$i] = $r['num'];
                // wlog("{$r['surname']} {$r['name']} [{$r['num']}]");
            }
        }
    }
    else {
        $WeeklyUserInfo = array();
        while ($r = $DB->fetch()) {
            $WeeklyUserInfo[$r['index']] = $r['value'];
        }
    }
    
    if ($_SESSION['Login']) {
        for ($i = 1; $i <= $WeeklyUserInfo['most_retw']; $i++) {
            $query = "SELECT *
                FROM twitter_updates t
                -- LEFT JOIN twitter_users u ON u.id = t.user_id
                WHERE t.id = '".$WeeklyUserInfo['most_retw_'.$i]."'";
            if ($DB->querynum($query)) {
                $WeeklyUserInfo['most_retw_'.$i] = $DB->queryfetch($query);
            }
            else {
                $query = "SELECT *
                    FROM social_updates_new t
                    -- LEFT JOIN twitter_users u ON u.id = t.user_id
                    WHERE t.id = '".$WeeklyUserInfo['most_retw_'.$i]."'";
                $WeeklyUserInfo['most_retw_'.$i] = $DB->queryfetch($query);
            }
        }
        
        for ($i = 1; $i <= $WeeklyUserInfo['most_retw_pol']; $i++) {
            $idpol = $WeeklyUserInfo['most_retw_pol_id_'.$i];
            $Pol = new Politician($idpol);
            $polinfo = $Pol->addPolInfo();
            $WeeklyUserInfo['most_retw_pol_pic_'.$i] = $polinfo['image_small'];
            $WeeklyUserInfo['most_retw_pol_name_'.$i] = $Pol->getName();
            $WeeklyUserInfo['most_retw_pol_sex_'.$i] = $Pol->getSexSuffix();
        }
        
        for ($i = 1; $i <= $WeeklyUserInfo['most_cited_pol']; $i++) {
            $idpol = $WeeklyUserInfo['most_cited_pol_id_'.$i];
            $Pol = new Politician($idpol);
            $polinfo = $Pol->addPolInfo();
            $WeeklyUserInfo['most_cited_pol_pic_'.$i] = $polinfo['image_small'];
            $WeeklyUserInfo['most_cited_pol_name_'.$i] = $Pol->getName();
            $WeeklyUserInfo['most_cited_pol_sex_'.$i] = $Pol->getSexSuffix();
        }
    }

    $ThisDescription = "L'attivit&agrave; politica di {$UserR['name']}\nTweet politici: {$WeeklyUserInfo['tw_to_pol']} - Retweet di politici: {$WeeklyUserInfo['rt_of_pol']} - Retweet ricevuti: {$WeeklyUserInfo['rt_received']}";
    
    include("tpl/attivista.php");
    
    // echo "<pre>";
    // print_r($WeeklyUserInfo);
    // echo "</pre>";
    
    $Text = ob_get_clean();
    $Include = "tpl/dashboard.php";
    break;
    
    
    case "logout":
    l("logout");
    session_destroy();
    setcookie('username', '', time() - 10);
    setcookie('password', '', time() - 10);
    header("Location: /");
    // header("Location: ?");
    exit();
    break;
    
    
    
    case "login_form":
    if ($_SESSION['Login']) {
        header("Location: /dashboard");
    }
    echo "<p>Il login non è andato a buon fine, riprovare a effettuarlo tramite il pulsante \"Accedi\" in alto a destra dello schermo.</p>";
    $Text = ob_get_clean();
    $Include = "tpl/dashboard.php";
    break;
    
    
    
    case "chi_siamo":
    l("chi_siamo");
    $Include = $Folder_web."/tpl/chisiamo.php";
    $Active = "chi_siamo";
    break;
    
    
    
    case "signin_form":
    l("signin_form");
    if ($_SESSION['Login']) {
        $_SESSION['Message'] = getMessage("Eseguire il logout prima di registrarsi");
    }
    else {
        $Include = $Folder_web."/tpl/signin.php";
    }
    break;
    
    
    
    case "registered":
    $email = $_SESSION['Form']['email'];
    $Include = $Folder_web."/tpl/registered.php";
    break;
    
    
    
    case "activation":
    $ID = addslashes($_REQUEST['id']);
    $code = addslashes($_REQUEST['code']);
    
    $query = "SELECT * FROM users WHERE activation = '$code' AND id = '$ID' AND active = '0'";
    if ($DB->querynum($query)) {
        $query = "UPDATE users SET active = '1' WHERE id = '$ID'";
        $DB->query($query);
        setLogin($ID, true);
        back(Link::Dashboard(), "Attivazione avvenuta con successo", 1);
    }
    back(Link::Login(), "Errore nell'attivazione");
    break;
    
    
    
    case "list-of-sources":
    
    $query = "SELECT s.name, t.name typename
        FROM sites s
        LEFT JOIN site_types t ON t.id = s.type
        WHERE active = '1'
        ORDER BY s.name";
    $DB->query($query);
    
    $PageTitle = "Elenco Fonti";
    
    ob_start();
    echo '<table class="table table-striped sortable filterable" id="table-list-sources">';
    echo '<thead><tr>
        <th class="unsortable">&nbsp;</th>
        <th>Fonte</th>
        <th>Tipo</th>
    </tr></thead>';
    echo '<tbody>';
    $i = 0;
    while ($r = $DB->fetch()) {
        $i++;
        echo "<tr>
            <td style='width: 50px;'>$i.</td>
            <td class=\"col_name\">{$r['name']}</td>
            <td class=\"col_type\">{$r['typename']}</td>
        </tr>";
    }
    echo "</tbody>";
    echo "</table>";
    $Text = ob_get_clean();
    
    l("list-of-sources");
    $Include = $Folder_web."/tpl/list-of-sources.php";
    $Active = "list-of-sources";
    break;
    
    
    case "tag":
    $Tag = addslashes($_REQUEST['tag']);
    $txttag = str_replace("_", " " , $Tag);
    $txttag = utf8_encode($txttag);
    $txttag = preg_replace("/\(.*\)/", "", $txttag);
    $txttag = trim($txttag);
    
    $PageTitle = $txttag;
    
    ob_start();
    echo "<p>".$Tag."</p>";
    $Text = ob_get_clean();
    
    // l("social_stats");
    $Include = $Folder_web."/tpl/tag-stats.php";
    break;
    
    
    
    case "tweet_link_top":
    $link = addslashes($_REQUEST['link']);

    $query = "SELECT url FROM articles_done WHERE link = '$link'";
    if ($DB->querynum($query)) {
        $r = $DB->fetch();

        $query = "SELECT p.name, p.surname, s.image, p.id
            FROM articles a
            LEFT JOIN politicians p ON a.politician = p.id
            LEFT JOIN politician_info s ON s.politician = p.id
            WHERE a.link = '$link'";
        $Citations = array();
        if ($DB->querynum($query)) {
            while ($r = $DB->fetch()) {
                $name = htmlentities("{$r['name']} {$r['surname']}", ENT_QUOTES);

                $txt = "";
                $txt .= "<a title='$name' href='http://$Host/{$r['id']}' target='_top'>\n";
                $txt .= "<span title='$name' style='background-image: url(\"{$r['image']}\");' class='img-politician-very-small'>\n";
                $txt .= "</span></a>";
                $Citations[] = $txt;
                // print_r($r);
            }
        }

        include($Folder_web."/tpl/frame-top.php");
    }
    else {
        header("Location: http://$Host");
    }
    exit();
    break;



    case "tweet_link":
    $link = addslashes($_REQUEST['link']);

    $query = "SELECT url FROM articles_done WHERE link = '$link'";
    if ($DB->querynum($query)) {
        $r = $DB->fetch();

        $Link1 = "http://$Host/?action=tweet_link_top&link=$link";
        $Link2 = $r['url'];
        include($Folder_web."/tpl/frames.php");
        l("tweet_link", $link);
        // header("Location: ".$r['url']);
    }
    else {
        header("Location: http://$Host");
    }
    exit();
    break;



    case "trace_link":
    $hash = $_REQUEST['hash'];
    $link = $_REQUEST['link'];
    $user = $_REQUEST['user'];
    if (getHashForLink($user, $link) == $hash) {
        $dati = array();
        $dati['link'] = $link;
        $dati['user'] = $user;
        $DB->queryreplace("user_clicks", $dati);
    }
    else {
        // echo "ERRORE!\n";
        // echo getHashForLink($user, $link)."\n";
        // echo $hash;
        // exit();
    }
    
    header("Location: ".$link);
    exit();
    break;
    
    
    
    case "twlogin":
    // if (isset($_REQUEST['oauth_token']) && $_SESSION['Tw_Signin_oauth_token'] !== $_REQUEST['oauth_token']) {
    //     exit("Old token!");
    // }
    
    $Tw_Signin = new TwitterOAuth($Tw_Signin_consumer_key, $Tw_Signin_consumer_secret, $_SESSION['Tw_Signin_oauth_token'], $_SESSION['Tw_Signin_oauth_token_secret']);
    
    // Need to be saved
    // echo "Sending {$_REQUEST['oauth_verifier']}<br />\n";
    $access_token = $Tw_Signin->getAccessToken($_REQUEST['oauth_verifier']);
    
    unset($_SESSION['Tw_Signin_oauth_token']);
    unset($_SESSION['Tw_Signin_oauth_token_secret']);
    
    // print_r($access_token);
    $twID = $access_token['user_id'];
    $twName = $access_token['screen_name'];
    
    if ($twID) {
        $query = "SELECT * FROM users WHERE regtype = 'twitter' AND regid = '$twID'";

        // User is in DB
        if ($DB->querynum($query)) {
            $r = $DB->fetch();

            $dati = array();
            $dati['name'] = $twName;
            $DB->queryupdate("users", $dati, array("id" => $r['id']));

            // Login with the user
            // $_SESSION['Login'] = $r['id'];
            setLogin($r['id'], true);
        }

        // User is not in DB
        else {
            $dati = array();
            $dati['name'] = $twName;
            $dati['regid'] = $twID;
            $dati['regtype'] = "twitter";
            $DB->queryinsert("users", $dati);

            // Login with the user
            // $_SESSION['Login'] = $DB->last_id;
            setLogin($DB->last_id, true);
        }
    }
    
    if (isset($_SESSION['last_page'])) {
        header("Location: ".$_SESSION['last_page']);
        exit();
    }
    header("Location: ".Link::Dashboard());
    exit();
    
    // $Tw_Signin = new TwitterOAuth($Tw_Signin_consumer_key, $Tw_Signin_consumer_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
    // print_r($Tw_Signin->get('account/verify_credentials'));
    // exit();
    
    break;
    
    
    
    case "fblogin":
    
    if (!$_SESSION['Login']) {
        // Get User ID
        $fbUser = $Fb_app->getUser();

        if ($fbUser) {
            try {
                $fbUSerProfile = $Fb_app->api('/me');

                // Search for user in our Database and login with this user
                $fbID = $fbUSerProfile['id'];
                $fbName = $fbUSerProfile['first_name'];
                $fbSurname = $fbUSerProfile['last_name'];
                $fbEmail = $fbUSerProfile['email'];

                $query = "SELECT * FROM users WHERE regtype = 'facebook' AND regid = '$fbID'";

                // User is in DB
                if ($DB->querynum($query)) {
                    $r = $DB->fetch();

                    $dati = array();
                    // $dati['name'] = utf8_decode($fbName);
                    // $dati['surname'] = utf8_decode($fbSurname);
                    $dati['name'] = $fbName;
                    $dati['surname'] = $fbSurname;
                    $dati['email'] = $fbEmail;
                    $DB->queryupdate("users", $dati, array("id" => $r['id']));

                    // Login with the user
                    // $_SESSION['Login'] = $r['id'];
                    setLogin($r['id'], true);
                }

                // User is not in DB
                else {
                    $dati = array();
                    // $dati['name'] = utf8_decode($fbName);
                    // $dati['surname'] = utf8_decode($fbSurname);
                    $dati['name'] = $fbName;
                    $dati['surname'] = $fbSurname;
                    $dati['email'] = $fbEmail;
                    $dati['regid'] = $fbID;
                    $dati['regtype'] = "facebook";
                    $DB->queryinsert("users", $dati);

                    // Login with the user
                    // $_SESSION['Login'] = $DB->last_id;
                    setLogin($DB->last_id, true);
                }
            } catch (FacebookApiException $e) {
                error_log($e);
                $fbUser = null;
            }
        }
    }
    
    if (isset($_SESSION['last_page'])) {
        header("Location: ".$_SESSION['last_page']);
        exit();
    }
    header("Location: ".Link::Dashboard());
    exit();
    break;
    
    
    
    case "resetpass":
    $ID = addslashes($_REQUEST['user']);
    $token = addslashes($_REQUEST['key']);
    
    $query = "SELECT * FROM lostpass WHERE user = '$ID' AND token = '$token'";
    if ($DB->querynum($query)) {
        
        $query = "SELECT * FROM users WHERE id = '$ID'";
        $r = $DB->queryfetch($query);
        
        $password = generatePassword(8);
        
        $mail = new PHPMailer();
        if ($noreplyEmail != "") {
        	$mail->SetFrom($noreplyEmail, $settings["title"]);
        }
        $mail->AddAddress($r['email'], $r['name']." ".$r['surname']);
        $mail->Subject = "Nuova password";

        $txt = sprintf(file_get_contents($Folder_web."/templates/mail-newpass.txt"), $password);
        $mail->AltBody = strip_tags($txt);
        $txt = nl2br($txt);
        $txt = sprintf(file_get_contents($Folder_web."/templates/mail.html"), $txt);

        $mail->IsHTML(true);
        //$mail->AddEmbeddedImage($Folder_web."/templates/pokedem-mail.png", "pokedem-image", "pokedem-mail.png");
        $mail->AddEmbeddedImage($settings["image"], "pokedem-image", $settings["image"]);
        
        $mail->MsgHTML($txt);

        if (!$mail->Send()) {
            $_SESSION['Message'] = getMessage("Errore nell'invio dell'e-mail");
        }
        
        $password = sha1($password);
        $query = "UPDATE users SET `password` = '$password' WHERE id = '$ID'";
        $DB->query($query);
        
        $query = "DELETE FROM lostpass WHERE user = '$ID'";
        $DB->query($query);
        
        $_SESSION['Message'] = getMessage("Nuova password inviata correttamente", "alert-success");
        l("new_password_sent", "", "", $r['email'], $ID);
    }
    else {
        e("reset_password", "", "id: $ID - token: $token");
        $_SESSION['Message'] = getMessage("Errore");
    }
    
    back(Link::Login());
    break;
    
    
    
    case "lostpass":
    if ($_POST['email']) {
        $_SESSION['Form']['email'] = $_POST['email'];
        $email = addslashes($_POST['email']);
        
        $query = "SELECT * FROM users WHERE email = '$email' AND regtype = 'web'";
        if ($DB->querynum($query)) {
            $r = $DB->fetch();
            $ID = $r['id'];
            $password = generatePassword(8);
            $password = md5($password);
            $link = "http://$Host/?action=resetpass&user=$ID&key=$password";
            $link = "<a href='$link'>$link</a>";
            
            $mail = new PHPMailer();
            if ($noreplyEmail != "") {
        		$mail->SetFrom($noreplyEmail, $settings["title"]);
        	}
        	$mail->AddAddress($r['email'], $r['name']." ".$r['surname']);
            $mail->Subject = "Richiesta nuova password";

            $txt = sprintf(file_get_contents($Folder_web."/templates/mail-lostpass.txt"), $link);
            $mail->AltBody = strip_tags($txt);
            $txt = nl2br($txt);
            $txt = sprintf(file_get_contents($Folder_web."/templates/mail.html"), $txt);

            $mail->IsHTML(true);
            //$mail->AddEmbeddedImage($Folder_web."/templates/pokedem-mail.png", "pokedem-image", "pokedem-mail.png");
            $mail->AddEmbeddedImage($settings["image"], "pokedem-image", $settings["image"]);
       
            $mail->MsgHTML($txt);

            if (!$mail->Send()) {
                $_SESSION['Message'] = getMessage("Errore nell'invio dell'e-mail");
            }
            else {
                $query = "DELETE FROM lostpass WHERE user = '$ID'";
                $DB->query($query);
                
                $dati = array();
                $dati['user'] = $ID;
                $dati['token'] = $password;
                $DB->queryinsert("lostpass", $dati);
                $_SESSION['Message'] = getMessage("Password inviata correttamente", "alert-success");
                unset($_SESSION['Form']);
                l("lost_password", "", "id: $ID - token: $token", $r['email']);
            }
        }
        else {
            e("lost_password", "", "id: $ID - token: $token");
            $_SESSION['Message'] = getMessage("Indirizzo e-mail non registrato oppure login effettuato tramite social network");
        }
    }
    $Include = $Folder_web."/tpl/lostpass.php";
    break;
    
    
    case "login":
    $email = addslashes($_POST['email']);
    $password = sha1($_POST['password']);
    $query = "SELECT * FROM users WHERE email = '$email' AND password = '$password'";
    if ($DB->querynum($query)) {
        $r = $DB->fetch();
        if ($r['active'] == 1) {
        	setLogin($r['id'], true);
            if (isset($_SESSION['last_page'])) {
                header("Location: ".$_SESSION['last_page']);
                exit();
            }
            header("Location: ".Link::Dashboard());
        	exit();
    	} else {
			e("incorrect_login", "", "$email");
	        $_SESSION['Message'] = getMessage("Account non attivato, verifica la tua email");
		}
    } else {
        e("incorrect_login", "", "$email");
        $_SESSION['Message'] = getMessage("Login non corretto, riprovare");
    }
    header("Location: ".Link::Login());
    exit();
    break;
    
    
    
    default:
    // if ($_SESSION['Login']) {
    //     header("Location: ?action=dashboard");
    //     exit();
    // }
    
    $Meta['title'] = $uimsg["site_title"];
    $Meta['description'] = $uimsg["site_description"];
    
    // --

    ob_start();

    echo "<div class='home-row'>";
    echo "<h3 class='title-class-1'>Speciale #Europee2014</h3>";
    echo "</div>";

    $ranks = array();

    // Piùcitati
    $thisrank = array();
    $thisrank['tw-label'] = "#piùcitati";
    $thisrank['link'] = "piucitatiEU";
    $thisrank['allow-click'] = true;
    $thisrank['detail-page'] = "news";
    $thisrank['img-field'] = "image";
    $thisrank['img-css'] = "img-politician-small";
    $thisrank['obj'] = new PiuCitati();
    $ranks[] = $thisrank;
    
    // Piùsocial
    $thisrank = array();
    $thisrank['tw-label'] = "#piùsocial";
    $thisrank['link'] = "piusocialEU";
    $thisrank['allow-click'] = true;
    $thisrank['detail-page'] = "social";
    $thisrank['img-field'] = "image";
    $thisrank['img-css'] = "img-politician-small";
    $thisrank['obj'] = new PiuSocial();
    $ranks[] = $thisrank;
    
    // Piùtwittati
    $thisrank = array();
    $thisrank['tw-label'] = "#piùtwittati";
    $thisrank['link'] = "piutwittatiEU";
    $thisrank['allow-click'] = true;
    $thisrank['detail-page'] = "tweets";
    $thisrank['img-field'] = "image";
    $thisrank['img-css'] = "img-politician-small";
    $thisrank['obj'] = new PiuTwittati();
    $ranks[] = $thisrank;

    // echo "<table class='table table-striped table-home-class'>";
    // echo "<tbody>";
    echo "<div id='table-home-class-1'>";
    
    foreach ($ranks as $rankinfo) {
        $link = Link::Chart($rankinfo['link'], 0);
        
        // echo "<tr class='home-row'><td>";
        echo "<div class='home-row'>";

        echo "<div class='titles-class'>";
        echo "<h3 class='title-class'><a href='$link'>{$rankinfo['tw-label']} #Europee2014</a></h3>";
        echo "<p class='home-time'>nelle ultime due ore</p>";
        echo "</div>";

        $rows = $rankinfo['obj']->setType($rankinfo['link']);
        $rows = $rankinfo['obj']->get();
        $pos = 0;
        foreach ($rows as $r) {
            $pos++;
            $newpos = $r['pos'];

            $showname = $r['name']." ".$r['surname'];
            if (!trim($showname)) {
                $showname = $r['username'];
            }
            $showname = htmlentities($showname, ENT_QUOTES, "UTF-8");
            $linkPol = Link::Politician($r['label'], $rankinfo['detail-page']);

            $img = "arrows/{$r['trend']}.png";
            if ($r['trend'] == "new") {
                $img = "arrows/up.png";
            }

            // if (isset($r['followers']) && $r['followers'] == 0) {
            //     $query = "INSERT IGNORE INTO twitter_blacklisted (id) VALUES ({$r['id']})";
            //     $DB->query($query, 123);
            //     tlog("blacklist", $r['username']);
            // }

            if (!isset($r['active']) || $r['active']) {
                ?>
                    <div class='home-class-position'>
                        <span class='pos-number'><?php echo $newpos; ?></span>
                        <?php if ($rankinfo['allow-click']) { ?>
                        <a title='<?php echo $showname; ?>' href='<?php echo $linkPol; ?>'>
                        <?php } ?>
                            <span title='<?php echo $showname; ?>' style='background-image: url(<?php echo $r[$rankinfo['img-field']]; ?>);' class='<?php echo $rankinfo['img-css']; ?>' />
                        <?php if ($rankinfo['allow-click']) { ?>
                        </a>
                        <?php } ?>
                        <img src='img/<?php echo $img; ?>' class='img-arrow position-tend' />
                    </div>
                <?php
            }
            else {
                ?>
                    <div class='home-class-position'>
                        <span class='pos-number'><?php echo $newpos; ?></span>
                        <span title='<?php echo $showname; ?>' style='background-image: url("<?= $settings["image_default"] ?>");' class='<?php echo $rankinfo['img-css']; ?>' />
                        <img src='img/<?php echo $img; ?>' class='img-arrow position-tend' />
                    </div>
                <?php
            }

            if ($pos >= 3) {
                break;
            }

        }

        // echo "</td></tr>";
        echo "</div>";
    }
    
    // echo "</tbody>";
    // echo "</table>";
    echo "</div>";

    $ClassificheEU = ob_get_clean();

    // --

    ob_start();

    echo "<div class='home-row'>";
    echo "<h3 class='title-class-1'>Le classifiche di ". $settings['title'] ."</h3>";
    //echo "</div>";
    
    $ranks = array();

    // Piùcitati
    $thisrank = array();
    $thisrank['tw-label'] = "#piùcitati";
    $thisrank['page'] = "statistics";
    $thisrank['link'] = "piucitati";
    $thisrank['allow-click'] = true;
    $thisrank['detail-page'] = "news";
    $thisrank['img-field'] = "image";
    $thisrank['img-css'] = "img-politician-small";
    $thisrank['obj'] = new PiuCitati();
    $ranks[] = $thisrank;
    
    // Piùsocial
    $thisrank = array();
    $thisrank['tw-label'] = "#piùsocial";
    $thisrank['page'] = "social_stats";
    $thisrank['link'] = "piusocial";
    $thisrank['allow-click'] = true;
    $thisrank['detail-page'] = "social";
    $thisrank['img-field'] = "image";
    $thisrank['img-css'] = "img-politician-small";
    $thisrank['obj'] = new PiuSocial();
    $ranks[] = $thisrank;
    
    // Piùtwittati
    $thisrank = array();
    $thisrank['tw-label'] = "#piùtwittati";
    $thisrank['page'] = "considerati_stats";
    $thisrank['link'] = "piutwittati";
    $thisrank['allow-click'] = true;
    $thisrank['detail-page'] = "tweets";
    $thisrank['img-field'] = "image";
    $thisrank['img-css'] = "img-politician-small";
    $thisrank['obj'] = new PiuTwittati();
    $ranks[] = $thisrank;
        
    // Piùcommentati
    // $thisrank = array();
    // $thisrank['tw-label'] = "#piùcommentati";
    // $thisrank['page'] = "";
    // $thisrank['link'] = "piucommentati";
    // $thisrank['allow-click'] = true;
    // $thisrank['detail-page'] = "social";
    // $thisrank['img-field'] = "image";
    // $thisrank['img-css'] = "img-politician-small";
    // $thisrank['obj'] = new PiuCommentati();
    // $ranks[] = $thisrank;
    
    // Piùattivi
    // $thisrank = array();
    // $thisrank['tw-label'] = "#piùattivi";
    // $thisrank['page'] = "attivisti_stats";
    // $thisrank['link'] = "piuattivi";
    // $thisrank['allow-click'] = false;
    // $thisrank['img-field'] = "picture";
    // $thisrank['img-css'] = "img-attivisti-small";
    // $thisrank['obj'] = new PiuAttivi();
    // $ranks[] = $thisrank;
    
    // echo "<table class='table table-striped table-home-class' id='table-home-class'>";
    // echo "<tbody>";
echo "<div id='table-home-class-1'>";
    
    foreach ($ranks as $rankinfo) {
        $link = Link::Chart($rankinfo['link'], 0);
        
        // echo "<tr class='home-row'><td>";
        //echo "<div class='home-row'>";
 	echo "<div style='padding: 10px; display: inline-block; border-left: 1px solid #444; float: left'>";

        echo "<div class='titles-class'>";
        echo "<h3 class='title-class'><a href='$link'>{$rankinfo['tw-label']}</a></h3>";
        echo "<p class='home-time'>nelle ultime due ore</p>";
        echo "</div>";

        $rows = $rankinfo['obj']->setType($rankinfo['link']);
        $rows = $rankinfo['obj']->get();
        $pos = 0;
        foreach ($rows as $r) {
            $pos++;
            $newpos = $r['pos'];

            $showname = $r['name']." ".$r['surname'];
            if (!trim($showname)) {
                $showname = $r['username'];
            }
            $showname = htmlentities($showname, ENT_QUOTES, "UTF-8");
            $linkPol = Link::Politician($r['label'], $rankinfo['detail-page']);

            $img = "arrows/{$r['trend']}.png";
            if ($r['trend'] == "new") {
                $img = "arrows/up.png";
            }

            if (isset($r['followers']) && $r['followers'] == 0) {
                $query = "INSERT IGNORE INTO twitter_blacklisted (id) VALUES ({$r['label']})";
                $DB->query($query, 123);
                tlog("blacklist", $r['username']);
            }

            if (!isset($r['active']) || $r['active']) {
                ?>
                    <div class='home-class-position'>
                    	<img src='img/<?php echo $img; ?>' class='img-arrow' />
                        <span class='pos-number'><?php echo $newpos; ?></span>
                        <?php 
                        	if ($rankinfo['allow-click']) { 
                        		print "<a href='$linkPol'>";
                        	}
                        	if ($r[$rankinfo['img-field']] != "") {
                            	print "<span title='$showname' style='background-image: url(".$r[$rankinfo['img-field']]. ");' class='". $rankinfo['img-css'] ."' />";
                        	} else {
                        		print "<span>$showname</span>";
                        	}
                        	if ($rankinfo['allow-click']) {
                       			print "</a>";
                        	}
                        	//CG: put the img arrow before the naming
                        	//print "<img src='img/$img' class='img-arrow position-tend' />";
                        ?>
                        
                    </div>
                <?php
            }
            else {
                ?>
                    <div class='home-class-position'>
                        <span class='pos-number'><?php echo $newpos; ?></span>
                        <span title='<?php echo $showname; ?>' style='background-image: url("<?= $settings["image_default"] ?>");' class='<?php echo $rankinfo['img-css']; ?>' />
                        <img src='img/<?php echo $img; ?>' class='img-arrow position-tend' />
                    </div>
                <?php
            }

            if ($pos >= 3) {
                break;
            }

        }

        // echo "</td></tr>";
        echo "</div>";
	
    }
    
    // echo "</tbody>";
    // echo "</table>";
    echo "</div>";
echo "</div>";

    $Classifiche = ob_get_clean();

    // --
    
    $Cloud = new HashCloud($QP);
    $Cloud->setHowmany(8);
    $Cloud->setNoTags("Nessun tag rilevante");
    $HomeCloud = $Cloud->trendinghashes(2);
    
    $Active = "home";
    $Include = $Folder_web."/tpl/home.php";
    break;
}

include($Folder_web.'tpl/index.php');
unset($_SESSION['Message']);
unset($_SESSION['Form']);

