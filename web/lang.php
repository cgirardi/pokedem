<?php
//============================================================+
// File name   : lang.php
// Date        : 2015-03-30 
// 
// Description : Language module for Pokedem
//               (contains translated texts)
// 
// Author: Christian Girardi
// 
//============================================================+

$settings["title"] = "AtokaNews";
$settings["image"] = "http://www.groupexport.ca/uploads/membres/logos/Canneberges%20Atoka%20c.jpeg";
$settings["description"] = "Le tue ..... di news, aggiornate tantissimo";
$settings["privacy_link"] = "http://nonciclopedia.wikia.com/wiki/Privacy";
 
$settings["copyright_label"] = "SpazioDati";
$settings["copyright_link"] = "http://spaziodati.eu";
$settings["copyright_year"] = "2015";

$settings["rate_default"] = 24;
$settings["image_default"] = "/img/company.png";

$settings["server"] = "pokedem.atoka.io";
$settings["email"] = ""; //registration sender email

$uimsg["site_title"] = "Segui la tua azienda preferita";
$uimsg["site_description"] = $settings["description"];

$uimsg["iamlucky"] = ""; //put a message like "Mi sento fortunato!" to enable it
$uimsg["search_name"] = "Cerca il nome di un&apos;azienda";

$uimsg["piucitati_title"] = "Quali sono le aziende più citate?";
$uimsg["piucitati_subtitle"] = "In questa classifica sono elencate le aziende che hanno ricevuto più citazioni sui media online.";

$uimsg["piusocial_title"] = "Quali sono le aziende più social?";
$uimsg["piusocial_subtitle"] = "In questa classifica sono elencate le aziende che hanno scritto più interventi social su Twitter e Facebook.";

$uimsg["piutwittati_title"] = "Quali sono le aziende più twittate?";
$uimsg["piutwittati_subtitle"] = "In questa classifica sono elencate le aziende che hanno ricevuto più cinguettii sul loro account Twitter.";

$uimsg["piucommentati_title"] = "Quali sono le aziende più commentate su Facebook?";
$uimsg["piucommentati_subtitle"] = "In questa classifica sono elencati le aziende che hanno ricevuto più commenti su Facebook.";

$uimsg["piuattivi_title"] =  "Chi sono gli utenti più attivi?";
$uimsg["piuattivi_subtitle"] = "In questa classifica sono elencati gli utenti Twitter che hanno scritto più tweet rivolti alle aziende.";


?>

