function updateWidget() {
    $('.btn-group .btn').each(function() {
        var txt = $(this).html();
        txt = txt.replace(" giorni", "g");
        txt = txt.replace(" ore", "h");
        $(this).html(txt);
        $(this).addClass("btn-mini");
    });
    
    $('#body-mini .item-tweet-dashboard .answer').each(function() {
        $(this).html('Risp');
    });
    
    $('#body-mini .item-tweet-dashboard .retweet').each(function() {
        $(this).html('Ritw');
    });
    
    $('#body-mini .item-tweet-dashboard .favorite').each(function() {
        $(this).html('Pref');
    });
}

jQuery(document).ready(function() {
    updateWidget();
});

_gaq.push(['_trackEvent', 'Widget', '_inside', document.location.href]);

