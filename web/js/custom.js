// Facebook
(function(d){
    var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1&appId=138958742943135";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));

// Twitter
function twitter() {
    $.getScript('http://platform.twitter.com/widgets.js');
}
twitter();

jQuery.ajaxSetup({
    beforeSend: function() {
        $('#loader').show();
    },
    complete: function(){
        $('#loader').hide();
    },
    success: function() {}
});


// Begin Table filter

$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

$(function() {
    $('#accedi-button').on('show-dropdown', function(e) {
        _gaq.push(['_trackEvent', 'Accedi', 'Accedi', '']);
    });
    $('#login-menu, #signin-menu').click(function(e) {
        e.stopPropagation();
    });
    $('.dropdown a.close').click(function () {
        $(this).parents('li.dropdown').removeClass('open');
    })

    $('.btn-selectall').click(function() {
        $(this).parents('div').find(':checkbox').prop('checked', 'checked');
    });

    $('.btn-selectnone').click(function() {
        $(this).parents('div').find(':checkbox').prop('checked', false);
    });

    $(".facebook-share").click(function() {
        var link = $(this).data("link");
        var name = $(this).data("name");
        var description = $(this).data("description");
        var picture = $(this).data("picture");

        FB.ui({
            method: 'feed',
            link: link,
            name: name,
            description: description,
            picture: picture,
            app_id: 138958742943135,
        }, function(response){});

        return false;
    });

    $('#type-twitter-user-filter button').click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
        else {
            $(this).parents('div').children('button').removeClass('active');
            $(this).addClass('active');
        }
        updateTweetTableFilters('table-twitter-user-filter', 'input-twitter-user-filter', 'type-twitter-user-filter');
    });

    $('#input-twitter-user-filter').keyup(function(event) {
        if (event.keyCode == 27) {
            $("#input-twitter-user-filter").val("");
        }

        updateTweetTableFilters('table-twitter-user-filter', 'input-twitter-user-filter', 'type-twitter-user-filter');
    });


    // Filter for sources
    $('#input-filter-list-sources').keyup(function(event) {
        if (event.keyCode == 27) {
            $("#input-filter-list-sources").val("");
        }

        $("#table-list-sources tbody tr").show();

        if ($('#input-filter-list-sources').val() != "") {
            $("#table-list-sources tbody tr").each(function(index, element) {
                var name = "";
                name += $(element).children(".col_name").html();
                name += " ";
                name += $(element).children(".col_type").html();

                var completename = name.toLowerCase();
                var filter = $("#input-filter-list-sources").val().toLowerCase();

                if (completename.indexOf(filter) === -1) {
                    $(element).hide();
                }
            });
        }

    });

});

function updateTweetTableFilters(tableID, inputID, btngroupID) {
    $("#" + tableID + " tbody tr").show();

    var alternativeClass = $("#" + btngroupID).find('.active').data('class');
    if (alternativeClass != null) {
        $("#" + tableID + " tbody tr").hide();
        $("#" + tableID + " tbody tr." + alternativeClass + "-row").show();
    }

    if ($('#' + inputID).val() != "") {
        var filter = $("#" + inputID).val().toLowerCase();

        $("#" + tableID + " tbody tr").each(function(index, element) {
            var name = $(element).find(".name a").html();

            var completename = name.toLowerCase();

            if (completename.indexOf(filter) === -1) {
                $(element).hide();
            }
        });
    }

    // $('.isotope-filters').hide();
    // setTimeout(function() {
    //     $('.isotope-filters').show();
    //     console.log("eccomi");
    // }, 1000);
}

// End Table filter

function isotopami() {
    if ($('.detail-left .item').length > 0) {
        $('.detail-left').isotope({
            itemSelector : '.item'
        });
    }

    $(".isotope-filters a.apply-filter, .isotope-filters a.reset-filter").click(function() {

        var parents = $(this).parents('.isotope-filters').find('td');
        parents.removeClass('selected');
        $(this).parents('tr').find('td').addClass('selected');

        var selector = $(this).data('filter');
        $('.detail-left').isotope({
            filter: selector
        });
        return false;
    });

    // $('.isotope-filters a.apply-filter').click(function() {
    //     $('#type-twitter-user-filter button').removeClass('active');
    //     updateTweetTableFilters('table-twitter-user-filter', 'input-twitter-user-filter', 'type-twitter-user-filter');
    // });

    $(document).tooltip();
}


function forgetNews() {
    $.ajax({
        url: '/',
        dataType: 'json',
        data: {
            action: 'ajax',
            sub: 'forgetNews'
        }
    });

    return false;
}

$(document).ready(function(){

    stuff();

    // Autocomplete
    if (jQuery("#politician-name-field").length > 0) {
        jQuery("#politician-name-field").autocomplete({
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    url: "/",
                    dataType: 'json',
                    data: {
                        action: 'ajax',
                        sub: 'getpoliticianbyname',
                        q: request.term.replace(" ", "_")
                    },
                    success: function (data) {
                        var users = new Array();
                        var i = 0;
                        jQuery.each(data.pol, function (index, value) {
                            users[i++] = {
                                label: value.name,
                                value: value.id,
                                image: value.image,
                                party: value.party
                            };
                        });
                        response(users);
                    }
                });
            },
            open: function (){
                $(this).autocomplete('widget').css('z-index', 1050);
                // After menu has been opened, set width to auto
                $('.ui-menu').width('auto');
                return false;
            },
            select: function (event, ui) {
                event.preventDefault();
                location.href = "/" + ui.item.value;
            },
            focus: function (event, ui) {
                event.preventDefault();
            }
        })
            .data("ui-autocomplete")._renderItem = renderAutompleteItem;
    }
    $("#politician-name-field").keyup(function(event){
        if (event.keyCode == 27) {
            $("#politician-name-field").val("");
            return false;
        }
    });

    // Politician autocomplete
    if (jQuery(".inputPolitician").length > 0) {
        jQuery(".inputPolitician").autocomplete({
        	minLength: 1,
            source: function (request, response) {
                $.ajax({
                    url: "/",
                    dataType: 'json',
                    data: {
                        action: 'ajax',
                        sub: 'getpoliticianbyname',
                        q: request.term.replace(" ", "_")
                    },
                    success: function (data) {
                        var users = new Array();
                        var i = 0;
                        jQuery.each(data.pol, function (index, value) {
                            users[i++] = {
                                label: value.name,
                                value: value.id,
                                image: value.image,
                                party: value.party
                            };
                        });
                        response(users);
                    }
                });
            },
            open: function (){
                $(this).autocomplete('widget').css('z-index', 1050);
                // After menu has been opened, set width to auto
                $('.ui-menu').width('auto');
            	return false;
            },
            select: function (event, ui) {
                event.preventDefault();
                setAutocompleteWidget(ui.item.value, ui.item.label, this);
            },
            focus: function (event, ui) {
                event.preventDefault();
            }
        }).data("ui-autocomplete")._renderItem = renderAutompleteItem;
        
        jQuery(".inputPolitician").each(function(index, el) {
            var hidden_val = $(el).parents('.politician-controls').find('.inputPolitician_hidden').val();
            if (hidden_val != undefined && hidden_val.length > 0) {
                loadPolitician(hidden_val, el);
            }
        });
    }
});

jQuery(document).ajaxComplete(function() {
    stuff();
});

function stuff() {
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy - hh:ii",
        autoclose: 1,
        todayHighlight: 1,
        todayBtn: 1,
        language: 'it'
    });

    $("a.a-confirm").click(function() {
        if (!confirm("Si desidera procedere con l'eliminazione? Operazione irreversibile!")) {
            return false;
        }
        return true;
    });

    $(".form_datetime").each(function(index, element) {
        var linkField = $(element).data('link-field');
        if (linkField == undefined) {
            return;
        }
        var inputField = $("#" + linkField);
        if (inputField.length == 0) {
            return;
        }

        var val = inputField.val();
        if (val.length == 0) {
            return;
        }

        var d = new Date(val);
        $(element).datetimepicker('update', d);
    });

    isotopami();

    $(".btn-submit-form").click(function() {
        var page = $(this).data("page");
        var type = $(this).data("type");

        if (page == undefined) {
            return false;
        }
        if (type == undefined) {
            return false;
        }

        if (type == "export") {
            return sendProData(page, this, true);
        }
        else {
            return sendProData(page, this, false);
        }
    });

    // $('.link-to-user').each(function() {
    //     var user = $(this).data('user');
    //     $(this).click(function() {
    //         sendProData("userInfo", '.btn-submit-form', false, { "sub": "userInfo", "id": user });
    //         return false;
    //     });
    // });

    jQuery(".imgToBeChecked").each(function() {
        $(this).error(function() {
            $(this).parents("a").remove();
            isotopami();
        });
    });

    isotopami();
    renewTagClouds();
    jQuery(".colorpickami").colorpicker();

    // Generic size
    // $(".update-size").each(function() {
    //     var width = $(this).width();
    //     console.log(width);
    // });

}

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

Highcharts.setOptions({
    lang: {
        loading: 'Caricamento...',
        months: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
        resetZoom: 'Annulla zoom',
        resetZoomTitle: 'Livello di zoom 1:1',
        weekdays: ['Domenica', 'Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato'],
        downloadPNG: 'Scarica immagine PNG',
        downloadJPEG: 'Scarica immagine JPEG',
        downloadPDF: 'Scarica immagine come PDF',
        downloadSVG: 'Scarica immagine SVG',
        exportButtonTitle: 'Esporta come raster o immagine',
        printButtonTitle: 'Stampa il grafico'
    },
    exporting: {
        url: '/includes/highcharts/export.php'
    }
});



// Functions part

function hideOnValue(sel, id) {
    var value = $(sel).val();
    var div = $("#" + id);
    if (value == undefined || value.length == 0) {
        div.show();
    }
    else {
        div.hide();
    }

    return false;
}

function sendProData(page, it, lochref, addenda) {
    $(it).attr("disabled", "disabled");

    var form = $(it).parents('form');
    if (form.length == 0) {
        console.log("No form available");
        return false;
    }

    _gaq.push(['_trackEvent', 'Pro', 'Refine_' + page, $(form).serialize()]);

    switch (page) {

        case "tweetUserInfo":
            var data = $(form).serializeObject();
            console.log(data);
            data['action'] = 'ajax';
            data['sub'] = page;

            var div = $("#user-tweets-content");

            div.block({message: "<img src='img/loadinfo.net.gif' />", css: {border: 0, background: "transparent"}});
            $.ajax({
                url: "/?",
                data: data,
                dataType: "json",
                success: function(data) {
                    if (data.ok) {
                        div.replaceWith(data.data);
                        stuff();
                    }
                    else {
                        alert(data.error);
                    }
                },
                complete: function() {
                    $(it).removeAttr("disabled");
                    div.unblock();
                }
            });
            break;

        case "userInfo":
        case "polProStats":
        case "polProStatsFB":
            var data = $(form).serializeObject();
            if (addenda) {
                $.extend(data, addenda);
            }
            data['action'] = 'ajax';
            data['sub'] = page;

            var div = $("#contentstats");

            div.block({message: "<img src='img/loadinfo.net.gif' />", css: {border: 0, background: "transparent"}});
            $.ajax({
                url: "/?",
                data: data,
                dataType: "json",
                success: function(data) {
                    if (data.ok) {
                        div.html(data.data);
                        // if (data.query) {
                        //     console.log(data.query);
                        // }
                        isotopami();
                    }
                    else {
                        alert(data.error);
                    }
                },
                complete: function() {
                    $(it).removeAttr("disabled");
                    div.unblock();
                }
            });
            break;

        default:
            if (lochref) {
                $(it).removeAttr("disabled");
                location.href = "/?action=ajax&page=" + page + "&sub=proQuery&export=1&" + $(form).serialize();
            }
            else {
                var data = $(form).serializeObject();
                data['action'] = 'ajax';
                data['sub'] = 'proQuery';
                data['page'] = page;

                var div = $(".detail-left");

                div.block({message: "<img src='img/loadinfo.net.gif' />", css: {border: 0, background: "transparent"}});
                $.ajax({
                    url: "/?",
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        if (data.ok) {
                            $(".detail-left").replaceWith(data.html);
                            if (data.query) {
                                console.log(data.query);
                            }
                            isotopami();
                        }
                        else {
                            alert(data.error);
                        }
                    },
                    complete: function() {
                        $(it).removeAttr("disabled");
                        div.unblock();
                    }
                });
            }
            break;
    }

    return false;
}

function triggerLogin(e) {
    // jQuery.proxy(jQuery('#accedi-button').click(), jQuery('#accedi-button'));
    // jQuery('#accedi-button').trigger('click');
    e.stopPropagation();
    jQuery('#accedi-button').dropdown('toggle');
    return false;
}

function setAutocompleteWidget(id, label, me) {
    $(me).parents('.politician-controls').find('.inputPolitician_hidden').val(id);
    $(me).parents('.politician-controls').find('.inputPolitician_cancel').show();
    $(me).val(label);
    $(me).prop('disabled', true);

    var callback = $(me).data("callback");
    if (callback != undefined) {
        window[callback](me);
    }
    return false;
}

function renderAutompleteItem(ul, item) {
    var img = item.image;
    if (img == "" || img == null)  {
        img ="/img/company.png";
    }

    var itm = ''
        + "<div class='typeahead_wrapper'>"
        //+ "<div class='typeahead_labels'>"
        + "<img class='typeahead_photo' style='background-image: url(" + img + ");' />"
        + "<div class='typeahead_primary'>" + item.label + "</div>"
        //+ "<div class='typeahead_secondary'>" + item.party + "</div>"
        //+ "</div>"
        + "</div>";
    return $( "<li>" )
        .append( "<a>" + itm + "</a>" )
        .appendTo(ul);
}

function loadPolitician(politician, input) {
    $.ajax({
        url: "/?",
        data: {
            action: "ajax",
            sub: "getPoliticianById",
            id: politician
        },
        dataType: "json",
        success: function(data) {
            if (data.ok) {
                setAutocompleteWidget(politician, data.name, input);
            }
            else {
                alert(data.error);
            }
        }
    });

}

function cancelPolitician(a) {
    $(a).parents('.politician-controls').find('.inputPolitician_hidden').val('');
    $(a).parents('.politician-controls').find('.inputPolitician_cancel').hide();
    $(a).parents('.politician-controls').find('.inputPolitician').val('');
    $(a).parents('.politician-controls').find('.inputPolitician').prop('disabled', false);

    $(a).parents('.politician-controls').find('.inputPolitician').focus();

    var callback = $(a).data("callback");
    window[callback](a);

    return false;
}

function loadWidget(el) {
    var type = $('#widgetType').val();

    var topDiv = $(el).parents('.widget-hidden');
    var width = $(topDiv).find('.inputWidth').val();
    var height = $(topDiv).find('.inputHeight').val();

    if (isNaN(height)) {
        height = 300;
    }
    if (isNaN(width)) {
        width = 300;
    }

    if (width < 200) {
        width = 200;
    }
    if (width > 1000) {
        width = 1000;
    }
    if (height < 300) {
        height = 300;
    }
    if (height > 1000) {
        height = 1000;
    }
    $(topDiv).find('.inputWidth').val(width);
    $(topDiv).find('.inputHeight').val(height);

    var idPol = null;
    if ($(topDiv).find('.inputPolitician').parents('.control-group').is(':visible')) {
        if ($(topDiv).find('.inputPolitician_hidden').length > 0) {
            var idPol = $(topDiv).find('.inputPolitician_hidden').val();
            if (idPol.length == 0 || isNaN(idPol)) {
                cancelWidget(el);
                return false;
            }
        }
    }

    $('.widget-no').hide();

    var widgetUrl = 'http://'+ window.server +'/widget?width=' + width + '&height=' + height;
    widgetUrl += '&type=' + encodeURIComponent(type);
    widgetUrl += '&bgColor=' + encodeURIComponent($(topDiv).find('.inputBgColor').val());
    widgetUrl += '&lnColor=' + encodeURIComponent($(topDiv).find('.inputLinkColor').val());
    widgetUrl += '&fgColor=' + encodeURIComponent($(topDiv).find('.inputTextColor').val());
    widgetUrl += '&hdColor=' + encodeURIComponent($(topDiv).find('.inputHeaderColor').val());

    if (idPol != null) {
        widgetUrl += '&id=' + idPol;
    }

    //onload="document.getElementById('loadImg').style.display='none';"
    $(topDiv).find('.widget-loading img').show();
    var iframeUrl = '<iframe width="' +
        width + '" height="' + height + '" src="' + widgetUrl + '" frameborder="0"></iframe>';
    var iframeLoad = '<iframe onload=\'$(this).parents(".widget-hidden").find(".widget-loading img").hide();\' width="' +
        width + '" height="' + height + '" src="' + widgetUrl + '" frameborder="0"></iframe>';

    $(topDiv).find('.widget-iframe-here').html(iframeLoad);
    $(topDiv).find('.widget-iframe-here').width(width);
    $(topDiv).find('.embed-code textarea').val(iframeUrl);
    return false;
}

function cancelWidget(el) {
    var topDiv = $(el).parents('.widget-hidden');

    $(topDiv).find('.widget-no').show();
    $(topDiv).find('.widget-iframe-here').html('');
    $(topDiv).find('.embed-code textarea').val('');
    return false;
}

function selectFirstStepWidget(select) {
    var action = $(select).val();
    var topDiv = $(".widget-hidden");
    $(topDiv).hide();

    if (action != undefined && action.length > 0) {
        $(".widget-hidden").show();
        switch (true) {
            case /^piu/.test(action):
            case (action == "tendenze"):
                $(topDiv).find('.inputPolitician').parents('.control-group').hide();
                break;

            case /Flow$/.test(action):
            case /Box$/.test(action):
            case /Pie$/.test(action):
                $(topDiv).find('.inputPolitician').parents('.control-group').show();
                break;

        }
    }

    loadWidget($(".widget-hidden").children()[0]);
    return false;
}

function submitPertinenceNotification(politician, link) {
    $.ajax({
        url: "/?",
        data: {
            action: "ajax",
            sub: "pertinence",
            politician: politician,
            link: link
        },
        dataType: "json",
        success: function(data) {
            var mess = "Grazie per la tua segnalazione, ci aiuterà a migliorare la qualità dei nostri servizi.";
            var messDone = "Questo articolo è già stato segnalato. Grazie comunque per la collaborazione.";

            if (data.ok) {
                bootbox.alert(mess);
            }
            else {
                if (data.errno == 3) {
                    bootbox.alert(messDone);
                }
                else {
                    bootbox.alert("Errore sconosciuto (" + data.errno + ")");
                }
            }
        }
    });
}

function submitSummaryRequest(a, hash) {

    _gaq.push(['_trackEvent', 'Summary', hash, document.location.href]);

    var oldHtml = $(a).html();
    $(a).html("Attendere...").css({
        cursor: "default",
        'pointer-events': "none",
        color: '#888'
    });

    $.ajax({
        url: "/?",
        data: {
            action: "ajax",
            sub: "summary",
            hash: hash
        },
        dataType: "json",
        success: function(data) {
            if (data.ok) {
                bootbox.alert(data.data);
            }
            else {
                alert(data.error);
            }
        },
        complete: function() {
            $(a).html(oldHtml).removeAttr('style');
        }
    });
}

function renewTagClouds() {
    $.fn.tagcloud.defaults = {
        size: {start: 12, end: 20, unit: 'pt'},
        color: {start: '#678', end: '#f89625'}
    };

    // Non widgets

    jQuery('.tag-cloud a').tagcloud();

    $('#sidebar .tag-cloud').each(function() {
        jQuery(this).children('a').tagcloud({
            size: {start: 9, end: 14, unit: 'pt'}
        });
    });

    $('.trending-topics-inside .tag-cloud').each(function() {
        jQuery(this).children('a').tagcloud({
            size: {start: 14, end: 24, unit: 'pt'}
        });
    });

    // Widgets

    $('#widget-div .tag-cloud').each(function() {
        jQuery(this).children('a').tagcloud({
            size: {start: 8, end: 14, unit: 'pt'}
        });
    });

    $('#body-mini .tag-cloud').each(function() {
        jQuery(this).children('a').tagcloud({
            size: {start: 8, end: 12, unit: 'pt'}
        });
    });

    $('#widget-div .trending-topics-inside .tag-cloud').each(function() {
        jQuery(this).addClass("vert-cloud");
    });

    // Size of tag cloud
    var len = 0;
    $('#hash-home .tag-cloud a').each(function() {
        len += $(this).width() + parseInt($(this).css('padding-left').replace("px", "")) + parseInt($(this).css('padding-right').replace("px", ""));
    });

    if (len > 0) {
        var divlen = $('#hash-home .tag-cloud').width();
        var ratio = len / divlen;
        if (ratio < .5) {
            ratio = .5;
        }

        $('#hash-home .tag-cloud a').each(function() {
            var size = parseInt($(this).css('font-size').replace("px", ""));
            var size = size / ratio;
            $(this).css('font-size', Math.floor(size) + "px");
        });
    }
}

function updateTrending(el, type) {
    if (jQuery(el).hasClass("active")) {
        return false;
    }

    _gaq.push(['_trackEvent', 'TrendingWidget', type.toString(), '']);

    jQuery(el).parent().children().removeClass("active");
    jQuery(el).addClass("active");

    var div = jQuery(el).parents("div.trending-topics");

    div.block({message: "<img src='img/loadinfo.net.gif' />", css: {border: 0, background: "transparent"}});

    jQuery.ajax({
        url: "/",
        dataType: "json",
        data: {
            action: "ajax",
            sub: "updatetrending",
            type: type
        },
        success: function(data) {
            if (data.ok) {
                jQuery(div).children(".trending-topics-inside").showHtml(data.data, 500);
                renewTagClouds();
                postponeLoading('postponed-trendingPie', {"type":type,"sub":"trendingPie"});
            }
            else {
                alert(data.error);
            }
        },
        complete: function() {
            div.unblock();
        }
    });

    return false;
}

function postponeLoading(div, pars) {
    pars.action = "ajax";
    pars.div = div;

    _gaq.push(['_trackEvent', 'WidgetLoaded', pars.sub, '']);

    div = jQuery("#" + div);

    if (div.length == 0) {
        return;
    }

    div.block({message: "<img src='img/loadinfo.net.gif' />", css: {border: 0, background: "transparent"}});

    jQuery.ajax({
        url: "/",
        dataType: "json",
        data: pars,
        success: function(data) {
            if (data.ok) {
                div.showHtml(data.data, 500);
                renewTagClouds();
                isotopami();
            }
            else {
                alert(data.error);
            }
        },
        complete: function() {
            div.unblock();
            updateWidgetExists = typeof updateWidget === 'function';
            if (updateWidgetExists) {
                updateWidget();
            }
        }
    });

    return false;
}

function changeState(el, id) {
    var group = $(el).parents("div.btn-group");
    group.find(".btn").addClass("disabled");

    $.ajax({
        url: '/?',
        dataType: 'json',
        data: {
            action: 'ajax',
            sub: 'changeState',
            id: id
        },
        success: function(data) {
            if (data.ok) {
                group.replaceWith(data.data);
            }
            else {
                alert(data.error);
            }
        },
        complete: function(data) {
        }
    });

    return false;
}

function changeFollowedState(el, status, id, myEvent) {
    $(el).find("span").attr("class", "pull-right");
    $(el).find("span").addClass("icon-refresh");

    var wasChecked = $(el).hasClass("checked");

    $.ajax({
        url: '/?',
        dataType: 'json',
        data: {
            action: 'ajax',
            sub: 'notification',
            id: id,
            remove: wasChecked,
            type: status
        },
        success: function(data) {
            if (data.ok) {
                $(el).find("span").attr("class", "pull-right");
                if (data.tick) {
                    $(el).find("span").addClass("icon-ok");
                    $(el).addClass("checked");
                }
                else {
                    $(el).removeClass("checked");
                }
            }
            else {
                alert(data.error);
            }
            // alert(data);
        },
        complete: function(data) {
            $(el).find("span").removeClass("icon-refresh");
        }
    });

    var e = myEvent || event || window.event;
    e.stopPropagation();
    return false;
}

function clicky(event, link) {
    event = event || window.event;
    if (event != null) {
        var target = event.target || event.srcElement;
        if (target.tagName.toLowerCase() === 'a')
            return true;
        window.open(link, '_blank');
    }
}
