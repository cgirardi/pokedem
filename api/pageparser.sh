#!/bin/sh
DIR=`dirname $0`

LIBDIR=$DIR/dist

/usr/bin/java -Dfile.encoding=UTF-8 -cp "$LIBDIR/raven-log4j-5.0.2.jar:$LIBDIR/guava-18.0.jar:$LIBDIR/log4j-1.2.17.jar:$LIBDIR/raven-5.0.2.jar:$LIBDIR/pokedem.jar" eu.fbk.fm.pokedem.download.PageParser $*

